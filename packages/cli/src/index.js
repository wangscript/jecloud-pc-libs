#!/usr/bin/env node
const program = require('commander');
const create = require('../commands/init');
const list = require('../commands/list');
const add = require('../commands/add');
const del = require('../commands/delete');
// 项目信息
program.name('jecli').usage('<command>').version(require('../package').version);

// 初始化模板
program.command('init').description('初始化项目模板').action(create);

// 查看模板
program.command('list').alias('ls').description('查看所有项目模板').action(list);

// 添加模板
program
  .command('add')
  .description('添加本地项目模板, 地址#分支 例如 https://github.com/xxx/xxx.git#master')
  .action(add);

// 移除模板
program.command('delete').alias('del').description('删除本地项目模板').action(del);

// 提取命令
//
const command = process.argv[2];
const commands = ['-V', '--version', '-h', '--help', 'add', 'init', 'list', 'ls', 'delete', 'del'];
if (commands.includes(command)) {
  // 解析命令行参数
  program.parse(process.argv);
} else {
  // 输出帮助信息
  program.outputHelp();
}
