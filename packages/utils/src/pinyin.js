import { pinyin as py } from 'pinyin-pro';
/**
 * 文字转拼音
 * @param {string} str 中文
 * @param {string} type 拼音类型'py' | 'PY' | 'pinyin' | 'PINYIN' | 'PinYin'
 * @returns {string}
 */
export function pinyin(str, type = 'pinyin') {
  if (!str) return str;
  const pyArray = py(str, {
    type: 'array',
    toneType: 'none',
  });
  const strs = [];
  let temp = '';
  for (let i = 0; i < str.length; i++) {
    const val = str.substring(i, i + 1);
    if (val.match(/^[\u4E00-\u9FFF]+$/)) {
      if (temp.length) {
        strs.push(temp);
      }
      strs.push(val);
      temp = '';
    } else {
      temp += val;
      if (i === str.length - 1) {
        strs.push(temp);
      }
    }
  }
  return pyArray
    .map((item, index) => {
      const oldStr = strs[index];
      if (oldStr === item) return item;
      switch (type) {
        case 'py':
          return item.substring(0, 1);
        case 'PY':
          return item.substring(0, 1).toLocaleUpperCase();
        case 'PINYIN':
          return item.toLocaleUpperCase();
        case 'PinYin':
          return item.substring(0, 1).toLocaleUpperCase() + item.substring(1);
        default:
          return item;
      }
    })
    .join('');
}
