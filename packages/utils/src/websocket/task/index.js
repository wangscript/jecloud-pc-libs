import { isApp } from '../../index';
import SocketTaskWeb from './socket-task-web';
import SocketTaskApp from './socket-task-app';

/**
 * Socket任务，兼容websocket写法
 */
export default {
  /**
   * 创建Socket任务
   * @param  {...any} args
   * @returns
   */
  createTask(...args) {
    return isApp ? new SocketTaskApp(...args) : new SocketTaskWeb(...args);
  },
  /**
   * 校验Socket
   * @returns
   */
  valid() {
    return isApp ? SocketTaskApp.valid() : SocketTaskWeb.valid();
  },
};
