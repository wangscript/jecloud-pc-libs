/**
 * web socket
 */
export default class SocketTaskWeb {
  socket = null;
  constructor(url) {
    this.socket = new window.WebSocket(url);
  }

  get readyState() {
    return this.socket.readyState;
  }
  set onopen(callback) {
    this.socket.onopen = callback;
  }
  set onclose(callback) {
    this.socket.onclose = callback;
  }
  set onmessage(callback) {
    this.socket.onmessage = callback;
  }
  set onerror(callback) {
    this.socket.onerror = callback;
  }

  send(...args) {
    this.socket.send(...args);
  }
  close(...args) {
    this.socket.close(...args);
  }

  static valid() {
    if (!window.navigator.onLine) {
      // 未联网
      return Promise.reject({ type: 'unnetwork' });
    } else {
      return Promise.resolve();
    }
  }
}
