/**
 * socket发送类型
 */
export const SendCommandEnum = Object.freeze({
  /**
   * 未知命令
   */
  UNKNOWN: 0,
  /**
   * 心跳
   * type:ping->pong
   */
  HEARTBEAT: 1,
  /**
   * 握手
   */
  HANDSHAKE: 2,
  /**
   * 踢出
   */
  KICK: 3,
  /**
   * 推送消息
   */
  PUSH: 4,
  /**
   * 成功消息
   */
  OK: 5,
  /**
   * 错误消息
   */
  ERROR: 6,
  /**
   * 握手检测---证书检查
   */
  HANDSHAKECHECK: 7,
});

/**
 * socket接收类型
 */
export const ReceiveMessageEnum = Object.freeze({
  /**
   * 握手消息类型
   */
  HANDSHAKE: 0,
  /**
   * 系统消息类型-只触发全局脚本库里的事件
   */
  SYS_MESSAGE: 1,
  /**
   * 脚本消息类型-只触发前台js注册的脚本事件
   */
  SCRIPT_MESSAGE: 2,
  /**
   * 握手未读消息推送消息类型
   */
  NOREAD_MESSAGE: 3,
  /**
   * 通知消息类型
   */
  NOTICE_MESSAGE: 4,
  /**
   * 心跳消息类型
   */
  HEARTBEAT: 5,
});
