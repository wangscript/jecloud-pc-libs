/**
 * 发送消息体
 */
export default class SendMessage {
  /**
   * 命令类型
   */
  cmd = 0;
  /**
   * 命令类型的类型
   * 请求:0
   * 响应:1
   * 集群转发请求：2
   * 集群转发响应：4
   * 其他类型
   * 如果是集群转发过来的请求和相应则不要再转发了
   */
  type = 0;
  /**
   * 会话ID
   */
  sessionId;

  /**
   * 标识当前启用的特性
   * 0 无任何特性
   * 1 压缩
   * 2 加密
   */
  flags = 0;
  /**
   * 消息体
   */
  body;

  constructor(options) {
    this.cmd = options.cmd || this.cmd;
    this.type = options.type || this.type;
    this.flags = options.flags || this.flags;
    this.sessionId = options.sessionId;
    this.body = options.body;
  }
}
