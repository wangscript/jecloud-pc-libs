import CryptoJS from 'crypto-js';
/**
 * DES加密
 * @param {string} message 加密信息
 * @param {string} key 秘钥
 * @returns String
 */
export function encryptByDES(message, key) {
  const keyHex = CryptoJS.enc.Utf8.parse(key); // 秘钥
  const encrypted = CryptoJS.DES.encrypt(message, keyHex, {
    mode: CryptoJS.mode.ECB, // 加密模式
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.ciphertext.toString(); //  加密出来为 hex格式密文
}
/**
 * DES解密
 * @param {string} ciphertext 密文
 * @param {string} key 秘钥
 * @returns String
 */
export function decryptByDES(ciphertext, key) {
  const keyHex = CryptoJS.enc.Utf8.parse(key);
  const decrypted = CryptoJS.DES.decrypt(
    {
      ciphertext: CryptoJS.enc.Hex.parse(ciphertext),
    },
    keyHex,
    {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    },
  );
  return decrypted.toString(CryptoJS.enc.Utf8);
}
