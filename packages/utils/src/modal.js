import { useJE } from '.';
/**
 * 全局Modal函数
 */
export const Modal = {
  alert: (...args) => uiModalAction('alert', ...args),
  confirm: (...args) => uiModalAction('confirm', ...args),
  dialog: (...args) => uiModalAction('dialog', ...args),
  message: (...args) => uiModalAction('message', ...args),
  notice: (...args) => uiModalAction('notice', ...args),
  window: (...args) => uiModalAction('window', ...args),
  status: {
    /**
     * 消息
     */
    info: 'info',
    /**
     * 成功
     */
    success: 'success',
    /**
     * 警告
     */
    warning: 'warning',
    /**
     * 错误
     */
    error: 'error',
    /**
     * 疑问
     */
    question: 'question',
  },
};

/**
 * 调用UI的Modal函数
 * @param {*} action
 * @param  {...any} args
 * @returns
 */
const uiModalAction = (action, ...args) => {
  return useJE()
    .useUi()
    ?.Modal?.[action](...args);
};
