export * from './system';
export * from './websocket';
export * from './data';
export * from './hooks';
export * from './web';
export * from './custom';
export * from './date';
export * from './pinyin';
export * from './mitt';
export * from './color';
export * from './deferred';
export * from './des';
export * from './http';
export * from './qs';
export * from './loadjs';
export * from './je';
export * from './modal';

/**
 * APP环境
 */
export let isApp = false;
/**
 * 使用App环境
 */
export function useApp() {
  isApp = true;
}
