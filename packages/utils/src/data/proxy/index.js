import { useAjaxPrexy } from './ajax-proxy';
import { isFunction } from '../../index';
export function useProxy(proxy) {
  if (isFunction(proxy)) {
    return { read: proxy };
  } else {
    return useAjaxPrexy(proxy);
  }
}

export default { useProxy };
