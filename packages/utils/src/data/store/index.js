import { reactive } from 'vue';
import GridStore from './grid-store';
import TreeStore from './tree-store';
/**
 * 响应式列表store
 * @param {*} options
 * @returns
 */
export function useGridStore(options) {
  return reactive(new GridStore(options));
}
/**
 * 响应式树形store
 * @param {*} options
 * @returns
 */
export function useTreeStore(options) {
  return reactive(new TreeStore(options));
}
