import { isBoolean, isFunction } from 'lodash-es';

const rawDefineProperty = Object.defineProperty;

// is bind function
function isBoundFunction(target) {
  return (
    // eslint-disable-next-line no-prototype-builtins
    isFunction(target) && target.name.indexOf('bound ') === 0 && !target.hasOwnProperty('prototype')
  );
}

function isBoundedFunction(value = { __IS_BOUND_FUNCTION__: false }) {
  if (isBoolean(value.__IS_BOUND_FUNCTION__)) return value.__IS_BOUND_FUNCTION__;
  return (value.__IS_BOUND_FUNCTION__ = isBoundFunction(value));
}

function isConstructor(value = { __IS_CONSTRUCTOR__: false }) {
  if (isBoolean(value.__IS_CONSTRUCTOR__)) return value.__IS_CONSTRUCTOR__;

  const valueStr = value.toString();

  const result =
    (value.prototype?.constructor === value &&
      Object.getOwnPropertyNames(value.prototype).length > 1) ||
    /^function\s+[A-Z]/.test(valueStr) ||
    /^class\s+/.test(valueStr);

  return (value.__IS_CONSTRUCTOR__ = result);
}

export default function bindFunctionToRawWindow(rawWindow, value) {
  if (value.__BOUND_WINDOW_FUNCTION__) return value.__BOUND_WINDOW_FUNCTION__;

  if (!isConstructor(value) && !isBoundedFunction(value)) {
    const bindRawWindowValue = value.bind(rawWindow);

    for (const key in value) {
      bindRawWindowValue[key] = value[key];
    }

    // eslint-disable-next-line no-prototype-builtins
    if (value.hasOwnProperty('prototype')) {
      rawDefineProperty(bindRawWindowValue, 'prototype', {
        value: value.prototype,
        configurable: true,
        enumerable: false,
        writable: true,
      });
    }

    return (value.__BOUND_WINDOW_FUNCTION__ = bindRawWindowValue);
  }

  return value;
}
