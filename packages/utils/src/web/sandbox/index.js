import { isEmpty, isFunction } from '../../index';
import bindFunctionToRawWindow from './bind_function';
/**
 * 执行JS代码
 * @param {string} code js代码
 * @param {Object} sandbox 沙箱代理对象，可在函数内直接使用对象属性
 * @param {Object} async 异步函数
 * @returns {Object}
 */
export function execScript(code, sandbox, async) {
  if (isEmpty(code)) return code;
  const sourceCode = code;
  try {
    const rawWindow = window;
    code = `with (sandbox) { 
      ${code}
     }`;
    // 通过新的方法创建异步函数
    const AsyncFunction = Object.getPrototypeOf(async function () {}).constructor;
    const fn = async ? new AsyncFunction('sandbox', code) : new Function('sandbox', code);
    // 沙箱代理
    const proxy = new Proxy(sandbox || rawWindow, {
      // 拦截所有属性，防止到 Proxy 对象以外的作用域链查找。
      has(target, key) {
        return true;
      },
      get(target, key) {
        // 加固，防止逃逸
        if (key === Symbol.unscopables) {
          return undefined;
        }
        // 当前对象获取
        if (Reflect.has(target, key)) {
          return Reflect.get(target, key);
        } else {
          // 当前窗口获取
          const rawValue = Reflect.get(rawWindow, key);
          return isFunction(rawValue) ? bindFunctionToRawWindow(rawWindow, rawValue) : rawValue;
        }
      },
    });
    return fn(proxy);
  } catch (ex) {
    console.trace('execScript error!', sourceCode);
    console.error(ex);
  }
  return null;
}

/**
 * 执行JS代码，异步函数，支持await语法
 * @param {string} code js代码
 * @param {Object} sandbox 沙箱代理对象，可在函数内直接使用对象属性
 * @returns {Object}
 */
export function execScript4Async(code, sandbox) {
  return execScript(code, sandbox, true);
}
/**
 * 执行JS代码，返回结果，不支持传参
 * @param {string} code js代码
 * @returns {Object}
 */
export function execScript4Return(code) {
  let flag;
  try {
    code = `return ( 
      ${code} 
    )`;
    flag = new Function(code)();
  } catch (ex) {
    console.trace('execScript4Return error!', code);
    console.error(ex);
  }
  return flag;
}
