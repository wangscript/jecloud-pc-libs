import { uniqueId, isObject, isEmpty, kebabCase } from '../../index';
import { addResizeListener, removeResizeListener } from './dom-event';
import { getOffsetBox, highlightElement } from './dom-utils';

/**
 * 创建HtmlElement工具类
 * @param {*} dom HtmlElement
 * @returns Element
 */
export function createElement(dom) {
  return new Element(dom);
}

/**
 * HtmlElement工具类
 * @module Element
 */
export class Element {
  /**
   * 构造函数
   * @param {String|HTMLElement|Element} element id|dom|element
   */
  constructor(element) {
    if (!element) return null;
    if (element.isElement) return element;

    /**
     * dom对象
     * @public
     */
    if (typeof element == 'string') {
      this.dom = document.getElementById(element) || document.querySelector(element);
    } else {
      this.dom = element;
    }
    if (!this.dom) return null;
    /**
     * dom id
     * @public
     */
    this.id = this.dom.id = this.dom.id || uniqueId('je-el-');
    /**
     * Element表示
     */
    this.isElement = true;
  }

  /**
   * 注册的事件
   */
  _listeners = {};

  /**
   * 添加样式
   * @param  {...any} cls 样式名，可以添加多个
   */
  addClass(...cls) {
    this.dom.classList.add(...cls);
  }

  /**
   * 删除样式
   * @param  {...any} cls 样式名，可以删除多个
   */
  removeClass(...cls) {
    this.dom.classList.remove(...cls);
  }

  /**
   * 包含样式
   * @param {string} cls 样式名
   * @returns {boolean} 包含结果
   */
  hasClass(cls) {
    return this.dom.classList.contains(cls);
  }

  /**
   * 切换类名
   * @param {string} cls 样式名
   * @param {boolean} toggle true添加样式，false删除样式
   * @returns {boolean} 操作结果
   */
  toggleClass(cls, toggle) {
    return this.dom.classList.toggle(cls, toggle);
  }

  /**
   * 获取属性
   * @param {string} key 属性
   * @returns {string}
   */
  getAttribute(key) {
    return this.dom.getAttribute(key);
  }

  /**
   * 设置属性
   * @param {string} key 属性
   * @param {string} val 值
   * @returns {Element}
   */
  setAttribute(key, val) {
    this.dom.setAttribute(key, val);
    return this;
  }

  /**
   * 设置样式
   * @param {String|Object} key 样式属性或者对象
   * @param {string} val 样式值
   * @returns {Element}
   */
  setStyle(key, val) {
    let styles = { [key]: val };
    if (isObject(key)) {
      styles = key;
    }
    for (let p in styles) {
      this.dom.style.setProperty(kebabCase(p), styles[p]);
    }
    return this;
  }
  /**
   * 获取样式
   * @param {*} key
   * @returns
   */
  getStyle(key) {
    return this.dom.style.getPropertyValue(kebabCase(key));
  }

  /**
   * 获得高
   * @returns {number}
   */
  getHeight() {
    return this.dom.offsetHeight;
  }

  /**
   * 获得宽
   * @returns {number}
   */
  getWidth() {
    return this.dom.offsetWidth;
  }

  /**
   * 添加dom事件
   * @param {*} eventName
   * @param {*} fn
   */
  on(eventName, fn) {
    this.dom.addEventListener(eventName, fn);
    const ls = this._listeners[eventName] || [];
    ls.push(fn);
    this._listeners[eventName] = ls;
  }

  /**
   * 移除dom事件
   * @param {*} eventName
   * @param {*} fn
   */
  un(eventName, fn) {
    const ls = this._listeners[eventName] || [];
    let unIndex = -1; // 删除单个事件的索引
    ls.forEach((_fn, index) => {
      if (!fn || fn == _fn) {
        this.dom.removeEventListener(eventName, _fn);
        if (fn) {
          unIndex = index;
          return false;
        }
      }
    });
    if (fn) {
      if (unIndex > 0) {
        ls.splice(unIndex, 1);
        this._listeners[eventName] = ls;
      }
    } else {
      delete this._listeners[eventName];
    }
  }

  /**
   * 添加resize事件
   * @param {*} fn
   */
  addResizeListener(fn) {
    addResizeListener(this.dom, fn);
  }
  /**
   * 移除resize事件
   * @param {*} fn
   */
  removeResizeListener(fn) {
    removeResizeListener(this.dom, fn);
  }
  /**
   * 向上查找dom元素
   * @param {*} simpleSelector css选择器
   * @param {*} limit 查找层级，默认50，超过层级数，停止查找
   * @param {*} returnDom 是否返回HTMLElement，默认返回Element
   * @returns Element | HTMLElement
   */
  up(simpleSelector, limit, returnDom) {
    let { parentNode } = this.dom;
    // 如果没有选择器，默认父节点
    if (isEmpty(simpleSelector)) return returnDom ? parentNode : new Element(parentNode);

    // 实现思路
    // 1.通过父节点的父节点，查找父父节点 > selector 符合的子节点
    // 2.将查找到的子节点跟当前父节点对比，如果符合，返回父节点

    let ppNode = parentNode.parentNode;
    let count = 0;
    let dom;
    limit = limit || 50;
    while (ppNode) {
      count++;
      const nodes = ppNode.querySelectorAll(`:scope > ${simpleSelector}`);
      nodes?.forEach((node) => {
        if (parentNode == node) {
          dom = node;
          return false;
        }
      });
      if (dom || (limit > 0 && limit == count)) {
        break;
      } else {
        parentNode = ppNode;
        ppNode = parentNode.parentNode;
      }
    }
    return dom && (returnDom ? dom : new Element(dom));
  }

  /**
   * 向下查找dom元素
   * @param {*} simpleSelector css选择器
   * @param {*} returnDom 是否返回HTMLElement，默认返回Element
   * @returns Element | HTMLElement
   */
  down(simpleSelector, returnDom) {
    return this.select(simpleSelector, returnDom)[0];
  }

  /**
   * 父节点
   * @param {*} returnDom 是否返回HTMLElement，默认返回Element
   * @returns Element | HTMLElement
   */
  parent(returnDom) {
    return this.up(null, 0, returnDom);
  }

  /**
   * 子节点
   * @param {*} simpleSelector css选择器
   * @param {*} returnDom 是否返回HTMLElement，默认返回Element
   * @returns Element | HTMLElement
   */
  child(simpleSelector, returnDom) {
    return this.down(`:scope > ${simpleSelector}`, returnDom);
  }

  /**
   * 查找子元素，多选
   * @param {*} simpleSelector css选择器
   * @param {*} returnDom 是否返回HTMLElement，默认返回Element
   * @returns Element | HTMLElement
   */
  select(simpleSelector, returnDom) {
    const doms = this.dom.querySelectorAll(simpleSelector);
    if (returnDom) return doms;
    const els = [];
    doms.forEach((dom) => {
      els.push(new Element(dom));
    });
    return els;
  }

  /**
   * 获得相对目标dom的位置信息
   * @param {*} target 目标dom
   * @returns
   */
  getOffsetBox(target) {
    const targetDom = target.isElement ? target.dom : target;
    return getOffsetBox(this.dom, targetDom);
  }

  /**
   * 高亮
   */
  highlight() {
    highlightElement(this.dom);
  }

  /**
   * 滚动
   * @param {*} target
   */
  scrollBy(target, pos = 'top') {
    const targetDom = target.isElement ? target.dom : target;
    const offsetBox = getOffsetBox(targetDom, this.dom);
    this.setStyle('scroll-behavior', 'smooth'); // 锚点动画
    if (pos === 'top') {
      this.dom.scrollTop = offsetBox.y;
    } else {
      this.dom.scrollLeft = offsetBox.x;
    }
  }

  /**
   * 销毁元素
   */
  destroy() {
    this.dom.parentNode?.removeChild(this.dom);
  }
}
