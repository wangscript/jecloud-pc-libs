import { ajax, encode, isArray, isString, transformAjaxData } from '../../index';
import {
  API_DICTIONARY_FIND_ASYNC_NODES,
  API_DICTIONARY_LOAD_ITEM_BY_CODE,
  API_DICTIONARY_LOAD_ITEM_BY_CODES,
  API_DICTIONARY_INIT_LOAD,
  API_DICTIONARY_LOAD_TREE,
} from './urls';

/**
 * 功能信息查询信息
 * @returns
 */
export function findDDAsyncNodesApi({ type = 'liketext', value, strData }) {
  return ajax({
    url: API_DICTIONARY_FIND_ASYNC_NODES,
    params: {
      type,
      value,
      strData,
    },
  });
}
/**
 * 加载字典数据项
 * @param {*} param0
 * @returns
 */
export function loadDDItemByCodeApi({ code, querys = {} }) {
  return ajax({
    url: API_DICTIONARY_LOAD_ITEM_BY_CODE,
    params: {
      DICTIONARY_DDCODE: code,
      j_query: encode(isArray(querys) ? { custom: querys } : querys),
    },
  });
}
/**
 * 加载字典树-单树
 * @param {Object} config
 * @param {string} config.code 字典编码
 * @param {string} [config.text] 字典名称
 * @param {string} [config.configInfo] 字典信息
 * @param {boolean} [config.async] 异步字典
 * @param {Object} [config.querys] 查询条件
 * @param {Object} [config.customVariables] 查询参数
 * @param {string} [config.rootId] 根节点ID，默认ROOT
 * @param {boolean} [config.onlyItem] 返回子节点数组，默认true
 * @param {boolean} [config.multiple] 多选，默认false
 * @param {Object} [config.excludes] 排除的属性
 * @returns
 */
export function loadDDItemByCode4TreeApi({
  code,
  text,
  configInfo,
  async,
  querys = {},
  customVariables = {},
  excludes = [],
  rootId = 'ROOT',
  onlyItem = true,
  multiple = false,
}) {
  return loadDDItemByCodes4TreeApi({
    codes: [{ code, text, configInfo, async, querys, rootId, customVariables }],
    rootId,
    onlyItem,
    multiple,
    excludes,
  });
}
/**
 * 加载字典数据项
 * @param {*} param0
 * @returns
 */
export function loadDDItemByCodesApi({ codes = [] }) {
  return ajax({
    url: API_DICTIONARY_LOAD_ITEM_BY_CODES,
    params: {
      ddListCodes: codes.join(','),
    },
  });
}
/**
 * 加载字典树
 * @param {Object} config
 * @param {Array} config.codes 字典数组
 * @param {string} config.codes.code 字典编码
 * @param {string} [config.codes.text] 字典名称
 * @param {string} [config.codes.configInfo] 字典信息
 * @param {boolean} [config.codes.async] 异步字典
 * @param {Object} [config.codes.querys] 查询条件
 * @param {string} [config.codes.rootId] 根节点ID，默认ROOT
 * @param {boolean} [config.onlyItem] 返回子节点数组，默认true
 * @param {boolean} [config.multiple] 多选，默认false
 * @param {Object} [config.excludes] 排除的属性
 * @returns
 */
export function loadDDItemByCodes4TreeApi({
  codes = [],
  rootId = 'ROOT',
  onlyItem = true,
  multiple = false,
  excludes = [],
}) {
  // 单选，排除checked属性
  if (!multiple) excludes.push('checked');
  const params = {
    node: rootId,
    onlyItem,
    excludes: excludes.join(','),
    strData: encode(
      codes.map((code) => {
        const item = isString(code) ? { code } : code;
        return {
          ddCode: item.code,
          ddName: item.text || item.code,
          configInfo: item.configInfo,
          rootId: item.rootId,
          j_query: isArray(item.querys) ? { custom: item.querys } : item.querys,
          customVariables: item.customVariables,
          async: item.async || false,
        };
      }),
    ),
  };
  return ajax({
    url: API_DICTIONARY_LOAD_TREE,
    params,
  });
}
/**
 * 获取字典信息数据
 * @param {*} ddCode
 * @returns
 */
export function initDDDataApi() {
  return ajax({
    url: API_DICTIONARY_INIT_LOAD,
    params: {
      queryColumns: [
        'JE_CORE_DICTIONARY_ID',
        'DICTIONARY_DDCODE',
        'DICTIONARY_DDNAME',
        'DICTIONARY_DDTYPE',
      ].join(','),
    },
  }).then(transformAjaxData);
}
