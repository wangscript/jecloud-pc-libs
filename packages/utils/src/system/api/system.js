import { ajax, isEmpty, transformAjaxData } from '../../index';
import {
  API_RBAC_ACCOUNT,
  API_RBAC_LOGOUT,
  API_RBAC_LOGIN,
  API_RBAC_VALIDATE_TOKEN,
  API_RBAC_LOAD_PLUGIN_PERMS,
  API_SYSTEM_FRONTEND_VARIABLES,
} from './urls';
/**
 * 当前用户
 *
 * @export
 * @return {Object}
 */
export function initCurrentAccountApi() {
  return ajax({ url: API_RBAC_ACCOUNT, method: 'GET' }).then(transformAjaxData);
}

/**
 * 系统变量
 *
 * @export
 * @return {Object}
 */
export function initSystemConfigApi() {
  return ajax(
    { url: API_SYSTEM_FRONTEND_VARIABLES, token: false },
    { headers: { pd: 'meta' } },
  ).then(transformAjaxData);
}
/**
 * 登录
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loginApi(params) {
  return ajax({ url: API_RBAC_LOGIN, params, token: false }).then(transformAjaxData);
}

/**
 * 退出登录
 * @returns
 */
export function logoutApi() {
  return ajax({ url: API_RBAC_LOGOUT, method: 'GET', responseInterceptorsCatch: false }).catch(
    (error) => {
      console.log(error);
    },
  );
}
/**
 * 验证token是否有效
 * @returns
 */
export function validateTokenApi() {
  // 禁用系统错误拦截器，自行处理
  return ajax({ url: API_RBAC_VALIDATE_TOKEN, responseInterceptorsCatch: false });
}

/**
 * 加载插件权限
 * @param {*} pluginCode
 * @returns
 */
export function loadPluginPermsApi(pluginCode) {
  return ajax({
    url: API_RBAC_LOAD_PLUGIN_PERMS,
    params: { pluginCode },
    method: 'GET',
  }).then(transformAjaxData);
}
