import { ajax, transformAjaxData, isApp } from '../../index';
import { API_META_GLOBAL_SCRIPT, API_META_GLOBAL_CSS, API_META_GLOBAL_STL } from './urls';
/**
 * 加载全局样式库
 * @returns
 */
export function initCssCodeApi() {
  return ajax({
    url: API_META_GLOBAL_SCRIPT,
    params: {
      media: isApp ? 'app' : 'pc',
    },
  }).then(transformAjaxData);
}
/**
 * 加载全局脚本库
 * @returns
 */
export function initJsCodeApi() {
  return ajax({
    url: API_META_GLOBAL_CSS,
    params: {
      media: isApp ? 'app' : 'pc',
    },
  }).then(transformAjaxData);
}
/**
 * 加载Sql模板数据
 * @returns
 */
export function initSqlTplApi() {
  return ajax({
    url: API_META_GLOBAL_STL,
  }).then(transformAjaxData);
}
/**
 * 初始化Api数据
 * @returns
 */
export function loadApiDataApi() {
  return ajax({
    url: '/je/common/tree/getTree',
    params: {
      node: 'ROOT',
      onlyItem: true,
      excludes: 'checked',
      funcCode: 'JE_CORE_JEAPI',
      tableCode: 'JE_CORE_JEAPI',
      j_query: '[]',
    },
    headers: {
      pd: 'meta',
    },
  }).then(transformAjaxData);
}
