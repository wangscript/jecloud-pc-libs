import { ajax, transformAjaxData } from '../../index';
/**
 * 删除文件
 * @param {Array} fileKeys
 * @returns
 */
export function removeFileApi(fileKeys) {
  return ajax({
    url: '/je/document/deleteByFileKeys',
    params: {
      fileKeys: fileKeys.join(','), //删除的文件id
    },
  }).then(transformAjaxData);
}
/**
 * 获取文件属性
 * @param {string} fileKey
 * @returns
 */
export function getFilePropertyApi(fileKey) {
  return ajax({ url: '/je/document/selectMetaDataByFileKey', params: { fileKey } }).then(
    transformAjaxData,
  );
}
