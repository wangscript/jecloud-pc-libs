import { useJE, execScript, createStyleSheet } from '../index';
import { initJsCodeApi, initCssCodeApi, loadApiDataApi } from './api/code';
import { jsCodeMap } from './system';
let apiData = [];
/**
 * 初始化全局脚本
 * @returns
 */
export function initCodes() {
  jsCodeMap.clear();
  // 加载脚本库
  return Promise.all([initJsCodeApi(), initCssCodeApi()])
    .then(([jsCodes, cssCodes]) => {
      // 缓存js脚本库
      const initJs = [];
      jsCodes.forEach((item) => {
        if (item.QJJBK_SPJZ == '1') {
          initJs.push(item.QJJBK_JBDM);
        } else {
          jsCodeMap.set(item.QJJBK_FFM, item.QJJBK_JBDM);
        }
      });
      // 首屏加载js脚本库
      const initJsCode = '__init__';
      initJs.forEach((code) => {
        jsCodeMap.set(initJsCode, code);
        callCustomFn(initJsCode);
      });
      jsCodeMap.delete(initJsCode);

      // 解析css样式库
      const initCss = [];
      cssCodes.forEach((item) => {
        initCss.push(item.QJCSS_YSDM);
      });
      // 写入样式
      if (initCss.length > 0) {
        createStyleSheet(initCss.join('\n'));
      }
      return jsCodeMap;
    })
    .catch(() => {
      console.log('js,css template error');
    });
}

/**
 * 执行全局脚本库
 * @param {string} methodName 方法名
 * @param {Object} params 参数
 * @returns {Object}
 */
export function callCustomFn(methodName, params) {
  let fnCode = jsCodeMap.get(methodName);
  if (fnCode) {
    try {
      return execScript(fnCode, { JE: useJE(), EventOptions: params || {} });
    } catch (error) {
      console.error(`js全局脚本库：【${methodName}】出错了！`);
      console.error(error);
    }
  }
}
/**
 * 更新全局脚本
 * @private
 * @param {*} code 方法名
 * @param {*} codes 代码
 */
export function updateCustomFn(code, codes) {
  if (code && codes) {
    jsCodeMap.set(code, codes);
  }
}
/**
 * 注册样式
 * @param {*} code 样式名
 * @param {*} codes 代码
 * @returns
 */
export function registCustomStyle(code, codes) {
  createStyleSheet(codes, 'je_coustom_' + code);
}

/**
 * 加载Api数据
 * @returns
 */
export function loadApiData() {
  if (apiData.length) {
    return Promise.resolve(apiData);
  } else {
    return loadApiDataApi().then((data) => {
      apiData = data.children || [];
      return apiData;
    });
  }
}
