/**
 * 账号
 */
export default class Account {
  /**
   * 主键ID
   */
  id;
  /**
   * 机构用户名称
   */
  name;
  /**
   * 机构用户编码
   */
  code;
  /**
   * 头像
   */
  avatar;
  /**
   * 手机号
   */
  phone;
  /**
   * 邮件
   */
  email;
  /**
   * 开放ID
   */
  openId;
  /**
   * 过期时间
   */
  expireTime;
  /**
   * 是否锁定
   */
  locked;
  /**
   * 账户卡号
   */
  cardNum;
  /**
   * 所属机构
   */
  platformOrganization;
  /**
   * 真实用户
   */
  realUser;
  /**
   * 角色集合
   */
  roles = [];
  /**
   * 角色ID集合
   */
  roleIds = [];
  /**
   * 角色编码集合
   */
  roleCodes = [];
  /**
   * 角色名称集合
   */
  roleNames = [];
  /**
   * 权限集合
   */
  permissions = [];
  /**
   * 账号部门视图id
   */
  deptId;
  /**
   * 访问状态
   */
  status;
  /**
   * 租户Id
   */
  tenantId;
  /**
   * 租户名称
   */
  tenantName;
}
