import { ajax, transformAjaxData } from '../index';
import { initSqlTplApi } from './api/code';
import { sqlTemplateMap } from './system';
const API_SQL_INSERT = '/je/common/script/sql/insert';
const API_SQL_INSERTANDINIT = '/je/common/script/sql/insertAndInit';
const API_SQL_UPDATE = '/je/common/script/sql/update';
const API_SQL_DELETE = '/je/common/script/sql/delete';
const API_SQL_SELECT = '/je/common/script/sql/select';

/**
 * 执行插入SQL
 * @param {string} templateCode 模板编码
 * @param {Object} params 模板参数
 * @returns {Promise}
 */
export function executeInsert(templateCode, params) {
  return executeSql({ url: API_SQL_INSERT, templateCode, params });
}
/**
 * 执行插入SQL，并初始化系统字段值
 * @param {string} templateCode 模板编码
 * @param {Object} params 模板参数
 * @returns {Promise}
 */
export function executeInsertAndInit(templateCode, params) {
  return executeSql({ url: API_SQL_INSERTANDINIT, templateCode, params });
}

/**
 * 执行修改SQL
 * @param {string} templateCode 模板编码
 * @param {Object} params 模板参数
 * @returns {Promise}
 */
export function executeUpdate(templateCode, params) {
  return executeSql({ url: API_SQL_UPDATE, templateCode, params });
}
/**
 * 执行删除SQL
 * @param {string} templateCode 模板编码
 * @param {Object} params 模板参数
 * @returns {Promise}
 */
export function executeDelete(templateCode, params) {
  return executeSql({ url: API_SQL_DELETE, templateCode, params });
}
/**
 * 执行查询SQL
 * @param {string} templateCode 模板编码
 * @param {Object} params 模板参数
 * @returns {Promise}
 */
export function executeSelect(templateCode, params) {
  return executeSql({ url: API_SQL_SELECT, templateCode, params });
}

/**
 * 执行sql
 * @param {*} param0
 * @returns
 */
function executeSql({ url, templateCode, params }) {
  const template = sqlTemplateMap.get(templateCode);
  if (template) {
    // 产品编码
    const projectCode = template?.pd;
    return ajax({
      url,
      headers: { pd: projectCode },
      params: {
        templateCode,
        projectCode,
        ...params,
      },
    }).then(transformAjaxData);
  } else {
    return Promise.reject({ code: '404', message: `未找到【${templateCode}】SQL模板` });
  }
}
/**
 * 初始化sql模板数据
 * @returns
 */
export function initSqlTemplateData() {
  sqlTemplateMap.clear();
  return initSqlTplApi()
    .then((data) => {
      data.forEach((item) => {
        sqlTemplateMap.set(item.QJSQL_MBBM, {
          id: item.JE_CORE_QJSQL_ID,
          pd: item.QJSQL_PROJECT_CODE,
          code: item.QJSQL_MBBM,
          type: item.QJSQL_SQLJBZXCL_CODE,
        });
      });
      return sqlTemplateMap;
    })
    .catch(() => {
      console.log('sql template error');
    });
}
