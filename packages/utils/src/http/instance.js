import { AjaxEventEnum, RequestEnum, ContentTypeEnum } from './enum';
import { mitt } from '../mitt';

/**
 * ajax实例
 */
export default class AjaxInstance {
  constructor() {}
  /**
   * 基础接口地址
   *
   * @memberof AjaxInstance
   */
  baseURL = '';
  /**
   * 事件管理
   *
   * @memberof AjaxInstance
   */
  emitter = mitt();

  /**
   * 基础配置
   *
   * @memberof AjaxInstance
   */
  defaultConfig = {
    timeout: 30 * 1000, // 超时时间
    method: RequestEnum.POST, // 请求方法
    headers: { 'Content-Type': ContentTypeEnum.FORM_URLENCODED }, // 请求头
  };

  /**
   * 设置默认配置
   * @param {Object} config 默认配置
   * @param {string} config.baseURL 基础接口地址
   * @param {number} config.timeout 超时时间：30000
   * @param {string} config.method 请求方法：POST
   * @param {Object} config.headers 请求头：{'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
   */
  setDefaultConfig(config = {}) {
    if (config.timeout < 1) {
      config.timeout = this.defaultConfig.timeout;
    }
    Object.assign(this.defaultConfig, config);
  }
  /**
   * 获取默认配置
   * @returns 默认配置
   */
  getDefaultConfig() {
    return this.defaultConfig;
  }
  /**
   * 设置基础接口地址
   * @param {string} baseURL
   */
  setBaseURL(baseURL = '') {
    if (baseURL.endsWith('/')) {
      baseURL = baseURL.substring(0, baseURL.length - 1);
    }
    this.baseURL = baseURL;
  }
  /**
   * 基础接口地址
   * @returns baseURL
   */
  getBaseURL() {
    return this.baseURL || '';
  }
  /**
   * 全局请求前事件，return false可以终止请求
   * @param {Function} fn
   */
  onBeforeRequest(fn) {
    this.emitter.on(AjaxEventEnum.BEFORE_REQUEST, fn);
  }
  /**
   * 全局请求完成事件
   * @param {Function} fn
   */
  onResponse(fn) {
    this.emitter.on(AjaxEventEnum.RESPONSE, fn);
  }
  /**
   * 全局请求失败事件
   * @param {Function} fn
   */
  onResponseCatch(fn) {
    this.emitter.on(AjaxEventEnum.RESPONSE_CATCH, fn);
  }

  /**
   * 注册事件
   * @param {*} eventName
   * @param  {...any} args
   */
  on(eventName, ...args) {
    this.emitter.on(eventName, ...args);
  }

  /**
   * 触发事件
   * @param {string} eventName 事件名称
   * @param  {...any} args
   */
  fire(eventName, ...args) {
    return this.emitter.emit(eventName, ...args);
  }
}
