import { isApp } from '../../index';
import { request4web } from './request-web';
import { request4app } from './request-app';
/**
 * request
 * @param {*} config
 * @returns
 */
export default function (config) {
  return isApp ? request4app(config) : request4web(config);
}
