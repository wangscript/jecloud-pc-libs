/**
 *   Request result
 */
export const ResultEnum = Object.freeze({
  SUCCESS: 0,
  ERROR: 1,
  TIMEOUT: 401,
  TYPE: 'success',
});

/**
 * request method
 */
export const RequestEnum = Object.freeze({
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
});

/**
 * ContentType
 */
export const ContentTypeEnum = Object.freeze({
  // json
  JSON: 'application/json;charset=UTF-8',
  // form-data qs
  FORM_URLENCODED: 'application/x-www-form-urlencoded;charset=UTF-8',
  // form-data  upload
  FORM_DATA: 'multipart/form-data;charset=UTF-8',
});

/**
 * ajax事件
 */
export const AjaxEventEnum = Object.freeze({
  BEFORE_REQUEST: 'before-request',
  RESPONSE: 'response',
  RESPONSE_CATCH: 'response-catch',
});
