import { reactive } from 'vue';
import Mock from 'mockjs';
export function useGridFunc() {
  const grid = reactive({
    scrollY: {
      oSize: 50,
    },
    border: true,
    draggable: true,
    keepSource: true,
    showOverflow: true,
    height: 500,
    rowConfig: { keyField: 'id' },
    editConfig: { trigger: 'click', mode: 'cell', showStatus: true },
    columns: [
      {
        type: 'checkbox',
        fixed: 'left',
      },
      { title: 'No.', type: 'seq', width: 50, fixed: 'left' },
      { title: '编码', width: 200, fixed: 'left' },
      { title: '名称', width: 200, fixed: 'left' },
      { title: '隐藏', type: 'switch', width: 50 },
      { title: '编辑', type: 'switch', width: 50 },
      { title: '自动填充', type: 'switch', width: 100 },
      { title: '加载项', type: 'switch', width: 100 },
      { title: '列宽', width: 50 },
      {
        title: '标题对齐方式',
        type: 'switch',
        switchConfig: {
          layout: 'buttons',
          options: [
            { label: '居左', value: 'left' },
            { label: '居中', value: 'center' },
            { label: '居右', value: 'right', icon: 'fas fa-bolt' },
          ],
        },
        width: 300,
      },
      {
        title: '数据对齐方式',
        type: 'switch',
        switchConfig: {
          layout: 'buttons',
          options: [
            { label: '居左', value: 'left' },
            { label: '居中', value: 'center' },
            { label: '居右', value: 'right', icon: 'fas fa-bolt' },
          ],
        },
        width: 300,
      },
      { title: '事件', width: 50 },
      { title: '查询类型', width: 120 },
      { title: '快速查询', width: 120 },
      { title: '批量更新', type: 'switch', width: 120 },
      { title: '允许展示', type: 'switch', width: 120 },
      { title: '关联查询列', type: 'switch', width: 120 },
      { title: '关联查询时快速查询', width: 300 },
      { title: '所属多表头', width: 120 },
      { title: '索引', width: 50 },
      { title: '提示', type: 'switch', width: 50 },
      { title: '锁定列', width: 50 },
      { title: '排序', type: 'switch', width: 50 },
    ],
  });

  const mockRules = {};
  grid.columns.forEach((column, index) => {
    const field = `field${index}`;
    switch (column.type) {
      case 'switch':
        column.field = field;
        if (column.switchConfig) {
          mockRules[`${field}|1`] = column.switchConfig.options.map((item) => item.value);
        } else {
          mockRules[`${field}|1`] = [0, 1];
        }
        break;
      case undefined:
        column.field = field;
        mockRules[`${field}|1-3`] = column.title;
        break;
    }
  });

  const data = Mock.mock({
    'rows|1000': [mockRules],
  });
  grid.data = data.rows;
  console.log(grid);
  return { grid };
}
