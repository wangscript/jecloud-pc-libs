import Mock from 'mockjs';
export default function () {
  return Mock.mock({
    'children|20': [
      {
        'index|+1': 1,
        'type|1': ['field', 'group'],
        'hidden|1-2': true,
        id: function () {
          return this.index.toString();
        },
        text: function () {
          return `${this.type == 'field' ? '字段' : '分组框'}${this.index}`;
        },
        groupId: undefined,
      },
    ],
  }).children;
}
