import { defineComponent, nextTick, onMounted, ref } from 'vue';
import Sortable from 'sortablejs';
import { dragClass, dragGroup, dragGhostClass } from './hooks/config';

export default defineComponent({
  name: 'DargFields',
  props: {
    data: {
      type: Array,
      default() {
        return [];
      },
    },
  },
  setup(props) {
    const el = ref();
    onMounted(() => {
      nextTick(() => {
        new Sortable(el.value, {
          handle: '.' + dragClass,
          ghostClass: dragGhostClass,
          group: {
            name: dragGroup,
            put: false, // Do not allow items to be put into this list
          },
          sort: false, // To disable sorting: set sort to false
        });
      });
    });
    return () => (
      <div class="form-hidden-fields" ref={el}>
        {props.data.map((item) => {
          return (
            <div
              class={{
                'drag-item': true,
                [dragClass]: true,
              }}
              key={item.id}
              data-id={item.id}
              data-type={item.type}
            >
              {item.text}
            </div>
          );
        })}
      </div>
    );
  },
});
