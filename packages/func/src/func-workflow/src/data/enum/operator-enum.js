/**
 * 流程操作枚举
 */
export const OperatorEnum = Object.freeze({
  //----------------------------------------流程----------------------------------------

  /**
   * 获取流程定义操作
   */
  GET_PROCESS_DEFINITION_OPERATOR: 'getProcessDefinitionOperation',
  /**
   * 获取流程定义集合操作
   */
  GET_PROCESS_DEFINITION_LIST_OPERATOR: 'getProcessDefinitionListOperation',
  /**
   * 流程定义暂停操作
   */
  PROCESS_DEFINITION_HANG_OPERATOR: 'processDefinitionHangOperation',
  /**
   * 流程定义激活操作
   */
  PROCESS_DEFINITION_ACTIVE_OPERATOR: 'processDefinitionActiveOperation',
  /**
   * 流程部署删除
   */
  PROCESS_DEPLOYMENT_DELETE_OPERATOR: 'processDeploymentDeleteOperation',
  /**
   * 流程部署集合
   */
  PROCESS_DEPLOYMENT_LIST_OPERATOR: 'processDeploymentGetListOperation',
  /**
   * 流程部署获取
   */
  PROCESS_DEPLOYMENT_GET_OPERATOR: 'processDeploymentGetOperation',
  /**
   * 流程模型获取
   */
  PROCESS_MODEL_LIST_OPERATOR: 'processModelGetListOperation',
  /**
   * 流程模型获取
   */
  PROCESS_MODEL_GET_OPERATOR: 'processModelGetOperation',
  /**
   * 流程模型获取
   */
  PROCESS_GET_RUN_FROM_CONFIG_OPERATION: 'processGetRunFromConfigOperation',
  /**
   * 流程模型获取
   */
  PROCESS_LAST_MODEL_GET_OPERATOR: 'processLastModelGetOperation',
  /**
   * 流程模型删除
   */
  PROCESS_MODEL_DELETE_OPERATOR: 'processModelDeleteOperation',
  /**
   * 流程模型部署
   */
  PROCESS_MODEL_DEPLOY_OPERATOR: 'processModelDeployOperation',
  /**
   * 流程模型保存
   */
  PROCESS_MODEL_SAVE_OPERATOR: 'processModelSaveOperation',
  /**
   * 流程空启动
   */
  PROCESS_EMPTY_START_OPERATOR: 'processEmptyStartOperation',
  /**
   * 流程按钮
   */
  PROCESS_GET_INITIAL_BUTTON_OPERATOR: 'processGetInitialButtonOperator',
  /**
   * 流程消息启动
   */
  PROCESS_MESSAGE_START_OPERATOR: 'processMessageStartOperation',
  /**
   * 流程空发起
   */
  PROCESS_EMPTY_SPONSOR_OPERATOR: 'processEmptySponsorOperation',
  /**
   * 流程消息发起
   */
  PROCESS_MESSAGE_SPONSOR_OPERATOR: 'processMessageSponsorOperation',
  /**
   * 流程挂起
   */
  PROCESS_HANG_OPERATOR: 'processHangOperation',
  /**
   * 流程激活
   */
  PROCESS_ACTIVE_OPERATOR: 'processActivateOperation',
  /**
   * 流程撤销
   */
  PROCESS_CANCEL_OPERATOR: 'processCancelOperation',
  /**
   * 流程作废
   */
  PROCESS_INVALID_OPERATOR: 'processInvalidOperation',
  /**
   * 获取提交节点信息
   */
  PROCESS_GET_NEXT_ELEMENT_OPERATOR: 'processGetNextElementOperator',
  /**
   * 获取提交节点信息处理人
   */
  PROCESS_GET_NEXT_ELEMENT_ASSIGNEE_OPERATOR: 'processGetNextElementAssigneeOperator',
  /**
   * 获取传阅节点信息
   */
  PROCESS_GET_CIRCULATED_INFO_OPERATOR: 'processGetCirculatedInfoOperator',
  /**
   * 获取驳回节点信息
   */
  PROCESS_GET_DISMISS_ELEMENT_OPERATOR: 'processGetDismissElementOperator',
  /**
   * 获取委托节点信息
   */
  PROCESS_GET_DELEGATE_ELEMENT_OPERATOR: 'processGetDelegateElementOperator',
  /**
   * 获取提交节点信息
   */
  PROCESS_GET_GO_BACK_OPERATOR: 'processGetGobackElementOperator',

  //----------------------------------------任务----------------------------------------
  /**
   * 任务提交
   */
  TASK_SUBMIT_OPERATOR: 'taskSubmitOperation',
  /**
   * 任务委托
   */
  TASK_DELEGATE_OPERATOR: 'taskDelegateOperation',
  /**
   * 任务取消委托
   */
  TASK_CANCEL_DELEGATE_OPERATOR: 'taskCancelDelegateOperation',
  /**
   * 任务更换负责人
   */
  TASK_CHANGE_ASSIGNEE_OPERATOR: 'taskChangeAssigneeOperation',
  /**
   * 任务领取
   */
  TASK_CLAIM_OPERATOR: 'taskClaimOperation',
  /**
   * 任务领取
   */
  TASK_DELAY_OPERATOR: 'taskDelayOperation',
  /**
   * 任务直送
   */
  TASK_DIRECT_SEND_OPERATOR: 'taskDirectSendOperation',
  /**
   * 任务驳回
   */
  TASK_DISMISS_OPERATOR: 'taskDismissOperation',
  /**
   * 任务退回
   */
  TASK_GOBACK_OPERATOR: 'taskGobackOperation',
  /**
   * 任务传阅
   */
  TASK_PASSROUND_OPERATOR: 'taskPassroundOperation',
  /**
   * 任务传阅已读
   */
  TASK_PASSROUND_READ_OPERATOR: 'taskPassroundReadOperation',
  /**
   * 任务取回
   */
  TASK_RETREIEVE_OPERATOR: 'taskRetrieveOperation',
  /**
   * 任务转办
   */
  TASK_TRANSFER_OPERATOR: 'taskTransferOperation',
  /**
   * 任务催办
   */
  TASK_URGE_OPERATOR: 'taskUrgeOperation',
  /**
   * 任务通过
   */
  TASK_PASS_OPERATOR: 'taskPassOperator',
  /**
   * 任务否决
   */
  TASK_VETO_OPERATOR: 'taskVetoOperator',
  /**
   * 任务弃权
   */
  TASK_ABSTAIN_OPERATOR: 'taskAbstainOperator',
  /**
   * 会签-改签
   */
  TASK_REBOOK_OPERATOR: 'taskRebookOperator',
  /**
   * 会签-人员调整
   */
  TASK_PERSONNEL_ADJUSTMENTS_OPERATOR: 'taskPersonnelAdjustmentsOperator',
  /**
   * 会签-加签
   */
  TASK_COUNTERSIGNED_ADD_SIGNATURE_OPERATOR: 'taskCountersignedAddSignatureOperator',
  /**
   * 会签-减签
   */
  TASK_COUNTERSIGNED_VISA_REDUCTION_OPERATOR: 'taskCountersignedVisaReductionOperator',

  /**
   * 切换任务，前端使用
   */
  TASK_CHANGE_OPERATOR: 'taskChangeOperator',
});
