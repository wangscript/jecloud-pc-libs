export const StatusEnum = Object.freeze({
  /**
   * 未启动
   */
  NOSTATUS: 'NOSTATUS',
  /**
   * 审批中
   */
  WAIT: 'WAIT',
  /**
   * 已挂起
   */
  HANDUPED: 'HANDUPED',
  /**
   * 已作废
   */
  SUSPEND: 'SUSPEND',
  /**
   * 已结束
   */
  ENDED: 'ENDED',
});
