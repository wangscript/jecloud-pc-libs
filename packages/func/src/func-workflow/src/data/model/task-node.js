import { toBoolean } from '@jecloud/utils';

/**
 * 任务节点
 */
export default class TaskNode {
  constructor(options) {
    this.id = options.nodeId ?? options.currentNodeId;
    this.name = options.nodeName ?? options.currentNodeName;
    this.target = options.target ?? options.currentTarget ?? options.nodeId;
    this.endTask = options.endTask;
    this.type = options.type;
    this.asynTree = toBoolean(options.asynTree);
    this.selectAll = toBoolean(options.selectAll); // 人员全选
    this.multiple = toBoolean(options.multiple); // 人员多选
    this.showSequentialConfig = toBoolean(options.showSequentialConfig); // 展示 并序，顺序配置
    this.sequential = options.sequential; // 并序，顺序
    this.simpleApproval = toBoolean(options.simpleApproval); // 简易审批
    this.submitDirectly = toBoolean(options.submitDirectly); // 直接提交
    this.personnelAdjustments = toBoolean(options.personnelAdjustments); // 人员选择器是否只读
    this.isJump = toBoolean(options.isJump) ? '1' : '0'; // 是否可跳跃

    this.end = this.type === 'end'; // 结束节点
    this.users = [];
  }
  setUsers(users = []) {
    const data = [];
    users.forEach((item) => {
      const node = new TaskNode(item.workflowConfig);
      node.children = [];
      item.users.forEach((user) => {
        node.children.push({
          id: user.assignmentConfigType,
          name: user.assignmentConfigTypeName,
          children: user.user.children ?? [],
        });
      });
      data.push(node);
    });
    this.users = data;
  }
}
