import { toBoolean } from '@jecloud/utils';
import { StatusEnum } from '../enum';
/**
 * 流程配置
 */
export default class WorkflowConfig {
  isModel = true;
  constructor(options) {
    if (options.isModel) return options;
    /**
     * 异步树
     */
    this.asynTree = toBoolean(options.asynTree);
    /**
     * 全选人员
     */
    this.selectAll = toBoolean(options.selectAll);
    /**
     * 简易审批
     */
    this.simpleApproval = toBoolean(options.simpleApproval);

    /**
     * 流程审批状态
     */
    this.status = options.audFlag;
    this.statusText = options.audFlagName;

    /**
     * 流程结束
     */
    this.isEnd = this.status === StatusEnum.ENDED;
    /**
     * 流程审批中
     */
    this.isWait = this.status === StatusEnum.WAIT;
    /**
     * 流程未启动
     */
    this.isUnStart = this.status === StatusEnum.NOSTATUS;

    /**
     * 当前节点
     */
    this.currentNode = {
      nodeId: options.currentNodeId,
      nodeName: options.currentNodeName,
      target: options.currentTarget,
    };
  }
}
