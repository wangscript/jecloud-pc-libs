import { OperatorEnum } from '../enum';

/**
 * 任务操作按钮
 */
export default class TaskButton {
  constructor(options) {
    this.appListeners = options.appListeners;
    this.appVersion = options.appVersion;
    this.backendListeners = options.backendListeners;
    this.code = options.code;
    this.displayExpression = options.displayExpression;
    this.id = options.id;
    this.name = options.name;
    this.operationId = options.operationId;
    this.pcListeners = options.pcListeners;
    this.pdid = options.pdid;
    this.piid = options.piid;
    this.taskId = options.taskId;
    this.workflowConfig = options.workflowConfig;
  }
}
/**
 * 切换任务按钮，前端使用
 */
export const TaskChangeButton = () => ({
  id: 'taskChangeBtn',
  code: 'taskChangeBtn',
  name: '切换任务',
  icon: 'fal fa-exchange',
  operationId: OperatorEnum.TASK_CHANGE_OPERATOR,
});

/**
 * 流程结束按钮，前端使用
 */
export const TaskEndButton = () => ({
  id: 'taskEndBtn',
  code: 'taskEndBtn',
  name: '流程已结束',
  isEnd: true,
});

/**
 * 流程作废按钮，前端使用
 * @returns
 */
export const TaskCancellationButton = () => ({
  id: 'taskCancellationBtn',
  code: 'taskCancellationBtn',
  name: '流程已作废',
  isEnd: true,
});

/**
 * 背景色
 */
export const TaskButtonBgColor = '#edf0f1';
