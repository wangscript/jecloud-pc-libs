/**
 * 操作参数
 */
import TaskCommonParams from './task-common-params';
import { OperatorEnum } from '../enum';
import { encode, pick } from '@jecloud/utils';
export default class TaskOperationParams {
  constructor(options) {
    this.commomParams = new TaskCommonParams(options);
    this.operationParams = this._buildOperationParams(options);
  }
  /**
   * 操作参数的keys
   */
  operationParamKeys = {
    assignee: 'assignee', // 提交人
    comment: 'comment', // 审批意见
    target: 'target', // 目标id
    sequentials: 'sequentials', // 多人审批，0 并序执行，1 顺序执行
    isJump: 'isJump', // 是否可跳跃, 1可跳跃,0不可跳跃
  };

  /**
   * 所有参数
   * @returns
   */
  toParams() {
    return {
      ...pick(this.operationParams, [
        this.operationParamKeys.assignee,
        this.operationParamKeys.comment,
        this.operationParamKeys.target,
        this.operationParamKeys.sequentials,
        this.operationParamKeys.isJump,
      ]),
      ...this.commomParams,
    };
  }

  /**
   * 精准的参数
   * @returns
   */
  toParams4accurate() {
    const operationId = this.commomParams.operationId;
    switch (operationId) {
      case OperatorEnum.PROCESS_EMPTY_START_OPERATOR: // 启动
      case OperatorEnum.PROCESS_CANCEL_OPERATOR: // 撤销
      case OperatorEnum.TASK_RETREIEVE_OPERATOR: // 取回
      case OperatorEnum.TASK_URGE_OPERATOR: // 催办
      case OperatorEnum.TASK_CLAIM_OPERATOR: // 领取
      case OperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR: // 取消委托
      case OperatorEnum.PROCESS_HANG_OPERATOR: // 挂起
      case OperatorEnum.PROCESS_ACTIVE_OPERATOR: // 激活
        return this.commomParams;
      case OperatorEnum.PROCESS_EMPTY_SPONSOR_OPERATOR: // 发起
      case OperatorEnum.TASK_SUBMIT_OPERATOR: // 送交
        return {
          ...pick(this.operationParams, [
            this.operationParamKeys.assignee,
            this.operationParamKeys.comment,
            this.operationParamKeys.target,
          ]),
          ...this.commomParams,
        };
      case OperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
      case OperatorEnum.TASK_GOBACK_OPERATOR: // 退回
      case OperatorEnum.PROCESS_INVALID_OPERATOR: // 作废
      case OperatorEnum.TASK_PASS_OPERATOR: //通过
      case OperatorEnum.TASK_VETO_OPERATOR: // 否决
      case OperatorEnum.TASK_ABSTAIN_OPERATOR: // 弃权
        return {
          ...pick(this.operationParams, [this.operationParamKeys.comment]),
          ...this.commomParams,
        };
      case OperatorEnum.TASK_TRANSFER_OPERATOR: // 转办
      case OperatorEnum.TASK_DELEGATE_OPERATOR: // 委托
        return {
          ...pick(this.operationParams, [
            this.operationParamKeys.assignee, // 单人
            this.operationParamKeys.comment,
          ]),
          ...this.commomParams,
        };
      case OperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
        return {
          ...pick(this.operationParams, [
            this.operationParamKeys.comment,
            this.operationParamKeys.target,
          ]),
          ...this.commomParams,
        };
    }
  }

  /**
   * 解析操作store的参数
   * @returns
   */
  _buildOperationParams({ store }) {
    if (!store) return {};
    const params = {
      target: store.activeNode.target, // 目标顺序流id
      isJump: store.activeNode.isJump,
      comment: store.taskComment ?? '', // 审批意见
      assignee: [], // 送交人信息
      sequential: store.sequential ?? '0', // 多人审批，0 并序执行，1 顺序执行
    };

    // 解析送交人信息
    params.assignee = TaskOperationParams.parseAssigneeParam(Object.values(store.taskUsers));
    // 解析会签、多人节点审批顺序传参
    params.sequentials = TaskOperationParams.parseSequentialParam(Object.values(store.taskUsers));
    return params;
  }
  /**
   * 解析送交人
   * @param {*} nodes
   * @returns
   */
  static parseAssigneeParam(nodes) {
    const assignees = [];
    nodes.forEach((node) => {
      // 送交节点
      const assignee = { nodeId: node.nodeId, nodeName: node.nodeName };
      // 送交人
      assignee.assignee = node.users.map((item) => item.id).join(',');
      assignee.assigneeName = node.users
        .map((item) => (item.bean && item.bean.ACCOUNT_NAME) || item.text)
        .join(',');
      assignees.push(assignee);
    });
    return encode(assignees);
  }
  /**
   * 解析会签、多人节点审批顺序传参
   * @param {*} nodes
   * @returns
   */
  static parseSequentialParam(nodes) {
    const sequentials = [];
    nodes.forEach((node) => {
      if (['countersign', 'batchtask'].indexOf(node.type) != -1) {
        sequentials.push({ nodeId: node.nodeId, sequential: node.sequential });
      }
    });
    return encode(sequentials);
  }
}
