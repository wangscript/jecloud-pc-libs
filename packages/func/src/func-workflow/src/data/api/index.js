import { ajax, transformAjaxData, pick } from '@jecloud/utils';
import { OperatorEnum } from '../enum';
import * as urls from './urls';

/**
 * 获取流程信息
 * @param {*} funcCode 功能编码
 * @param {*} tableCode 表编码
 * @param {*} beanId 主键
 * @param {*} prod 产品编码
 * @returns
 */
export function getWorkflowInfo({ funcCode, tableCode, beanId, prod }) {
  return baseAjax(urls.API_WORKFLOW_GET_INFO, { funcCode, tableCode, beanId, prod });
}

/**
 * 获取提交节点信息
 * @param {*} taskId 当前任务id
 * @param {*} taskId 流程部署id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} prod 产品编码
 * @returns
 */
export function getSubmitNodeInfo({ taskId, pdid, beanId, tableCode, prod }) {
  return baseAjax(urls.API_WORKFLOW_GET_SUBMIT_NODE_INFO, {
    taskId,
    pdid,
    beanId,
    tableCode,
    prod,
  });
}
/**
 * 获取节点人员信息
 * @param {*} taskId 当前任务id
 * @param {*} pdid 流程部署id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} prod 产品编码
 * @param {*} target 产品编码
 * @returns
 */
export function getSubmitNodeUsers(options) {
  return baseAjax(urls.API_WORKFLOW_GET_SUBMIT_NODE_USERS, options);
}

/**
 * 获取流程按钮操作参数详情
 * @param {*} operationId 按钮操作ID
 * @returns
 */
export function getButtonParams({ operationId }) {
  return baseAjax(urls.API_WORKFLOW_BUTTON_GET_PARAMS, { operationId });
}

/**
 * 执行流程按钮操作
 * @param {*} operationId 按钮操作ID
 * @param {*} pdid 流程部署id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} funcCode 功能编码
 * @param {*} prod 流程部署id
 * @returns
 */
export function doButtonOperate(options) {
  return baseAjax(urls.API_WORKFLOW_BUTTON_OPERATE, options);
}
/**
 * 获取任务节点信息
 * @param {*} operationId 操作id
 * @param {*} pdid 流程部署id
 * @param {*} piid 流程实例id
 * @param {*} taskId 当前任务id
 * @returns
 */
export function getTaskNodeInfo(options) {
  let url = '';
  const { operationId } = options;
  switch (operationId) {
    case OperatorEnum.TASK_GOBACK_OPERATOR: // 退回
      url = urls.API_WORKFLOW_GET_GOBACK_NODE;
      break;
    case OperatorEnum.TASK_RETREIEVE_OPERATOR:
      url = urls.API_WORKFLOW_GET_RETRIEVE_NODE; // 取回
      break;
    case OperatorEnum.TASK_DELEGATE_OPERATOR:
      url = urls.API_WORKFLOW_GET_DELEGATION_NODE; // 委托
      break;
    case OperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
      url = urls.API_WORKFLOW_GET_DIRECT_DELIVERY_NODE;
      break;
    case OperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
      url = urls.API_WORKFLOW_GET_REJECT_NODE;
      break;
  }
  return url && baseAjax(url, pick(options, ['pdid', 'piid', 'taskId']));
}

/**
 * 获取流程传阅人
 * @param {*} pdid 流程部署id
 * @param {*} piid 流程实例id
 * @param {*} taskId 当前任务id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} funcCode 功能编码
 * @param {*} prod 流程部署id
 * @returns
 */
export function getPassroundUsers(options) {
  return baseAjax(urls.API_WORKFLOW_GET_PASSROUND_USERS, options);
}

/**
 * 获取会签操作人
 * @param {*} piid 流程实例id
 * @param {*} taskId 当前任务id
 * @returns
 */
export function getCounterSignerOperationalUsers(options) {
  return baseAjax(
    urls.API_WORKFLOW_GET_COUNTER_SIGNER_OPERATIONAL_USERS,
    pick(options, ['piid', 'taskId']),
  );
}

/**
 * 获得流程历史
 * @param {*} beanId 功能业务id
 * @returns
 */
export function getWorkflowHistory(options) {
  return baseAjax(urls.API_WORKFLOW_GET_CIRCULATION_HISTORY, options);
}

/**
 * 获得流程图
 * @param {*} beanId 功能业务id
 * @param {*} pdId 工作流id
 * @returns
 */
export function previewImage(params) {
  return ajax({
    url: urls.API_WORKFLOW_PREVIEW_IMAGE,
    params,
    method: 'GET',
  }).then(transformAjaxData);
}

/**
 * 获得会签节点的信息
 * @param {*} taskId 单前任务id
 * @returns
 */
export function getCountersignApprovalOpinion(options) {
  return baseAjax(urls.API_WORKFLOW_GET_COUNTERSIGN_APPROVAL_OPINION, options);
}

/**
 * 获得传阅流程历史
 * @param {*} params
 * @returns
 */
export function getPassRoundUsersListByTaskId(params) {
  return baseAjax(urls.API_WORKFLOW_GET_PASSROUND_USERS_LIST_BY_TASKID, params);
}

/**
 * 获得提醒方式的数据
 * @param {*} params
 * @returns
 */
export function getMessageType(params) {
  return baseAjax(urls.API_WORKFLOW_GET_MESSAGE_TYPE, params);
}

/**
 * 获得催办历史
 * @param {*} params
 * @returns
 */
export function getUrgeData(params) {
  return baseAjax(urls.API_WORKFLOW_GET_URGE_DATA, params);
}

/** 获得被催办人员
 * @param {*} params
 * @returns
 */
export function getAssigneeByTaskId(params) {
  return baseAjax(urls.API_WORKFLOW_GET_ASSINGNEE_BY_TASKID, params);
}

/** 获得被催办人员
 * @param {*} params
 * @returns
 */
export function getCommonUser(params) {
  return baseAjax(urls.API_WORKFLOW_GET_COMMON_USER, params);
}
/**
 * 基础ajax
 * @param {*} url
 * @param {*} params
 * @returns
 */
function baseAjax(url, params) {
  return ajax({
    url: url,
    params,
  }).then(transformAjaxData);
}
