import { defineComponent, h, ref } from 'vue';
import { uuid, ajax } from '@jecloud/utils';
/**
 * 流程历史
 */
export default defineComponent({
  name: 'WorkflowImage',
  inheritAttrs: false,
  props: {
    params: Object, // 功能参数
  },
  setup(props, { expose }) {
    //获得流程图previewImageApi
    const imgUrl = ref();
    const initImgUser = () => {
      imgUrl.value = '';
      // 接口获取数据
      ajax({
        url: '/je/workflow/processInfo/preview',
        params: { beanId: props.params.beanId, pdid: props.params.pdid, uuid: uuid() },
        method: 'GET',
      }).then((res) => {
        imgUrl.value = res;
      });
    };
    expose({
      refresh() {
        initImgUser();
      },
    });
    return () =>
      h('div', {
        innerHTML: imgUrl.value,
        class: 'je-workflow-image',
      });
  },
});
