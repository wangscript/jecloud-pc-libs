import { ref } from 'vue';
import { Modal } from '@jecloud/ui';
/**
 * 切换任务
 * @param {*} param0
 */
export function showTaskChange({ button, callback }) {
  let activeTask = ref(button.currentNodeId);
  Modal.dialog({
    title: '切换任务',
    icon: button.icon,
    width: 340,
    height: 540,
    content: () => {
      return (
        <div class="je-workflow-task-change" style="border:1px solid #e6e6e6;height:100%">
          {button.taskList.map((task) => {
            const { workflowConfig } = task;
            return (
              <div
                class={{
                  'task-item': true,
                  'is--active': workflowConfig.currentNode.nodeId === activeTask.value,
                }}
                onClick={() => {
                  activeTask.value = workflowConfig.currentNode.nodeId;
                }}
              >
                {workflowConfig.currentNode.nodeName}
              </div>
            );
          })}
        </div>
      );
    },
    okButton() {
      const task = button.taskList.find(
        (item) => item.workflowConfig.currentNode.nodeId === activeTask.value,
      );
      Modal.notice(`切换【${task.workflowConfig.currentNode.nodeName}】成功！`, 'success');
      callback({ processInfo: [task] });
    },
  });
}
