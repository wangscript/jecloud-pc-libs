import { reactive, ref } from 'vue';
import { getMessageType, getAssigneeByTaskId, doButtonOperate } from '../../../data/api/index';
import { cloneDeep } from '@jecloud/utils';
import { Modal } from '@jecloud/ui';

export function tackOperation({ props, context, Form }) {
  const loading = ref(true);
  const okBtnLoading = ref(false);
  const verifyData = [
    {
      required: true,
      message: '该输入项为必填项',
    },
  ];
  const { emit } = context;
  const sendFormData = reactive({
    reminderMethod: 'WEB',
    personBeingUrgedIds: '',
    personBeingUrgedNames: '',
    urgentContent: '',
    ccUserIds: '',
    ccUserNames: '',
    ccContent: '',
  });
  const rulesRef = reactive({
    reminderMethod: verifyData,
    personBeingUrgedIds: verifyData,
    urgentContent: verifyData,
  });
  const { validate, validateInfos } = Form.useForm(sendFormData, rulesRef);
  console.log(props.taskOperationParams);
  // 激活面板的key
  const activeKey = ref('promptDispatch');
  // 获得通知类型
  const messageDatas = ref([]);
  // 获得被催办的人
  const uraeUsers = ref([]);
  Promise.all([
    getMessageType(),
    getAssigneeByTaskId({
      taskId: props.taskOperationParams.taskId,
      currentNodeId: props.workflowConfig.currentNode.nodeId,
    }),
  ]).then(([messages, users]) => {
    messageDatas.value = messages;
    uraeUsers.value = users;
    loading.value = false;
  });
  // 选中或取消被催办人
  const selectUser = (user) => {
    user.checked = !user.checked;
    getSelectUser();
    validate('personBeingUrgedIds');
  };

  // 处理被催办人数据
  const getSelectUser = () => {
    const userId = [],
      userName = [];
    uraeUsers.value.forEach((user) => {
      if (user.checked) {
        userId.push(user.id);
        userName.push(user.name);
      }
    });
    sendFormData.personBeingUrgedIds = userId.join(',');
    sendFormData.personBeingUrgedNames = userName.join(',');
  };

  // 保存操作
  const onSave = () => {
    okBtnLoading.value = true;
    const params = cloneDeep(sendFormData);
    Object.assign(params, props.taskOperationParams);
    validate()
      .then(() => {
        doButtonOperate(params)
          .then((data) => {
            Modal.notice(`催办成功！`, 'success');
            okBtnLoading.value = false;
            emit('modalClose', { type: 'ok', data });
          })
          .catch((e) => {
            Modal.alert(e.message, 'error');
            okBtnLoading.value = false;
          });
      })
      .catch((e) => {
        okBtnLoading.value = false;
      });
  };
  //取消操作
  const onClose = () => {
    emit('modalClose', { type: 'close' });
  };
  return {
    activeKey,
    messageDatas,
    sendFormData,
    uraeUsers,
    selectUser,
    onSave,
    onClose,
    validateInfos,
    okBtnLoading,
    loading,
  };
}
