import { ref, reactive, watch } from 'vue';
import { Data } from '@jecloud/utils';
import { getUrgeData } from '../../../data/api/index';
export function operationGrid({ props, context }) {
  const loading = ref(true);
  const gridStore = Data.Store.useGridStore({ data: [] });
  const columnData = ref([]);

  const tablePage = reactive({
    currentPage: 1,
    pageSize: 50,
    pageSizes: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
    totalResult: 30,
  });
  const onPageChange = () => {
    const type = props.typeKey == 'iAskedForIt' ? 'FROM_' : 'TO_';
    initGridData(type);
  };

  // 初始化列信息
  const initColumns = ({ type }) => {
    if (type == 'iAskedForIt') {
      columnData.value = [
        { title: '被催办人', field: 'TO_NAME_', width: 130 },
        { title: '方式', field: 'REMINDER_METHOD_NAME', width: 100 },
        { title: '催办时间', field: 'CREATE_TIME_', width: 200 },
        { title: '内容', field: 'CONTENT_', minWidth: 220 },
      ];
    } else {
      columnData.value = [
        { title: '催办人', field: 'FROM_NAME_', width: 130 },
        { title: '催办时间', field: 'CREATE_TIME_', width: 200 },
        { title: '内容', field: 'CONTENT_', minWidth: 220 },
      ];
    }
  };

  // 初始化列表数据
  const initGridData = (type) => {
    loading.value = true;
    getUrgeData({
      type,
      piid: props.taskOperationParams.piid,
      taskId: props.taskOperationParams.taskId,
      currentNodeId: props.workflowConfig.currentNode.nodeId,
      page: tablePage.currentPage,
      start: (tablePage.currentPage - 1) * tablePage.pageSize,
      limit: tablePage.pageSize,
    }).then((data) => {
      tablePage.totalResult = data.totalCount;
      gridStore.loadData(data.rows);
      loading.value = false;
    });
  };
  watch(
    () => props.type,
    (newVal) => {
      if (newVal == props.typeKey) {
        initColumns({ type: newVal });
        const type = newVal == 'iAskedForIt' ? 'FROM_' : 'TO_';
        initGridData(type);
      }
    },
    { immediate: true },
  );
  return { loading, gridStore, columnData, tablePage, onPageChange };
}
