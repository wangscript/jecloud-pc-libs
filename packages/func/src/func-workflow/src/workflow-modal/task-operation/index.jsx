import { defineComponent } from 'vue';
import { Tabs, Form, Checkbox, Input, InputSelect, Row, Col, Button, Panel } from '@jecloud/ui';
import { tackOperation } from './hooks/task-operation';
import WorkFlowOperation from './operation-grid';

export default defineComponent({
  name: 'WorkFlowTaskOperation',
  inheritAttrs: false,
  props: { taskOperationParams: Object, workflowConfig: Object },
  emits: ['modalClose'],
  setup(props, context) {
    const {
      activeKey,
      messageDatas,
      sendFormData,
      uraeUsers,
      selectUser,
      onSave,
      onClose,
      validateInfos,
      okBtnLoading,
      loading,
    } = tackOperation({
      props,
      context,
      Form,
    });
    return () => (
      <div className="je-workflow-task-operation" v-loading={loading.value}>
        {!loading.value ? (
          <Tabs v-model:activeKey={activeKey.value} style="height:100%">
            <Tabs.TabPane key="promptDispatch" tab="催办发送">
              <Panel
                class="je-workflow-task-operation-send-panel"
                v-slots={{
                  bbar: () => {
                    return (
                      <div class="je-workflow-task-operation-send-panel-button">
                        <Button onClick={onSave} type="primary" loading={okBtnLoading.value}>
                          发送
                        </Button>
                        <Button onClick={onClose} style="margin-left:15px">
                          取消
                        </Button>
                      </div>
                    );
                  },
                }}
              >
                <Panel.Item>
                  <div class="je-workflow-task-operation-send">
                    <Form
                      style="width:100%;height:100%"
                      model={sendFormData}
                      labelCol={{ span: 3 }}
                    >
                      <Form.Item
                        label="提醒方式"
                        name="reminderMethod"
                        {...validateInfos.reminderMethod}
                      >
                        <Checkbox.Group
                          v-model:value={sendFormData.reminderMethod}
                          options={messageDatas.value}
                        ></Checkbox.Group>
                      </Form.Item>
                      <Form.Item label="被催办人" {...validateInfos.personBeingUrgedIds}>
                        <div class="form-urae-user">
                          <Row gutter={[10, 10]}>
                            {uraeUsers.value.map((item) => {
                              return (
                                <Col span={4}>
                                  <div
                                    onClick={() => selectUser(item)}
                                    class={{
                                      'form-urae-user-item': true,
                                      select: item.checked,
                                      women: item.gender === 'WOMAN',
                                    }}
                                  >
                                    {item.name}
                                    <i class="icon fal fa-check" />
                                  </div>
                                </Col>
                              );
                            })}
                          </Row>
                        </div>
                      </Form.Item>
                      <Form.Item label="内容" name="urgentContent" {...validateInfos.urgentContent}>
                        <Input.TextArea
                          v-model:value={sendFormData.urgentContent}
                          rows="6"
                        ></Input.TextArea>
                      </Form.Item>
                      <Form.Item label="抄送" name="ccUserNames">
                        <InputSelect.User
                          v-model:value={sendFormData.ccUserNames}
                          configInfo="JE_RBAC_VACCOUNTDEPT,ccUserIds~ccUserNames,id~text,M"
                        ></InputSelect.User>
                      </Form.Item>
                      <Form.Item label="内容" name="ccContent">
                        <Input.TextArea
                          v-model:value={sendFormData.ccContent}
                          rows="6"
                        ></Input.TextArea>
                      </Form.Item>
                    </Form>
                  </div>
                </Panel.Item>
              </Panel>
            </Tabs.TabPane>
            <Tabs.TabPane key="iAskedForIt" tab="我催办的">
              <WorkFlowOperation
                typeKey={'iAskedForIt'}
                type={activeKey.value}
                taskOperationParams={props.taskOperationParams}
                workflowConfig={props.workflowConfig}
              />
            </Tabs.TabPane>
            <Tabs.TabPane key="urgeMe" tab="催办我的">
              <WorkFlowOperation
                typeKey={'urgeMe'}
                type={activeKey.value}
                taskOperationParams={props.taskOperationParams}
                workflowConfig={props.workflowConfig}
              />
            </Tabs.TabPane>
          </Tabs>
        ) : null}
      </div>
    );
  },
});
