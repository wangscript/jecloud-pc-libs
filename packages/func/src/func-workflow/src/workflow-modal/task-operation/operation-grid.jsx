import { defineComponent } from 'vue';
import { Grid, Pager } from '@jecloud/ui';
import { operationGrid } from './hooks/operation-grid';

export default defineComponent({
  name: 'WorkFlowOperation',
  inheritAttrs: false,
  props: { type: String, taskOperationParams: Object, workflowConfig: Object, typeKey: String },
  setup(props, content) {
    const { loading, gridStore, columnData, tablePage, onPageChange } = operationGrid({
      props,
      content,
    });
    return () => (
      <div style="height:100%;padding:0 20px;">
        <Grid
          border
          stripe
          auto-resize
          show-overflow="ellipsis"
          keep-source
          show-header-overflow
          highlight-hover-row
          resizable
          height="100%"
          store={gridStore}
          v-loading={loading.value}
          size="mini"
          header-align="center"
          v-slots={{
            pager: () => {
              return (
                <Pager
                  v-model:current-page={tablePage.currentPage}
                  v-model:page-size={tablePage.pageSize}
                  page-sizes={tablePage.pageSizes}
                  align="left"
                  background
                  size="mini"
                  total={tablePage.totalResult}
                  layouts={['JumpNumber', 'Sizes', 'FullJump', 'Total', 'Refresh']}
                  onLoad={onPageChange}
                ></Pager>
              );
            },
          }}
        >
          <Grid.Column title="序号" type="seq" width="50" align="center"></Grid.Column>
          {columnData.value.map((item) => {
            return (
              <Grid.Column
                field={item.field}
                title={item.title}
                align="center"
                width={item.width}
                min-width={item.minWidth}
              ></Grid.Column>
            );
          })}
        </Grid>
      </div>
    );
  },
});
