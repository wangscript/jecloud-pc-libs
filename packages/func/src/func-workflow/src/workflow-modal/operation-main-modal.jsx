import { Modal } from '@jecloud/ui';
import { getBodyHeight, getBodyWidth } from '@jecloud/utils';
import WorkflowMain from '../workflow-main';
/**
 * 打开流程追踪页面
 * @param {*} param0
 */
export function showWorkflowMain({ params, callback }) {
  const width = getBodyWidth() - 100;
  const height = getBodyHeight() - 100;
  Modal.window({
    title: '流程追踪',
    width,
    height,
    resize: false,
    minWidth: width - 500,
    minHeight: height - 500,
    headerStyle: 'height:50px;',
    bodyStyle: 'padding:0 20px 20px;',
    content() {
      return <WorkflowMain params={params} onOperateDone={callback} />;
    },
  });
}
