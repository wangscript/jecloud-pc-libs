import { ref, reactive, watch } from 'vue';
import { cloneDeep, getBodyHeight, isEmpty } from '@jecloud/utils';
import { Modal, Tabs, Panel } from '@jecloud/ui';
import { getPassroundUsers, doButtonOperate } from '../data/api';
import TaskOperationParams from '../data/model/task-operation-params';
import WorkflowOperationComment from '../workflow-operation/src/operation-comment';
import WorkflowUser from '../workflow-operation/src/operation-user';
import WorkflowCommonUser from '../workflow-operation/src/operation-common-user';
/**
 * 传阅操作
 * @param {*} param0
 */
export function showTaskPassRound({ params, callback, passRoundbutton, $func }) {
  const { currentNode } = passRoundbutton.workflowConfig;
  const loading = ref(true);
  const workflowUser = ref();
  const userConfig = reactive({
    activeTypeId: '',
    types: [],
    users: [],
    resultUsers: [],
  });
  Modal.dialog({
    title: passRoundbutton.name,
    icon: passRoundbutton.icon,
    width: 790,
    minWidth: 790,
    class: 'je-workflow-task-passround',
    height: getBodyHeight() - 100,
    headerStyle: { height: '50px' },
    content: () => {
      return (
        <div v-loading={loading.value} style="height:100%">
          {loading.value ? null : (
            <Panel>
              {userConfig.types.length > 1 ? (
                <Panel.Item region="tbar">
                  <div class="task-user-type">
                    <div class="task-user-type-label">筛选条件：</div>
                    <Tabs
                      class="task-user-type-items"
                      type="card"
                      v-model:activeKey={userConfig.activeTypeId}
                    >
                      {userConfig.types.map((item) => {
                        return (
                          <Tabs.TabPane
                            class={{
                              'task-user-type-items-item': true,
                              'is--active': item.assignmentConfigType === userConfig.activeTypeId,
                            }}
                            key={item.assignmentConfigType}
                            tab={item.assignmentConfigTypeName}
                          />
                        );
                      })}
                    </Tabs>
                  </div>
                </Panel.Item>
              ) : null}
              <Panel.Item>
                <WorkflowUser
                  funcObj={$func}
                  ref={workflowUser}
                  draggable={false}
                  userDisabled={true}
                  multiple={true}
                  users={userConfig.users}
                  v-model:result={userConfig.resultUsers}
                ></WorkflowUser>
              </Panel.Item>
              <Panel.Item region="bbar" style="height:35px">
                <WorkflowCommonUser onSelectUser={selectUser} />
              </Panel.Item>
            </Panel>
          )}
        </div>
      );
    },
    okButton: {
      text: '传阅',
      closable: false,
      handler({ $modal, button }) {
        button.loading = true;
        // 获得选中的人员信息
        if (userConfig.resultUsers.length > 0) {
          // 封装参数
          const passroundData = cloneDeep(params);
          currentNode.users = userConfig.resultUsers;
          passroundData.assignee = TaskOperationParams.parseAssigneeParam([currentNode]);
          // 调用提交流程接口
          doButtonOperate(passroundData)
            .then((data) => {
              Modal.notice(`${passRoundbutton.name}成功！`, 'success');
              callback(data);
              button.loading = false;
              $modal.close();
            })
            .catch((error) => {
              Modal.alert(error.message, 'error');
              button.loading = false;
              $modal.close();
            });
        } else {
          button.loading = false;
          Modal.alert('请选择人员信息！', Modal.status.warning);
        }
      },
    },
  });
  // 加载传阅人员信息
  getPassroundUsers(params)
    .then((data) => {
      if (data.length > 0) {
        userConfig.types = data;
        userConfig.activeTypeId = data[0].assignmentConfigType;
        userConfig.users = data[0].user;
      }
      loading.value = false;
    })
    .catch((e) => {
      Modal.alert(e.message, 'error');
    });
  // 监听筛选条件值,刷新人员选择数据
  watch(
    () => userConfig.activeTypeId,
    (newVal) => {
      if (newVal && workflowUser.value) {
        const activeType = userConfig.types.find((item) => item.assignmentConfigType === newVal);
        workflowUser.value.loadUser(activeType.user);
      }
    },
  );
  // 常用人员组件选择人
  const selectUser = ({ row }) => {
    workflowUser.value.selectHistoryUser({ userId: row.userId });
  };
}
/**
 * 审阅操作
 * @param {*} param0
 */
export function showTaskCheckApprove({ params, callback, passRoundbutton }) {
  const taskComment = ref('已阅');
  Modal.dialog({
    title: passRoundbutton.name,
    icon: passRoundbutton.icon,
    width: 790,
    minWidth: 790,
    height: getBodyHeight() - 200,
    class: 'je-workflow-task-checkapprove',
    headerStyle: { height: '50px' },
    content: () => {
      return (
        <WorkflowOperationComment
          v-model:taskComment={taskComment.value}
        ></WorkflowOperationComment>
      );
    },
    buttons: [
      {
        text: passRoundbutton.name,
        type: 'primary',
        handler: ({ $modal, button }) => {
          button.loading = true;
          if (isEmpty(taskComment.value)) {
            Modal.alert('请填写审批意见！', 'warning');
            return (button.loading = false);
          }
          // 封装参数
          const passroundData = cloneDeep(params);
          passroundData.comment = taskComment.value;
          // 调用提交流程接口
          doButtonOperate(passroundData)
            .then((data) => {
              Modal.notice(`${passRoundbutton.name}成功！`, 'success');
              callback(data);
              button.loading = false;
              $modal.close();
            })
            .catch((error) => {
              Modal.alert(error.message, 'error');
              button.loading = false;
              $modal.close();
            });
        },
      },
    ],
  });
}
