import { ref, watch } from 'vue';
import { Modal, Panel, Tabs } from '@jecloud/ui';
import WorkflowUser from '../workflow-operation/src/operation-user';
import { getSubmitNodeUsers, getCounterSignerOperationalUsers, doButtonOperate } from '../data/api';
import { OperatorEnum } from '../data/enum';
import TaskOperationParams from '../data/model/task-operation-params';
import { isNotEmpty, cloneDeep, getBodyHeight } from '@jecloud/utils';

/**
 * 会签-人员调整
 * @param {*} param0
 */
export function showTaskChangeUser({ params, button, callback, $func }) {
  const { currentNode } = button.workflowConfig;
  const target = currentNode.nodeId;
  Promise.all([
    getSubmitNodeUsers({ ...params, target }),
    getCounterSignerOperationalUsers(params),
  ]).then(([taskUsers, resultUser]) => {
    //负责人
    const manager = { id: resultUser.oneBallotUserId, text: resultUser.oneBallotUserName };
    //节点人员信息
    let users = taskUsers.nodeAssigneeInfo[0]?.users || [];
    // 节点人员分类tab数据
    let tabData = [];
    // 有多种选人的节点
    if (users.length > 1) {
      //users.
      users.forEach((item) => {
        tabData.push({
          id: item.assignmentConfigType,
          name: item.assignmentConfigTypeName,
        });
      });
    }
    const doneUsers = []; // 已审批人员
    let unDoneUsers = []; // 未审批人员
    const filterUserIds = manager.id ? [manager.id] : []; //需要过滤掉的人员id(已审批人员和负责人)
    //封装已审批人员和未审批人员信息
    const usersInfo = resultUser.usersInfo || [];
    if (usersInfo.length > 0) {
      usersInfo.forEach((item) => {
        if (isNotEmpty(item.opinion)) {
          doneUsers.push({ id: item.userId, text: item.userName });
          filterUserIds.push(item.userId);
        } else {
          unDoneUsers.push({ id: item.userId, text: item.userName });
        }
      });
    }
    // 特殊处理更换负责人的左侧树数据
    if (button.operationId === OperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR) {
      tabData = [];
      users = [{ user: [] }];
      let userDatas = cloneDeep(unDoneUsers);
      if (userDatas.length > 0) {
        userDatas.forEach((item) => {
          item.code = item.id;
          item.nodeType = 'LEAF';
          item.icon = 'fal fa-user-alt';
          item.children = [];
          item.nodePath = '/ROOT/' + item.id;
          item.parent = 'ROOT';
          item.nodeInfoType = 'json';
        });
      }
      //去掉负责人
      if (manager.id) {
        userDatas = userDatas.filter((item) => {
          return item.id != manager.id;
        });
      }
      // 拼接tree的数据
      let rootData = {
        async: false,
        checked: false,
        children: userDatas,
        code: 'ROOT',
        expandable: true,
        expanded: false,
        id: 'ROOT',
        nodeInfoType: '',
        text: 'ROOT',
      };
      if (userDatas.length > 0) {
        users[0].user = rootData;
      }
      unDoneUsers = [];
    }

    // 送审人员
    const assignee = cloneDeep(doneUsers);
    if (manager.id) {
      assignee.push(manager);
    }

    showModal({
      params,
      button,
      callback,
      currentNode,
      $func,
      userInfo: {
        users,
        manager,
        doneUsers,
        tabData,
        unDoneUsers,
        assignee,
        filterUserIds,
      },
    });
  });
}
/**
 * 打开窗口
 */
function showModal({ params, button, callback, userInfo, currentNode, $func }) {
  const resultUsers = ref(userInfo.unDoneUsers);
  const multiple = button.operationId === OperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR;
  const activeTypeId = ref(userInfo.tabData[0]?.id);
  const users = ref(userInfo.users[0]?.user);
  const workflowUser = ref();
  // 监听人员选择节点切换
  watch(
    () => activeTypeId.value,
    (newData) => {
      const data = userInfo.users.filter((item) => {
        return newData == item.assignmentConfigType;
      });
      workflowUser.value.loadUser(data[0]?.user);
    },
  );

  Modal.dialog({
    title: button.name,
    icon: button.icon,
    width: 750,
    class: 'je-workflow-user-adjustment',
    height: getBodyHeight() - 200,
    headerStyle: { height: '50px' },
    content: () => {
      return (
        <Panel class="je-workflow-task-change-user">
          <Panel.Item region="tbar">
            {userInfo.manager.id ? (
              <div class="je-workflow-task-change-user-div" style="padding:4px;">
                负责人：{userInfo.manager.text}
              </div>
            ) : null}
            {userInfo.doneUsers.length ? (
              <div class="je-workflow-task-change-user-div" style="padding:4px;">
                已审批人员：{userInfo.doneUsers.map((item) => item.text).join('，')}
              </div>
            ) : null}
            {userInfo.tabData.length > 0 ? (
              <div class="task-user-type">
                <div class="task-user-type-label">筛选条件：</div>
                <Tabs
                  class="task-user-type-items"
                  type="card"
                  v-model:activeKey={activeTypeId.value}
                >
                  {userInfo.tabData.map((item) => {
                    return (
                      <Tabs.TabPane
                        class={{
                          'task-user-type-items-item': true,
                          'is--active': item.id === activeTypeId.value,
                        }}
                        key={item.id}
                        tab={item.name}
                      />
                    );
                  })}
                </Tabs>
              </div>
            ) : null}
          </Panel.Item>
          <Panel.Item>
            <WorkflowUser
              funcObj={$func}
              ref={workflowUser}
              draggable={false}
              userDisabled={true}
              multiple={multiple}
              users={users.value}
              v-model:result={resultUsers.value}
            ></WorkflowUser>
          </Panel.Item>
        </Panel>
      );
    },
    okButton: {
      closable: false,
      handler({ $modal }) {
        if (resultUsers.value.length) {
          // 送交人
          currentNode.users = userInfo.assignee.concat(resultUsers.value);
          if (multiple) {
            //已选人中过滤掉负责人和已审批人员
            if (userInfo.filterUserIds.length > 0) {
              const selectUserDatas = cloneDeep(resultUsers.value);
              const userDatas = selectUserDatas.filter((item) => {
                return userInfo.filterUserIds.indexOf(item.id) == -1;
              });
              currentNode.users = userInfo.assignee.concat(userDatas);
            }
          } else {
            currentNode.users = resultUsers.value;
          }
          params.assignee = TaskOperationParams.parseAssigneeParam([currentNode]);
          doButtonOperate(params)
            .then((data) => {
              Modal.notice(`${button.name}成功！`, 'success');
              callback(data);
              $modal.close();
            })
            .catch((error) => {
              Modal.alert(error.message, 'error');
              $modal.close();
            });
        } else {
          Modal.alert('请选择人员信息！', Modal.status.warning);
        }
      },
    },
  });
}
