import { Modal } from '@jecloud/ui';
import { getBodyHeight } from '@jecloud/utils';
import { watch, ref } from 'vue';
import WorkflowOperation from '../workflow-operation';
/**
 * 操作窗口
 * @param {*} param0
 */
export function showWorkflowOperation({ params, button, callback, $func }) {
  // 关闭窗体
  const closeModal = () => {
    modal.close();
  };
  const modeSizeChange = ref(true);
  const modal = Modal.window({
    title: button.name,
    width: 790,
    minWidth: 790,
    height: getBodyHeight() - 100,
    content() {
      return (
        <WorkflowOperation
          funcObj={$func}
          params={params}
          button={button}
          onOperateDone={callback}
          modeSizeChange={modeSizeChange.value}
          onCloseModal={closeModal}
        />
      );
    },
  });
  watch(
    () => modal.reactData.zoomLocation,
    () => {
      modeSizeChange.value = !modeSizeChange.value;
    },
  );
}
