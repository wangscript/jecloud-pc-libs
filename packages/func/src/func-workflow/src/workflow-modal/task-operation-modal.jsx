import { Modal } from '@jecloud/ui';
import { getBodyHeight } from '@jecloud/utils';
import WorkFlowTaskOperation from './task-operation';
export function showTaskOperationModal({ taskOperationParams, callback, operationbutton }) {
  const { workflowConfig } = operationbutton;
  const modalClose = ({ type, data }) => {
    $Modal && $Modal.close();
    if (type == 'ok') {
      callback(data);
    }
  };
  const $Modal = Modal.dialog({
    title: operationbutton.name,
    icon: operationbutton.icon,
    class: 'je-workflow-task-operation-modal',
    width: 790,
    minWidth: 790,
    height: getBodyHeight() - 100,
    showFooter: false,
    headerStyle: { height: '50px' },
    resize: false,
    content: () => {
      return (
        <WorkFlowTaskOperation
          onModalClose={modalClose}
          taskOperationParams={taskOperationParams}
          workflowConfig={workflowConfig}
        ></WorkFlowTaskOperation>
      );
    },
  });
}
