import { ref } from 'vue';
import { getBodyHeight } from '@jecloud/utils';
import { Modal } from '@jecloud/ui';
import { getPassRoundUsersListByTaskId } from '../../data/api';
export function showPassroundHistory({ taskId, userId }) {
  const loading = ref(true);
  const passroundDatas = ref([]);
  Modal.dialog({
    title: '传阅信息',
    width: 900,
    height: getBodyHeight() - 200,
    class: 'je-workflow-history-passround',
    headerStyle: { height: '50px' },
    buttons: [
      {
        text: '关闭',
        handler: ({ $modal }) => {
          $modal.close();
        },
      },
    ],
    buttonAlign: 'center',
    content: () => {
      return (
        <div class="je-workflow-history" v-loading={loading.value} style="margin:0">
          {passroundDatas.value.length > 0 ? (
            <table class="history-table">
              <thead>
                <tr>
                  <th width="80px">执行人</th>
                  <th width="80px">操作</th>
                  <th min-width="220px">审批意见</th>
                  <th width="110px">审核时间</th>
                </tr>
              </thead>
              <tbody>
                {passroundDatas.value.map((item) => {
                  return (
                    <tr style={{ color: item.operation == '未处理' ? '#7F7F7F' : '' }}>
                      <td>{item.handlerName}</td>
                      <td>{item.operation}</td>
                      <td>{item.comment}</td>
                      <td>{item.approvalTime}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          ) : loading.value ? null : (
            <div class="null-data">暂无数据！</div>
          )}
        </div>
      );
    },
  });
  // 加载传阅历史数据
  getPassRoundUsersListByTaskId({ taskId, userId })
    .then((data) => {
      loading.value = false;
      passroundDatas.value = data;
    })
    .catch((e) => {
      loading.value = false;
      Modal.alert(e.message, 'error');
    });
}
