import { defineComponent, h, ref, onMounted, nextTick } from 'vue';
import { isNotEmpty } from '@jecloud/utils';
import { getWorkflowHistory } from '../../data/api';
import { showPassroundHistory } from '../hooks/workflow-history';
/**
 * 流程历史
 */
export default defineComponent({
  name: 'WorkflowHistory',
  inheritAttrs: false,
  props: {
    params: Object, // 功能参数
  },
  emits: ['rendered', 'collapsed'],
  setup(props, { expose, emit }) {
    const loading = ref(true);
    //初始化工作流历史数据
    let workflowHistoryData = ref([]);

    const initData = (beanId) => {
      if (isNotEmpty(beanId)) {
        getWorkflowHistory({ beanId }).then((data) => {
          workflowHistoryData.value = data;
          loading.value = false;
          emit('collapsed', { flag: workflowHistoryData.value.length > 0 });
        });
      } else {
        workflowHistoryData.value = [];
      }
    };

    expose({
      refresh({ beanId, clearType }) {
        loading.value = true;
        if (clearType) {
          workflowHistoryData.value = [];
        }
        // 执行刷新操作
        initData(beanId);
      },
    });
    onMounted(() => {
      nextTick(() => {
        emit('rendered');
      });
    });
    //格式化执行人名称
    const getFormattingName = (data) => {
      const nameHtml = [];
      if (isNotEmpty(data) && data.length > 0) {
        data.forEach((item) => {
          nameHtml.push(h('div', { textContent: item }));
        });
      }
      if (nameHtml.length > 0) {
        return nameHtml;
      }
    };
    return () =>
      workflowHistoryData.value.length > 0 ? (
        <div class="je-workflow-history">
          <table class="history-table" v-loading={loading.value}>
            <thead>
              <tr>
                <th width="140px">节点名称</th>
                <th width="80px">执行人</th>
                <th width="80px">操作</th>
                <th min-width="220px">审批意见</th>
                <th width="80px">状态</th>
                <th width="110px">接收时间</th>
                <th width="110px">审核时间</th>
                <th width="80px">耗时</th>
              </tr>
            </thead>
            <tbody>
              {workflowHistoryData.value.map((item, index) => {
                return item.nodeType == 'inclusiveGateway' ? (
                  <tr key={index}>
                    <td colspan="8" class="gateway">
                      ----- {item.nodeName} -----
                    </td>
                  </tr>
                ) : (
                  <tr
                    key={index}
                    style={{ color: item.operationType == '正在处理' ? '#7F7F7F' : '' }}
                  >
                    <td>
                      {item.nodeName}
                      {item.comments.length == 1 && item.comments[0].hasCirculationInformation ? (
                        <span
                          class="history-table-td-context"
                          onClick={() =>
                            showPassroundHistory({
                              taskId: item.comments[0].taskId,
                              userId: item.comments[0].userId,
                            })
                          }
                        >
                          传阅信息
                        </span>
                      ) : null}
                    </td>
                    <td>{getFormattingName(item.assignees)}</td>
                    <td>{item.operationType}</td>
                    {isNotEmpty(item.title) || item.comments.length > 1 ? (
                      <td class="opinion-td">
                        <table class="opinion-table">
                          <tr v-show={item.isShowTitle == '1'}>
                            <td colspan="4" class="countersign-title" v-html={item.title}></td>
                          </tr>
                          {item.comments.map((rec, index) => {
                            return (
                              <tr
                                style={{
                                  color: rec.commentType == '未处理' ? '#7F7F7F' : '#3f3f3f',
                                }}
                              >
                                <td width="80px">
                                  {rec.userName}
                                  {rec.hasCirculationInformation && item.comments.length > 1 ? (
                                    <span
                                      class="history-table-td-context"
                                      onClick={() =>
                                        showPassroundHistory({
                                          taskId: rec.taskId,
                                          userId: rec.userId,
                                        })
                                      }
                                    >
                                      传阅信息
                                    </span>
                                  ) : null}
                                </td>
                                <td
                                  v-show={item.nodeType == 'kaiteCounterSignUserTask'}
                                  width="80px"
                                >
                                  {rec.commentType}
                                </td>
                                <td min-width="220px">{rec.comment}</td>
                                <td width="110px">{rec.endTime}</td>
                              </tr>
                            );
                          })}
                        </table>
                      </td>
                    ) : (
                      <td>{item.comments[0]?.comment}</td>
                    )}
                    <td style={{ color: item.state == 'NORMAL' ? '#0A7900' : '#D8001B' }}>
                      {item.stateName}
                    </td>
                    <td>{item.startTime}</td>
                    <td>{item.endTime}</td>
                    <td>{item.durationInMillis}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      ) : null;
  },
});
