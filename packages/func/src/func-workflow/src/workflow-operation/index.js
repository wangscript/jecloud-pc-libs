import JeWorkflowOperation from './src';
import { withInstall } from '../../../utils';
export const WorkflowOperation = withInstall(JeWorkflowOperation);
export default WorkflowOperation;
