import { defineComponent, watch } from 'vue';
import { Panel, Loading } from '@jecloud/ui';
import ProcessOne from './operation-process-one';
import ProcessTwo from './operation-process-two';
import ProcessThree from './operation-process-three';
import TBar from './operation-tbar';
import BBar from './operation-bbar';
import OperationEmpty from './operation-empty';
import { useWorkflow } from './hooks/use-workflow';

export default defineComponent({
  name: 'WorkflowOperation',
  inheritAttrs: false,
  props: {
    button: Object, // 操作按钮
    params: Object, // 功能参数
    modeSizeChange: Boolean,
    funcObj: Object,
  },
  emits: ['operate-done', 'closeModal'],
  setup(props, context) {
    const { loading, nodeDataEmpty, closeModal } = useWorkflow({ props, context });
    return () =>
      !nodeDataEmpty.value ? (
        <Panel class="je-workflow-operation" v-loading={loading.value}>
          <Panel.Item region="tbar">
            <TBar />
          </Panel.Item>
          <Panel.Item>
            <ProcessOne />
            <ProcessTwo modeSizeChange={props.modeSizeChange} funcObj={props.funcObj} />
            <ProcessThree />
          </Panel.Item>
          <Panel.Item region="bbar">
            <BBar />
          </Panel.Item>
        </Panel>
      ) : (
        <OperationEmpty onCloseModal={closeModal} v-loading={loading.value} />
      );
  },
});
