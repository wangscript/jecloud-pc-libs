import { isEmpty, forEach, loadDDItemByCode, isArray, isNotEmpty } from '@jecloud/utils';
import { Modal } from '@jecloud/ui';
import {
  getSubmitNodeInfo,
  getSubmitNodeUsers,
  doButtonOperate as _doButtonOperate,
  getTaskNodeInfo,
} from '../../../data/api';
import TaskNode from '../../../data/model/task-node';
import { OperatorEnum } from '../../../data/enum';
export function useWorkflowOperation({ props, context, $workflow }) {
  const { store } = $workflow;
  const { operationId } = store;
}
/**
 * 初始化节点信息
 * @param {*} param0
 * @returns
 */
export function initSubmitNodeInfo({ store, button, nodeDataEmpty, loading, $modal }) {
  const { currentNode } = button.workflowConfig;
  const { operationId } = store;
  let promise;
  switch (operationId) {
    case OperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
    case OperatorEnum.TASK_GOBACK_OPERATOR: // 退回
    case OperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
      // 请求节点信息
      promise = getTaskNodeInfo(store.getParams()).then((data) => {
        return isArray(data) ? data : [data];
      });

      // 直接操作，不需要人员
      store.directOperation = true;
      // 驳回操作在第一步
      if (operationId == OperatorEnum.TASK_DISMISS_OPERATOR) {
        store.directOperationFirst = true;
      }
      break;
    case OperatorEnum.PROCESS_INVALID_OPERATOR: // 作废
    case OperatorEnum.TASK_PASS_OPERATOR: // 通过
    case OperatorEnum.TASK_VETO_OPERATOR: // 否决
    case OperatorEnum.TASK_ABSTAIN_OPERATOR: // 弃权
      // 使用当前节点
      promise = Promise.resolve([currentNode]);
      // 直接操作，不需要人员
      store.directOperation = true;
      break;
    case OperatorEnum.TASK_DELEGATE_OPERATOR: // 委托
    case OperatorEnum.TASK_TRANSFER_OPERATOR: // 转办
      // 使用当前节点
      promise = Promise.resolve([currentNode]);
      break;
    default:
      // 正常提交操作
      promise = getSubmitNodeInfo(store.getParams());
      break;
  }

  return Promise.all([promise, loadDDItemByCode('JE_WF_APPROOPINION')])
    .then(([nodes, messages]) => {
      //如果节点信息为空
      if (nodes.length <= 0) {
        nodeDataEmpty.value = true;
        loading.value = false;
        return false;
      }
      store.taskMessages = messages;
      store.taskNodes = nodes?.map((node) => new TaskNode(node));
      store.activeNode = store.taskNodes[0];
      // 直接操作，不需要步骤
      if (store.directOperation || store.activeNode.submitDirectly) {
        //在第一步(节点有多个)
        if (store.directOperationFirst && store.taskNodes.length > 1) {
          store.activeStep = 1;
        } else {
          //在第最后一步
          store.activeStep = 3;
        }
        store.directOperation = true;
      } else if (store.taskNodes.length === 1) {
        if (store.activeNode.end) {
          store.directOperation = true;
          store.activeStep = 3;
          store.name = '结束';
        } else {
          // 如果只有一个节点，转到下一步
          store.nextSetp();
        }
      }
    })
    .catch((e) => {
      Modal.alert(e.message, 'error');
      $modal && $modal.close();
    });
}

/**
 * 初始节点人员信息
 * 切换节点后，更新人员信息
 * @param {*} param0
 * @returns
 */
export function initSubmitNodeUsers({ store, $modal }) {
  let promise;
  if (isEmpty(store.activeNode.users)) {
    const { target } = store.activeNode;
    promise = getSubmitNodeUsers(store.getParams({ target }))
      .then((data) => {
        store.activeNode.setUsers(data.nodeAssigneeInfo);
        //如果选中的节点name为空需要重新赋值一下
        /*if (isEmpty(store.activeNode.name) && isNotEmpty(data.nodeAssigneeInfo)) {
        store.activeNode.name = data.nodeAssigneeInfo[0].workflowConfig.currentNodeName;
      }*/
        return store.activeNode.users;
      })
      .catch((e) => {
        Modal.alert(e.message, 'error');
        $modal && $modal.close();
      });
  } else {
    promise = Promise.resolve(store.activeNode.users);
  }
  return promise.then((data) => {
    // if (isEmpty(store.taskUsers)) {
    store.updateTaskUsers(data);
    //}
    return data;
  });
}

/**
 * 提交流程方法
 * @param {*} param0
 */
function submitOperate({ store, $modal, context, submitBtn }) {
  const { emit } = context;
  if (isNotEmpty(submitBtn)) {
    submitBtn.value.loading = true;
  }
  _doButtonOperate(store.getOperatParams())
    .then((data) => {
      $modal.close();
      Modal.notice(`${store.name}成功！`, 'success');
      emit('operate-done', data);
      if (isNotEmpty(submitBtn)) {
        submitBtn.value.loading = false;
      }
    })
    .catch((error) => {
      Modal.alert(error.message, 'error');
      $modal.close();
    });
}

/**
 * 执行流程按钮操作
 * @param {*} param0
 * @returns
 */
export function doButtonOperate({ store, $modal, context, $func, submitBtn }) {
  // 表单保存方法
  const { doFormSave } = $func.action;
  // 表单对象
  const $form = $func.getFuncForm();
  // 表单保存按钮
  const saveBtn = $form.getButtons('formSaveBtn');
  // 表单是否修改
  const changeData = $form.getChanges();
  // 如果表单保存按钮没有隐藏并且表单有数据修改,先执行表单保存再提交流程
  if ($func) {
    if (!saveBtn.hidden && isNotEmpty(changeData)) {
      doFormSave().then(() => {
        saveBtn.loading = false;
        submitOperate({ store, $modal, context, submitBtn });
      });
    } else {
      // 提交流程
      submitOperate({ store, $modal, context, submitBtn });
    }
  } else {
    // 提交流程
    submitOperate({ store, $modal, context, submitBtn });
  }
}
/**
 * 操作步骤
 * @param {*} param0
 */
export function doOperationSetp({ store, next, submitBtn }) {
  if (next) {
    switch (store.activeStep) {
      // 前往第2步
      case 1:
        store.nextSetp();
        break;
      // 前往第3步
      case 2:
        const userKeys = Object.keys(store.taskUsers);
        if (isEmpty(userKeys)) {
          Modal.alert(`请选择人员信息！`, Modal.status.warning);
          return;
        }
        let userEmpty = false;
        forEach(userKeys, (key) => {
          const node = store.taskUsers[key];
          if (isEmpty(node.users)) {
            userEmpty = true;
            Modal.alert(`请选择【${node.nodeName}】的人员信息！`, Modal.status.warning);
            return false;
          }
        });
        // 如果有空的人员，停止下一步
        !userEmpty && store.nextSetp();
        break;
      // 提交操作
      case 3:
        if (store.taskComment) {
          store.operate({ submitBtn });
        } else {
          Modal.alert('请填写审批意见！', Modal.status.warning);
        }
        break;
    }
  } else {
    store.prevSetp();
  }
}
