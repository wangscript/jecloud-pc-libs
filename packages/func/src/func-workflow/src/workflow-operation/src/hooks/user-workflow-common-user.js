import { reactive } from 'vue';
import { isNotEmpty } from '@jecloud/utils';
import { getCommonUser } from '../../../data/api/index';

export function userWorkFlowCommonUser(props, context) {
  const { emit } = context;
  const commonUser = reactive({ showUserDatas: [], menuUserDatas: [], selectUserId: '' });

  // 获得常用人员
  getCommonUser().then((data) => {
    //处理流程常用人数据
    if (data.length > 0) {
      //如果大于7条会有更多按钮
      if (data.length > 8) {
        commonUser.showUserDatas = data.splice(0, 8);
        commonUser.menuUserDatas = data;
      } else {
        commonUser.showUserDatas = data;
      }
    }
  });

  const selectUser = ({ row }) => {
    commonUser.selectUserId = row.userId;
    emit('selectUser', { row });
  };
  const handlerMenu = (e) => {
    const key = e.key;
    if (isNotEmpty(key)) {
      selectUser({ row: { userId: key } });
    }
  };

  return { commonUser, selectUser, handlerMenu };
}
