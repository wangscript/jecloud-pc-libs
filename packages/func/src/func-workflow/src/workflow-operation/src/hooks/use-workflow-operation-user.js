import { reactive, ref, nextTick, onMounted, watch } from 'vue';
import { Data, Modal, Hooks } from '@jecloud/ui';
import Sortable from 'sortablejs';
import { isNotEmpty, getCurrentUser } from '@jecloud/utils';
import { OperatorEnum } from '../../../data/enum';
export function useWorkflowOperationUser({ props, context }) {
  const { expose, emit } = context;
  const $userTree = ref();
  const $userResult = ref();
  const treeStore = Data.Store.useTreeStore({ data: [] });
  const result = Hooks.useModelValue({ props, context, key: 'result' });
  const sequential = Hooks.useModelValue({ props, context, key: 'sequential' });
  const userConfig = reactive({
    activeUser: null,
  });
  const $user = {
    loadUser(users) {
      treeStore.loadData(users);
      nextTick(() => {
        // 默认展开第一层节点
        treeStore.data.forEach((item) => {
          $userTree.value.setTreeExpand(item, true);
        });
        // 默认选择全部人员
        if (props.selectAll && props.multiple) {
          selectUser();
        }
      });
    },
    selectHistoryUser({ userId }) {
      const record = $userTree.value.getRowById(userId);
      if (isNotEmpty(record)) {
        $userTree.value.setSelectRow(record);
        selectUser({ row: record });
      } else {
        $userTree.value.clearSelectedRecords();
        if (isNotEmpty(userId)) {
          Modal.notice('没有该人员！', 'warning');
        }
      }
    },
  };
  expose($user);

  // 拖拽排序
  onMounted(() => {
    nextTick(() => {
      // 加载用户数据
      $user.loadUser(props.users);
      if (props.draggable) {
        const userSort = new Sortable($userResult.value, {
          handle: '.users-item',
          ghostClass: 'users-item-sortable-ghost',
          disabled: !props.userDisabled,
          onEnd({ newIndex, oldIndex }) {
            const users = result.value;
            const user = users.splice(oldIndex, 1)[0];
            users.splice(newIndex, 0, user);
          },
        });
        // 是否可以拖拽
        watch(
          () => props.userDisabled,
          (newVal) => {
            userSort?.option('disabled', !newVal);
          },
        );
      }
    });
  });

  /**
   * 树形选取人员
   * @param {*} param0
   */
  const selectUser = (options = {}) => {
    const { row } = options;
    let rows = [];
    //选中传入的数据
    if (row) {
      // 转办和委托不能选自己
      const currentUserId = getCurrentUser()?.deptmentUserId;
      if (props.wfButton) {
        const { operationId, name } = props.wfButton;
        if (
          [OperatorEnum.TASK_DELEGATE_OPERATOR, OperatorEnum.TASK_TRANSFER_OPERATOR].indexOf(
            operationId,
          ) != -1 &&
          currentUserId == row.id
        ) {
          Modal.alert('不可以' + name + '给自己！', 'warning');
          return false;
        }
      }
      row.nodeInfoType === 'json' && rows.push(row);
      //全选
    } else {
      treeStore.cascade((node) => {
        if (node.nodeInfoType === 'json') {
          //去重操作
          const rowData = rows.filter((row) => {
            return row.id == node.id;
          });
          if (rowData.length <= 0) {
            rows.push(node);
          }
        }
      });
    }

    // 多人任务，追加人员信息
    if (props.multiple) {
      const users = result.value;
      const ids = users.map((user) => user.id);
      rows = rows.filter((row) => !ids.includes(row.id));
      users.push(...rows);
      userConfig.activeUser = row ? row : rows[0];
    } else if (rows.length) {
      // 更新选中人员信息
      result.value = rows;
      userConfig.activeUser = rows[0];
    }
  };

  /**
   * 移动用户
   * @param {*} type
   */
  const moveUser = (type) => {
    switch (type) {
      case 'moveTreeUser':
        const row = $userTree.value.getSelectedRecords()[0];
        if (row) {
          selectUser({ row });
        } else {
          Modal.alert('请选择要添加人员！', Modal.status.warning);
        }
        break;
      case 'moveTreeUsers':
        selectUser({});
        break;
      case 'moveSelectUser':
        const users = result.value;
        const user = users.find((item) => item.id === userConfig.activeUser?.id);
        if (user) {
          const index = users.indexOf(user);
          users.splice(index, 1);
          userConfig.activeUser = users[0];
        } else {
          Modal.alert('请选择要移除人员！', Modal.status.warning);
        }
        break;
      case 'moveSelectUsers':
        result.value = [];
        userConfig.activeUser = null;
        break;
    }
  };

  watch(
    () => sequential.value,
    (newVal) => {
      emit('changeSequential', newVal);
    },
  );

  return {
    userConfig,
    selectUser,
    $userTree,
    treeStore,
    moveUser,
    $userResult,
    result,
    sequential,
  };
}
