import { useProvideWorkflow } from './context';
import { ref } from 'vue';
import { useStore } from './user-store';
import { initSubmitNodeInfo } from './use-workflow-operation';
import TaskButton from '../../../data/model/task-button';
import { Modal } from '@jecloud/ui';
export function useWorkflow({ props, context }) {
  const { expose } = context;
  const loading = ref(true);
  const nodeDataEmpty = ref(false);
  // 流程对象
  const $workflow = {
    props,
    context,
    $modal: Modal.useInjectModal(),
    button: new TaskButton(props.button), // 操作按钮
    nodeDataEmpty,
    loading,
  };
  // 操作store
  $workflow.store = useStore({ props, context, $workflow });
  // 对外暴露功能
  expose($workflow);
  // 对子暴露功能
  useProvideWorkflow($workflow);

  loading.value = true;
  // 加载流程信息
  initSubmitNodeInfo($workflow).then(() => {
    loading.value = false;
  });

  // 关闭窗体
  const closeModal = () => {
    context.emit('closeModal');
  };

  return { loading, nodeDataEmpty, closeModal };
}
