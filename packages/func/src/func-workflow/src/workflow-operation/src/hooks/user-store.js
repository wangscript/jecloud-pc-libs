import { reactive } from 'vue';
import { pick } from '@jecloud/utils';
import TaskCommonParams from '../../../data/model/task-common-params';
import TaskOperationParams from '../../../data/model/task-operation-params';
import { doButtonOperate } from './use-workflow-operation';
/**
 * 操作store
 * @param {*} props
 * @returns
 */
export function useStore({ props, $workflow }) {
  const { button, params } = props;
  const store = reactive({
    activeStep: 1, // 激活的流程步骤
    activeNode: null, // 当前激活节点
    directOperation: false, // 直接操作，不需要步骤操作
    taskMessages: [], // 审批意见
    taskNodes: [], // 任务节点列表
    taskUsers: {}, // 流程人员信息
    taskComment: button.customizeComments || '同意', // 流程提交信息
    sequential: '0', //并序执行
    commonParams: new TaskCommonParams({ ...button, ...params }), // 公共参数
    ...pick(button, ['name', 'operationId', 'pdid', 'piid', 'taskId']),
    nextSetp() {
      // store.directOperation == true 单个节点不选人直接到第三步
      // store.activeNode.submitDirectly == true  第一步有多个节点 该值为true不选人直接跳第三步
      store.activeStep =
        store.directOperation || (store.activeNode && store.activeNode.submitDirectly)
          ? 3
          : store.activeStep + 1;
    },
    prevSetp() {
      // store.directOperation == true 单个节点不选人直接到第一步
      // store.activeNode.submitDirectly == true 第一步有多个节点 该值为true不选人直接跳第一步
      store.activeStep =
        store.directOperation || (store.activeNode && store.activeNode.submitDirectly)
          ? 1
          : store.activeStep - 1;
    },
    getParams(options = {}) {
      return { ...store.commonParams, ...options };
    },
    getOperatParams() {
      const taskOperation = new TaskOperationParams({ ...store.commonParams, store });
      return taskOperation.toParams();
    },
    updateTaskUsers(nodes = []) {
      const oldTaskUsers = store.taskUsers;
      store.taskUsers = {};
      nodes.forEach((node) => {
        store.taskUsers[node.id] = {
          nodeId: node.id,
          sequential: node.sequential,
          nodeName: node.name,
          type: node.type,
          users: oldTaskUsers[node.id] ? oldTaskUsers[node.id].users || [] : [],
        };
      });
    },
    setTaskUsers(node, users = []) {
      store.taskUsers[node].users = users;
    },
    getTaskUsers(node) {
      return store.taskUsers[node]?.users;
    },
    operate({ submitBtn }) {
      doButtonOperate({ ...$workflow, $func: params.$func, submitBtn });
    },
  });

  return store;
}
