import { reactive, watch, ref } from 'vue';
import { useInjectWorkflow } from './context';
import { initSubmitNodeUsers } from './use-workflow-operation';
import { isNotEmpty } from '@jecloud/utils';
import { Modal } from '@jecloud/ui';
export function useWorkflowProcessTwo() {
  const $workflow = useInjectWorkflow();
  const { store, button } = $workflow;
  const loading = ref(true);
  const $user = ref();
  const userConfig = reactive({
    nodes: [],
    types: [],
    users: [],
    activeNode: null,
    activeNodeId: null,
    activeTypeId: null,
    resultUsers: [],
    wfButton: button,
  });
  // 同步人员选择数据
  watch(
    () => userConfig.resultUsers,
    (users) => {
      userConfig.activeNodeId && store.setTaskUsers(userConfig.activeNodeId, users);
    },
    { deep: true },
  );
  // 监听任务节点切换
  watch(
    () => userConfig.activeNodeId,
    () => {
      userConfig.activeNode = userConfig.nodes.find((item) => item.id === userConfig.activeNodeId);
      if (userConfig.activeNode) {
        // 设置人员结果集
        userConfig.resultUsers = store.getTaskUsers(userConfig.activeNodeId);
        // 设置人员类型
        userConfig.types = userConfig.activeNode.children ?? [];
        userConfig.activeTypeId = userConfig.types[0]?.id;
        //重置常用人员选中状态
        /* if (isNotEmpty(userConfig.historyUserData.selectUserId)) {
          userConfig.historyUserData.selectUserId = '';
          $user.value.selectHistoryUser({ userId: '' });
        } */
        // 固定人,随机节点默认全部选中
        if (['to_assignee', 'random'].indexOf(userConfig.activeNode.type) != -1) {
          userConfig.activeNode.selectAll = true;
          userConfig.activeNode.multiple = true;
        }
      } else {
        Modal.alert('节点配置有误,请联系管理员！', 'error');
      }
    },
  );
  // 监听筛选条件切换
  watch(
    () => userConfig.activeTypeId,
    () => {
      // 节点的人员为空
      if (!userConfig.activeTypeId) {
        $user.value.loadUser([]);
        Modal.alert('当前节点未配置可处理人信息，请联系管理员进行操作！', 'error');
        return;
      }
      const activeType = userConfig.types.find((item) => item.id === userConfig.activeTypeId);
      $user.value.loadUser(activeType.children);
    },
  );
  // 切换节点，更新用户数据
  watch(
    () => store.activeStep,
    (nv, ov) => {
      if (ov === 1 && nv === 2) {
        loading.value = true;
        initSubmitNodeUsers($workflow).then((data) => {
          userConfig.nodes = data;
          userConfig.activeNodeId = userConfig.nodes[0]?.id;
          loading.value = false;
        });
      }
    },
  );
  //选中常用人员
  const selectUser = ({ row }) => {
    if (isNotEmpty(row) && isNotEmpty(row.userId) && userConfig.activeNode.personnelAdjustments) {
      $user.value.selectHistoryUser({ userId: row.userId });
    }
  };

  //会签节点审批顺序值 添加到主数据中
  const changeSequential = (data) => {
    userConfig.activeNode.sequential = data;
    if (isNotEmpty(store.taskUsers) && isNotEmpty(store.taskUsers[userConfig.activeNodeId])) {
      store.taskUsers[userConfig.activeNodeId].sequential = data;
    }
  };

  return { userConfig, loading, store, $user, selectUser, changeSequential };
}
