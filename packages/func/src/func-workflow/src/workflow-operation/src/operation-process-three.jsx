import { Col, Row, Input, Dropdown, Menu } from '@jecloud/ui';
import { defineComponent, computed } from 'vue';
import { useInjectWorkflow } from './hooks/context';
import { decode } from '@jecloud/utils';
import WorkflowOperationComment from './operation-comment';

export default defineComponent({
  name: 'WorkflowProcessThree',
  inheritAttrs: false,
  setup() {
    const { store } = useInjectWorkflow();
    const messages = ['请您审批', '同意', '不同意', '已阅'];
    const users = computed(() => {
      const items = [];
      Object.keys(store.taskUsers).forEach((key) => {
        items.push(...store.getTaskUsers(key));
      });
      return items;
    });
    return () => (
      <div
        class="je-workflow-operation-process-three"
        style={{ display: store.activeStep === 3 ? undefined : 'none' }}
      >
        {/* 选中节点 */}
        <div class="task-node is--active">
          {store.activeNode?.name || store.name}
          <i class="icon fal fa-check" />
        </div>
        {/* 选中的人员 */}
        <div class="task-users" style={{ display: users.value.length ? undefined : 'none' }}>
          <Row gutter={[10, 10]}>
            {users.value.map((item) => {
              const info = decode(item.nodeInfo);
              return (
                <Col span={item.type === 'team' ? 8 : 4}>
                  <div
                    class={{
                      'task-users-selecked-item': true,
                      women: info.sex === 'WOMAN',
                      team: info.sex === 'team',
                    }}
                  >
                    {item.bean.ACCOUNT_NAME || info.ACCOUNT_NAME}
                    <i class="icon fal fa-check" />
                  </div>
                </Col>
              );
            })}
          </Row>
        </div>
        {/* 审批意见 */}
        <WorkflowOperationComment
          v-model:taskComment={store.taskComment}
        ></WorkflowOperationComment>
      </div>
    );
  },
});
