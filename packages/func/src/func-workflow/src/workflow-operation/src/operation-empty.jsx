import { defineComponent } from 'vue';
import { Panel, Button, Toolbar } from '@jecloud/ui';

export default defineComponent({
  name: 'WorkflowOperationEmpty',
  inheritAttrs: false,
  emits: ['closeModal'],
  setup(props, context) {
    return () => (
      <Panel class="je-workflow-operation">
        <Panel.Item>
          <div class="je-workflow-operation-null-context">
            <span>没有节点信息，请联系管理员~</span>
          </div>
        </Panel.Item>

        <Panel.Item region="bbar">
          <Toolbar class="je-workflow-operation-bbar">
            <Toolbar.Fill />
            <Button
              onClick={() => {
                context.emit('closeModal');
              }}
            >
              关闭
            </Button>
          </Toolbar>
        </Panel.Item>
      </Panel>
    );
  },
});
