import { defineComponent } from 'vue';
import { useInjectWorkflow } from './hooks/context';

export default defineComponent({
  name: 'WorkflowProcessOne',
  inheritAttrs: false,
  setup() {
    const { store } = useInjectWorkflow();
    return () => (
      <div
        class="je-workflow-operation-process-one"
        style={{ display: store.activeStep === 1 ? undefined : 'none' }}
      >
        {store.taskNodes?.map((node) => {
          return (
            <div
              class={{ 'task-node': true, 'is--active': node.id === store.activeNode?.id }}
              onClick={() => {
                store.activeNode = node;
              }}
              key={node.id}
            >
              {node.name || store.name}
              <i v-show={node.id === store.activeNode?.id} class="icon fal fa-check" />
            </div>
          );
        })}
      </div>
    );
  },
});
