import { defineComponent, watch, ref } from 'vue';
import { useWorkflowProcessTwo } from './hooks/use-workflow-process-two';
import { Tabs } from '@jecloud/ui';
import OperationUser from './operation-user';
import WorkflowCommonUser from './operation-common-user';
export default defineComponent({
  name: 'WorkflowProcessTwo',
  inheritAttrs: false,
  props: {
    modeSizeChange: Boolean,
    funcObj: Object,
  },
  setup(props, context) {
    const { userConfig, loading, store, selectUser, $user, changeSequential } =
      useWorkflowProcessTwo({
        props,
        context,
      });
    const workflowOperationProcessTwo = ref();

    const setOperationUserHeight = () => {
      //总面板的高度
      const totalHeight = workflowOperationProcessTwo.value.offsetHeight;
      if (totalHeight > 0) {
        //子面板对象
        const childNodes = workflowOperationProcessTwo.value.childNodes;
        let childNodesHeight = 0;
        let userDom = null;
        //计算其他子面板的高度 去除loading页和选择人面板
        childNodes.forEach((item) => {
          const height = item.offsetHeight;
          if (
            height > 0 &&
            ['je-loading', 'je-workflow-operation-user'].indexOf(item.className) == -1
          ) {
            childNodesHeight += height;
          } else if (item.className == 'je-workflow-operation-user') {
            userDom = item;
          }
        });
        if (childNodesHeight > 0 && userDom) {
          //给选择人面板赋值高度
          const height =
            totalHeight - childNodesHeight < 260 ? 260 : totalHeight - childNodesHeight;
          userDom.style.height = height + 'px';
        }
      }
    };

    watch(
      () => store.activeStep,
      (newVal) => {
        //动态计算第二步选择人面板的高度
        if (newVal == 2) {
          setTimeout(() => {
            setOperationUserHeight();
          }, 100);
        }
      },
    );

    watch(
      () => [userConfig.nodes.length, userConfig.types.length, props.modeSizeChange],
      () => {
        //动态计算第二步选择人面板的高度
        setTimeout(() => {
          setOperationUserHeight();
        }, 300);
      },
    );
    return () => (
      <div
        v-loading={loading.value}
        class="je-workflow-operation-process-two"
        style={{ display: store.activeStep === 2 ? undefined : 'none' }}
        ref={workflowOperationProcessTwo}
      >
        {/* 选中节点 */}
        <div class="task-node is--active">
          {store.activeNode?.name}
          <i class="icon fal fa-check" />
        </div>
        {/* 分支任务 */}
        {userConfig.nodes.length > 1 ? (
          <div class="task-user-type">
            <div class="task-user-type-label">分支任务：</div>
            <Tabs
              class="task-user-type-items fork-nodes"
              type="card"
              v-model:activeKey={userConfig.activeNodeId}
            >
              {userConfig.nodes.map((item) => {
                return (
                  <Tabs.TabPane
                    class={{
                      'task-user-type-items-item': true,
                      'is--active': item.id === userConfig.activeNodeId,
                    }}
                    key={item.id}
                    tab={item.name}
                  />
                );
              })}
            </Tabs>
          </div>
        ) : null}
        {/* 筛选条件 */}
        {userConfig.nodes.length > 1 || userConfig.types.length > 1 ? (
          <div class="task-user-type">
            <div class="task-user-type-label">筛选条件：</div>
            <Tabs
              class="task-user-type-items"
              type="card"
              v-model:activeKey={userConfig.activeTypeId}
            >
              {userConfig.types.map((item) => {
                return (
                  <Tabs.TabPane
                    class={{
                      'task-user-type-items-item': true,
                      'is--active': item.id === userConfig.activeTypeId,
                    }}
                    key={item.id}
                    tab={item.name}
                  />
                );
              })}
            </Tabs>
          </div>
        ) : null}
        {/* 人员选择器 */}
        <OperationUser
          ref={$user}
          funcObj={props.funcObj}
          multiple={userConfig.activeNode?.multiple}
          selectAll={userConfig.activeNode?.selectAll}
          showSequentialConfig={userConfig.activeNode?.showSequentialConfig}
          v-model:result={userConfig.resultUsers}
          sequential={userConfig.activeNode?.sequential}
          userDisabled={userConfig.activeNode?.personnelAdjustments}
          onChangeSequential={changeSequential}
          wfButton={userConfig.wfButton}
        ></OperationUser>
        {/**常用审批人 */}
        <WorkflowCommonUser onSelectUser={selectUser} />
      </div>
    );
  },
});
