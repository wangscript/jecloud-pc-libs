import { defineComponent, ref } from 'vue';
import { Toolbar, Button } from '@jecloud/ui';
import { useInjectWorkflow } from './hooks/context';
import { doOperationSetp } from './hooks/use-workflow-operation';

export default defineComponent({
  name: 'WorkflowOperationTbar',
  inheritAttrs: false,
  setup() {
    const { store } = useInjectWorkflow();

    const submitBtn = ref(null);
    const submitBtnLoading = ref(false);

    return () => (
      <Toolbar class="je-workflow-operation-bbar">
        <Toolbar.Fill />
        <Button
          onClick={() => doOperationSetp({ store })}
          style={{ display: store.activeStep === 1 ? 'none' : undefined }}
        >
          上一步
        </Button>
        <Button
          type="primary"
          ref={submitBtn}
          loading={submitBtnLoading.value}
          onClick={() => doOperationSetp({ store, next: true, submitBtn })}
        >
          {store.activeStep === 3 ? store.name : '下一步'}
        </Button>
      </Toolbar>
    );
  },
});
