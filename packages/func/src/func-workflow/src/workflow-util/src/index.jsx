import { Modal, Button } from '@jecloud/ui';
import { OperatorEnum } from '../../data/enum';
import TaskOperationParams from '../../data/model/task-operation-params';
import {
  doButtonOperate,
  getWorkflowInfo as _getWorkflowInfo,
  getCountersignApprovalOpinion,
} from '../../data/api';
import WorkflowConfig from '../../data/model/workflow-config';
import {
  TaskChangeButton,
  TaskEndButton,
  TaskButtonBgColor,
  TaskCancellationButton,
} from '../../data/model/task-button';
import FormConfig from '../../data/model/workflow-form-config';
import {
  showTaskChange,
  showWorkflowOperation,
  showWorkflowMain,
  showTaskChangeUser,
  showTaskPassRound,
  showTaskCheckApprove,
  showTaskOperationModal,
} from '../../workflow-modal';
import {
  isArray,
  isNotEmpty,
  toTemplate,
  execScript4Return,
  execScript,
  decode,
  isEmpty,
  isPromise,
  useJE,
  createDeferred,
} from '@jecloud/utils';

/**
 * 流程追踪
 */
export { showWorkflowMain };

/**
 * 流程按钮操作
 * @param {*} param0
 */
export function doWorkflowOperation({ button, params, $func, callback }) {
  const { operationId } = button;
  const taskOperation = new TaskOperationParams({ ...params, ...button }); // 操作参数
  const taskOperationParams = taskOperation.toParams();
  switch (operationId) {
    case OperatorEnum.PROCESS_CANCEL_OPERATOR: // 撤销
      Modal.confirm('是否要撤销流程（撤销后将删除审批记录）！', () => {
        doButtonOperate(taskOperationParams)
          .then((data) => {
            Modal.notice(`${button.name}成功！`, 'success');
            callback(data);
          })
          .catch((e) => {
            Modal.alert(e.message, 'error');
          });
      });
      break;
    case OperatorEnum.PROCESS_EMPTY_START_OPERATOR: // 启动
    case OperatorEnum.TASK_RETREIEVE_OPERATOR: // 取回
    case OperatorEnum.TASK_CLAIM_OPERATOR: // 领取
    case OperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR: // 取消委托
      doButtonOperate(taskOperationParams)
        .then((data) => {
          Modal.notice(`${button.name}成功！`, 'success');
          callback(data);
        })
        .catch((e) => {
          Modal.alert(e.message, 'error');
        });
      break;
    case OperatorEnum.TASK_URGE_OPERATOR: // 催办
      showTaskOperationModal({ taskOperationParams, callback, operationbutton: button });
      break;
    case OperatorEnum.TASK_CHANGE_OPERATOR: // 切换任务
      showTaskChange({ button, callback });
      break;
    case OperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR: // 人员调整
    case OperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR: // 更换负责人
      showTaskChangeUser({ params: taskOperationParams, button, callback, $func });
      break;
    case OperatorEnum.TASK_REBOOK_OPERATOR: //改签
      doRebookOperator({ taskOperationParams, callback });
      break;
    case OperatorEnum.TASK_PASSROUND_OPERATOR: //传阅
      showTaskPassRound({ params: taskOperationParams, callback, passRoundbutton: button, $func });
      break;
    case OperatorEnum.TASK_PASSROUND_READ_OPERATOR: //审阅
      showTaskCheckApprove({ params: taskOperationParams, callback, passRoundbutton: button });
      break;
    default:
      showWorkflowOperation({ params, button, callback, $func });
      break;
  }
}

/**
 * 改签操作
 * @param {*} param0
 */
const doRebookOperator = ({ taskOperationParams, callback }) => {
  //调用接口
  const doOperate = ({ taskOperationParams, buttonName, button, $modal }) => {
    doButtonOperate(taskOperationParams).then((data) => {
      Modal.notice(`${buttonName}成功！`, 'success');
      callback(data);
      button.loading = false;
      $modal.close();
    });
  };
  //查询该会签节点的审批状态
  getCountersignApprovalOpinion({ taskId: taskOperationParams.taskId }).then((data) => {
    Modal.dialog({
      content: '是否要进行改签重新投票！',
      title: '改签',
      buttons: [
        {
          text: '通过',
          type: 'primary',
          closable: false,
          disabled: data == 'PASS',
          handler: ({ button, $modal }) => {
            button.loading = true;
            taskOperationParams.opinionType = 'PASS';
            doOperate({ taskOperationParams, button, buttonName: '通过', $modal });
          },
        },
        {
          text: '否决',
          type: 'primary',
          closable: false,
          disabled: data == 'VETO',
          handler: ({ button, $modal }) => {
            button.loading = true;
            taskOperationParams.opinionType = 'VETO';
            doOperate({ taskOperationParams, button, buttonName: '否决', $modal });
          },
        },
        {
          text: '弃权',
          type: 'primary',
          closable: false,
          disabled: data == 'ABSTAIN',
          handler: ({ button, $modal }) => {
            button.loading = true;
            taskOperationParams.opinionType = 'ABSTAIN';
            doOperate({ taskOperationParams, button, buttonName: '弃权', $modal });
          },
        },
        {
          text: '取消',
        },
      ],
    });
  });
};

/**
 * 流程按钮插槽
 * @returns
 */
export const useWorkflowButtonSlot = (options) => {
  const { buttons = [], buttonClass, params, callback, activeBean, $func, $form } = options;
  //获得展示按钮 应对流程追踪有多个发起按钮
  const getButton = (button) => {
    //如果是发起按钮就处理启动表单式的逻辑
    if (doStartExpression({ button, activeBean, $func })) {
      return null;
    }
    return isEmpty(params.pdid) || params.pdid == button.pdid || button.id == 'taskChangeBtn' ? (
      <Button
        icon={button.icon}
        class="workflow-btn"
        style="color:#2196F3;border-color:#e6e6e6"
        {...bindWorkflowBtnEvent4Promise({ ...options, button })}
      >
        {button.name}
      </Button>
    ) : null;
  };

  return () =>
    params && (buttons.value ?? buttons).length > 0
      ? (buttons.value ?? buttons).map((button) => {
          return button.isEnd ? <span>{button.name}</span> : getButton(button);
        })
      : null;
};

/**
 * 处理启动表达式
 * @param {*} params 参数
 * @returns
 */
const doStartExpression = (params) => {
  let flag = false;
  const { button, activeBean, $func } = params;
  const { id, displayExpressionWhenStarted, displayExpressionWhenStartedFn } = button;
  //是流程启动按钮和发起按钮
  if (['sponsorBtn', 'startBtn'].indexOf(id) != -1) {
    //有启动表达式
    if (isNotEmpty(displayExpressionWhenStarted) && isNotEmpty(activeBean)) {
      flag = !execScript4Return(toTemplate(displayExpressionWhenStarted, activeBean));
    }
    //有启动表单式方法
    if (isNotEmpty(displayExpressionWhenStartedFn)) {
      flag = !execScript(displayExpressionWhenStartedFn, {
        EventOptions: { $func, button, isTrusted: true, type: 'workflow' },
        JE: useJE(),
      });
    }
  }

  return flag;
};

/**
 * 流程按钮点击方法
 * @param {*} param
 * @returns
 */
const bindWorkflowBtnEvent4Promise = ({ params, callback, $func, button, $form }) => {
  const onClick = () => {
    beforeButtonOperation($func).then(() => {
      //触发按钮事件
      doWorkflowBtnEvent({ type: 'beforeClick', button, $func, $form }).then(() => {
        params.$func = $func;
        doWorkflowOperation({
          params,
          button,
          $func,
          callback(...args) {
            callback(...args);
            doWorkflowBtnEvent({ type: 'afterClick', button, $func, $form });
            const { processInfo } = args[0];
            if (isNotEmpty(processInfo)) {
              const { workflowConfig } = processInfo[0];
              //同步刷新列表
              if (workflowConfig && workflowConfig.listSynchronization == 1) {
                $func.getFuncGrid().store.reload();
              }
            }
          },
        });
      });
    });
  };
  return { onClick };
};

/**
 * 处理按钮事件
 * @param {*} params 参数
 * @returns
 */
const doWorkflowBtnEvent = ({ type, button, $func, $form }) => {
  const { pcListeners } = button;
  let flag = true;
  if (isNotEmpty(pcListeners)) {
    const eventData = decode(pcListeners);
    if (isNotEmpty(eventData[type])) {
      flag = execScript(eventData[type], {
        EventOptions: { $func, $form, button, isTrusted: true, type: 'workflow' },
        JE: useJE(),
      });
      // 拦截器前事件
      if (isPromise(flag)) {
        return flag;
      } else {
        return flag !== false ? Promise.resolve() : Promise.reject();
      }
    } else {
      return Promise.resolve();
    }
  } else {
    return Promise.resolve();
  }
};

/**
 * 获取流程信息
 * @param {*} params 参数
 * @param {*} initData 初始数据
 * @returns
 */
export function getWorkflowInfo(params, initData, activeBean) {
  let promise = null;
  if (initData) {
    promise = Promise.resolve(initData);
  } else {
    promise = _getWorkflowInfo(params);
  }
  return promise.then((data) => {
    //如果不是数组转成数组
    if (!isArray(data)) {
      data = [data];
    }
    data.forEach((info) => {
      if (info.workflowConfig) {
        info.formConfig = new FormConfig(info.formConfig);
        info.workflowConfig = new WorkflowConfig(info.workflowConfig);

        const { workflowConfig } = info;
        // 将流程信息配置绑定到按钮上
        info.buttonList?.forEach((button) => {
          button.workflowConfig = workflowConfig;
        });
        // 流程结束标记 todo 后面应该后台吧类型直接传过来做判断
        if (workflowConfig.isEnd) {
          //已作废
          if (isNotEmpty(activeBean) && activeBean.SY_AUDFLAG == 'SUSPEND') {
            info.buttonList.push(TaskCancellationButton());
          } else {
            info.buttonList.push(TaskEndButton());
          }
        }
        // 多任务节点，增加切换任务按钮
        if (data.length > 1) {
          const flag = info.buttonList.some((item) => {
            return item.id == 'taskChangeBtn';
          });
          // 切换任务按钮去重
          if (!flag) {
            info.buttonList.push({
              ...TaskChangeButton(),
              currentNodeId: workflowConfig.currentNode.nodeId,
              taskList: data,
            });
          }
        }
      }

      // 将流程信息配置绑定到按钮上
      info.buttonList?.forEach((button) => {
        button.bgColor = TaskButtonBgColor;
      });
    });
    // 将多任务，转成单任务
    let currentTask = data[0];
    // 该逻辑用于多任务打开历史留痕时把当前表单切换的任务同步到历史留痕的弹窗中
    if (
      isNotEmpty(params.currentNode) &&
      isNotEmpty(params.currentNode.nodeId) &&
      data.length > 1
    ) {
      const oldCurrentTask = data.find(
        (item) => item.workflowConfig.currentNode.nodeId === params.currentNode.nodeId,
      );
      if (oldCurrentTask) {
        currentTask = oldCurrentTask;
      }
    }

    return currentTask;
  });
}

/**
 * 流程按钮操作前处理
 * 用于表单保存数据
 * @param {*} $func
 */
function beforeButtonOperation($func) {
  const deferred = createDeferred();
  // 如果表单保存按钮没有隐藏并且表单有数据修改,先执行表单保存再提交流程
  if ($func) {
    // 表单对象
    const $form = $func.getFuncForm();
    // 表单保存按钮
    const saveBtn = $form?.getButtons('formSaveBtn');
    // 表单是否修改
    validateFrom({ $form })
      .then(() => {
        $func.action.doFormSave().then((options) => {
          saveBtn.loading = false;
          // 错误提示
          if (options?.formError) {
            $func.store.formError = {
              popoverVisible: true,
              errorFields: options.errorFields,
            };
            deferred.reject();
          } else {
            $func.store.formError = {
              popoverVisible: false,
              errorFields: [],
            };
            deferred.resolve();
          }
        });
      })
      .catch(() => {
        // 提交流程
        deferred.resolve();
      });
  } else {
    // 提交流程
    deferred.resolve();
  }
  return deferred.promise;
}
/**
 * 表单是否要进行保存操作
 * @param {*} param0
 * @returns
 */
function validateFrom({ $form }) {
  const deferred = createDeferred();
  // 表单保存按钮
  const saveBtn = $form?.getButtons('formSaveBtn');
  if (saveBtn && !saveBtn.hidden) {
    // 表单是否修改
    const changeData = $form?.getChanges();
    if (isNotEmpty(changeData)) {
      deferred.resolve();
    }
    // 校验表单
    $form
      .validate()
      .then(() => {
        deferred.reject();
      })
      .catch(() => {
        // 表单校验没通过是要进行保存操作的
        deferred.resolve();
      });
  } else {
    deferred.reject();
  }
  return deferred.promise;
}
