import { ref } from 'vue';
import { getWorkflowInfo, useWorkflowButtonSlot } from '../../workflow-util';
export function useWorkflowMan({ props, context }) {
  const buttons = ref([]);
  const wfImage = ref();
  const wfHistory = ref();
  const { params } = props;
  const { emit } = context;
  // 初始化流程信息
  const initWorfkowInfo = (initData) => {
    buttons.value = [];
    return getWorkflowInfo(params, initData, params.activeBean).then((data) => {
      buttons.value = data && data.buttonList;
      //找到图片组件,执行刷新方法
      wfImage.value && wfImage.value.refresh(params);
      //找到历史流程组件,执行刷新方法
      wfHistory.value && wfHistory.value.refresh(params);
      return data;
    });
  };
  // 流程按钮操作事件
  const callback = (data) => {
    initWorfkowInfo(data.processInfo).then(() => {
      emit('operate-done', data);
    });
  };
  // 按钮插槽
  const buttonSlot = useWorkflowButtonSlot({
    buttons,
    buttonClass: 'bgcolor-grey',
    params,
    $func: params.$func,
    activeBean: params.activeBean,
    callback,
  });

  // 初始化数据
  initWorfkowInfo();

  return { buttonSlot, wfImage, wfHistory, buttons };
}
