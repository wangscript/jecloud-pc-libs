import { defineComponent, ref } from 'vue';
import { Toolbar, Panel } from '@jecloud/ui';
import { useWorkflowMan } from './hooks';
import { WorkflowHistory } from '../../workflow-history';
import { WorkflowImage } from '../../workflow-image';

/**
 * 流程追踪
 */
export default defineComponent({
  name: 'WorkflowMain',
  inheritAttrs: false,
  props: {
    params: Object, // 功能参数
  },
  emits: ['operate-done'],
  setup(props, context) {
    const { buttonSlot, wfImage, wfHistory, buttons } = useWorkflowMan({ props, context });
    const $panel = ref();
    const bottomHidden = ref(true);

    const setCollapsed = ({ flag }) => {
      bottomHidden.value = !flag;
    };

    return () => (
      <Panel class="je-workflow-main" ref={$panel}>
        {buttons.value && buttons.value.length > 0 ? (
          <Panel.Item region="tbar" size="50" class="workflow-tbar">
            <Toolbar class="je-workflow-main-tbar">{buttonSlot()}</Toolbar>
          </Panel.Item>
        ) : null}
        <Panel.Item class="workflow-default">
          <WorkflowImage ref={wfImage} params={props.params} />
        </Panel.Item>
        <Panel.Item region="bottom" collapsible split hidden={bottomHidden.value} size="350">
          <WorkflowHistory ref={wfHistory} params={props.params} onCollapsed={setCollapsed} />
        </Panel.Item>
      </Panel>
    );
  },
});
