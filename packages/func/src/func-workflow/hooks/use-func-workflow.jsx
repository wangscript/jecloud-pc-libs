import { watch } from 'vue';
import { WorkflowUtil } from '../src';
import { Button, Tooltip, Dropdown, Menu } from '@jecloud/ui';
import { useWorkflowFormPerm } from './use-workflow-perm';
import { isNotEmpty, isEmpty } from '@jecloud/utils';

/**
 * 功能流程
 * @param {*} param0
 * @returns
 */
export function useFuncWorkflow({ $func, $form }) {
  const { workflowFormPerm } = useWorkflowFormPerm({ $func });
  /**
   * 功能公共参数
   * @returns
   */
  const getCommonParams = () => {
    const funcData = $func.getFuncData();
    const { tableCode, funcCode, productCode, pkCode, funcId } = funcData.info;
    const beanId = $func.store.activeBean[pkCode];
    return beanId
      ? {
          tableCode,
          funcCode,
          funcId,
          beanId,
          prod: productCode,
        }
      : null;
  };
  /**
   * 刷新流程信息
   */
  const refreshWorkflowInfo = (initData, closeFlag) => {
    const params = getCommonParams();
    // 流程启用，初始化流程信息
    let promise = Promise.resolve(null);
    if (params) {
      promise = WorkflowUtil.getWorkflowInfo(params, initData, $func.store.activeBean);
    }
    return promise.then((data) => {
      $func.store.activeWorkflow = data;
      workflowFormPerm();
      //如果有表单弹窗,并且没有提交按钮 就关闭弹窗
      if (data && isNotEmpty($func.$modal) && $func.isFuncForm && closeFlag) {
        let flag = true;
        //按钮为空关闭
        if (isEmpty(data.buttonList)) {
          flag = false;
        } else {
          //没有提交，通过，否决，弃权，领取，直送按钮关闭
          flag = data.buttonList.some(
            (item) =>
              [
                'submitBtn',
                'passBtn',
                'vetoBtn',
                'abstainBtn',
                'receiveBtn',
                'directSendBtn',
              ].indexOf(item.id) != -1,
          );
        }
        if (!flag) {
          $func.$modal.close();
        }
      }
    });
  };

  /**
   * 刷新功能列表表单数据
   */
  const refreshFuncData = ({ bean }) => {
    if (bean) {
      $func.store.setActiveBean(bean);
      const funcGrid = $func.getFuncGrid();
      funcGrid && funcGrid.store.commitRecord(bean);
    }
  };

  /**
   * 监听表单bean，刷新流程信息
   */
  const watchBean4Workflow = () => {
    watch(
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      () => {
        const funcData = $func.getFuncData();
        $func.store.activeWorkflow = null;
        if (funcData.info.enableWorkflow) {
          refreshWorkflowInfo();
        }
      },
    );
  };

  /**
   * 流程按钮插槽
   * @returns
   */
  const workflowButtonSlot = () => {
    const params = getCommonParams();
    const buttonSlot = WorkflowUtil.useWorkflowButtonSlot({
      buttons: $func.store.activeWorkflow?.buttonList,
      params,
      activeBean: $func.store.activeBean,
      $func,
      $form,
      callback({ bean, processInfo }) {
        //刷新列表表单数据
        refreshFuncData({ bean });
        processInfo && refreshWorkflowInfo(processInfo, true);
      },
    });
    return buttonSlot();
  };
  /**
   * 流程追踪插槽
   * @returns
   */
  const workflowMainButtonSlot = () => {
    const params = getCommonParams();
    if (isEmpty(params)) {
      return false;
    }
    params.activeBean = $func.store.activeBean;
    params.$func = $func;
    let buttonList = [];
    let workflowConfig = {};
    if (isNotEmpty($func.store.activeWorkflow)) {
      buttonList = $func.store.activeWorkflow.buttonList || [];
      workflowConfig = $func.store.activeWorkflow.workflowConfig || {};
    }
    //功能有多个流程,并且流程未启动,下拉菜单展示
    if (workflowConfig.status == 'NOSTATUS' && buttonList.length > 1) {
      //根据按钮数据过滤下拉菜单的数据
      const menuDatas = [];
      buttonList.forEach((item) => {
        menuDatas.push({
          title: item.modelName || item.name,
          pdid: item.pdid,
        });
      });
      return isNotEmpty(workflowConfig) ? (
        <Tooltip title="流程追踪">
          <Dropdown
            trigger={['click']}
            v-slots={{
              overlay() {
                return (
                  <Menu
                    onClick={(e) => {
                      params.pdid = e.key;
                      WorkflowUtil.showWorkflowMain({
                        params,
                        callback({ bean, processInfo }) {
                          //刷新列表表单数据
                          refreshFuncData({ bean });
                          refreshWorkflowInfo(processInfo);
                        },
                      });
                    }}
                  >
                    {menuDatas.map((item) => {
                      return <Menu.Item key={item.pdid}>{item.title}</Menu.Item>;
                    })}
                  </Menu>
                );
              },
            }}
          >
            <Button icon="fal fa-sitemap" class="bgcolor-grey" />
          </Dropdown>
        </Tooltip>
      ) : null;
    } else {
      //单个流程按钮展示
      return isNotEmpty(workflowConfig) ? (
        <Tooltip title="流程追踪">
          <Button
            icon="fal fa-sitemap"
            class="bgcolor-grey"
            onClick={() => {
              if (buttonList && buttonList.length > 0) {
                params.pdid = buttonList[0] && buttonList[0].pdid;
              }
              params.currentNode = workflowConfig?.currentNode;
              WorkflowUtil.showWorkflowMain({
                params,
                callback({ bean, processInfo }) {
                  //刷新列表表单数据
                  refreshFuncData({ bean });
                  refreshWorkflowInfo(processInfo);
                },
              });
            }}
          />
        </Tooltip>
      ) : null;
    }
  };

  return { workflowButtonSlot, workflowMainButtonSlot, watchBean4Workflow };
}
