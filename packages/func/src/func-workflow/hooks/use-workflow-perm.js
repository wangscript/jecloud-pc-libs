import { WorkflowUtil } from '../src';
import { isEmpty, isNotEmpty } from '@jecloud/utils';
import { FuncButtonTypeEnum } from '../../func-manager/enum';
/**
 * 流程按钮权限控制
 * @param {*} param0
 * @returns
 */
export function workflowButtonPerm({ $func, buttons }) {
  const funcData = $func.getFuncData();
  const { store } = $func;
  const { enableWorkflow, pkCode } = funcData.info;
  if (enableWorkflow && store.activeBean) {
    // 有流程信息，处理按钮
    if ($func.store.activeWorkflow?.workflowConfig) {
      const { workflowConfig, formConfig } = $func.store.activeWorkflow;
      const { buttonConfig } = formConfig;
      return buttons.filter(
        (button) =>
          $func.isFormBackButton(button.code) ||
          workflowConfig.isUnStart ||
          buttonConfig[button.code]?.enable,
      );
    } else if (store.activeBean[pkCode]) {
      // 有业务主键，流程信息未加载完成时，隐藏按钮
      return buttons.filter((button) => $func.isFormBackButton(button.code));
    } else {
      // 添加操作
      return buttons;
    }
  } else {
    return buttons;
  }
}
/**
 * 流程表单权限控制
 */
export function useWorkflowFormPerm({ $func }) {
  const workflowFormPerm = () => {
    //新建，未启动，恢复表单
    resetFormMetaConfig({ $func }).then(() => {
      doWorkflowFormPerm({ $func });
    });
  };
  return { workflowFormPerm };
}
/**
 * 执行流程表达权限
 * @param {*} param0
 */
export function doWorkflowFormPerm({ $func }) {
  const form = $func.getFuncForm();
  // 流程启动，全部只读，然后根据权限控制
  if (!workflowUnStart({ $func })) {
    const { formConfig } = $func.store.activeWorkflow || {};
    if (!formConfig.formEditable) {
      form.setReadOnly(true);
    }
    let editableFieldNum = 0;
    if (formConfig) {
      // 表单只读
      // 字段权限
      Object.values(formConfig.fieldConfig).forEach((config) => {
        const field = form.getFields(config.code);
        if (field) {
          if (config.display) {
            // 显示
            field.hidden = false;
          } else if (config.hidden) {
            //隐藏
            field.hidden = true;
          }
          if (config.editable) {
            // 编辑
            editableFieldNum++;
            field.readonly = false;
          } else if (config.readonly) {
            // 只读
            field.readonly = true;
          }
          if (config.required) {
            //必填
            field.required = config.required;
          }
        }
      });

      // 按钮权限
      Object.values(formConfig.buttonConfig).forEach((config) => {
        const button = form.getButtons(config.code);
        if (config.enable && button) {
          button.hidden = false;
        }
      });

      //如果表单内一个字段可编辑就有保存按钮
      if (editableFieldNum > 0 || formConfig?.formEditable) {
        const button = form.getButtons('formSaveBtn');
        if (button && button.hidden) {
          button.hidden = false;
        }
      }

      // 子功能权限
      Object.values(formConfig.childFuncConfig).forEach((config) => {
        const child = form.getChildFuncConfig(config.code);
        if (child) {
          if (config.display) {
            // 显示
            child.hidden = false;
          } else if (config.hidden) {
            //隐藏
            child.hidden = true;
          }
          if (config.editable) {
            // 编辑
            child.readonly = false;
          }
        }
      });
    }
  }
}
/**
 * 重置原始配置
 * @param {*} param0
 */
export function resetFormMetaConfig({ $func }) {
  const $form = $func.getFuncForm();
  return $form?.resetMetaConfig();
}

/**
 * 流程未启动状态
 * 用于业务处理校验
 * @param {*} param0
 * @returns
 */
export function workflowUnStart({ $func }) {
  const funcData = $func.getFuncData();
  const { enableWorkflow, pkCode } = funcData.info;
  const { store } = $func;
  const status = store.activeBean?.SY_AUDFLAG ?? store.activeWorkflow?.workflowConfig?.status;
  const isUnStart =
    isEmpty(store.activeBean[pkCode]) ||
    isEmpty(status) ||
    status === WorkflowUtil.StatusEnum.NOSTATUS;
  // 未启用流程 || 未启动
  return !enableWorkflow || isUnStart;
}

/**
 * 判断数据是否可以删除,流程启动的数据不可删除
 * 用于功能列表删除数据
 * @param {*} param0
 */
export function disableRemove4Workflow({ rows, enableWorkflow }) {
  let flag = false;
  if (rows && rows.length > 0 && enableWorkflow) {
    //判断流程是否启动
    flag = rows.some((item) => {
      return isNotEmpty(item.SY_AUDFLAG) && item.SY_AUDFLAG != 'NOSTATUS';
    });
  }
  return flag;
}
