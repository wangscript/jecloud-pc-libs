import { WorkflowHistory } from '../src';
import { watch, ref } from 'vue';

export function useWorkflowHistory($func) {
  // 功能数据
  const { enableWorkflow, pkCode } = $func.getFuncData().info;
  if (!enableWorkflow) {
    return { workflowHistorySlot() {} };
  }
  const wfHistory = ref();
  //功能的数据源
  const { store } = $func;
  //功能主键数据
  const beanId = store.activeBean[pkCode];
  const params = ref({ beanId, refresh: true });
  const initData = (newVal) => {
    const beanId = newVal[pkCode];
    //刷新历史留痕
    let clearType = false;
    if (beanId != params.value.beanId) {
      params.value.beanId = beanId;
      clearType = true;
    }
    wfHistory.value && wfHistory.value.refresh({ beanId, clearType });
  };
  watch(
    () => [store.activeBean, store.activeBeanEmitter],
    () => {
      initData(store.activeBean);
    },
  );
  const workflowHistorySlot = ({ colSpan }) => {
    return (
      <WorkflowHistory
        ref={wfHistory}
        params={params.value}
        colSpan={colSpan}
        onRendered={() => {
          initData(store.activeBean);
        }}
      />
    );
  };
  return {
    workflowHistorySlot,
  };
}
