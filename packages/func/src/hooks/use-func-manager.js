import { Modal } from '@jecloud/ui';
import { useManager, FuncRefEnum, useProvideFunc } from '../func-manager';
import { useWatchActiveBean } from './use-func-active-bean';
import { useFuncWorkflow } from '../func-workflow/hooks';
import { useEventAction } from '../events/use-event-action';
import { watch } from 'vue';

/**
 * 功能管理
 * @param {*} options
 * @returns
 */
export function useFuncManager(options) {
  const { props, context } = options;
  const { expose } = context;
  const $func = useManager(options);

  // 功能表单
  if ($func.isFuncForm) {
    $func.setActiveView(FuncRefEnum.FUNC_EDIT);
  }
  // 监听功能只读
  watch(
    () => props.readonly,
    (readonly) => {
      $func.store.readonly = readonly;
    },
  );
  // 对外暴露功能
  expose($func);
  // 对子暴露功能
  useProvideFunc($func);

  // 绑定eventAction
  useEventAction($func);
  // 绑定窗口对象
  if ($func.isWindow) {
    $func.$modal = Modal.useInjectModal();
  }
  // 监听bean
  useWatchActiveBean({ $func });
  // 流程
  useFuncWorkflow({ $func }).watchBean4Workflow();

  return $func;
}
