import { watch, nextTick } from 'vue';

export function useWatchActiveBean({ $func }) {
  /**
   * 监听主功能的bean，刷新子功能数据
   */
  const watchParentBean = () => {
    $func.parentFunc &&
      watch(
        () => [$func.parentFunc.store.activeBean, $func.parentFunc.store.activeBeanEmitter],
        () => {
          nextTick(() => {
            $func.resetFunc();
          });
        },
      );
  };

  /**
   * 监听bean
   */
  const watchBean = () => {
    watch(
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      () => {
        // 执行表单审核
        $func.action.doFormAuditExecute();
        //判断表单是否有保存按钮,没有表单只读
        if (!$func.hasFormSavePerm()) {
          $func.getFuncForm()?.setReadOnly(true);
        }
      },
    );
  };

  watchParentBean();
  watchBean();
}
