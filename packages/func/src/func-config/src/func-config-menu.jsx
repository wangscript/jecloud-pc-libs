import { defineComponent } from 'vue';
import { Dropdown, Menu, Button } from '@jecloud/ui';
export default defineComponent({
  name: 'JeFuncConfigMenu',
  props: {
    menus: Array,
  },
  slots: ['title'],
  emits: ['click'],
  setup(props, { emit }) {
    const clickMenu = (options) => {
      emit('click', options);
    };
    return () => (
      <Dropdown
        trigger={['click']}
        placement="bottomRight"
        v-slots={{
          overlay: () => (
            <Menu
              onClick={clickMenu}
              class={{ 'je-func-config-menu': true, 'is-empty': props.menus?.length == 0 }}
            >
              {props.menus.map((menu) => {
                return [
                  <div style={{ background: menu.background }} class="title">
                    {menu.text}
                  </div>,
                  <div class="child-container">
                    {menu.children?.map((item) => {
                      return (
                        <Menu.Item
                          class="child-item"
                          key={item.key}
                          icon={item.icon}
                          iconColor={item.color}
                        >
                          {item.text}
                        </Menu.Item>
                      );
                    })}
                  </div>,
                ];
              })}
              <div class="refresh-button">
                <Button
                  icon="jeicon jeicon-refresh"
                  onClick={() => {
                    clickMenu({ key: 'FuncRefresh' });
                  }}
                >
                  刷新
                </Button>
              </div>
            </Menu>
          ),
        }}
      >
        <Button icon="fal fa-cog" class="bgcolor-grey"></Button>
      </Dropdown>
    );
  },
});
