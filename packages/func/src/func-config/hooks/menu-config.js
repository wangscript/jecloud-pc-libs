/*--------------------------------------- 配置菜单 ---------------------------------------*/
export const MenuConfig = [
  {
    text: '功能配置',
    background: 'linear-gradient(103deg,#fce7e7 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      { key: 'FuncMain', text: '功能配置', icon: 'fal fa-cog', color: '#FC5554' },
      { key: 'FuncSimple', text: '简易配置', icon: 'jeicon jeicon-function', color: '#3CCE95' },
      { key: 'FuncButton', text: '按钮配置', icon: 'fal fa-mouse', color: '#FF6381' },
      {
        key: 'FuncChildren',
        text: '子功能项',
        icon: 'jeicon jeicon-subfunction',
        color: '#00BE84',
      },
      { key: 'FuncListeners', text: '功能事件', icon: 'fal fa-bolt', color: '#8BD2FC' },
      {
        key: 'MicroDataPerm',
        text: '数据权限',
        icon: 'jeicon jeicon-seal',
        color: '#7070E0',
      },
      // { key: '', text: '功能打包?', icon: 'fal fa-folder-download', color: '#FF8888' },
      { key: 'FuncHelp', text: '使用说明', icon: 'fal fa-question-circle', color: '#7070E0' },
    ],
  },
  {
    text: '列表配置',
    background: 'linear-gradient(103deg,#e7effc 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      { key: 'FuncListBase', text: '列表配置', icon: 'jeicon jeicon-table', color: '#F88F32' },
      { key: 'FuncListFields', text: '字段配置', icon: 'jeicon jeicon-table', color: '#00BE84' },
      // {
      //   key: 'FuncListCustom',
      //   text: '自定义列表',
      //   icon: 'jeicon jeicon-table',
      //   color: '#7070E0',
      //   complete: true,
      // },
      // { key: 'FuncListPrint', text: '列表打印', icon: 'fal fa-print', color: '#FF6381' },
      {
        key: 'FuncQueryStrategy',
        text: '查询策略',
        icon: 'jeicon jeicon-query-strategy',
        color: '#8BD2FC',
      },
      {
        key: 'FuncQueryAdvanced',
        text: '高级查询',
        icon: 'jeicon jeicon-advanced-search',
        color: '#FB464E',
      },
    ],
  },
  {
    text: '表单配置',
    background: 'linear-gradient(103deg,#fcf8e7 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      { key: 'FuncFormBase', text: '表单配置', icon: 'jeicon jeicon-from', color: '#5091C6' },
      { key: 'FuncFormFields', text: '字段配置', icon: 'jeicon jeicon-from', color: '#DC4FF7' },
      { key: 'FuncFormDesign', text: '界面设计', icon: 'fal fa-tools', color: '#FB464E' },
      // { key: 'FuncFormPrint', text: '表单打印', icon: 'fal fa-print', color: '#7EB31A' },
    ],
  },

  {
    text: '其他配置',
    background: 'linear-gradient(103deg,#e7fcf1 30%, rgba(179,169,169,0.00) 100%)',
    children: [
      {
        key: 'MicroWorkflow',
        text: '创建流程',
        icon: 'jeicon jeicon-treetable',
        color: '#FC5554',
      },
      {
        key: 'FuncSqlBuilder',
        text: '查询拼装器',
        icon: 'fal fa-puzzle-piece',
        color: '#FDD171',
      },
      {
        key: 'MicroTable',
        text: '打开资源表',
        icon: 'fal fa-database',
        color: '#DC4FF7',
      },
      {
        key: 'MicroFunction',
        text: '应用中心',
        icon: 'fal fa-server',
        color: '#8BD2FC',
      },
    ],
  },
];

/**
 * 系统使用到的默认微应用编码
 */
export const MicroApps = Object.freeze({
  JE_CORE_RBAC_DATA: { code: 'JE_CORE_RBAC_DATA', text: '数据权限' },
  JE_CORE_TABLE: { code: 'JE_CORE_TABLE', text: '资源表' },
  JE_CORE_WORKFLOW: { code: 'JE_CORE_WORKFLOW', text: '流程引擎' },
  JE_CORE_FUNCTION_CONFIG: { code: 'JE_CORE_FUNCTION_CONFIG', text: '功能配置' },
});
