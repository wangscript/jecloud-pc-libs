import ConfigMenu from '../src/func-config-menu';
import { getFuncCache, clearFuncCache as _clearFuncCache } from '../../func-util';
import { clearDDItemCache } from '@jecloud/utils';
import { clearFuncStaticizeApi } from '../../func-manager';
import { MenuConfig, MicroApps } from './menu-config';
import { useJE } from '@jecloud/utils';
import { renderSecurityConfigMenu } from '../../func-manager';

/**
 * 功能配置
 * @param {*} param0
 * @returns
 */
export function useFuncConfig({ $func, parent, disabled }) {
  if (disabled || !$func.hasConfigPerm()) {
    return { configMenuSlot() {} };
  }
  let menus = [];
  const admin = useJE().useAdmin();
  // 动态加载配置插件
  if (admin?.hasMicroConfig(MicroApps.JE_CORE_FUNCTION_CONFIG.code)) {
    admin?.loadDynamicApp(MicroApps.JE_CORE_FUNCTION_CONFIG.code);
    menus = MenuConfig;
  }
  // 加载密级菜单
  menus = renderSecurityConfigMenu({ $func, menus });
  /**
   * 功能配置菜单
   * @returns
   */
  const configMenuSlot = () => (
    <ConfigMenu
      onClick={({ key }) => {
        const menu = findConfigMenuByKey(key, menus);
        if (menu?.handler) {
          menu.handler({ $func });
        } else {
          showFuncConfig({ key, parent, $func });
        }
      }}
      menus={menus}
    />
  );
  return { configMenuSlot };
}
/**
 * 查找菜单信息
 * @param {*} key
 * @param {*} menus
 * @returns
 */
function findConfigMenuByKey(key, menus) {
  let menu;
  menus.forEach((subMenu) => {
    const item = subMenu.children.find((item) => item.key === key);
    if (item) {
      menu = item;
    }
  });
  return menu;
}
/**
 * 字段操作
 * @param {*} param0
 * @returns
 */
export function useFuncFieldConfig({ $func }) {
  /**
   * 表单字段双击配置
   * @param {*} param0
   */
  const labelDblclick = (field) => {
    if (!$func.hasConfigPerm()) return;
    showFuncConfig({ key: 'FuncFormField', code: field.name, $func });
  };
  return { labelDblclick };
}
/**
 * 打开表格列的配置
 * @param {*} param0
 * @returns
 */
export function showFuncColumnConfig({ $func, column }) {
  if (['actionColumn'].includes(column.field) || column.type == 'checkbox') {
    return false;
  }
  // 功能权限,子功能集合那边传参children-func
  const configPerm =
    $func.entry == 'children-func' ? $func.parentFunc.hasConfigPerm() : $func.hasConfigPerm();
  if (configPerm) {
    showFuncConfig({
      key: 'FuncListField',
      code: column.field,
      $func,
    });
  }
}

/**
 * 打开功能配置
 * @param {*} param0
 * @returns
 */
function showFuncConfig({ key, code, parent, $func }) {
  const admin = useJE().useAdmin();
  const { funcId, funcCode, funcName, tableCode, tableId, productId, productCode, productName } =
    $func.getFuncData().info;
  // 刷新功能
  if (key === 'FuncRefresh') {
    clearFuncCache({ funcCode, tableCode, parent });
  } else if (admin) {
    switch (key) {
      case 'MicroDataPerm': // 数据权限
        admin.emitMicroEvent(MicroApps.JE_CORE_RBAC_DATA.code, 'data-authority-init', {
          funcCode,
          funcId,
        });
        break;
      case 'MicroWorkflow': // 流程
        admin.emitMicroEvent(MicroApps.JE_CORE_WORKFLOW.code, 'workflow-init', {
          funcData: {
            funcName, //功能名称
            funcCode,
            funcId, //功能Id
            tableCode, //功能对应资源表的code
            SY_PRODUCT_ID: productId, //功能对应产品的id
            SY_PRODUCT_CODE: productCode, //功能对应产品的code
            SY_PRODUCT_NAME: productName, //功能对应产品的名称
          },
        });
        break;
      case 'MicroTable': // 资源表
        admin.openMicroApp(MicroApps.JE_CORE_TABLE.code, {
          query: { productId, id: tableId },
        });
        break;
      case 'MicroFunction': // 应用中心
        admin.openMicroApp('JE_CORE_FUNCTION', {
          query: { productId, id: funcId },
        });
        break;
      default:
        // 打开功能配置
        admin.emitMicroEvent(MicroApps.JE_CORE_FUNCTION_CONFIG.code, 'function-show-config', {
          key,
          funcId,
          code,
        });
        break;
    }
  }
}
/**
 * 清除功能缓存
 * @param {*} param0
 */
function clearFuncCache({ funcCode, tableCode, parent }) {
  const funcData = getFuncCache(funcCode);
  const codes = [funcCode];
  // 清理功能字典缓存
  clearDDItemCache(Array.from(funcData.basic.dictionarys.values()));
  // 清除子功能集合的缓存
  funcData.fields.childFunc.forEach((item) => {
    _clearFuncCache(item.funcCode);
    if (!codes.includes(item.funcCode)) {
      codes.push(item.funcCode);
    }
  });
  // 清除后台缓存
  clearFuncStaticizeApi({ funcCode: codes.join(','), tableCode }).then(() => {
    parent?.reload?.();
  });
}
