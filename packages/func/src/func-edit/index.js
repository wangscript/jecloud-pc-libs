import JeFuncEdit from './src/func-edit';
import { withInstall } from '../utils';

export const FuncEdit = withInstall(JeFuncEdit);
export default FuncEdit;
