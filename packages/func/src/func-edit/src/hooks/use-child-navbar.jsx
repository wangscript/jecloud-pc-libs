import { nextTick, ref, watch } from 'vue';
import { Panel, Toolbar } from '@jecloud/ui';
import { useInjectFunc, useInjectMainFunc } from '../../../hooks';
import { isEmpty, isNumber } from '@jecloud/utils';

export function useChildNavbar({ $edit }) {
  const funcContext = useInjectFunc(); // 功能
  const mainFuncContext = useInjectMainFunc(); // 主功能
  const parentFuncContext = funcContext?.parentFunc; // 父功能
  const methods = {
    /**
     * 启用导航条
     */
    useNavbar: funcContext === mainFuncContext,
    /**
     * 导航内容
     */
    navigation: ref([]),
    /**
     * 前进
     * @param {*}} item
     */
    downNavigation(item) {
      methods.navigation.value.push(item);
    },
    /**
     * 后退
     */
    upNavigation() {
      methods.navigation.value.pop();
    },
    /**
     * 调转
     * @param {*} item
     */
    jumpNavigation(item) {
      if (isNumber(item)) {
        item = methods.navigation.value[item];
      }
      // 获得功能编辑页面
      const editRef = item.funcContext.getRefMaps('edit').value;
      // 切换编辑页面到表单
      editRef?.setActiveItem(editRef?.funcCode);
      // 显示编辑页面tabs
      editRef?.setTabsVisible(true);

      // 更新导航条
      const { navigation } = methods;
      const index = navigation.value.indexOf(item);
      const closeNavs = navigation.value.slice(index + 1);
      nextTick(() => {
        closeNavs.forEach((cn) => {
          cn.funcContext.doBack();
        });
      });
    },
  };
  Object.assign($edit, methods);
  // 初始化导航
  const funcEdit = methods.useNavbar ? methods : mainFuncContext.getRefMaps('edit').value;
  const funcData = funcContext.getFuncData();
  const funcInfo = funcData.info;
  const children = funcContext.getChildFuncConfig();
  const navItem = {
    title: funcInfo.funcName,
    code: funcInfo.funcCode,
    icon: funcInfo.icon,
    funcContext,
  };

  // 更新导航条
  watch(
    () => funcContext.store.activeView,
    (activeItem) => {
      if (isEmpty(children)) {
        activeItem === 'edit' && $edit.setTabsVisible(false);
      } else {
        const $parentEdit = parentFuncContext?.getRefMaps('edit')?.value;
        if (activeItem === 'edit') {
          // 编辑，前进
          funcEdit.downNavigation(navItem);
          // 修改父功能标签显隐
          $parentEdit?.setTabsVisible(false);
        } else if (activeItem === 'data') {
          //列表，后退
          funcEdit.upNavigation();
          // 修改父功能标签显隐
          $parentEdit?.setTabsVisible(true);
        }
      }
    },
    { immediate: true },
  );

  // 导航条插槽
  const childNavbarSlot = () => {
    return methods.useNavbar ? (
      <Panel.Item region="tbar">
        <Toolbar class="je-func-edit-navbar">
          {methods.navigation.value.map((item, index) => {
            return (
              <span
                class="navbar-item"
                key={item.code}
                onClick={() => {
                  methods.jumpNavigation(item);
                }}
              >
                <span class="navbar-item-title">{item.title}</span>
                {methods.navigation.value.length === index + 1 ? null : (
                  <span class="navbar-item-separator">/</span>
                )}
              </span>
            );
          })}
          <Toolbar.Separator />
          <span
            class="navbar-item-back"
            onClick={() => {
              funcContext.doBack();
              methods.jumpNavigation(0);
            }}
          >
            <i class="jeicon jeicon-return-withdraw" />
            返回
          </span>
        </Toolbar>
      </Panel.Item>
    ) : null;
  };

  return { childNavbarSlot, methods };
}
