import { defineComponent, nextTick, onMounted, ref, watch } from 'vue';
import { Form, Fieldset, Panel, Loading } from '@jecloud/ui';
import { pick, omit } from '@jecloud/utils';
import FormItem from './form-item';
import { useFormToolbar } from './hooks/use-form-toolbar';
import { useForm } from './hooks/use-form';
import FuncChildren from '../../func-children';
import { FuncFieldTypeEnum } from '../../func-manager/enum';
import FormAnchor from './form-anchor';
import { useWorkflowHistory } from '../../func-workflow/hooks';
import { FuncRefEnum } from '../../func-manager/enum';
import { renderSecurityFormMark } from '../../func-manager';

export default defineComponent({
  name: 'JeFuncForm',
  inheritAttrs: false,
  props: {
    fields: {
      // 字段配置
      type: Array,
      default() {
        return [];
      },
    },
    cols: { type: Number, default: 3 }, // 列数
    labelCol: {
      // label配置
      type: Object,
      default() {
        return { style: { width: '140px' } };
      },
    },
    wrapperCol: {
      // 字段区域配置
      type: Object,
      default() {
        return { span: 24 };
      },
    },
    loading: Boolean,
    type: {
      type: String,
      default: 'func',
      validator: (value) => ['func', 'query', 'custom'].includes(value),
    },
    funcCode: String,
    model: Object,
    readonly: Boolean,
  },
  emits: ['model-change', 'activate', 'rendered'],
  setup(props, context) {
    const { attrs, slots, emit } = context;
    const $panel = ref();
    const { $form, $func, fields, cols, $plugin, formProps, anchors, formBody } = useForm({
      props,
      context,
    });
    const { tbarSlot } = useFormToolbar({ $form });
    const { workflowHistorySlot } = useWorkflowHistory($func);

    // 级联处理字段
    const cascadeFields = (items = [], parent) => {
      const vnodes = [];
      // 分组框内的字段标签宽度
      let labelCol;
      if (parent?.xtype === FuncFieldTypeEnum.FIELDSET.xtype) {
        if (parent.otherConfig.titleWidth) {
          labelCol = { style: { width: `${parent.otherConfig.titleWidth}px` } };
        }
      }

      items.forEach((field) => {
        if (field.xtype === FuncFieldTypeEnum.FUNC_CHILD.xtype) {
          // 子功能
          vnodes.push(
            <FuncChildren funcCode={props.funcCode} field={field} colSpan={cols.value} />,
          );
        } else if (field.xtype === FuncFieldTypeEnum.WORKFLOW_HISTORY.xtype) {
          // 审批记录
          const node = workflowHistorySlot({ colSpan: cols.value });
          node && vnodes.push(node);
        } else {
          const childNodes = field.children?.length ? cascadeFields(field.children, field) : [];
          vnodes.push(
            <FormItem
              key={field.code}
              field={field}
              model={props.model}
              labelCol={labelCol}
              colSpan={field.colSpan}
              hidden={field.hidden}
              form={$form}
            >
              {childNodes}
            </FormItem>,
          );
        }
      });
      return vnodes;
    };
    const loading = ref(true);
    onMounted(() => {
      nextTick(() => {
        loading.value = false;
        emit('rendered', { $form });
        // 判断表单是否有保存按钮,没有就只读
        if (props.readonly || !$func.hasFormSavePerm()) {
          $form.setReadOnly(true);
        }
        // 功能表单，触发activeBean的首次监听事件
        if ($func) {
          if ($func.isFuncForm) {
            // 初始化表单数据
            $form.reset();
          } else {
            $func.store.activeBeanEmitter++;
          }
        }
      });
    });

    // model监听事件
    watch(
      () => props.model,
      (model) => {
        emit('model-change', { model });
      },
      { deep: true },
    );
    // $func.activeItem监听事件触发表单激活事件
    watch(
      () => $func.store.activeView,
      (val) => {
        if (val == FuncRefEnum.FUNC_EDIT) {
          emit('activate', { $form, $func });
        }
      },
      { immediate: true },
    );
    return () =>
      loading.value ? (
        <Loading loading={loading.value} />
      ) : (
        <Panel
          class={{
            'je-form-func': true,
            'is--loading': props.loading,
          }}
          ref={$panel}
          {...pick(attrs, ['style', 'class'])}
          v-slots={{
            ...pick(slots, Panel.Config.slots),
          }}
        >
          <Panel.Item region="tbar" class="je-form-func-tbar-wrapper">
            {tbarSlot()}
          </Panel.Item>
          <Panel.Item class="je-form-func-body-wrapper">
            <div class="je-form-func-wrapper" style={formProps.mainStyle}>
              <Form
                ref={$plugin}
                {...omit(attrs, ['style', 'class'])}
                {...pick(props, ['labelCol', 'wrapperCol', 'model'])}
                {...omit(formProps, ['wrapperStyle', 'mainStyle'])}
                v-loading={props.loading}
              >
                <Fieldset
                  ref={formBody}
                  simple
                  cols={cols.value}
                  class="je-form-wrapper"
                  style={formProps.wrapperStyle}
                >
                  {slots.default?.() ?? cascadeFields(fields.value)}
                </Fieldset>
                {renderSecurityFormMark({ $func })}
              </Form>
            </div>
            {anchors.value.length ? (
              <FormAnchor target={() => formBody.value.$el} options={anchors.value}></FormAnchor>
            ) : null}
          </Panel.Item>
        </Panel>
      );
  },
});
