import { ref } from 'vue';
import { Form, Modal } from '@jecloud/ui';
import { FuncFieldTypeEnum } from '../../../func-manager/enum';
import { useInjectFunc } from '../../../hooks';
import { useFuncFieldConfig } from '../../../func-config/hooks/use-func-config';
import { useFormField } from './use-func-field';
import { toBoolean } from '@jecloud/utils';

/**
 * 表单项hook
 * @param {*} param0
 * @returns
 */
export function useFuncFormItem({ props, context }) {
  const { slots, attrs } = context;
  // 字段对应组件
  const component = FuncFieldTypeEnum.getComponent(props.field?.xtype);
  // 功能
  const $func = useInjectFunc();
  const funcData = $func.getFuncData();
  // 表单配置
  const { formOptions } = funcData.info;
  // 开发者权限
  const { labelDblclick } = useFuncFieldConfig({ $func });
  // 帮助提示
  const onHelpClick = () => {
    const { help, label } = props.field;
    Modal.window({
      title: `【${label}】帮助提示`,
      width: help.width,
      height: help.height,
      content() {
        return help.html ? <div v-html={help.message} /> : help.message;
      },
    });
  };
  // 表单项
  const $field = ref();
  // 增加字段ref对象
  if (props.field) {
    props.form?.addFields4Ref?.(props.field.name, $field);
  }
  const itemSlot = () => {
    if (component) {
      const { field, model, type, form } = props;
      const item = useFormField({
        field,
        model,
        type,
        slots,
        $form: form,
        $func,
        $field,
        labelDblclick,
      });
      if (field.isFormItem()) {
        const itemSlots = {};
        // label插槽
        if (field.label && !field.labelHidden) {
          itemSlots.label = () => {
            return (
              <>
                {field.help.enable ? (
                  <i class="je-form-func-item-help jeicon jeicon-doubt-s" onClick={onHelpClick}></i>
                ) : null}
                {field.required ? <span class="je-form-func-item-required">*</span> : null}
                <span
                  style={{ color: field.labelColor || formOptions.labelColor }}
                  onDblclick={() => {
                    labelDblclick(field);
                  }}
                >
                  {field.label}
                </span>
              </>
            );
          };
        }
        // 子功能集合
        // 1. hiddenErrors 隐藏错误提示
        // 2. autoLink 取消关联表单域 , 自行处理监听 blur 和 change 事件，来达到自动校验的目的
        const isChildField = [FuncFieldTypeEnum.FUNC_CHILD_FIELD.type].includes(field.xtype);
        return (
          <Form.Item
            name={field.name}
            rules={field.getRules({ model, $form: form, $func, $field })}
            colon={false}
            htmlFor=""
            labelText={field.label}
            labelAlign={field.labelAlign}
            hiddenErrors={isChildField}
            autoLink={!isChildField}
            {...attrs}
            v-slots={itemSlots}
          >
            {item}
          </Form.Item>
        );
      } else {
        return item;
      }
    } else {
      return slots.default?.();
    }
  };

  // 表单项容器
  const itemAttrs = {
    'data-code': props.field?.name,
    'data-func': props.form?.funcCode,
    'data-anchor': props.field?.anchor || undefined,
  };
  // 分组框
  if ([FuncFieldTypeEnum.FIELDSET.xtype].includes(props.field?.xtype)) {
    // 无样式
    const noStyle = toBoolean(props.field.otherConfig.noStyle);
    if (!noStyle) {
      itemAttrs.style = { padding: '10px' };
    }
  }
  return { itemSlot, itemAttrs };
}
