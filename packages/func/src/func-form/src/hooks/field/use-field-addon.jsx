import { isNotEmpty, toBoolean } from '@jecloud/utils';
import { doEvents4Sync } from '../../../../func-manager';
import { FuncFieldTypeEnum } from '../../util';
import { Tooltip } from '@jecloud/ui';

/**
 * 字段前后缀
 * @param {*} param0
 */
export function useFieldAddon({ $func, field }) {
  const addonConfig = { slots: {}, props: {} };
  // 纵向布局,文本域组件使用
  const verticalLayout =
    field.xtype === FuncFieldTypeEnum.TEXTAREA.xtype || field.otherConfig.inputType === 'textarea';
  // 解析插槽
  ['addonAfter', 'addonBefore'].forEach((key) => {
    const addon = field[key];
    if (addon) {
      // 背景色
      let addonBgColor;
      // 节点
      const vnodes = [];
      addon.forEach((item) => {
        const { bgColor } = item;
        // 设置整体背景色，默认取第一个配置
        if (!addonBgColor && bgColor) {
          addonBgColor = bgColor;
        }
        const addonNode = addonItemSlot({ $func, item });
        if (addonNode) {
          vnodes.push(addonNode);
        }
      });
      // 插槽
      Object.assign(addonConfig.slots, {
        [key]: vnodes.length ? () => vnodes : undefined,
      });
      // 属性
      Object.assign(addonConfig.props, {
        [`${key}Class`]: 'je-input-group-addon ' + (verticalLayout ? 'vertical-layout' : ''),
        [`${key}Style`]: { backgroundColor: addonBgColor },
      });
    }
  });

  return addonConfig;
}
/**
 * 后缀项插槽
 * @param {*} param0
 * @returns
 */
function addonItemSlot({ $func, item }) {
  const { text, showWays, textColor, textfold, functionText, icon } = item;
  let items = [];
  switch (showWays) {
    case 'text':
      if (text) {
        items.push(text);
      } else if (icon) {
        items.push(<i class={icon}></i>);
      }
      break;
    case 'icon':
      if (icon) {
        items.push(
          <Tooltip title={text}>
            <i class={icon}></i>
          </Tooltip>,
        );
      } else if (text) {
        items.push(text);
      }
      break;
    case 'icon-text':
      if (icon) {
        items.push(<i class={icon}></i>);
      }
      if (text) {
        items.push(text);
      }
      if (items.length == 2) {
        items.splice(1, 0, ' ');
      }
      break;
  }
  if (items.length) {
    // 样式处理
    const style = {
      color: textColor,
      fontWeight: toBoolean(textfold) ? 'bold' : undefined,
    };
    // 事件处理
    const onClick = () => {
      doEvents4Sync({ $func, code: functionText });
    };
    return (
      <div
        class={{ 'addon-item': true, 'addon-item-link': isNotEmpty(functionText) }}
        style={style}
        onClick={onClick}
      >
        {items}
      </div>
    );
  }
}
