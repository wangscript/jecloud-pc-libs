import { resolveComponent, h } from 'vue';
import { omit, isNotEmpty } from '@jecloud/utils';
import { Grid } from '@jecloud/ui';
import { parseFieldEvents, useModelValue, parseColumnFieldProps } from '../../util';
/**
 * 列表字段
 * @param {*} param0
 */
export function useColumnField({ field, $func, $grid }) {
  let editRender, editSlot;
  const { component, props } = parseColumnFieldProps({ field, $func });

  if (component) {
    editRender = Grid.Renderer[component] ?? {};
    editSlot = (options) => {
      const { row } = options;
      const vnode = resolveComponent(component);
      if (component !== vnode) {
        let parentModel = {};
        if ($func && isNotEmpty($func.parentFunc)) {
          parentModel = $func.parentFunc.store.activeBean || {};
        }
        // 绑定列表字段事件
        Object.assign(props, parseFieldEvents({ field, type: 'column', $grid, $func, ...options }));
        Object.assign(props, useModelValue({ model: row, key: field.name }));
        Object.assign(props, { model: row, parentModel });
        return h(vnode, omit(props, ['height']));
      } else {
        return null;
      }
    };
  }
  return { editRender, editSlot };
}
