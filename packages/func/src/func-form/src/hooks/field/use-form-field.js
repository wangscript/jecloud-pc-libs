import { resolveComponent, h } from 'vue';
import { encode, pick, toBoolean, isNotEmpty } from '@jecloud/utils';
import { parseFieldProps, parseFieldEvents, useModelValue, FuncFieldTypeEnum } from '../../util';
import { useFieldAddon } from './use-field-addon';
import { renderSecurityField } from '../../../../func-manager';
/**
 * 表单字段
 * @param {*} param0
 * @returns
 */
export function useFormField({ field, model, $func, $form, $field, slots, labelDblclick }) {
  let component = FuncFieldTypeEnum.getComponent(field.xtype);
  if (component !== resolveComponent(component)) {
    let props = parseFieldProps({ field, $func });
    props.model = model;
    if (isNotEmpty($func.parentFunc)) {
      props.parentModel = $func.parentFunc.store.activeBean || {};
    }
    switch (field.xtype) {
      case FuncFieldTypeEnum.FIELDSET.xtype: // 分组框
        props = {
          cols: field.cols,
          title: field.label,
          simple: toBoolean(field.otherConfig.noStyle), //无样式
          collapsed: toBoolean(field.otherConfig.keepUp), //保持收起
          ...pick(field.otherConfig, ['bgColor', 'titleColor', 'titleBgColor', 'borderColor']),
          onTitleDblclick() {
            labelDblclick(field);
          },
        };
        break;
      case FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype: // 子功能集合
        const [funcCode, fkCode] = props.configInfo?.split(',') || ['', ''];
        Object.assign(props, {
          funcCode,
          fkCode,
          actionColumn: field.otherConfig,
          field,
          formView: $form,
          maximize: toBoolean(field.otherConfig.enableDataPanel), // 最大化
          // 绑定私有change事件，用于表达式处理
          ...$func.action.bindFormFieldPrivateChange({ field }),
        });
        break;
      case FuncFieldTypeEnum.RADIO_GROUP.xtype: // 单选框
      case FuncFieldTypeEnum.CHECKBOX_GROUP.xtype: // 复选框
        const otherConfig = field.otherConfig;
        const config = {};
        if (otherConfig.layoutStyle == 'gridLayout') {
          // 栅格布局;
          config.wrap = true;
          config.cols = otherConfig.showCloum;
        } else if (otherConfig.layoutStyle == 'singleLayout') {
          // 单行布局
        } else {
          // 流式布局
          config.wrap = true;
        }
        Object.assign(props, config);
        break;
    }
    // 前后缀
    const addonConfig = useFieldAddon({ $func, field });
    Object.assign(props, addonConfig.props);

    // 功能表单，绑定事件
    if ($form?.type === 'func' && ![FuncFieldTypeEnum.FUNC_CHILD.xtype].includes(field.xtype)) {
      Object.assign(props, parseFieldEvents({ field, type: 'form', $func, $form, model }));
    }
    // 绑定组件
    props.ref = $field;
    // vnode配置
    const vnode = {
      component,
      props: { ...props, ...useModelValue({ model, key: field.name }), ...addonConfig.props },
      slots: { ...slots, ...addonConfig.slots },
    };

    // 附件密级控制
    renderSecurityField({ $func, vnode, field });

    return h(resolveComponent(vnode.component), vnode.props, vnode.slots);
  } else {
    return h(resolveComponent('JeDisplay'), { value: encode(field) });
  }
}
