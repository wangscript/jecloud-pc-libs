import { isNotEmpty, split, toNumber, toBoolean, decode } from '@jecloud/utils';
import { FuncFieldTypeEnum } from '../../../func-manager/enum';
import { Modal } from '@jecloud/ui';
import FuncUtil from '../../../func-util';
export { FuncFieldTypeEnum };
/**
 * 解析字段公共有效的配置
 * @param {*} field
 * @returns
 */
export function parseFieldProps({ field, $func }) {
  const props = {};
  ['disabled', 'configInfo', 'placeholder', 'selectExp', 'height', 'width', 'querys'].forEach(
    (key) => {
      if (isNotEmpty(field[key], true)) {
        props[key] = field[key];
      }
    },
  );
  // 功能属性
  Object.assign(props, parseFieldCommonProps({ field, $func }));
  const {
    inputType,
    pkName,
    winHeight,
    winWidth,
    cascadeType,
    optionalAndInput,
    optionalAndQuery,
    penetrate,
    rootId,
  } = field.otherConfig;
  switch (field.xtype) {
    // 选择字段
    case FuncFieldTypeEnum.INPUT_SELECT_GRID.xtype:
    case FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype:
    case FuncFieldTypeEnum.INPUT_SELECT_USER.xtype:
      // 文本域
      props.textarea = inputType === 'textarea';
      // 可选可输入
      props.editable = toBoolean(optionalAndInput);
      // 其他配置项
      props.selectOptions = {
        idProperty: pkName,
        width: winWidth > 0 ? winWidth : undefined,
        height: winHeight > 0 ? winHeight : undefined,
        cascadeType,
        rootId,
      };
      if (isNotEmpty(field.dicValueConfig)) {
        props.dicValueConfig = field.dicValueConfig;
      }

      // 人员选择器特殊处理配置
      if (field.xtype == FuncFieldTypeEnum.INPUT_SELECT_USER.xtype) {
        const { queryType } = field.otherConfig;
        let differenceSet = [];
        const allTab = new Set(['dept', 'role', 'organ', 'common']);
        const hideTab = new Set(queryType?.split(',') || []);
        differenceSet = Array.from(new Set([...allTab].filter((x) => !hideTab.has(x))));
        props.selectOptions['queryType'] = queryType;
        const querysConfig = {};
        ['dept', 'role', 'organ'].forEach((item) => {
          const key = item + 'Query';
          const querys = field.otherConfig[key];
          if (isNotEmpty(querys)) {
            querysConfig[item] = decode(querys);
          }
        });

        props.selectOptions['queryType'] = differenceSet.join(',');
        props.selectOptions['querysConfig'] = querysConfig;
      }
      // 可穿透
      if (field.xtype == FuncFieldTypeEnum.INPUT_SELECT_GRID.xtype && toBoolean(penetrate)) {
        useSelectGridPenetrate({ props });
      }
      break;
    case FuncFieldTypeEnum.UPLOAD.xtype: // 多附件
    case FuncFieldTypeEnum.UPLOAD_INPUT.xtype: // 附件
    case FuncFieldTypeEnum.UPLOAD_IMAGE.xtype: //图片选择器
      Object.assign(props, parseUploadFieldProps({ $func, field }));
      break;
    case FuncFieldTypeEnum.SELECT.xtype: // 下拉框
      // 可选可查询
      props.showSearch = toBoolean(optionalAndQuery);
      // 可选可输入
      props.editable = toBoolean(optionalAndInput);
      if (isNotEmpty(field.dicValueConfig)) {
        props.dicValueConfig = field.dicValueConfig;
      }
      break;
    case FuncFieldTypeEnum.RADIO_GROUP.xtype: // 单选框
    case FuncFieldTypeEnum.CHECKBOX_GROUP.xtype: // 复选框
      if (isNotEmpty(field.dicValueConfig)) {
        props.dicValueConfig = field.dicValueConfig;
      }
      break;
    case FuncFieldTypeEnum.EDITOR_CODE.xtype: // 代码编辑器
      props.readonly = field.readonly;
      props.language = field.otherConfig.language;
      props.hideApi = toBoolean(field.otherConfig.hideApi);
      break;
  }

  return props;
}
/**
 * 解析列表编辑字段
 * @param {*} param0
 */
export function parseColumnFieldProps({ field, $func }) {
  let component = FuncFieldTypeEnum.getComponent(field.xtype);
  let props = parseFieldProps({ field, $func });
  // 禁用列表编辑的组件
  if (!FuncFieldTypeEnum.includesColumnXtype(field.xtype) || props.readonly) {
    component = null;
  } else if (
    // 单选框，复选框转成下拉框
    [FuncFieldTypeEnum.RADIO_GROUP.component, FuncFieldTypeEnum.CHECKBOX_GROUP.component].includes(
      component,
    )
  ) {
    component = FuncFieldTypeEnum.SELECT.component;
  } else if (
    // 所有的选择字段，禁用textarea，统一使用input
    [
      FuncFieldTypeEnum.INPUT_SELECT_GRID.component,
      FuncFieldTypeEnum.INPUT_SELECT_TREE.component,
      FuncFieldTypeEnum.INPUT_SELECT_USER.component,
    ].includes(component)
  ) {
    props.textarea = false;
  }

  // 菜单渲染父节点
  props.getPopupContainer = () => document.body;
  return { props, component };
}
/**
 * 附件属性
 * @param {*} param0
 * @returns
 */
export function parseUploadFieldProps({ $func, field }) {
  const props = {};
  const { fileBucket, tableCode, pkCode, productCode } = $func.getFuncData().info;
  const { fileSize, fileNum, allow, notallow, showButtons } = field.otherConfig;
  props.maxSize = toNumber(fileSize);
  props.includeSuffix = split(allow, ',');
  props.excludeSuffix = split(notallow, ',');
  props.params = { bucket: fileBucket, tableCode, pkCode, productCode };
  // 删除后，更新表单数据
  props.onFileRemove = ({ value, $upload }) => {
    const pkValue = $upload.props.model?.[pkCode];
    if (pkValue) {
      $func.action.doFormUpdateById(pkValue, { [field.name]: value });
    }
  };
  // 多附件
  if (field.xtype == FuncFieldTypeEnum.UPLOAD.xtype) {
    props.menuButtons = isNotEmpty(showButtons) ? showButtons.split(',') : [];
  }
  //图片选择器
  if (field.xtype == FuncFieldTypeEnum.UPLOAD_IMAGE.xtype && fileNum) {
    props.maxCount = toNumber(fileNum);
  }
  return props;
}

/**
 * 公共属性
 * 不受表单，列表，高级查询影响
 * @param {*} param0
 */
export function parseFieldCommonProps({ field }) {
  const props = {};
  switch (field.xtype) {
    case FuncFieldTypeEnum.INPUT_SELECT_COLOR.xtype: // 颜色选择器
      const colors = split(field.configInfo, ',');
      props.colors = colors.length ? colors : undefined;
      break;
    case FuncFieldTypeEnum.DATE_PICKER.xtype: // 日期
    case FuncFieldTypeEnum.TIME_PICKER.xtype: // 时间
      const picker = field.otherConfig.dateType;
      props.format = field.configInfo || undefined;
      if (picker) {
        props.picker = picker;
      }
      break;
    case FuncFieldTypeEnum.INPUT_NUMBER.xtype: // 数值框
      if (isNotEmpty(field.configInfo)) {
        props.precision = toNumber(field.configInfo);
      }
      break;
  }
  return props;
}

/**
 * v-model
 * @param {*} param0
 * @returns
 */
export function useModelValue({ model, key }) {
  return {
    value: model[key],
    'onUpdate:value': function (val) {
      model[key] = val;
    },
  };
}

/**
 * 解析字段事件
 * @param {*} param0
 * @returns
 */
export function parseFieldEvents({ field, type, $func, model, ...options }) {
  const { bindFormFieldEvents } = $func.action;
  return bindFormFieldEvents({ field, type, model, ...options });
}

/**
 * 关联查询-可穿透
 */
function useSelectGridPenetrate({ props }) {
  props.editable = false;
  props.onLinkClick = () => {
    const selectOptions = FuncUtil.parseConfigInfo({ configInfo: props.configInfo });
    const pkName = props.selectOptions?.idProperty;
    // 表单对应存储主键值的字段名
    const formPkName = selectOptions.fieldMaps.sourceToTarget[pkName]?.[0];
    const values = split(props.model?.[formPkName], ',');
    const beanId = values[0];
    if (!pkName || !formPkName) {
      Modal.alert('字段配置项【唯一】为空，请检查！');
    } else if (!beanId) {
      Modal.alert('主键为空，请检查数据！');
    } else {
      FuncUtil.showFuncForm(selectOptions.code, { beanId });
    }
  };
}
