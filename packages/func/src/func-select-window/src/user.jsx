import { ref, h } from 'vue';
import { Modal } from '@jecloud/ui';
import { useUserSelect } from './hooks/use-renderer';
/**
 *
 * @param {Object} options
 * @param {String} configInfo 配置项
 * @param {String|Number} width 宽
 * @param {String|Number} height 高
 */

export function userSelect(options) {
  const { width = 1000, height = 700, title = '人员选择', model } = options;
  const modal = ref();
  const { pluginSlot, modalOptions } = useUserSelect({
    selectOptions: options,
    modal: modal,
    model,
  });

  modal.value = Modal.window({
    title,
    width,
    height,
    minWidth: width,
    resize: false,
    minHeight: height,
    content: () => pluginSlot(),
    ...modalOptions,
    buttonAlign: 'right',
    className: 'je-func-select-window',
  });
  return modal.value;
}
