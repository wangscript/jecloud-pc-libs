import { ref } from 'vue';
import { Modal } from '@jecloud/ui';
import { useTreeSelect } from './hooks/use-renderer';
/**
 *
 * @param {Object} options
 * @param {String} configInfo 配置项
 * @param {String|Number} width 宽
 * @param {String|Number} height 高
 * @param {string} rootId 根节点ID
 */

export function treeSelect(options) {
  const { width = 500, height = 700, title = '树形选择' } = options;
  const modal = ref();

  const { pluginSlot, onCollapseButtonClick, onRefreshButtonClick, modalOptions } = useTreeSelect({
    selectOptions: options,
    modal,
  });

  modal.value = Modal.window({
    title,
    width,
    height,
    maximizable: false,
    minWidth: width,
    minHeight: height,
    content: () => pluginSlot(),
    tools: [
      { icon: 'fal fa-chevron-up', text: '收起', handler: onCollapseButtonClick },
      { icon: 'fal fa-sync', text: '刷新', handler: onRefreshButtonClick },
    ],
    ...modalOptions,
    className: 'je-func-select-window',
  });
  return modal.value;
}
