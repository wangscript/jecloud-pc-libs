import { ref } from 'vue';
import { useFuncSelectEvents } from '../../../events/func-select-events';
import FuncSelect from '../../../func/src/func-select';
import FuncTree from '../../../func-tree';
import JeFuncSelectUser from '../components/user';
import { toQuerysTemplate, isNotEmpty, cloneDeep } from '@jecloud/utils';
export function useFuncSelect({ selectOptions }) {
  const $plugin = ref();
  const pluginSlot = () => {
    return <FuncSelect ref={$plugin} selectOptions={selectOptions}></FuncSelect>;
  };
  // 确定按钮
  const onOkButtonClick = () => {
    $plugin.value?.select();
  };

  // 窗口配置信息
  const modalOptions = useModalOptions({ selectOptions, $plugin, onOkButtonClick });

  return { pluginSlot, modalOptions };
}

export function useTreeSelect({ selectOptions, modal }) {
  const {
    querys,
    orders,
    title,
    product = 'meta',
    store,
    autoLoad,
    cascadeType,
    dicValueConfig,
    model,
    parentModel,
  } = selectOptions;
  const $plugin = ref();
  const $treeSelect = {
    $tree: $plugin,
    getPlugin() {
      return $plugin.value;
    },
    /**
     * 获得字典信息
     * @returns
     */
    getDictionarys({ dicValueConfig, model, parentModel }) {
      // 字典过滤数据
      const customVariables = {};
      if (isNotEmpty(dicValueConfig)) {
        const dicValueConfigData = cloneDeep(dicValueConfig);
        toQuerysTemplate({ querys: dicValueConfigData, model, parentModel });
        dicValueConfigData.forEach((item) => {
          customVariables[item.code] = item.value;
        });
      }
      // 解析rootId配置
      const root = { code: 'rootId', value: selectOptions.rootId };
      toQuerysTemplate({ querys: [root], model, parentModel });
      const rootId = root.value || 'ROOT';

      // 解析后的配置信息
      const { code, async } = $treeSelect.selectConfig;
      return [{ ddCode: code, ddName: title, async, orders, querys, customVariables, rootId }];
    },
  };
  // 事件
  const { selectConfig, onTreeCellDblclick, onTreeRendered } = useFuncSelectEvents({
    type: 'tree',
    $func: $treeSelect,
    modal,
    selectOptions,
  });
  // 选择模式
  const checkboxConfig = { checkStrictly: true }; // 严格模式
  switch (cascadeType) {
    case 'N': //上下都不级联
      checkboxConfig.cascadeParent = false;
      checkboxConfig.cascadeChild = false;
      break;
    case 'D': //选子节点级联选择父节点
      checkboxConfig.cascadeParent = true;
      checkboxConfig.cascadeChild = true;
      break;
    default:
      //选父节点级联选中子节点
      checkboxConfig.cascadeChild = true;
      break;
  }
  const pluginSlot = () => (
    <FuncTree
      ref={$plugin}
      store={store}
      autoLoad={autoLoad}
      multiple={selectConfig.multiple}
      product={product}
      checkboxConfig={checkboxConfig} // 严格模式
      bodyBorder
      dictionarys={$treeSelect.getDictionarys({ dicValueConfig, model, parentModel })}
      onCellDblclick={onTreeCellDblclick}
      onRendered={onTreeRendered}
      onlyItem={true}
    ></FuncTree>
  );
  // 确定按钮
  const onOkButtonClick = () => {
    onTreeCellDblclick();
  };
  // 刷新按钮
  const onRefreshButtonClick = () => {
    $plugin.value.reload();
  };
  // 折叠按钮
  const onCollapseButtonClick = () => {
    $plugin.value.setAllTreeExpand(false);
  };
  // 窗口配置信息
  const modalOptions = useModalOptions({ selectOptions, $plugin, onOkButtonClick });
  return { pluginSlot, onRefreshButtonClick, onCollapseButtonClick, modalOptions };
}

export function useUserSelect({ selectOptions, modal, model }) {
  const $plugin = ref();
  const pluginSlot = () => {
    return (
      <JeFuncSelectUser
        ref={$plugin}
        selectOptions={selectOptions}
        modal={modal}
        model={model}
      ></JeFuncSelectUser>
    );
  };
  // 确定按钮
  const onOkButtonClick = () => {
    $plugin.value?.select();
  };

  // 窗口配置信息
  const modalOptions = useModalOptions({ selectOptions, $plugin, onOkButtonClick });

  return { pluginSlot, modalOptions };
}

/**
 * 格式化窗口配置信息
 * @param {*} param0
 */
export function useModalOptions({ selectOptions, $plugin, onOkButtonClick }) {
  const { showButton = true, buttons } = selectOptions;
  const options = {
    footerStyle: { padding: '10px' },
    bodyStyle: { padding: '0 20px' },
    headerStyle: { height: '60px' },
  };
  // 按钮操作
  if (showButton) {
    if (buttons) {
      // 增加$plugin组件参数，方便调用
      buttons.forEach((button) => {
        if (button.handler) {
          const _handler = button.handler;
          button.handler = (args) => {
            _handler({ ...args, $plugin });
          };
        }
      });
      Object.assign(options, {
        footerStyle: null,
        buttons,
      });
    } else {
      Object.assign(options, {
        footerStyle: null,
        okButton: { closable: false, handler: onOkButtonClick },
        cancelButton: true,
      });
    }
  }
  return options;
}
