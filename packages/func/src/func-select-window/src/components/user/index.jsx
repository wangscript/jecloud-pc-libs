import { defineComponent, ref, onMounted, reactive } from 'vue';
import { Panel } from '@jecloud/ui';
import { toQuerysTemplate, cloneDeep } from '@jecloud/utils';
import JeFuncSelectUserResult from './default-panel';
import JeFuncSelectUserTabs from './left-panel';
import { isNotEmpty, encode } from '@jecloud/utils';
import { parseConfigInfo } from '../../../../func-util';
import { addUserCommonUserApi, getUserPersonInfoEchoApi } from '../../../../func-manager';

export default defineComponent({
  name: 'JeFuncSelectUser',
  inheritAttrs: false,
  props: { selectOptions: Object, modal: Object, model: Object },
  setup(props, context) {
    const { slots, attrs, emit, expose } = context;
    const configInfo = props.selectOptions.configInfo;
    let multipleType = props.selectOptions.multiple ? 'M' : 'S';
    const selectData = ref([]);
    const unCheckDatas = ref([]);
    const config = parseConfigInfo({ type: 'user', configInfo, options: props.selectOptions });
    const showDeveloper = ref(props.selectOptions.showDeveloper ? '1' : '0');
    const hideLeftPanel = ref(false);
    const showLeftTabs = ref(props.selectOptions.queryType);
    // 处理querys
    const querysConfig = ref(props.selectOptions.querysConfig);
    const querysObj = reactive({ dept: [], role: [], organ: [] });
    if (isNotEmpty(querysConfig.value)) {
      ['dept', 'role', 'organ'].forEach((item) => {
        const querys = cloneDeep(querysConfig.value[item]);
        if (isNotEmpty(querys)) {
          toQuerysTemplate({ querys, model: props.model });
          querysObj[item] = querys;
        }
      });
    }

    const initSelectData = () => {
      let params = {};
      //解析配置确定拿什么id做回显
      if (isNotEmpty(config) && isNotEmpty(config.sourceFields)) {
        //根据配置确定各种id的index(下标)
        let indexId, indexUserId, indexDeptId;
        config.sourceFields.forEach((item, index) => {
          if (item == 'id') {
            indexId = index;
          }
          if (item == 'USER_ID') {
            indexUserId = index;
          }
          if (item == 'DEPARTMENT_ID') {
            indexDeptId = index;
          }
        });
        if (isNotEmpty(config.targetFields)) {
          //如果是唯一id
          if (isNotEmpty(indexId)) {
            //从modal获得真实的数据
            const ids = props.selectOptions.model[config.targetFields[indexId]];
            //封装参数数据
            if (isNotEmpty(ids)) {
              params = { type: 'accountDept', jsonArray: [] };
              ids.split(',').forEach((item) => {
                params.jsonArray.push({ accountDeptId: item });
              });
            }
          }
          if (isNotEmpty(indexUserId)) {
            //从modal获得真实的数据
            const userIds = props.selectOptions.model[config.targetFields[indexUserId]];
            //用户id加上部门id
            if (isNotEmpty(userIds)) {
              if (isNotEmpty(indexDeptId)) {
                //从modal获得真实的数据
                const deptIds =
                  props.selectOptions.model[config.targetFields[indexDeptId]].split(',');
                //封装参数数据
                params = { type: 'userDept', jsonArray: [] };
                userIds.split(',').forEach((item, index) => {
                  params.jsonArray.push({ userId: item, deptId: deptIds[index] });
                });
                //用户Id
              } else {
                //封装参数数据
                params = { type: 'user', jsonArray: [] };
                userIds.split(',').forEach((item) => {
                  params.jsonArray.push({ userId: item });
                });
              }
            }
          }
        }
      }
      //调后台接口查询数据并赋值
      if (isNotEmpty(params) && params.jsonArray && params.jsonArray.length > 0) {
        params.jsonArray = encode(params.jsonArray);
        getUserPersonInfoEchoApi({ params }).then((data) => {
          if (isNotEmpty(data)) {
            selectData.value = data;
          }
          hideLeftPanel.value = true;
        });
      } else {
        hideLeftPanel.value = true;
      }
    };
    onMounted(() => {
      initSelectData();
    });

    if (isNotEmpty(configInfo)) {
      const type = configInfo.split(',')[3];
      multipleType = type || multipleType;
    }
    //选中的数据
    const getSelectData = ({ checked, data, closeModal }) => {
      let checkedDatas = [];
      //如果是单选
      if (multipleType == 'S') {
        if (checked) {
          selectData.value = data;
        } else {
          selectData.value = [];
        }
        if (closeModal) {
          select();
        }
        return false;
      }
      //如果是拖拽的数据直接复制
      if (checked == 'drop') {
        checkedDatas = data;
      } else {
        //多选处理
        if (selectData.value && selectData.value.length > 0) {
          checkedDatas = cloneDeep(selectData.value);
        }
        //选中
        if (checked) {
          const newDatas = [];
          data.forEach((item) => {
            const flag = checkedDatas.some((_item) => _item.id == item.id);
            if (!flag) {
              newDatas.push(item);
            }
          });
          checkedDatas = checkedDatas.concat(newDatas);
          //取消选中
        } else {
          const newDatas = [];
          checkedDatas.forEach((item) => {
            const flag = data.some((_item) => _item.id == item.id);
            if (!flag) {
              newDatas.push(item);
            }
          });
          checkedDatas = newDatas;
          unCheckDatas.value = data;
        }
      }
      selectData.value = checkedDatas;
    };

    //删除的用户数据
    const uncheckUser = (unCheckData, checkData) => {
      unCheckDatas.value = unCheckData;
      selectData.value = checkData;
    };

    //保存常用人员
    const addCommonUserData = () => {
      const ids = [];
      if (selectData.value.length > 0) {
        selectData.value.forEach((item) => {
          ids.push(item.id);
        });
        addUserCommonUserApi({ params: { accountDeptIds: ids.join(',') } });
      }
    };

    //点击确定按钮触发方法
    const select = () => {
      if (
        props.selectOptions.callback({
          rows: selectData.value,
          $model: props.modal.value,
          config,
        }) !== false
      ) {
        //保存常用人员
        addCommonUserData();
        //关闭弹窗
        props.modal.value.close();
      }
    };

    expose({ select });

    return () => (
      <Panel class="je-func-select-user">
        <Panel.Item region="left" size="401">
          <JeFuncSelectUserTabs
            multipleType={multipleType}
            onGetSelectData={getSelectData}
            unCheckDatas={unCheckDatas.value}
            hideLeftPanel={hideLeftPanel.value}
            checkedUser={selectData.value}
            showDeveloper={showDeveloper.value}
            showLeftTabs={showLeftTabs.value}
            querysObj={querysObj}
          />
        </Panel.Item>
        <Panel.Item>
          <div style="width:100%;height:100%">
            <JeFuncSelectUserResult
              selectData={selectData}
              onUncheckUser={uncheckUser}
              onGetSelectData={getSelectData}
            />
          </div>
        </Panel.Item>
      </Panel>
    );
  },
});
