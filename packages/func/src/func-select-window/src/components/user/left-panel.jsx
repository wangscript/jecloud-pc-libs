import { defineComponent, ref, watch } from 'vue';
import { Tabs, Loading } from '@jecloud/ui';
import JeFuncSelectUserTree from './left-panel-tree';
import { isNotEmpty } from '@jecloud/utils';

export default defineComponent({
  name: 'JeFuncSelectUserTabs',
  inheritAttrs: false,
  props: {
    multipleType: String,
    unCheckDatas: Object,
    checkedUser: Object,
    showDeveloper: String,
    hideLeftPanel: Boolean,
    showLeftTabs: String,
    querysObj: Object,
  },
  emits: ['getSelectData'],
  setup(props, context) {
    const { slots, attrs, emit, expose } = context;
    const defaultData = [
      { key: 'common', title: '常用', querys: [] },
      { key: 'dept', title: '部门', querys: props.querysObj['dept'] },
      { key: 'role', title: '角色', querys: props.querysObj['role'] },
      { key: 'organ', title: '机构', querys: props.querysObj['organ'] },
    ];
    const activeKey = ref('common');
    const tabsDatas = ref(defaultData);

    // 根据配置处理tab签的数据
    if (isNotEmpty(props.showLeftTabs)) {
      const showTabsCodes = props.showLeftTabs?.split(',');
      if (showTabsCodes && showTabsCodes.length > 0) {
        const newTabsDatas = defaultData.filter((item) => {
          return showTabsCodes.includes(item.key);
        });
        tabsDatas.value = newTabsDatas;
        activeKey.value = newTabsDatas[0].key;
      }
    }

    const changeSelectData = (data) => {
      emit('getSelectData', data);
    };

    const changeTabActiveKey = (key) => {
      if (isNotEmpty(key)) {
        const flag = tabsDatas.value.some((item) => {
          return item.key == key;
        });
        if (flag) {
          activeKey.value = key;
        }
      }
    };

    return () => (
      <Tabs
        class="je-func-select-user-tabs"
        style="height:100%"
        v-model:activeKey={activeKey.value}
      >
        {tabsDatas.value.map((item) => {
          return (
            <Tabs.TabPane tab={item.title} key={item.key} style="height:100%">
              {props.hideLeftPanel ? (
                <JeFuncSelectUserTree
                  activeKey={activeKey.value}
                  panelType={item.key}
                  querys={item.querys}
                  multipleType={props.multipleType}
                  onChangeSelectData={changeSelectData}
                  onChangeTabActiveKey={changeTabActiveKey}
                  unCheckDatas={props.unCheckDatas}
                  checkedUser={props.checkedUser}
                  showDeveloper={props.showDeveloper}
                />
              ) : (
                <Loading loading={props.hideLeftPanel} />
              )}
            </Tabs.TabPane>
          );
        })}
      </Tabs>
    );
  },
});
