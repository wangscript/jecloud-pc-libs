import { treeSelect } from './src/tree';
import { gridSelect } from './src/grid';
import { userSelect } from './src/user';
/**
 * 查询选择
 * @param {Object} options
 * @param {string} options.title 标题
 * @param {string} options.configInfo 配置项
 * @param {Function} options.callback 确定回调函数，return false不关闭窗口
 * @param {boolean} [options.multiple] 多选
 * @param {string} [options.value] 初始值
 * @param {string} [options.valueField] 初始值对应的字段编码，用于初始化时，根据初始值默认选中数据
 * @param {Array} [options.querys] 查询条件
 * @param {Array} [options.orders] 排序条件
 * @param {string|number} [options.width] 宽
 * @param {string|number} [options.height] 高
 * @param {string} [options.type] 选择器类型，grid|tree|user
 * @param {string} [options.product] 产品编码，树形选择器使用
 * @param {string} [options.selectExp] 可选表达式，树形选择器使用
 * @param {boolean} [options.showButton] 显示操作按钮，如果没有，将无法选择，默认显示
 * @param {Array} [options.buttons] 自定义按钮，启用后，默认的确定，取消按钮将禁用
 * @param {boolean} [options.disableDblclick 禁用双击选择，启用后，确定按钮也禁用，可以自定义操作
 * @param {Object} [options.model] 表单数据
 * @returns {$modal} 窗口对象
 */
export const SelectWindow = {
  show(options) {
    // 默认值对应的字段，兼容处理，跟value配合，方便理解
    if (options.valueField) {
      options.idProperty = options.valueField;
      delete options.valueField;
    }
    switch (options.type) {
      case 'tree':
        return treeSelect(options);
      case 'grid':
        return gridSelect(options);
      case 'user':
        return userSelect(options);
    }
  },
  showTree(options) {
    return SelectWindow.show({ ...options, type: 'tree' });
  },
  showGrid(options) {
    return SelectWindow.show({ ...options, type: 'grid' });
  },
  showUser(options) {
    return SelectWindow.show({ ...options, type: 'user' });
  },
};

export default SelectWindow;
