import { reactive, nextTick } from 'vue';
import { Data } from '@jecloud/ui';
import { encode } from '@jecloud/utils';
import { GridQueryParser } from '../../../func-manager/model';
import { FuncRefEnum } from '../../../func-manager/enum';
import { API_COMMON_LOAD, parseCommonUrl, getStatisticsApi } from '../../../func-manager';

export function useGridStore({ $func, querys, orders, pageSize, type }) {
  // 功能数据
  const funcData = $func.getFuncData();
  // 配置
  const {
    tableCode,
    funcCode,
    productCode = 'meta',
    pkCode,
    funcAction,
    gridOptions,
  } = funcData.info;
  const { timeout } = gridOptions;
  const fields = funcData.model;
  // 查询解析器
  const queryParser = new GridQueryParser({ querys, orders, $func, type });
  // JE_CORE_DICTIONARYITEM_ZWF
  const store = Data.Store.useGridStore({
    fields,
    autoLoad: false,
    pageSize: pageSize || funcData.info.pageSize,
    idProperty: pkCode,
    proxy: {
      type: 'ajax',
      timeout: timeout > 0 ? timeout * 1000 : undefined,
      url: parseCommonUrl(API_COMMON_LOAD, funcAction),
      headers: { pd: productCode },
    },
  });
  // 绑定解析器
  store.queryParser = queryParser;
  // 查询前解析查询条件
  store.on('before-load', function ({ options }) {
    options.params = options.params ?? {};
    // 默认参数
    Object.assign(options.params, { tableCode, funcCode, ...queryParser.toJQuery() });
  });
  // 列表统计数据
  store.statistics = reactive({
    single: { data: [], show: false },
    all: { data: [], show: false },
  });
  // 列表统计数据方法
  store.doStatistics = ({ $grid, columns, data }) => {
    const funcData = $func.getFuncData();
    const statisticsType = funcData.info.gridOptions.statisticsType;
    const columnsInfo = funcData.basic.columns;
    const singleStatistics = []; // 单页统计
    // 获得统计列code
    const statisticsColumnFields = funcData.columns.statistics;
    if (statisticsColumnFields.length > 0 && columns.length > 0 && data.length > 0) {
      // 获取到统计列表的下标
      const statisticsColumnObj = {};
      const statisticsColumnsConfig = {};
      columns.forEach((item, index) => {
        if (statisticsColumnFields.indexOf(item.field) != -1) {
          statisticsColumnObj[item.field] = index;
          statisticsColumnsConfig[item.field] = [];
          // 获得统计列的计算过的数据
          // 1.获得统计列的配置信息
          const statisticsConfig = columnsInfo.get(item.field).statistics.config;
          statisticsConfig.forEach((config) => {
            if (config.isShow) {
              statisticsColumnsConfig[item.field].push(config.name);
            }
          });
          // 配置有单页统计
          if (statisticsType.indexOf('single') != -1) {
            // 2.调用方法获得计算后的值
            const val = $grid.getPageStatistics(data, statisticsConfig, item.field);
            // 3.把值放入统计数据中
            singleStatistics[index] = val;
          }
        }
      });
      if (singleStatistics.length > 0) {
        store.statistics.single.data = singleStatistics;
        store.statistics.single.show = true;
      }
      // 总统计处理
      if (!store.statistics.all.show && statisticsType.indexOf('all') != -1) {
        store.statistics.all.show = true;
        getStatisticsApi({
          params: {
            tableCode: funcData.info.tableCode,
            funcCode: funcData.info.funcCode,
            strData: encode(statisticsColumnsConfig),
            j_query: encode(store.queryParser.getQuerys()),
          },
          pd: funcData.info.productCode,
        }).then((data) => {
          const val = $grid.getAllStatistics(
            data,
            statisticsColumnObj,
            statisticsColumnsConfig,
            columnsInfo,
          );
          if (val.length > 0) {
            store.statistics.all.data = val;
          }
          $grid.updateFooter();
        });
      }
      if (statisticsType.indexOf('all') == -1 && store.statistics.all.show) {
        store.statistics.all.show = false;
      }
    } else {
      store.statistics.single.data = [];
      store.statistics.all.data = [];
      store.statistics.all.show = false;
      store.statistics.single.show = false;
    }
    return store.statistics;
  };
  // 刷新统计数据
  store.loadStatistics = () => {
    const $grid = $func.getRefMaps(FuncRefEnum.FUNC_GRID).value;
    store.statistics.all.show = false;
    $grid.updateFooter();
  };
  // 查询前解析查询条件
  store.on('load', function () {
    // 刷新统计数据
    store.loadStatistics();
    // 功能列表刷新后，取消未在本页选中数据，恢复工具条选中状态
    type === 'func' &&
      nextTick(() => {
        $func.getFuncGrid().refreshSelection();
      });
  });
  return store;
}
