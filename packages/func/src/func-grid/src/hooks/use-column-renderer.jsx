import {
  decode,
  downloadFile,
  getDDItemCache,
  toBoolean,
  getDDCache,
  isNotEmpty,
  isString,
  isEmpty,
} from '@jecloud/utils';
import { parseConfigInfo } from '../../../func-util';
import { FuncFieldTypeEnum, FuncDataTypeEnum } from '../../../func-manager/enum';
import { DatePicker, File, Modal, Rate, Upload } from '@jecloud/ui';
import {
  parseColumnFieldProps,
  parseUploadFieldProps,
  useModelValue,
} from '../../../func-form/src/util';
import { reactive, h, resolveComponent } from 'vue';
/**
 * 列renderer
 * @param {Object} options
 * @param {Object} options.column 列配置
 * @param {Object} options.$func 功能对象
 * @param {string} options.type 功能类型
 * @return {Object}
 */
export function useColumnRenderer({ column, $func, type }) {
  const field = $func.getFuncData().getFieldConfig(column.field);
  const defaultSlot = ({ $table, row }) => {
    let text = row[column.field];
    const options = {
      column,
      field,
      row,
      $func,
      $table,
      type,
    };
    text = dictionaryRenderer(text, options);
    text = fileRenderer(text, options);
    text = dateRenderer(text, options);
    text = starRenderer(text, options);
    text = linkRenderer(text, options);
    text = highlightRenderer(text, options);
    return text;
  };
  return defaultSlot;
}
/**
 * 头部renderer
 * @param {*} param0
 * @returns
 */
export function useColumnHeaderRenderer({ column, $func, type }) {
  const field = $func.getFuncData().getFieldConfig(column.field);
  if (!field || type === FuncDataTypeEnum.SELECT) return;

  const headerSlot = () => {
    // 帮助信息
    const helpNode = field.help?.enable ? (
      <i
        class="je-func-grid-header-action jeicon jeicon-doubt-s"
        onClick={() => showColumnHelp({ field })}
      ></i>
    ) : null;
    // 批量编辑
    const batchEditNode =
      column.batchEditable &&
      FuncFieldTypeEnum.includesColumnXtype(field.xtype) &&
      !field.readonly ? (
        <i
          class="je-func-grid-header-action fal fa-pen-line"
          onClick={() => showBatchEditer({ field, column, $func })}
        ></i>
      ) : null;
    return (
      <span style={{ color: column.titleColor }}>
        {helpNode}
        {column.title}
        {batchEditNode}
      </span>
    );
  };
  return headerSlot;
}

/**
 * 字典
 * @param {*} param0
 */
function dictionaryRenderer(text, { column, field, row }) {
  if (!column.dictionary) return text;
  // 字典处理
  const ddItemList = getDDItemCache(column.dictionary);
  const ddCache = getDDCache(column.dictionary);
  // 复选框样式
  if (column.dictionary === 'YESORNO' || column.switchStyle.enable) {
    const checked = toBoolean(text);
    text = (
      <div class="switch-cell switch-layout-checkbox">
        <i
          style="font-size:18px"
          class={{
            'switch-cell-item-checked': checked,
            [column.switchStyle.checkedIcon || 'fas fa-check-square']: checked,
            [column.switchStyle.unCheckedIcon || 'fal fa-square']: !checked,
          }}
        />
      </div>
    );
  } else if (text) {
    if (['LIST', 'TREE'].indexOf(ddCache?.type) != -1 && ddItemList) {
      const codes = text.split(',');
      // 启用字典图标
      const enableIcon = column.ddColor.icon;
      // 启用字典颜色
      const enableColor = column.ddColor.textColor;
      // 启用字典背景颜色
      const enableBgColor = column.ddColor.backgroundColor;
      let color = '';
      let bgColor = '';
      const nodes = [];
      codes.forEach((code) => {
        const item = ddItemList.find((item) => item.code === code || item.text === code);
        let node = code;
        if (item) {
          // 字体颜色值
          enableColor && (color = item.textColor);
          enableBgColor && (bgColor = item.backgroundColor);
          // 图标
          node =
            enableIcon && item.icon ? (
              <i class={['je-column-dictionary-icon', item.icon]}></i>
            ) : (
              item.text
            );
        }
        nodes.push(node, ',');
      });
      // 删除最后的逗号
      nodes.pop();

      // 颜色处理
      text = (
        <div
          style={{
            backgroundColor: bgColor,
            color,
            ...(enableBgColor
              ? {
                  padding: '4px 8px',
                  borderRadius: '6px',
                  lineHeight: 1,
                  display: 'inline-block',
                }
              : {}),
          }}
        >
          {nodes}
        </div>
      );
    } else {
      const config = parseConfigInfo({ configInfo: field.configInfo });
      const sourceToTargetText = config.fieldMaps.sourceToTarget?.text;
      if (isNotEmpty(sourceToTargetText)) {
        text = row[sourceToTargetText[0]];
      }
    }
  }
  return text;
}
/**
 * 附件
 * @param {*} param0
 */
function fileRenderer(text, { column, field, $func, row }) {
  if (
    [
      FuncFieldTypeEnum.UPLOAD_INPUT.xtype,
      FuncFieldTypeEnum.UPLOAD.xtype,
      FuncFieldTypeEnum.UPLOAD_IMAGE.xtype,
    ].includes(field?.xtype)
  ) {
    // 附件
    const files = decode(text) || [];
    // 单附件
    const singleFile = field.xtype === FuncFieldTypeEnum.UPLOAD_INPUT.xtype;
    // 可编辑
    const editable = column.editable && !$func.store.readonly;
    text = (
      <div class="je-column-upload-cell">
        {files.map((item) => {
          return (
            <span class="je-column-upload-cell-content">
              {item.relName}
              <span
                class="je-column-upload-cell-content-span"
                onClick={() => File.previewFile(item)}
              >
                预览
              </span>
              |
              <span
                class="je-column-upload-cell-content-span"
                onClick={() => downloadFile(item.fileKey)}
              >
                下载
              </span>
            </span>
          );
        })}
        {singleFile || !editable ? null : (
          <i
            class="fal fa-edit je-column-upload-cell-edit"
            onClick={() => doFilesEdit({ row, column, field, $func })}
          ></i>
        )}
      </div>
    );
  }
  return text;
}
/**
 * 日期
 * @param {*} param0
 * @returns
 */
function dateRenderer(text, { field }) {
  if (
    [FuncFieldTypeEnum.DATE_PICKER.xtype, FuncFieldTypeEnum.TIME_PICKER.xtype].includes(
      field?.xtype,
    )
  ) {
    // 日期处理
    const format = field.configInfo;
    const picker =
      FuncFieldTypeEnum.TIME_PICKER.xtype === field.xtype ? 'time' : field.otherConfig.dateType;
    text = DatePicker.rendererDateValue({ value: text, picker, format });
  }

  return text;
}
/**
 * 评星
 * @param {*} param0
 * @returns
 */
function starRenderer(text, { field }) {
  if (field?.xtype == FuncFieldTypeEnum.STAR.xtype) {
    text = <Rate v-model:value={text} disabled />;
  }
  return text;
}
/**
 * 超链接
 * @param {*} param0
 * @returns
 */
function linkRenderer(text, { column, type }) {
  // 超链接
  if (column.link.enable && type === FuncDataTypeEnum.FUNC) {
    text = <span class="je-column-link-cell">{text}</span>;
  }
  return text;
}

/**
 * 高亮
 * @param {*} param0
 * @returns
 */
function highlightRenderer(text, { column, $func }) {
  let highlightText = text;
  const keywordFields = $func.getFuncData().querys.func.keyword || [];
  if (keywordFields.includes(column.field) && isNotEmpty(text) && isString(text)) {
    const $grid = $func.getFuncGrid();
    const keywordText = $grid?.getKeywordText();
    if (keywordText) {
      const splitStr = '`__`';
      let vns = [];
      const keys = text
        .toString?.()
        ?.replaceAll(keywordText, splitStr + keywordText + splitStr)
        ?.split(splitStr);
      vns.push(
        keys.map((key) => {
          return key === keywordText ? <span style="color:red">{key}</span> : key;
        }),
      );
      highlightText = vns;
    }
  }
  return highlightText;
}

/**
 * 多附件编辑
 * @param {*} param0
 */
const doFilesEdit = ({ row, field, column, $func }) => {
  const funcData = $func.getFuncData();
  const props = parseUploadFieldProps({ $func, field });
  Upload.showUploadModal({
    ...props,
    title: column.title,
    value: row[column.field],
    type: field.xtype == FuncFieldTypeEnum.UPLOAD_IMAGE.xtype ? 'image' : 'files',
    onFileRemove({ value }) {
      // 删除后，更新代码
      const pkValue = row[funcData.info.pkCode];
      if (pkValue) {
        $func.action.doFormUpdateById(pkValue, { [field.name]: value });
        row[column.field] = value;
      }
    },
  }).then((value) => {
    row[column.field] = value;
  });
};
/**
 * 展示帮助信息
 * @param {*} param0
 */
function showColumnHelp({ field }) {
  const { help, label } = field;
  Modal.window({
    title: `【${label}】帮助提示`,
    width: help.width,
    height: help.height,
    content() {
      return help.html ? <div v-html={help.message} /> : help.message;
    },
  });
}

/**
 * 批量修改
 */
function showBatchEditer({ column, field, $func }) {
  const $grid = $func.getFuncGrid();
  // 1. 检查是否有新增或未保存的数据
  const changes = $grid.store.getChanges(true);
  if (isNotEmpty(changes)) {
    Modal.alert('列表有数据变动，请先保存再进行批量修改！');
    return;
  }

  // 2. 生成数据
  const { component, props } = parseColumnFieldProps({ field, $func });
  const data = { [field.name]: '' };
  // 2.1 处理带值配置
  if (
    [
      FuncFieldTypeEnum.SELECT.component,
      FuncFieldTypeEnum.INPUT_SELECT_GRID.component,
      FuncFieldTypeEnum.INPUT_SELECT_TREE.component,
      FuncFieldTypeEnum.INPUT_SELECT_USER.component,
    ].includes(component)
  ) {
    // 增加查询选择字段带值属性
    const config = parseConfigInfo({ configInfo: field.configInfo });
    config.targetFields.forEach((targetField) => {
      data[targetField] = '';
    });
  }
  // 2.2 生成model数据
  const model = reactive(data);
  let parentModel = {};
  if ($func && isNotEmpty($func.parentFunc)) {
    parentModel = $func.parentFunc.store.activeBean || {};
  }
  Object.assign(props, {
    model,
    parentModel,
    name: field.name,
    style: { width: '100%' },
    ...useModelValue({ model, key: field.name }),
  });

  // 3. 打开编辑窗口
  const state = reactive({
    type: 'query',
    message: '',
  });
  Modal.window({
    title: `【${column.title}】数据修改`,
    width: 450,
    minWidth: 450,
    height: 220,
    minHeight: 220,
    content: () =>
      h('div', { class: '' }, [
        h(resolveComponent(component), props),
        h(resolveComponent(FuncFieldTypeEnum.RADIO_GROUP.component), {
          options: [
            { label: '列表筛选后的全部数据', value: 'query' },
            { label: '列表选中的数据', value: 'select' },
          ],
          value: state.type,
          'onUpdate:value': function (val) {
            state.type = val;
          },
          style: {
            width: '100%',
            'justify-content': 'center',
            paddingTop: '10px',
          },
        }),
        h(
          'div',
          { style: 'color:red;line-height: 18px;height: 18px;text-align:center;' },
          state.message,
        ),
      ]),
    okButton: {
      closable: false,
      handler: ({ $modal }) => {
        if (isEmpty(model[field.name])) {
          state.message = '请输入修改的数据！';
          return;
        }
        state.message = '';
        $func.action
          .doGridBatchModify({
            type: state.type,
            bean: model,
            ids: $grid.getSelectedIds(),
            j_query: $grid.getQueryParser().toJQuery().j_query,
          })
          .then(() => {
            Modal.message('更新成功！');
            $modal.close();
          })
          .catch((error) => {
            state.message = error?.message;
          });
      },
    },
    cancelButton: true,
    buttonAlign: 'center',
  });
}
