import {
  isEmpty,
  omit,
  isNotEmpty,
  toTemplate,
  execScript4Return,
  cloneDeep,
} from '@jecloud/utils';
import { FuncDataTypeEnum } from '../../../func-manager/enum';
import { useColumnField } from '../../../func-form/src/hooks/use-func-field';
import { nextTick, onMounted, watch } from 'vue';
import { useColumnHeaderRenderer, useColumnRenderer } from './use-column-renderer';
export function useColumn({ gridAttrs, $grid, $func }) {
  const { type } = $grid;
  const funcData = $grid.getFuncData();

  // 处理列信息
  const columns = cloneDeep(funcData.getColumns(type));
  parseColumns({ columns, $func, type, $grid });
  const checkboxColumn = { type: 'checkbox', field: '__checkbox__' };
  // 多选列处理
  columns.unshift(checkboxColumn);
  watch(
    () => gridAttrs.multiple,
    (multiple) => {
      $grid[multiple ? 'showColumn' : 'hideColumn'](checkboxColumn.field);
    },
  );
  onMounted(() => {
    nextTick(() => {
      if (!gridAttrs.multiple) {
        $grid.hideColumn(checkboxColumn.field);
      }
    });
  });

  // 处理固定列
  const fixedLeftColumn = columns.find((column) => column.lock === 'before');
  const fixedLeftColumnIndex = columns.indexOf(fixedLeftColumn);
  const fixedRightColumn = columns.find((column) => column.lock === 'after');
  const fixedRightColumnIndex = columns.indexOf(fixedRightColumn);
  columns.forEach((column, index) => {
    if (index <= fixedLeftColumnIndex) {
      column.fixed = 'left';
    } else if (fixedRightColumnIndex != -1 && index >= fixedRightColumnIndex) {
      column.fixed = 'right';
    }
  });

  return columns;
}
const types = ['seq', 'checkbox', 'radio', 'expand', 'html'];
/**
 * 列数据格式化处理
 * @param {*} param0
 */
function parseColumns({ columns, $func, type, $grid }) {
  columns?.forEach((column) => {
    column.slots = column.slots ?? {};
    // 解析事件
    parseEvents({ column, $func, type, $grid });
    // 解析列配置
    parseColumnProps({ column, $func, type, $grid });
    if (column.field && isEmpty(column.children) && !types.includes(column.type)) {
      // 默认插槽
      parseDefaultSlot({ column, $func, type });
      // header插槽
      parseHeaderSlot({ column, $func, type });
      // footer插槽
      parseFooterSlot({ column, $func, type });
      // 编辑插槽
      parseEditSlot({ column, $func, type, $grid });
    }
    parseColumns({ columns: column.children, $func, type });
  });
}

/**
 * 默认展示插槽
 * @param {*} param0
 */
function parseDefaultSlot({ column, $func, type }) {
  if (column.slots.default) return;
  column.slots.default = useColumnRenderer({ column, $func, type });
}
/**
 * 列头插槽解析
 * @param {*} param0
 */
const parseHeaderSlot = ({ column, $func, type }) => {
  column.slots.header = useColumnHeaderRenderer({ column, $func, type });
};
/**
 * 表尾统计插槽
 *
 */
const parseFooterSlot = ({ column, $func }) => {
  // 获得统计列code
  const funcData = $func.getFuncData();
  const statisticsColumnFields = funcData.columns.statistics;
  if (statisticsColumnFields.indexOf(column.field) == -1) return;
  column.slots.footer = (options) => {
    const { items, $columnIndex } = options;
    const fieldData = items[$columnIndex];
    if (isNotEmpty(fieldData)) {
      return (
        <div class="je-footer-statistics-content">
          {fieldData.map((item) => {
            return <span class="je-footer-statistics-content-span">{item}</span>;
          })}
        </div>
      );
    }
  };
};
/**
 * 编辑插槽
 * @param {*} param0
 */
function parseEditSlot({ column, $func, type, $grid }) {
  if (!column.editable || type === FuncDataTypeEnum.SELECT) return;

  const funcData = $func.getFuncData();
  const field = funcData.basic.fields.get(column.field);
  if (!field) return;
  // 选择列处理
  if (column.dictionary && (column.dictionary === 'YESORNO' || column.switchStyle.enable)) {
    column.type = 'switch';
    // 选中，不选中样式
    Object.assign(column, omit(column.switchStyle, ['enable']));
    delete column.slots.default;
  } else {
    const { editRender, editSlot } = useColumnField({ field, column, $func, $grid });
    column.editRender = editRender;
    column.slots.edit = editSlot;
  }
}

/**
 * 解析事件
 * @param {*} param0
 */
function parseEvents({ column, $func, type, $grid }) {
  const { bindGridColumnEvents } = $func.action;
  Object.assign(column, bindGridColumnEvents({ column, $grid }));
  // 列格式化
  if (column.onRenderer) {
    column.slots.default = (options) => {
      return column.onRenderer(options);
    };
  }
}

/**
 * 解析自定义组件列
 * @param {*} param0
 */
function parseColumnProps({ column, $func, type, $grid }) {
  switch (column.type) {
    case 'action':
      const { bindActionButtonEvents } = $func.action;
      const buttons = $func.getActionButtons();
      buttons.forEach((button) => {
        // 处理action按钮的显隐表达式
        button.onHidden = ({ row }) => {
          let flag = true;
          // 显隐表达式
          const visibleExp = button.exps.visible.exp;
          if (isNotEmpty(visibleExp)) {
            flag = execScript4Return(toTemplate(visibleExp, row));
          }
          return !flag;
        };
        Object.assign(button, bindActionButtonEvents({ button, $grid }));
      });
      column.actionConfig = { buttons };
      break;
  }
}
