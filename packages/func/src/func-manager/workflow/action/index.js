import { useOperatorAction } from './operator-action';
export function useWorkflowAction($workflow) {
  const funcs = {
    ...useOperatorAction(),
  };
  const actions = {};
  Object.keys(funcs).forEach((key) => {
    actions[key] = funcs[key].bind(this, $workflow);
  });
  return actions;
}
