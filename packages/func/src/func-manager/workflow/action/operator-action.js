import {
  doButtonOperateApi,
  getCountersignApprovalOpinionApi,
  getSubmitNodeUsersApi,
  getCounterSignerOperationalUsersApi,
  getTaskNodeInfoApi,
  getSubmitNodeInfoApi,
} from '../api';
import {
  Modal,
  createDeferred,
  loadDDItemByCode,
  isEmpty,
  isNotEmpty,
  forEach,
  Data,
  getDDItemInfo,
} from '@jecloud/utils';
import { WorkflowOperatorEnum } from '../enum';
import TaskNode from '../model/task-node';
import TaskAssignee from '../model/task-assignee';

export function useOperatorAction() {
  return {
    initOperationInfo,
    initAssigneeNodes,
    doButtonOperation,
    doButtonSubmit,
    doOperationSetp,
    doChangeUsersSubmit,
    beforeButtonOperation,
    beforeButtonSubmit,
  };
}
/**
 * 初始化按钮操作信息
 */
function initOperationInfo($workflow, { button }) {
  const { operationId } = button;
  const { currentNode } = button.workflowConfig;
  const { operation } = $workflow;
  let promise;
  const params = operation.getOperationParams(); // 操作参数
  switch (operationId) {
    case WorkflowOperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
    case WorkflowOperatorEnum.TASK_GOBACK_OPERATOR: // 退回
    case WorkflowOperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
      // 请求节点信息
      promise = getTaskNodeInfoApi(params).then((data) => {
        return Array.isArray(data) ? data : [data];
      });

      // 直接操作，不需要人员
      operation.state.directOperation = true;
      break;
    case WorkflowOperatorEnum.PROCESS_INVALID_OPERATOR: // 作废
    case WorkflowOperatorEnum.TASK_PASS_OPERATOR: // 通过
    case WorkflowOperatorEnum.TASK_VETO_OPERATOR: // 否决
    case WorkflowOperatorEnum.TASK_ABSTAIN_OPERATOR: // 弃权
      // 使用当前节点
      promise = Promise.resolve([currentNode]);
      // 直接操作，不需要人员
      operation.state.directOperation = true;
      break;
    case WorkflowOperatorEnum.TASK_DELEGATE_OPERATOR: // 委托
    case WorkflowOperatorEnum.TASK_TRANSFER_OPERATOR: // 转办
      // 使用当前节点
      promise = Promise.resolve([currentNode]);
      break;
    default:
      // 正常提交操作
      promise = getSubmitNodeInfoApi(params);
      break;
  }

  return Promise.all([promise, loadDDItemByCode('JE_WF_APPROOPINION')]).then(
    ([nodes, messages]) => {
      //如果节点信息为空
      if (isEmpty(nodes)) {
        return Promise.reject();
      }
      // 处理数据
      operation.state.taskMessages = messages; // 提交常用语
      operation.state.taskNodes = nodes?.map((node) => new TaskNode(node)); // 提交节点
      operation.setActiveNode(operation.state.taskNodes[0]); // 当前激活节点
      // 直接操作，不需要步骤
      if (operation.state.directOperation || operation.state.activeNode.submitDirectly) {
        //在第一步(节点有多个)
        if (operation.state.taskNodes.length > 1) {
          operation.state.activeStep = 1;
        } else {
          //在第最后一步
          operation.state.activeStep = 3;
        }
        operation.state.directOperation = true;
      } else if (operation.state.taskNodes.length === 1) {
        if (operation.state.activeNode.end) {
          operation.state.directOperation = true;
          operation.state.activeStep = 3;
          operation.state.name = '结束';
        } else {
          // 如果只有一个节点，转到下一步
          operation.nextSetp();
        }
      }
      return operation;
    },
  );
}

/**
 * 初始节点人员信息
 * 切换节点后，更新人员信息
 * @param {*} param0
 * @returns
 */
function initAssigneeNodes($workflow) {
  const { operation } = $workflow;
  const params = operation.getOperationParams({ target: operation.state.activeNode.target });
  return getSubmitNodeUsersApi(params)
    .then((data) => operation.setAssigneeNodes(data))
    .catch((e) => {
      Modal.alert(e.message, 'error');
    });
}

/**
 * 操作步骤
 * @param {*} param0
 */
function doOperationSetp($workflow, { next }) {
  const { operation } = $workflow;
  if (next) {
    switch (operation.state.activeStep) {
      // 前往第2步
      case 1:
        operation.nextSetp();
        break;
      // 前往第3步
      case 2:
        const userKeys = Object.keys(operation.state.assignees);
        if (isEmpty(userKeys)) {
          Modal.alert(`请选择人员信息！`, Modal.status.warning);
          return;
        }
        let userEmpty = false;
        forEach(userKeys, (key) => {
          const assignee = operation.state.assignees[key];
          if (isEmpty(assignee.users)) {
            userEmpty = true;
            Modal.alert(`请选择【${assignee.nodeName}】的人员信息！`, Modal.status.warning);
            return false;
          }
        });
        // 如果有空的人员，停止下一步
        !userEmpty && operation.nextSetp();
        break;
      // 提交操作
      case 3:
        if (operation.state.taskComment) {
          doButtonSubmit($workflow);
        } else {
          Modal.alert('请填写审批意见！', Modal.status.warning);
        }
        break;
    }
  } else {
    operation.prevSetp();
  }
}
/**
 * 流程按钮操作前处理
 * 用于表单保存数据
 * @param {*} $workflow
 * @param {*} param1
 */
function beforeButtonOperation($workflow) {
  const { $func } = $workflow;
  const deferred = createDeferred();
  // 如果表单保存按钮没有隐藏并且表单有数据修改,先执行表单保存再提交流程
  if ($func) {
    // 表单对象
    const $form = $func.getFuncForm();
    // 表单保存按钮
    const saveBtn = $form?.getButtons('formSaveBtn');
    // 表单是否修改
    const changeData = $form?.getChanges();
    if ($form && saveBtn && !saveBtn.hidden && isNotEmpty(changeData)) {
      $func.action
        .doFormSave()
        .then(() => {
          saveBtn.loading = false;
          deferred.resolve();
        })
        .catch(deferred.reject);
    } else {
      // 提交流程
      deferred.resolve();
    }
  } else {
    // 提交流程
    deferred.resolve();
  }
  return deferred.promise;
}

/**
 * 按钮操作
 * @returns Promise({success,data})
 * 如果系统默认操作，success为true，如果系统没有操作，需要根据环境判断自行操作
 */
function doButtonOperation($workflow, { button }) {
  const { operation } = $workflow;
  const { operationId } = button;
  operation.init({ button }); // 初始化操作
  const params = operation.getOperationParams(); // 操作参数
  const customParams = { custom: true }; // 自定义操作参数
  const deferred = createDeferred();
  switch (operationId) {
    case WorkflowOperatorEnum.PROCESS_CANCEL_OPERATOR: // 撤销
      Modal.confirm('是否要撤销流程（撤销后将删除审批记录）！', () => {
        doButtonSubmit($workflow).then(deferred.resolve).catch(deferred.reject);
      });
      break;
    case WorkflowOperatorEnum.PROCESS_EMPTY_START_OPERATOR: // 启动
    case WorkflowOperatorEnum.TASK_RETREIEVE_OPERATOR: // 取回
    case WorkflowOperatorEnum.TASK_CLAIM_OPERATOR: // 领取
    case WorkflowOperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR: // 取消委托
    case WorkflowOperatorEnum.TASK_URGE_OPERATOR: // 催办
      doButtonSubmit($workflow).then(deferred.resolve).catch(deferred.reject);
      break;
    case WorkflowOperatorEnum.TASK_REBOOK_OPERATOR: //改签
      doRebookOperation({ params }).then((data) => {
        doButtonSubmit($workflow, data).then(deferred.resolve).catch(deferred.reject);
      });
      break;
    case WorkflowOperatorEnum.TASK_CHANGE_OPERATOR: // 切换任务
      doChangeTaskOperation({ params, button })
        .then((data) => ({ ...data, ...customParams }))
        .then(deferred.resolve)
        .catch(deferred.reject);
      break;
    case WorkflowOperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR: // 人员调整
    case WorkflowOperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR: // 更换负责人
      doChangeUsersOperation({ params, button, operation })
        .then((data) => ({ ...data, ...customParams }))
        .then(deferred.resolve)
        .catch(deferred.reject);
      break;
    default: // 打开审批页面
      deferred.resolve(customParams);
      break;
  }

  return deferred.promise;
}

/**
 * 按钮提交操作
 * @private
 * @param {WorkflowManager} $workflow 流程对象
 * @param {Object} [options] 自定义配置
 * @param {Object} options.button 操作按钮，用于操作成功后的提示
 * @param {Object} options.params 自定义参数，追加到默认参数
 * @returns
 */
function doButtonSubmit($workflow, options = {}) {
  const { operation } = $workflow;
  const { button = operation.button, params = {} } = options;
  const submitParams = operation.getSubmitParams(params);
  return doButtonOperateApi(submitParams)
    .then((data) => {
      afterButtonSubmit($workflow, data);
      Modal.notice(`${button.name || button.text}成功！`, 'success');
      return { data, success: true };
    })
    .catch((e) => {
      Modal.alert(e.message, 'error');
      return Promise.reject(e);
    });
}
/**
 * 校验流程提交
 * TODO:只有审批窗口提交使用
 * @param {*} $workflow
 * @returns
 */
function beforeButtonSubmit($workflow) {
  const { state } = $workflow.operation;
  // 校验审批意见
  if (isEmpty(state.comment)) {
    return Promise.reject({ message: '请输入审批意见！', type: 'comment' });
  }
  // 校验人员
  if (!state.directOperation) {
    const errors = [];
    Object.values(state.assignees).forEach((assignee) => {
      if (isEmpty(assignee.users)) {
        errors.push({ assignee });
      }
    });
    if (errors.length > 0) {
      return Promise.reject({ message: '请选择人员！', type: 'user', errors });
    }
  }
  return Promise.resolve();
}
/**
 * 流程完成后操作
 * @param {*} $workflow
 * @param {*} param1
 */
function afterButtonSubmit($workflow, { bean }) {
  const { $func } = $workflow;
  if (bean) {
    // 更新表单数据
    $func.store.setActiveBean(bean);
  }
}

/**
 * 改签操作
 * @private
 * @param {*} param0
 */
function doRebookOperation({ params }) {
  //查询该会签节点的审批状态
  return getCountersignApprovalOpinionApi({ taskId: params.taskId }).then((data) => {
    const deferred = createDeferred();
    const handler = ({ button }) => {
      deferred.resolve({ button, params: { opinionType: button.code } });
    };
    Modal.dialog({
      content: '是否要进行改签重新投票！',
      title: '改签',
      buttons: [
        {
          text: '通过',
          code: 'PASS',
          type: 'primary',
          disabled: data == 'PASS',
          handler,
        },
        {
          text: '否决',
          type: 'primary',
          code: 'VETO',
          disabled: data == 'VETO',
          handler,
        },
        {
          text: '弃权',
          type: 'primary',
          code: 'ABSTAIN',
          disabled: data == 'ABSTAIN',
          handler,
        },
        {
          text: '取消',
        },
      ],
    });

    return deferred.promise;
  });
}

/**
 * 会签-更换负责人，调整人员
 * @private
 */
function doChangeUsersOperation({ button, params, operation }) {
  const { currentNode } = button.workflowConfig;
  const target = currentNode.id;
  // 获取会签人员信息
  return Promise.all([
    getSubmitNodeUsersApi({ ...params, target }),
    getCounterSignerOperationalUsersApi(params),
  ]).then(([assigneeInfo, resultUser]) => {
    // 会签负责人
    const manager = { id: resultUser.oneBallotUserId, text: resultUser.oneBallotUserName };
    // 会签参与人员
    const operators = resultUser.usersInfo?.map((item) => {
      // 格式化状态信息
      if (item.opinion) {
        const stateDD = getDDItemInfo('JE_WF_OPERATION_TYPE', item.opinion);
        item.opinionColor = stateDD.textColor;
        item.opinionText = stateDD.text;
      }
      return item;
    });
    // 目标节点信息
    const { workflowConfig, users } = assigneeInfo.nodeAssigneeInfo[0];
    const node = new TaskNode({ assigneeNode: true, ...workflowConfig });
    // 更换负责人，设置用户数据
    const isChangeManager =
      button.operationId === WorkflowOperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR;
    if (isChangeManager) {
      node.multiple = false;
      node.users = [
        {
          text: node.name,
          id: node.id,
          children: operators.map((item) =>
            Data.createTreeNodeModel({
              id: item.userId,
              text: item.userName,
              nodeInfoType: 'json',
              bean: { ACCOUNT_AVATAR: item.userAvatar },
            }),
          ),
        },
      ];
    } else {
      // 调整人员，使用默认人员
      node.setUsers(users);
    }
    // 常用联系人
    operation.state.commonUsers = assigneeInfo.commonUserInfo || [];
    return { manager, operators, node, isChangeManager };
  });
}

/**
 * 会签-更换负责人，调整人员 提交操作
 * @param {*} $workflow
 * @param {*} param1
 */
function doChangeUsersSubmit($workflow, { users, operators, manager, isChangeManager }) {
  const { operation } = $workflow;
  let assignees;
  if (isChangeManager) {
    // 更换负责人
    assignees = users;
  } else {
    // 人员调整

    // 已操作人员
    assignees = operators
      .filter((item) => item.opinion)
      .map((item) => ({ id: item.userId, text: item.userName }));
    // 负责人
    if (manager.id) {
      assignees.push(manager);
    }
    // 调整人员
    const assigneeIds = assignees.map(({ id }) => id);
    // 排除已审批人员和负责人
    assignees.push(...users.filter((user) => !assigneeIds.includes(user.id)));
  }
  // 送交信息
  const taskAssignee = new TaskAssignee({ ...operation.state.activeNode, users: assignees });

  // 送交
  return doButtonSubmit($workflow, {
    params: {
      assignee: TaskAssignee.parseAssigneeParam([taskAssignee]),
    },
  });
}

/**
 * 任务切换
 * @private
 */
function doChangeTaskOperation({ button }) {
  const currentTaskId = button.currentNodeId;
  const tasks = button.taskList;
  return Promise.resolve({ currentTaskId, tasks });
}
