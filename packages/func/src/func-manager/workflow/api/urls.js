/**
 * 加载表单流程按钮和字段控制
 */
export const API_WORKFLOW_GET_INFO = '/je/workflow/processInfo/getInfo';

/**
 * 获取提交的节点信息
 */
export const API_WORKFLOW_GET_SUBMIT_NODE_INFO = '/je/workflow/processInfo/getSubmitOutGoingNode';
/**
 * 获取节点的人员信息
 */
export const API_WORKFLOW_GET_SUBMIT_NODE_USERS =
  '/je/workflow/processInfo/getSubmitOutGoingNodeAssignee';

/**
 * 获取流程按钮操作参数详情
 */
export const API_WORKFLOW_BUTTON_GET_PARAMS = '/je/workflow/button/getParams';
/**
 * 执行流程按钮操作
 */
export const API_WORKFLOW_BUTTON_OPERATE = '/je/workflow/button/operate';
/**
 * 获取退回节点名称
 */
export const API_WORKFLOW_GET_GOBACK_NODE = '/je/workflow/processInfo/getGobackNode';
/**
 * 获取取回节点名称
 */
export const API_WORKFLOW_GET_RETRIEVE_NODE = '/je/workflow/processInfo/getRetrieveNode';
/**
 * 获取委托节点信息
 */
export const API_WORKFLOW_GET_DELEGATION_NODE = '/je/workflow/processInfo/getDelegationNode';
/**
 * 获取驳回信息
 */
export const API_WORKFLOW_GET_REJECT_NODE = '/je/workflow/processInfo/getDismissOutGoingNode';
/**
 * 获取直送节点名称
 */
export const API_WORKFLOW_GET_DIRECT_DELIVERY_NODE =
  '/je/workflow/processInfo/getDirectDeliveryNode';

/**
 * 获取流程传阅人
 */
export const API_WORKFLOW_GET_PASSROUND_USERS = '/je/workflow/processInfo/getPassroundUsers';

/**
 * 获取会签操作人
 */
export const API_WORKFLOW_GET_COUNTER_SIGNER_OPERATIONAL_USERS =
  '/je/workflow/processInfo/getCounterSignerOperationalUsers';

/**
 * 获得流程历史
 */
export const API_WORKFLOW_GET_CIRCULATION_HISTORY =
  '/je/workflow/currentUserTask/getCirculationHistory';

/**
 * 获得流程图
 */
export const API_WORKFLOW_PREVIEW_IMAGE = '/je/workflow/processInfo/preview';

/**
 * 获得会签节点的信息
 */
export const API_WORKFLOW_GET_COUNTERSIGN_APPROVAL_OPINION =
  '/je/workflow/processInfo/getCountersignApprovalOpinion';
