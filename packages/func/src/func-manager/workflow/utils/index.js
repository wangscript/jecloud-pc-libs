import { isNotEmpty } from '@jecloud/utils';
/**
 * 判断数据是否可以删除,流程启动的数据不可删除
 * 用于功能列表删除数据
 * @param {*} param0
 */
export function disableRemove4Workflow({ rows, enableWorkflow }) {
  let flag = false;
  if (rows && rows.length > 0 && enableWorkflow) {
    //判断流程是否启动
    flag = rows.some((item) => {
      return isNotEmpty(item.SY_AUDFLAG) && item.SY_AUDFLAG != 'NOSTATUS';
    });
  }
  return flag;
}
