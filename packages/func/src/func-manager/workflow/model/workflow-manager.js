import { isNotEmpty, toDDItemText, getFileBaseURL } from '@jecloud/utils';
import { bindEvents4Promise } from '../../util';
import { getWorkflowInfoApi, loadWorkflowHistoryApi } from '../api';
import { API_WORKFLOW_PREVIEW_IMAGE } from '../api/urls';
import WorkflowConfig from './workflow-config';
import FormConfig from './workflow-form-config';
import { reactive } from 'vue';
import {
  TaskCancellationButton,
  TaskEndButton,
  TaskChangeButton,
  TaskButtonBgColor,
} from './task-button';
import { WorkflowStatusEnum } from '../enum';
import { useWorkflowAction } from '../action';
import WorkflowHistory from './workflow-history';
import TaskOperation from './task-operation';
export default class WorkflowManager {
  constructor(options) {
    this.$func = options.$func;
    this.action = useWorkflowAction(this);
    this.operation = new TaskOperation(this);
  }
  /**
   * 响应数据
   *
   * @memberof WorkflowManager
   */
  state = reactive({
    currentTask: null, // 当前任务
    currentTaskNode: null, // 当前任务节点信息
    buttons: [], // 操作按钮
    tasks: [], // 任务列表，多任务节点会有
  });

  /**
   * 业务数据主键
   * @returns
   */
  getBeanId() {
    return this.$func.store.getBeanId();
  }
  /**
   * 流程pdid
   * @returns
   */
  getPdid() {
    return this.$func.store.activeBean?.SY_PDID || '';
  }
  /**
   * 流程piid
   * @returns
   */
  getPiid() {
    return this.$func.store.activeBean?.SY_PIID || '';
  }

  /**
   * 当前流程状态
   * @returns
   */
  getAudFlag() {
    return (
      this.$func.store.activeBean?.SY_AUDFLAG ||
      this.state.currentTask?.workflowConfig.status ||
      WorkflowStatusEnum.NOSTATUS
    );
  }

  /**
   * 当前流程状态文字
   * @param {*} audFlag
   * @returns
   */
  getAudFlagText(audFlag) {
    return toDDItemText('JE_AUDFLAG', audFlag || this.getAudFlag());
  }

  /**
   * 是否启用流程
   * @returns
   */
  isEnable() {
    const funcData = this.$func.getFuncData();
    return funcData.info.enableWorkflow;
  }

  /**
   * 是否可以开始审批
   */
  isReady() {
    const beanId = this.getBeanId();
    // 启用流程，有主键，标识可以开始审批操作了
    return this.isEnable() && isNotEmpty(beanId);
  }

  /**
   * 判断流程是否启动
   * @returns
   */
  isStart() {
    const status = this.getAudFlag();
    // 启用流程，有主键，启动流程状态
    return this.isReady() && status !== WorkflowStatusEnum.NOSTATUS;
  }
  /**
   * 判断流程是否结束
   */
  isEnd() {
    const status = this.getAudFlag();
    return status === WorkflowStatusEnum.ENDED;
  }

  /**
   * 流程信息
   */
  refresh() {
    const funcData = this.$func.getFuncData();
    const { tableCode, funcCode, productCode, funcId } = funcData.info;
    const beanId = this.getBeanId();
    const params = {
      tableCode,
      funcCode,
      funcId,
      beanId,
      prod: productCode,
    };
    // 有业务数据，再进行流程数据处理
    return (beanId ? getWorkflowInfoApi(params) : Promise.resolve([])).then((data) => {
      // 处理流程数据
      const tasks = (Array.isArray(data) ? data : [data]).map((info) => {
        if (info.workflowConfig) {
          // 表单配置
          info.formConfig = new FormConfig(info.formConfig);
          // 流程配置
          info.workflowConfig = new WorkflowConfig(info.workflowConfig);
          // 流程按钮
          if (info.workflowConfig.isEnd) {
            // 流程结束
            // 添加结束按钮，如果是作废，换成作废按钮。TODO: 后面应该后台吧类型直接传过来做判断
            info.buttonList = [
              this.getAudFlag() === 'SUSPEND' ? TaskCancellationButton() : TaskEndButton(),
            ];
          } else if (data.length > 1) {
            // 多任务节点
            // 过滤以有的按钮
            const buttons = info.buttonList.filter((btn) => btn.id !== 'taskChangeBtn');
            //  增加切换任务按钮
            buttons.push(TaskChangeButton());
            info.buttonList = buttons;
          }
        }

        // 将流程信息配置绑定到按钮上
        info.buttonList?.forEach((button) => {
          Object.assign(button, {
            workflowConfig: info.workflowConfig,
            currentNodeId: info.workflowConfig?.currentNode.id,
            taskList: data,
            bgColor: TaskButtonBgColor,
          });
        });
        return info;
      });
      // 设置当前任务
      this.changeTask(tasks[0]);
      this.state.tasks = tasks;
      return this.state.currentTask;
    });
  }

  /**
   * 刷新表单状态
   */
  refreshForm() {
    if (!$form) return;
    const $form = this.$func.getFuncForm();
    // 1. 重置表单状态
    $form.resetMetaConfig();
    // 2. 流程启动，全部只读，然后根据权限控制
    const formConfig = this.state.currentTask?.formConfig;
    if (formConfig && this.isStart()) {
      // 字段编辑权限
      let fieldEditable = false;

      // 表单只读
      if (!formConfig.formEditable) {
        $form.setReadOnly(true);
      }

      // 字段权限
      Object.values(formConfig.fieldConfig).forEach((config) => {
        const field = $form.getFields(config.code);
        if (field) {
          if (config.display) {
            // 显示
            field.hidden = false;
          } else if (config.hidden) {
            //隐藏
            field.hidden = true;
          }
          if (config.editable) {
            // 编辑
            field.readonly = false;
            fieldEditable = true;
          } else if (config.readonly) {
            // 只读
            field.readonly = true;
          }
          if (config.required) {
            //必填
            field.required = config.required;
          }
        }
      });

      // 按钮权限
      Object.values(formConfig.buttonConfig).forEach((config) => {
        const button = $form.getButtons(config.code);
        if (config.enable && button) {
          button.hidden = false;
        }
      });

      //如果表单内一个字段可编辑就有保存按钮
      if (fieldEditable > 0 || formConfig?.formEditable) {
        const button = $form.getButtons('formSaveBtn');
        if (button && button.hidden) {
          button.hidden = false;
        }
      }

      // 子功能权限
      Object.values(formConfig.childFuncConfig).forEach((config) => {
        const child = $form.getChildFuncConfig(config.code);
        if (child) {
          if (config.display) {
            // 显示
            child.hidden = false;
          } else if (config.hidden) {
            //隐藏
            child.hidden = true;
          }
          if (config.editable) {
            // 编辑
            child.readonly = false;
          }
        }
      });
    }
  }

  /**
   * 操作按钮
   */
  getOperationButtons() {
    return this.state.buttons;
  }

  /**
   * 切换任务
   * @param {*} task
   */
  changeTask(task) {
    this.state.currentTask = task;
    this.state.currentTaskNode = task?.workflowConfig.currentNode;
    this.state.buttons = task?.buttonList || [];
  }

  /**
   * 加载流程历史
   */
  loadHistory() {
    return loadWorkflowHistoryApi({ beanId: this.getBeanId() }).then((data) =>
      data.map((item) => new WorkflowHistory(item)),
    );
  }
  /**
   * 流程图片地址
   */
  getPreviewImageUrl(pdid) {
    const beanId = this.getBeanId();
    pdid = pdid || this.getPdid();
    return `${getFileBaseURL()}${API_WORKFLOW_PREVIEW_IMAGE}?beanId=${beanId}&pdid=${pdid}&_t=${new Date().getTime()}`;
  }
  /**
   * 允许删除流程业务数据
   * @param {*} records
   * @returns
   */
  enableRemoveRecords(records = []) {
    //判断流程是否启动
    return (
      this.isEnable() &&
      records.some((item) => {
        return isNotEmpty(item.SY_AUDFLAG) && item.SY_AUDFLAG != 'NOSTATUS';
      })
    );
  }

  /**
   * 处理启动表达式
   * @param {*} params 参数
   * @returns
   */
  doStartExpression(button) {
    const { id, visibleExps } = button;
    //是流程启动按钮和发起按钮
    if (['sponsorBtn', 'startBtn'].includes(id) && visibleExps) {
      return this.$func.action.doFormFieldExp4Sync({
        $func: this.$func,
        model: this.$func.store.activeBean,
        exp: visibleExps,
        options: {
          button,
          isTrusted: true,
          type: 'workflow',
        },
      });
    } else {
      return true;
    }
  }

  /**
   * 绑定按钮事件
   * @param {*} options
   * @returns
   */
  bindButtonEvents(options) {
    const { button } = options;
    const defaultEvents = {
      click(eventOptions = {}) {
        return this.doOperation(eventOptions);
      },
    };
    return bindEvents4Promise({ $func: this.$func, events: button.events, defaultEvents, options });
  }
}
