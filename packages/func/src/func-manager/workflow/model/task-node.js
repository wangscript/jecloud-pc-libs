import { toBoolean, Data } from '@jecloud/utils';

/**
 * 任务节点
 */
export default class TaskNode {
  constructor(options) {
    this.id = options.id || options.nodeId || options.currentNodeId;
    this.name = options.name || options.nodeName || options.currentNodeName;
    this.target = options.target || options.currentTarget || options.nodeId; // 目标节点ID
    this.endTask = options.endTask; // 结束
    this.type = options.type; // 节点类型
    this.asynTree = toBoolean(options.asynTree);
    this.selectAll = toBoolean(options.selectAll); // 人员全选
    this.multiple = toBoolean(options.multiple); // 人员多选
    this.showSequentialConfig = toBoolean(options.showSequentialConfig); // 展示 并序，顺序配置
    this.sequential = options.sequential; // 并序，顺序
    this.simpleApproval = toBoolean(options.simpleApproval); // 简易审批
    this.submitDirectly = toBoolean(options.submitDirectly); // 直接提交
    this.personnelAdjustments = toBoolean(options.personnelAdjustments); // 人员选择器是否只读
    this.isJump = toBoolean(options.isJump) ? '1' : '0'; // 是否可跳跃

    // 固定人,随机节点默认全部选中
    if (['to_assignee', 'random'].includes(this.type)) {
      this.selectAll = true; // 人员全选
      this.multiple = true; // 人员多选
      this.personnelAdjustments = false; // 禁止人员调整
    }

    this.end = this.type === 'end'; // 结束节点
    this.assigneeNode = toBoolean(options.assigneeNode); // 目标节点
    this.users = []; // 目标节点对应的人员信息
    this.assigneeNodes = []; // 目标节点信息
  }
  /**
   * 设置目标节点
   * @param {*} nodes
   */
  setAssigneeNodes(nodes = []) {
    this.assigneeNodes = [];
    nodes.forEach((item) => {
      const node = new TaskNode({ assigneeNode: true, ...item.workflowConfig });
      // 设置目标节点可选用户
      node.setUsers(item.users);
      this.assigneeNodes.push(node);
    });
  }
  /**
   * 设置人员信息
   * @param {*} users
   */
  setUsers(users = []) {
    users.forEach((user) => {
      this.users.push({
        id: user.assignmentConfigType,
        name: user.assignmentConfigTypeName,
        children: user.user.children || [],
      });
    });
  }

  /**
   * 获取节点默认选中的人员数据
   * @returns
   */
  getDefaultSelectUsers() {
    // 只有多选和默认选中全部，才会获取数据
    if (this.selectAll && this.multiple && this.assigneeNode) {
      const root = Data.createTreeNodeModel({ children: this.users });
      const nodes = [];
      root.cascade((node) => {
        if (node.nodeInfoType === 'json') {
          nodes.push(node);
        }
      });
      return nodes;
    } else {
      return [];
    }
  }
}
