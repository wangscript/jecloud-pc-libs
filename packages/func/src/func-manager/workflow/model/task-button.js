import { WorkflowOperatorEnum } from '../enum';
import { decode, kebabCase } from '@jecloud/utils';

/**
 * 任务操作按钮
 */
export default class TaskButton {
  constructor(options) {
    this.code = options.code;
    this.id = options.id;
    this.name = options.name;
    this.operationId = options.operationId;
    this.pdid = options.pdid;
    this.piid = options.piid;
    this.taskId = options.taskId;
    this.workflowConfig = options.workflowConfig;
    this.events = parseEvents(options.pcListeners);
    // 显隐表达式
    if (options.displayExpressionWhenStarted || options.displayExpressionWhenStartedFn) {
      this.visibleExps = {
        exp: options.displayExpressionWhenStarted,
        fn: options.displayExpressionWhenStartedFn,
      };
    }
  }
}
/**
 * 解析事件
 * @param {*} data
 * @returns
 */
function parseEvents(data) {
  const eventData = data ? decode(data) : {};
  const events = {};
  Object.keys(eventData).forEach((key) => {
    events[kebabCase(key)] = eventData[key];
  });
  return events;
}

/**
 * 切换任务按钮，前端使用
 */
export const TaskChangeButton = () => ({
  id: 'taskChangeBtn',
  code: 'taskChangeBtn',
  name: '切换任务',
  icon: 'fal fa-exchange',
  operationId: WorkflowOperatorEnum.TASK_CHANGE_OPERATOR,
});

/**
 * 流程结束按钮，前端使用
 */
export const TaskEndButton = () => ({
  id: 'taskEndBtn',
  code: 'taskEndBtn',
  name: '流程已结束',
  isEnd: true,
});

/**
 * 流程作废按钮，前端使用
 * @returns
 */
export const TaskCancellationButton = () => ({
  id: 'taskCancellationBtn',
  code: 'taskCancellationBtn',
  name: '流程已作废',
  isEnd: true,
});

/**
 * 背景色
 */
export const TaskButtonBgColor = '#edf0f1';
