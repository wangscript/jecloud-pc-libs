import TaskCommonParams from './task-common-params';
import { pick } from '@jecloud/utils';
import { WorkflowOperatorEnum } from '../enum';
import { reactive } from 'vue';
import TaskAssignee from './task-assignee';
/**
 * 基础状态数据
 */
const baseState = {
  activeStep: 1, // 激活的流程步骤
  activeNode: null, // 当前激活节点
  directOperation: false, // 直接操作，不需要步骤操作
  commonUsers: [], // 常用人员
  taskMessages: [], // 审批意见
  taskNodes: [], // 任务节点
  assignees: {}, // 送交人员信息
  comment: '同意', // 流程提交信息
  name: '', // 操作按钮名称
};
/**
 * 流程审批操作
 */
export default class TaskOperation {
  /**
   * 流程对象
   */
  $workflow = null;
  /**
   * 操作按钮
   */
  button = null;
  /**
   * 状态数据
   */
  state = reactive({ ...baseState });
  /**
   * 参数
   */
  params = {};
  /**
   * 操作参数的keys
   */
  operationParamKeys = {
    assignee: 'assignee', // 提交人
    comment: 'comment', // 审批意见
    target: 'target', // 目标id
    sequentials: 'sequentials', // 多人审批，0 并序执行，1 顺序执行
    isJump: 'isJump', // 是否可跳跃, 1可跳跃,0不可跳跃
  };

  constructor($workflow) {
    this.$workflow = $workflow;
  }

  /**
   * 初始化，重置数据
   */
  init({ button }) {
    // 重置基础数据
    Object.assign(this.state, baseState);
    // 操作按钮名称
    this.state.name = button.name;
    // 默认审批意见
    this.state.comment = button.customizeComments || this.state.comment;
    // 操作按钮对象
    this.button = button;
    // 当前操作节点
    this.setActiveNode(this.$workflow.state.currentTaskNode);
  }

  /**
   * 下一步
   */
  nextSetp() {
    // this.state.directOperation == true 单个节点不选人直接到第三步
    // this.state.activeNode.submitDirectly == true  第一步有多个节点 该值为true不选人直接跳第三步
    this.state.activeStep =
      this.state.directOperation || this.state.activeNode?.submitDirectly
        ? 3
        : this.state.activeStep + 1;
  }
  /**
   * 上一步
   */
  prevSetp() {
    // this.state.directOperation == true 单个节点不选人直接到第一步
    // this.state.activeNode.submitDirectly == true 第一步有多个节点 该值为true不选人直接跳第一步
    this.state.activeStep =
      this.state.directOperation || this.state.activeNode?.submitDirectly
        ? 1
        : this.state.activeStep - 1;
  }

  /**
   * 更新任务节点数据
   * @param {*} nodes
   */
  updateTaskUsers(nodes) {
    const oldTaskUsers = this.state.taskUsers;
    this.state.taskUsers = {};
    nodes.forEach((node) => {
      this.state.taskUsers[node.id] = {
        nodeId: node.id,
        sequential: node.sequential,
        nodeName: node.name,
        type: node.type,
        users: oldTaskUsers[node.id] ? oldTaskUsers[node.id].users || [] : [],
      };
    });
  }

  /**
   * 设置常用人员信息
   * @param {*} users
   */
  setCommonUsers(users = []) {
    this.state.commonUsers = users;
  }

  /**
   * 设置当前送交的节点信息
   */
  setActiveNode(node) {
    // 设置激活节点
    this.state.activeNode = node;
  }

  /**
   * 设置送交节点信息
   * @param {*} param0
   * @returns
   */
  setAssigneeNodes({ nodeAssigneeInfo, commonUserInfo }) {
    //处理流程常用人数据
    this.state.commonUsers = commonUserInfo || [];
    // 设置目标节点信息
    this.state.activeNode.setAssigneeNodes(nodeAssigneeInfo);
    // 设置目标节点送交信息
    this.state.assignees = {};
    this.state.activeNode.assigneeNodes?.forEach((node) => {
      const assignee = new TaskAssignee(node);
      // 默认选中人员数据
      assignee.setUsers(node.getDefaultSelectUsers());
      this.state.assignees[node.id] = assignee;
    });
    return this.state.assignees;
  }

  /**
   * 获取操作参数
   * @param {*} params
   * @returns
   */
  getOperationParams(params = {}) {
    const beanId = this.$workflow.getBeanId();
    let funcParams = {};
    if (beanId) {
      const funcData = this.$workflow.$func.getFuncData();
      const { tableCode, funcCode, productCode, funcId } = funcData.info;
      funcParams = {
        tableCode,
        funcCode,
        funcId,
        beanId,
        prod: productCode,
      };
    }
    return { ...new TaskCommonParams({ ...this.button, ...funcParams }), ...params };
  }

  /**
   * 获取提交参数
   * @returns
   */
  getSubmitParams(params = {}) {
    const submitParams = {
      comment: this.state.comment || '', // 审批意见
      target: this.state.activeNode.target, // 目标顺序流id
      isJump: this.state.activeNode.isJump, // 是否可跳跃, 1可跳跃,0不可跳跃
      ...this.getOperationParams(),
    };
    // 送交信息
    const assignees = Object.values(this.state.assignees);
    // 解析送交人信息
    submitParams.assignee = TaskAssignee.parseAssigneeParam(assignees);
    // 解析会签、多人节点审批顺序传参
    submitParams.sequentials = TaskAssignee.parseSequentialParam(assignees);

    return { ...submitParams, ...params };
  }

  /**
   * 精准的参数
   * @returns
   */
  getSubmitParams4Accurate() {
    const operationId = this.commomParams.operationId;
    const paramkeys = Object.keys(this.state.commonParams);
    switch (operationId) {
      case WorkflowOperatorEnum.PROCESS_EMPTY_START_OPERATOR: // 启动
      case WorkflowOperatorEnum.PROCESS_CANCEL_OPERATOR: // 撤销
      case WorkflowOperatorEnum.TASK_RETREIEVE_OPERATOR: // 取回
      case WorkflowOperatorEnum.TASK_URGE_OPERATOR: // 催办
      case WorkflowOperatorEnum.TASK_CLAIM_OPERATOR: // 领取
      case WorkflowOperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR: // 取消委托
      case WorkflowOperatorEnum.PROCESS_HANG_OPERATOR: // 挂起
      case WorkflowOperatorEnum.PROCESS_ACTIVE_OPERATOR: // 激活
        break;
      case WorkflowOperatorEnum.PROCESS_EMPTY_SPONSOR_OPERATOR: // 发起
      case WorkflowOperatorEnum.TASK_SUBMIT_OPERATOR: // 送交
        paramkeys.push(
          this.operationParamKeys.assignee,
          this.operationParamKeys.comment,
          this.operationParamKeys.target,
        );
        break;
      case WorkflowOperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
      case WorkflowOperatorEnum.TASK_GOBACK_OPERATOR: // 退回
      case WorkflowOperatorEnum.PROCESS_INVALID_OPERATOR: // 作废
      case WorkflowOperatorEnum.TASK_PASS_OPERATOR: //通过
      case WorkflowOperatorEnum.TASK_VETO_OPERATOR: // 否决
      case WorkflowOperatorEnum.TASK_ABSTAIN_OPERATOR: // 弃权
        paramkeys.push(this.operationParamKeys.comment);
        break;
      case WorkflowOperatorEnum.TASK_TRANSFER_OPERATOR: // 转办
      case WorkflowOperatorEnum.TASK_DELEGATE_OPERATOR: // 委托
        paramkeys.push(
          this.operationParamKeys.assignee, // 单人
          this.operationParamKeys.comment,
        );
        break;
      case WorkflowOperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
        paramkeys.push(
          this.operationParamKeys.target, // 单人
          this.operationParamKeys.comment,
        );
        break;
    }
    return pick(this.getSubmitParams(), paramkeys);
  }
}
