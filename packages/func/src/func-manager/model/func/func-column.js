import {
  isPlainObject,
  toBoolean,
  forEach,
  toNumber,
  isNotEmpty,
  split,
  decode,
} from '@jecloud/utils';
import Base from './base';
import { parseEvents } from '../../util';
export default class FuncColumn extends Base {
  constructor(options) {
    super(options);
    /**
     * 列ID
     */
    this.id = options.JE_CORE_RESOURCECOLUMN_ID;
    /**
     * 列编码
     */
    this.field = options.RESOURCECOLUMN_CODE;
    /**
     * 列标题
     */
    this.title = options.RESOURCECOLUMN_NAME;
    /**
     * 列类型
     */
    this.type = parseColumnType(options.RESOURCECOLUMN_XTYPE);
    /**
     * 允许调整
     */
    this.resizable = true;
    /**
     * 可编辑
     */
    this.editable = toBoolean(options.RESOURCECOLUMN_ALLOWEDIT);
    /**
     * 批量编辑
     */
    this.batchEditable = toBoolean(options.RESOURCECOLUMN_ENABLEUPDATE);
    /**
     * 隐藏
     */
    this.hidden = toBoolean(options.RESOURCECOLUMN_HIDDEN);
    /**
     * 显示
     */
    this.visible = !this.hidden;
    /**
     * 查询选择的排序
     */
    this.queryIndex = options.RESOURCECOLUMN_QUERYINDEX;
    /**
     * 快速查询
     */
    this.treeQuery = toBoolean(options.RESOURCECOLUMN_QUERYTYPE);
    /**
     * 快速查询带值
     */
    this.treeQueryValue = toBoolean(options.RESOURCECOLUMN_QUICKQUERYVALUE);
    /**
     * 查询选择列
     */
    this.quickColumn = toBoolean(options.RESOURCECOLUMN_QUICKQUERY);
    /**
     * 查询选择查询类型
     */
    this.quickTreeQuery = toBoolean(options.RESOURCECOLUMN_QUICKQUERYTYPE);
    /**
     * 关键字查询类型
     */
    this.columnQueryType = options.RESOURCECOLUMN_COLUMNQUERYTYPE || 'no';
    /**
     * 排序
     */
    this.orderIndex = toNumber(options.SY_ORDERINDEX);
    /**
     * 内容对齐方式
     */
    this.align = options.RESOURCECOLUMN_ALIGN;
    /**
     * 列头对齐方式
     */
    this.headerAlign = options.RESOURCECOLUMN_ALIGN_TITLE || this.align;
    /**
     * 自动填充
     */
    this.fill = toBoolean(options.RESOURCECOLUMN_AUTO_FILL);

    /**
     * 宽
     */
    if (this.fill) {
      this.minWidth = toNumber(options.RESOURCECOLUMN_WIDTH);
    } else {
      this.width = toNumber(options.RESOURCECOLUMN_WIDTH);
    }
    /**
     * 排序
     */
    this.sortable = toBoolean(options.RESOURCECOLUMN_ORDER);

    /**
     * 锁定列
     */
    this.lock = options.RESOURCECOLUMN_LOCKED;

    /**
     * 列表提示，多行展示
     */
    this.showOverflow = 'ellipsis';
    // 提示
    if (toBoolean(options.RESOURCECOLUMN_COLUMNTIP)) {
      this.showOverflow = true;
      // 多行展示
    } else if (toBoolean(options.RESOURCECOLUMN_MULTIROWS)) {
      this.showOverflow = false;
    }

    /**
     * 字典颜色，COLOR，BGCOLOR，ICON
     */
    const ddColors = split(options.RESOURCECOLUMN_COLOR1, ',');
    this.ddColor = {
      textColor: ddColors.includes('COLOR'),
      backgroundColor: ddColors.includes('BGCOLOR'),
      icon: ddColors.includes('ICON'),
    };
    /**
     * 标题颜色
     */
    this.titleColor = options.RESOURCECOLUMN_TITLECOLOR;

    /**
     * 复选框样式
     */
    this.switchStyle = {
      enable: toBoolean(options.RESOURCECOLUMN_CHECKBOX_STYLE),
      checkedIcon: options.RESOURCECOLUMN_CHECKEDCLS,
      unCheckedIcon: options.RESOURCECOLUMN_UNCHECKEDCLS,
    };

    /**
     * 启用超链接
     */
    this.link = {
      enable: isNotEmpty(options.RESOURCECOLUMN_ENABLHYPERLINKS),
      enterForm: options.RESOURCECOLUMN_ENABLHYPERLINKS == '1',
      openForm: options.RESOURCECOLUMN_ENABLHYPERLINKS == '0',
      event: null,
    };
    /**
     * 统计配置
     */
    const statisticsTypes = split(options.RESOURCECOLUMN_STATISTICSTYPE, ',');
    const statisticsConfigs = isNotEmpty(options.RESOURCECOLUMN_STATISTICSCONFIG)
      ? decode(options.RESOURCECOLUMN_STATISTICSCONFIG)
      : [];
    this.statistics = {
      enable: statisticsTypes.length > 0,
      config: statisticsConfigs
        .filter((item) => statisticsTypes.includes(item.name))
        .map((item) => {
          // 格式化数据，兼容
          item.format = item.number;
          return item;
        }),
    };
    /**
     * 所有多表头
     */
    this.groupName = options.RESOURCECOLUMN_MORECOLUMNNAME || '';
    /**
     * 功能ID
     */
    this.funcId = options.RESOURCECOLUMN_FUNCINFO_ID;
    /**
     * 子列
     */
    this.children = [];

    /**
     * 字典配置，用于列表格式化
     */
    this.dictionary = null;

    /**
     * 拖动
     */
    this.draggable = true;
    /**
     * 事件
     */
    this.events = parseEvents(options.RESOURCECOLUMN_JSLISTENER);

    // 列表超链接事件
    if (this.events['link-click']) {
      this.link.enable = true;
      this.link.event = this.events['link-click'];
    }
  }

  /**
   * 列编码
   *
   * @readonly
   * @memberof FuncField
   */
  get code() {
    return this.field;
  }
  /**
   * 列名称
   *
   * @readonly
   * @memberof FuncColumn
   */
  get text() {
    return this.title;
  }

  addChild(data, cascade) {
    if (isPlainObject(data)) {
      data = [data];
    }
    forEach(data, (item) => {
      if (!item.isModel) {
        item = new FuncColumn(item);
      }
      if (this.field === item.groupName) {
        if (cascade) {
          item.addChild(data, cascade);
        }
        this.children.push(item);
      }
    });
  }
}

function parseColumnType(xtype) {
  switch (xtype) {
    case 'rownumberer':
      return 'seq';
    case 'actioncolumn':
      return 'action';
    default:
      return '';
  }
}
