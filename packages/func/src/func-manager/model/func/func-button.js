import { toBoolean, toNumber } from '@jecloud/utils';
import Base from './base';
import { parseEvents } from '../../util';

export default class FuncButton extends Base {
  constructor(options = {}) {
    super();
    /**
     * 主键
     */
    this.id = options.JE_CORE_RESOURCEBUTTON_ID;
    /**
     * 编码
     */
    this.code = options.RESOURCEBUTTON_CODE;
    /**
     * 标题
     */
    this.text = options.RESOURCEBUTTON_NAME;
    /**
     * 所属位置：列表，表单，action列
     */
    this.postion = options.RESOURCEBUTTON_TYPE;
    /**
     * 列表按钮条的位置
     */
    this.gridBarPosition = options.RESOURCEBUTTON_POSITION;

    /**
     * 依附按钮
     */
    this.groupName = options.RESOURCEBUTTON_YFAN || '';
    /**
     * 隐藏
     */
    this.hidden = toBoolean(options.RESOURCEBUTTON_HIDDEN);
    /**
     * 禁用
     */
    this.disabled = toBoolean(options.RESOURCEBUTTON_DISABLED);
    /**
     * 图标
     */
    this.icon = options.RESOURCEBUTTON_ICON;
    /**
     * 图标颜色
     */
    this.iconColor = options.RESOURCEBUTTON_ICONCOLOR;
    /**
     * 背景颜色
     */
    this.bgColor = options.RESOURCEBUTTON_BGCOLOR;
    /**
     * 字体颜色
     */
    this.fontColor = options.RESOURCEBUTTON_FONTCOLOR;
    /**
     * 边框颜色
     */
    this.borderColor = options.RESOURCEBUTTON_BORDERCOLOR;
    /**
     * 样式
     */
    this.cls = options.RESOURCEBUTTON_CLS;
    /**
     * 表达式
     */
    this.exps = {
      visible: {
        // 显隐表达式
        exp: options.RESOURCEBUTTON_INTERPRETER,
      },
    };
    /**
     * 启用表达式
     */
    this.expsEnable = !!this.exps.visible.exp;
    /**
     * 排序
     */
    this.orderIndex = toNumber(options.SY_ORDERINDEX);
    /**
     * 不受控，不受功能只读控制
     */
    this.noReadOnly = toBoolean(options.RESOURCEBUTTON_NOREADONLY);
    /**
     * 功能ID
     */
    this.funcId = options.RESOURCEBUTTON_FUNCINFO_ID;
    /**
     * 事件
     */
    this.events = parseEvents(options.RESOURCEBUTTON_JSLISTENER);
    /**
     * 绑定的事件，如onClick
     */
    this.listeners;
  }

  /**
   * 是否只读
   *
   * @readonly
   * @memberof FuncButton
   */
  get isReadOnly() {
    return !this.noReadOnly && this.hidden;
  }

  /**
   * 设置只读
   * @param {boolean} readonly
   */
  setReadOnly(readonly) {
    !this.noReadOnly && this.setVisible(!readonly);
  }
  /**
   * 设置按钮文字
   * @param {string} text
   */
  setText(text) {
    this.text = text;
  }
  /**
   * 设置按钮图标
   * @param {string} icon
   */
  setIcon(icon) {
    this.icon = icon;
  }
  /**
   * 显示
   */
  show() {
    this.setVisible(true);
  }
  /**
   * 隐藏
   */
  hide() {
    this.setVisible(false);
  }
  /**
   * 显隐
   * @param {*} visible
   */
  setVisible(visible) {
    this.hidden = !visible;
  }
  /**
   * 执行点击事件
   */
  click() {
    return this.listeners?.onClick?.();
  }
}
