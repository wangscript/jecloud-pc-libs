import Base from './base';
import { toBoolean, toNumber, decode } from '@jecloud/utils';
/**
 * 高级查询项
 */
export default class FuncGroupQuery extends Base {
  constructor(options) {
    super();
    /**
     * 主键
     */
    this.id = options.JE_CORE_GROUPQUERY_ID;
    /**
     * 标签
     */
    this.label = options.GROUPQUERY_MC;
    /**
     * 名称
     */
    this.name = options.GROUPQUERY_BM;
    /**
     * 查询类型
     */
    this.queryType = options.GROUPQUERY_CXLX;
    /**
     * 展示查询类型
     */
    this.showQueryType = toBoolean(options.GROUPQUERY_SHOW_QUERY_TYPE);
    /**
     * 隐藏标签
     */
    this.labelHidden = toBoolean(options.GROUPQUERY_BQYC);
    /**
     * 启用查询
     */
    this.enableSort = toBoolean(options.GROUPQUERY_ALLOW_SORT);
    /**
     * 占用列数
     */
    this.colSpan = toNumber(options.GROUPQUERY_SZLS) || 1;
    /**
     * 宽度
     */
    this.width = toNumber(options.GROUPQUERY_KD);
    /**
     * 标签宽度
     */
    this.labelWidth = toNumber(options.GROUPQUERY_BQKD);
    /**
     * 排序
     */
    this.orderIndex = toNumber(options.SY_ORDERINDEX);
    /**
     * 换行符
     */
    this.isBr = this.name === '-';
  }
  get querys() {
    return this.sql && decode(this.sql);
  }
}
