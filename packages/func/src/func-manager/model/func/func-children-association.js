import { decode, toBoolean } from '@jecloud/utils';
import Base from './base';

export default class FuncChildrenAssociation extends Base {
  constructor(options = {}) {
    super();
    /**
     * 子功能字段编码
     */
    this.code = options.ASSOCIATIONFIELD_CHIFIELDCODE;
    /**
     * 子功能字段名称
     */
    this.name = options.ASSOCIATIONFIELD_CHIFIELDNAME;
    /**
     * 主功能字段编码
     */
    this.parentCode = options.ASSOCIATIONFIELD_PRIFIELDCODE;
    /**
     * 主功能字段名称
     */
    this.parentName = options.ASSOCIATIONFIELD_PRIFIELDNAME;
    /**
     * 启用传值
     */
    this.enableValue = toBoolean(options.ASSOCIATIONFIELD_TRANSMIT);
    /**
     * 启用查询
     */
    this.enableQuery = toBoolean(options.ASSOCIATIONFIELD_WHERECON);
    /**
     * 查询类型
     */
    this.queryType = options.ASSOCIATIONFIELD_ASSOCIATION;
    if (this.queryType === 'no') {
      this.enableQuery = false;
    }

    /**
     * 自定义查询条件
     */
    this.customQuerys = options.ASSOCIATIONFIELD_SQL ? decode(options.ASSOCIATIONFIELD_SQL) : [];
  }
}
