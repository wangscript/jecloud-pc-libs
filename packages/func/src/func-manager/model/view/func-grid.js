import { QueryTypeEnum, FuncTypeEnum, QueryConnectorEnum, FuncButtonTypeEnum } from '../../enum';
import { isNotEmpty, split, isEmpty, omit } from '@jecloud/utils';
import BaseView from './base';
/**
 * 功能列表公共类
 * PC和APP共有的属性和方法，其他需要根据环境进行调用
 */
export default class FuncGridView extends BaseView {
  /**
   * 按钮信息
   *
   * @memberof FuncGridView
   */
  buttons = {};
  /**
   * 绑定的事件，如onClick
   *
   * @memberof FuncFormView
   */
  listeners = {};
  /**
   * 列表类型
   * func，select
   */
  type = 'func';
  /**
   * 查询高亮文字
   */
  highlightText = '';

  constructor(options) {
    super(options);
    this.store = options.store; // store
    this.type = options.props.type || this.type; // 列表类型
    this.addButton(this.$func.getGridButtons());
  }

  /**
   * 通过id获得record
   * @param {string} id
   * @returns {Object}
   */
  getRecordById(id) {
    return this.getStore()?.getRecordById(id);
  }
  /**
   * 获得record的id
   * @param {*} record
   * @returns
   */
  getRecordId(record) {
    return this.getStore()?.getRecordId(record);
  }
  /**
   * 获得数据store
   * @returns
   */
  getStore() {
    return this.store;
  }
  /**
   * 设置数据store
   * @param {TreeSore} store
   */
  setStore(store) {
    this.store = store;
  }
  /**
   * 添加按钮信息
   * @param {*} code
   * @param {*} button
   */
  addButton(code, button) {
    if (Array.isArray(code)) {
      code.forEach((item) => {
        this.addButton(item.code, item);
      });
    } else {
      this.buttons[code] = button;
    }
  }
  /**
   * 只读
   * @param {boolean} readonly
   * @returns
   */
  setReadOnly() {
    // 按钮只读
    // 不可多选
    // 不可侧滑选择
  }
  /**
   * 重置列表状态
   */
  reset() {}

  /**
   * 按照页码加载数据
   * @param {number} page 页码
   * @param {Object} options 请求配置
   * @param {Object} [options.params] 请求参数
   * @returns {Promise}
   */
  loadPage(page, ...args) {
    return this.getStore().loadPage(page, ...args);
  }

  /**
   * 重新加载数据
   * @returns {Promise}
   */
  reload(...args) {
    return this.getStore().reload(...args);
  }

  /**
   * 获得查询解析器
   * @returns
   */
  getQueryParser() {
    return this.getStore()?.queryParser;
  }

  /**
   * 获得关键字查询的文字
   * @returns {string}
   */
  getKeywordText() {
    return this.getStore()?.queryParser.keywordText;
  }

  /**
   * 设置查询条件
   */
  setQuerys(options) {
    const { type, load = true } = options;
    const args = { $grid: this, ...options };
    const queryParser = this.getQueryParser();
    let queryPromise = null;
    switch (type) {
      case QueryTypeEnum.KEYWORD:
        queryPromise = queryParser.setKeywordQuerys(args);
        break;
      case QueryTypeEnum.GROUP:
        queryPromise = queryParser.setGroupQuerys(args);
        break;
      case QueryTypeEnum.STRATEGY:
        queryPromise = queryParser.setStrategyQuerys(args);
        break;
      case QueryTypeEnum.TREE:
        queryPromise = queryParser.setTreeQuerys(args);
        break;
      case QueryTypeEnum.PARENT:
        queryPromise = queryParser.setParentQuerys(args);
        break;
      case QueryTypeEnum.TREE_FUNC:
        queryPromise = queryParser.setTreeFuncQuerys(args);
        break;
      case QueryTypeEnum.ORDER:
        queryPromise = queryParser.setOrderQuerys(args);
        break;
    }
    // 自动加载数据
    return queryPromise?.then((options = {}) => {
      if (options.success === false) {
        return Promise.reject(options);
      } else if (load) {
        return this.loadPage(1);
      }
    });
  }
  /**
   * 初始化查询条件
   */
  initQuerys() {
    const queryParser = this.getQueryParser();
    const funcData = this.getFuncData();
    const { defaultStrategy, info } = funcData;
    const querysPromise = [];
    // 重置查询条件
    queryParser?.resetQuerys();
    // 默认查询策略设置
    if (defaultStrategy) {
      querysPromise.push(
        this.setQuerys({ type: QueryTypeEnum.STRATEGY, querys: defaultStrategy, load: false }),
      );
    }
    // 树形功能
    if (info.funcType === FuncTypeEnum.TREE) {
      querysPromise.push(this.setQuerys({ type: QueryTypeEnum.TREE_FUNC, load: false }));
    }
    // 父功能查询条件
    const parentFunc = this.$func?.getParentFunc();
    if (parentFunc) {
      const { querys } = this.$func.store.getDefaults();
      // 外键，子功能集合使用
      const fkCode = this.props.fkCode;
      if (fkCode) {
        querys.push({
          code: fkCode,
          type: QueryConnectorEnum.EQ,
          value: parentFunc.store.getBeanId(),
        });
      }
      querysPromise.push(
        this.setQuerys({
          type: QueryTypeEnum.PARENT,
          querys,
          load: false,
        }),
      );
    }
    return Promise.all(querysPromise);
  }
  /**
   * 列表校验
   * @param {boolean|Object|Array} rows
   * null: 只校验临时变动的数据，例如新增或修改
   * true: 校验当前表格数据
   * row|rows: 校验指定行或多行
   * @returns {Promise}
   */
  validate(rows) {
    return this.pluginCall('validate', rows).then((errorMaps) =>
      isEmpty(errorMaps) ? Promise.resolve() : Promise.reject(errorMaps),
    );
  }
  /**
   * 清除列表校验
   * @returns {Promise}
   */
  clearValidate() {
    return this.pluginCall('clearValidate');
  }

  /**
   * 获得选中的主键
   * @returns {string[]}
   */
  getSelectedIds() {
    return this.getSelectedRecords().map((record) => this.getRecordId(record)) || [];
  }
  /**
   * 获得选中的主键字符串
   * @returns {string}
   */
  getSelectedIds4Str() {
    return this.getSelectedIds().join(',');
  }
  /**
   * 刷新选择数据的状态
   */
  refreshSelection() {
    const selectRecords = this.getSelectedRecords();
    const records = selectRecords.filter((record) => this.getRecordId(record));
    const plugin = this.getPlugin();
    if (plugin) {
      plugin.selection = records;
    }
  }

  /**
   * 获得选中数据
   * @returns {Array}
   */
  getSelectedRecords() {
    return this.pluginCall('getSelectedRecords');
  }
  /**
   * 根据字段编码返回选中数据的对象集合
   * @param {string[]} fieldCodes
   * @returns {Array}
   */
  getSelectedRecordsByFieldCode(fieldCodes = []) {
    return this.getSelectedRecords().map((record) => {
      if (fieldCodes.length) {
        const item = {};
        fieldCodes.forEach((code) => {
          item[code] = record[code];
        });
        return item;
      } else {
        return record;
      }
    });
  }

  /**
   * 设置选中数据
   * @param {Object|Array} records 要选中的数据
   * @param {boolean} selected 是否选中
   * @returns {Promise}
   */
  setSelectedRecords(...args) {
    return this.pluginCall('setSelectedRecords', ...args);
  }
  /**
   * 清除选中数据
   * @returns
   */
  clearSelectedRecords() {
    return this.pluginCall('clearSelectedRecords');
  }

  /**
   * 获得修改的数据
   * @param {boolean} [includeInsert] 是否包含新增的数据
   * @returns {Array}
   */
  getUpdatedRecords(...args) {
    return this.getStore().getUpdatedRecords(...args);
  }

  /**
   * 插入数据
   * @param {Object|Array} records 插入的数据
   * @param {index} index 插入的位置：null(第一行)，-1(最后一行)，数字(指定索引)
   * @param {Array} 新插入的数据
   */
  insertRow(record, index) {
    return this.getStore().insert(record, index);
  }
  /**
   * 获得whereSql
   * @param {string} [type=custom]
   * @returns {Array}
   */
  getWhereSql(type = QueryTypeEnum.CUSTOM) {
    const queryParser = this.getQueryParser();
    const types = Object.values(QueryTypeEnum);
    if (type === 'all') {
      const querys = {};
      types.forEach((key) => {
        querys[key] = queryParser[key];
      });
      return querys;
    } else if (Object.values(types).includes(type)) {
      return queryParser[type];
    }
  }
  /**
   * 设置whereSql
   * @param {Array} querys
   * @param {string} [type=custom]
   */
  setWhereSql(querys = [], type = QueryTypeEnum.CUSTOM) {
    const queryParser = this.getQueryParser();
    queryParser.setQuerys({ type: type, querys: querys });
  }
  /**
   * 获得orderSql
   * @returns {Array}
   */
  getOrderSql() {
    return this.getQueryParser()[QueryTypeEnum.ORDER];
  }

  /**
   * 设置orderSql
   * @param {Array} orders
   */
  setOrderSql(orders = []) {
    this.getQueryParser().setQuerys({ type: QueryTypeEnum.ORDER, querys: orders });
  }
  /**
   * 显示列
   * @param {string} fieldCode 字段编码
   * @returns {Promise}
   */
  showColumn(fieldCode) {
    return this.pluginCall('showColumn', fieldCode).then(() => this.refreshColumn());
  }
  /**
   * 隐藏列
   * @param {string} fieldCode 字段编码
   * @returns {Promise}
   */
  hideColumn(fieldCode) {
    return this.pluginCall('hideColumn', fieldCode).then(() => this.refreshColumn());
  }

  /**
   * 刷新列配置
   * @returns
   */
  refreshColumn() {
    return this.pluginCall('refreshColumn');
  }
  /**
   * 获得按钮
   * @param {string} buttonCode
   */
  getButton(buttonCode) {
    return this.getButtons(buttonCode);
  }
  getButtons(buttonCode) {
    return buttonCode ? this.buttons[buttonCode] : this.buttons;
  }
  /**
   * 设置按钮显隐
   * @param {string} buttonCode
   * @param {boolean} visible
   */
  setButtonVisible(buttonCode, visible) {
    const button = this.getButton(buttonCode);
    button && (button.hidden = !visible);
  }
  /**
   * 展开面板，快速查询专用
   * @param {boolean} expand true(展开)|false(收起)
   */
  setExpand(expand) {}

  /**
   * 混入
   * @param {Object} options
   */
  mixin(options = {}) {
    const excludes = ['validate', 'refreshColumn', 'showColumn', 'hideColumn'];
    Object.assign(this, omit(options, excludes));
  }
}

/**
 * 列表按钮
 */
function getGridButtons(buttons) {
  // 组装数据
  const tbarBtns = { barButtons: [], moreButtons: [], multiButtons: [], groupButtons: [] };
  buttons.forEach((button) => {
    const pos = split(button.gridBarPosition, ',');
    // 依附按钮，提取出来方便事件绑定
    if (isNotEmpty(button.children)) {
      tbarBtns.groupButtons.push(...button.children);
    }
    // 工具条
    if (pos.includes('BAR') || pos.length === 0) {
      tbarBtns.barButtons.push(button);
    }
    // 更多按钮
    if (pos.includes('MORE')) {
      tbarBtns.moreButtons.push(button);
    }
    // 多选按钮
    if (pos.includes('MULTI')) {
      tbarBtns.multiButtons.push(button);
    }
  });
  // 更多按钮
  if (tbarBtns.moreButtons.length) {
    tbarBtns.barButtons.push({
      icon: 'fal fa-ellipsis-h-alt',
      code: '__more__',
      children: tbarBtns.moreButtons,
    });
  }
  return tbarBtns;
}
/**
 * 移动端列表按钮
 * TODO: 移动端按钮规则
 * 除了多选按钮，其余都属于列表按钮，默认去掉【编辑】按钮
 * 多选按钮：侧滑和多选底部显示，超过两个，除删除外，其余放入更多按钮，如果没有删除，取第一个
 * 列表按钮：列表只放一个添加按钮，如果有多个，采用点击按钮，展开显示
 *
 */
function getAppGridButtons(buttons) {
  // 组装数据
  const tbarBtns = { barButtons: [], multiButtons: [], fabButton: null, moreButtons: [] };
  buttons
    .filter((button) => button.code !== FuncButtonTypeEnum.GRID_EDIT_BUTTON)
    .forEach((button) => {
      const pos = split(button.gridBarPosition, ',');
      // 依附按钮
      if (isNotEmpty(button.children)) {
        tbarBtns.barButtons.push(...button.children);
      }
      // 工具条，更多按钮
      if (pos.includes('BAR') || pos.includes('MORE') || pos.length === 0) {
        tbarBtns.barButtons.push(button);
      }
      // 多选按钮
      if (pos.includes('MULTI')) {
        tbarBtns.multiButtons.push(button);
      }
    });
  // 列表悬浮按钮
  if (tbarBtns.barButtons.length === 1) {
    tbarBtns.fabButton = tbarBtns.barButtons[0];
  }
  // 列表多选按钮
  if (tbarBtns.multiButtons.length > 2) {
    let firstButton = tbarBtns.multiButtons.find((item) => item.code === 'gridRemoveBtn');
    let moreButtons = [];
    // 删除按钮优先
    if (firstButton) {
      moreButtons = tbarBtns.multiButtons.filter((item) => item.code !== firstButton.code);
    } else {
      // 提取第一个按钮
      firstButton = tbarBtns.multiButtons[0];
      moreButtons = tbarBtns.multiButtons.slice(1);
    }
    tbarBtns.multiButtons = [
      {
        text: '更多...',
        code: '__more__',
        children: moreButtons,
      },
      firstButton,
    ];
  }
  return tbarBtns;
}
