import BaseView from './base';

export default class FuncTreeView extends BaseView {
  constructor(options) {
    super(options);
    this.store = options.store;
    this.type = options.type;
  }
  /**
   * 刷新数据
   */
  reload() {
    this.setAllTreeExpand(false);
    this.getStore()?.reload();
  }

  /**
   * 清空查询
   */
  doResetQuery() {
    this.clearSelectedRecords();
  }

  /**
   * 获得数据store
   * @returns
   */
  getStore() {
    return this.store;
  }
  /**
   * 设置数据store
   * @param {TreeSore} store
   */
  setStore(store) {
    this.store = store;
  }
  /**
   * 通过id获得record
   * @param {string} id
   * @returns Object
   */
  getRecordById(id) {
    return this.getStore()?.getRecordById(id);
  }
  /**
   * 获得record的id
   * @param {Object} record
   * @returns String
   */
  getRecordId(record) {
    return this.getStore()?.getRecordId(record);
  }
  /**
   * 获得选中的主键
   * @returns Array[string]
   */
  getSelectedIds() {
    return this.getSelectedRecords().map((record) => this.getRecordId(record)) || [];
  }
  /**
   * 获得选中的主键字符串,以逗号分隔
   * @returns String
   */
  getSelectedIds4Str() {
    return this.getSelectedIds().join(',');
  }
  /**
   * 获得选中数据
   * @returns Promise
   */
  getSelectedRecords() {
    return this.pluginCall('getSelectedRecords');
  }
  /**
   * 清除选中数据
   * @returns Promise
   */
  clearSelectedRecords() {
    return this.pluginCall('clearSelectedRecords');
  }
  /**
   * 展开节点
   * @param {Boolean} record 节点
   * @param {boolean} expand 展开，默认true
   * @returns Promise
   */
  setTreeExpand(record, expand = true) {
    return this.pluginCall('setTreeExpand', record, expand);
  }
  /**
   * 展开全部节点
   * @param {boolean} expand 展开，默认true
   * @returns Promise
   */
  setAllTreeExpand(expand = true) {
    return this.pluginCall('setAllTreeExpand', expand);
  }
}
