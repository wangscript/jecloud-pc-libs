import { nextTick } from 'vue';
import { isNotEmpty } from '@jecloud/utils';
import { FuncButtonTypeEnum, FuncFieldTypeEnum } from '../../enum';
import BaseView from './base';
import { parseConfigInfo } from '../../util';
export default class FuncFormView extends BaseView {
  /**
   * 字段信息
   *
   * @memberof FuncFormView
   */
  fields = {};
  /**
   * 按钮信息
   *
   * @memberof FuncFormView
   */
  buttons = {};

  /**
   * 子功能数据
   *
   * @memberof FuncFormView
   */
  childFuncs = {};

  /**
   * 表单锚点信息
   *
   * @memberof FuncFormView
   */
  anchors = {};

  /**
   * 绑定的事件，如onClick
   *
   * @memberof FuncFormView
   */
  listeners;
  /**
   * 表单类型
   * func, query, custom
   */
  type = 'func';
  constructor(options) {
    super(options);
    this.type = options.type || this.type; // 表单类型

    this.addButton(this.$func.getFormButtons());
    this.addAnchor(this.$func.getFormAnchors());
  }

  /**
   * 添加字段信息
   * @param {*} name
   * @param {*} field
   */
  addField(name, field) {
    this.fields[name] = field;
    field.$form = this;
    return field;
  }
  /**
   * 添加按钮信息
   * @param {*} code
   * @param {*} button
   */
  addButton(code, button) {
    if (Array.isArray(code)) {
      code.forEach((item) => {
        this.addButton(item.code, item);
      });
    } else {
      this.buttons[code] = button;
    }
  }
  /**
   * 添加锚点信息
   * @param {*} code
   * @param {*} anchor
   */
  addAnchor(code, anchor) {
    if (Array.isArray(anchor)) {
      anchor.forEach((item) => {
        this.addAnchor(item.code, item);
      });
    } else {
      this.anchors[code] = anchor;
    }
  }
  /**
   * 获得锚点信息
   * @param {*} code
   * @returns
   */
  getAnchor(code) {
    return this.anchors[code];
  }

  /**
   * 设置锚点显隐
   * @param {*} code
   * @param {*} visible
   */
  setAnchorVisible(code, visible) {
    const anchor = this.getAnchor(code);
    anchor && (anchor.hidden = !visible);
  }

  /**
   * 获得model数据
   * @returns
   */
  getModel() {
    return this.$func.store.activeBean || this.props.model;
  }
  /**
   * 获得表单所有值
   * @param {string} [fieldCode]
   * @returns {Object}
   */
  getValues(fieldCode) {
    const model = this.getModel();
    return fieldCode ? model?.[fieldCode] : model;
  }
  /**
   * 获得字段值
   * @param {*} fieldCode
   * @returns
   */
  getValue(fieldCode) {
    return this.getValues(fieldCode);
  }
  /**
   * 获得改变的值
   * @returns {Object}
   */
  getChanges() {
    return this.$func?.store.getBeanChanges();
  }
  getChangesData() {
    return this.getChanges();
  }
  /**
   * 重新加载表单数据
   */
  reloadData() {
    const funcData = this.getFuncData();
    const pkValue = this.$func.store.activeBean[funcData.info.pkCode];
    return isNotEmpty(pkValue)
      ? this.$func.getBeanById(pkValue).then((bean) => {
          if (bean) {
            this.$func.store.commitActiveBean(bean);
          }
          return bean;
        })
      : Promise.resolve();
  }
  reload() {
    return this.reloadData();
  }
  /**
   * 表单校验
   * @param {string|Array} names 字段名称，如果为空，默认全部
   */
  validate(names) {
    return this.pluginCall('validate', names);
  }
  /**
   * 清除表单校验
   * @param {string|Array} names 字段名称，如果为空，默认全部
   */
  clearValidate(names) {
    return this.pluginCall('clearValidate', names);
  }

  /**
   * 只读
   * @param {boolean} readonly
   */
  setReadOnly(readonly) {
    // 1. 字段只读
    Object.values(this.getFields()).forEach((field) => {
      field.readonly = readonly;
    });
    // 2. 按钮隐藏
    Object.values(this.getButtons()).forEach((button) => {
      if (button.code !== FuncButtonTypeEnum.FORM_BACK_BUTTON && !button.noReadOnly) {
        button.hidden = readonly;
      }
    });
    // 3. 子功能只读
    this.getChildFuncConfig().forEach((child) => {
      child.readonly = readonly;
    });
  }
  /**
   * 获得按钮
   * @param {string} buttonCode 按钮编码
   * @returns
   */
  getButton(buttonCode) {
    return this.getButtons(buttonCode);
  }
  getButtons(buttonCode) {
    return buttonCode ? this.buttons[buttonCode] : this.buttons;
  }
  /**
   * 设置值
   * @param {Object} values 值
   */
  setValues(values = {}) {
    Object.assign(this.getModel(), values);
  }
  /**
   * 设置单字段值
   * @param {string} fieldCode 字段编码
   * @param {Object} value 值
   */
  setValue(fieldCode, value) {
    this.setValues({ [fieldCode]: value });
  }
  /**
   * 重置字段数据
   * @param {string} fieldCode 字段编码
   * @param {boolean} useDefaultValue 使用字段默认值
   */
  resetField(fieldCode, useDefaultValue) {
    const field = this.getField(fieldCode);
    if (field) {
      field.reset(useDefaultValue);
      // 级联重置
      if (
        [
          FuncFieldTypeEnum.CHECKBOX_GROUP.xtype,
          FuncFieldTypeEnum.RADIO_GROUP.xtype,
          FuncFieldTypeEnum.SELECT.xtype,
          FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype,
          FuncFieldTypeEnum.INPUT_SELECT_GRID.xtype,
          FuncFieldTypeEnum.INPUT_SELECT_USER.xtype,
        ].includes(field.xtype)
      ) {
        const fields = parseConfigInfo({ configInfo: field.configInfo })?.targetFields || [];

        fields
          .filter((name, index) => name !== field.name && fields.indexOf(name) === index) // 去重
          .forEach((name) => {
            this.getField(name)?.reset(useDefaultValue);
          });
      }
    }
  }

  /**
   * 显示字段
   * @param {string} fieldCode 字段编码
   */
  showField(fieldCode) {
    this.setFieldVisible(fieldCode, true);
  }
  /**
   * 隐藏字段
   * @param {string} fieldCode 字段编码
   */
  hideField(fieldCode) {
    this.setFieldVisible(fieldCode, false);
  }

  /**
   * 设置字段显隐
   * @param {string} fieldCode 字段编码
   * @param {boolean} visible 显示
   */
  setFieldVisible(fieldCode, visible) {
    this.getField(fieldCode)?.[visible ? 'show' : 'hide']();
    // 锚点信息
    this.setAnchorVisible(fieldCode, visible);
  }

  /**
   * 设置字段只读
   * @param {string} fieldCode 字段编码
   * @param {boolean} readonly 只读
   */
  setFieldReadOnly(fieldCode, readonly) {
    this.getField(fieldCode)?.setReadOnly(readonly);
  }

  /**
   * 设置字段必填
   * @param {string} fieldCode 字段编码
   * @param {boolean} required 必填
   */
  setFieldRequired(fieldCode, required) {
    this.clearValidate(fieldCode);
    const field = this.getField(fieldCode);
    if (field) {
      field.required = required;
      nextTick(() => {
        this.validate(fieldCode);
      });
    }
  }
  /**
   * 设置子功能只读
   * @param {string} funcCode 功能编码
   * @param {boolean} readonly 只读
   */
  setChildFuncReadOnly(funcCode, readonly) {
    const child = this.getChildFuncConfig(funcCode);
    child && (child.readonly = readonly);
  }
  /**
   * 设置子功能显隐
   * @param {string} funcCode 功能编码
   * @param {boolean} visible 显示
   */
  setChildFuncVisible(funcCode, visible) {
    const child = this.getChildFuncConfig(funcCode);
    child && (child.hidden = !visible);
    // 锚点信息
    this.setAnchorVisible(funcCode, visible);
  }
  /**
   * 设置按钮显隐
   * @param {string} buttonCode 按钮编码
   * @param {boolean} visible 显示
   */
  setButtonVisible(buttonCode, visible) {
    const button = this.getButton(buttonCode);
    button && (button.hidden = !visible);
  }

  /**
   * 获得所有字段
   * @param {string} fieldCode 字段编码
   */
  getFields(fieldCode) {
    return fieldCode ? this.getField(fieldCode) : this.fields;
  }
  /**
   * 子功能配置
   * @returns
   */
  getChildFuncConfig(code) {
    return this.$func.getChildFuncConfig(code);
  }

  /**
   * 获得单个字段
   * @param {string} fieldCode 字段编码
   */
  getField(fieldCode) {
    return this.fields[fieldCode];
  }
  /**
   * 展开面板，高级查询专用
   * @param {boolean} expand true(展开)|false(收起)
   */
  setExpand(expand) {}
  /**
   * 执行查询，高级查询专用
   */
  executeQuery() {}

  /**
   * 重置原始配置
   */
  resetMetaConfig() {
    const $form = this;
    const $func = this.$func;
    // 重置表单只读状态
    let { readonly } = $func.store;
    // 如果没有保存按钮表单只读
    if (!$func.hasFormSavePerm()) {
      readonly = true;
    }
    $form.setReadOnly(readonly);

    // 重置表单字段原始配置
    Object.values($form.getFields()).forEach((field) => {
      field.hidden = field.metaData.hidden;
      field.readonly = readonly || field.metaData.readonly;
      field.required = field.metaData.required;
    });

    // 重置表单按钮原始配置
    Object.values($form.getButtons()).forEach((button) => {
      // 如果是返回按钮就走他以前的状态
      if (FuncButtonTypeEnum.FORM_BACK_BUTTON == button.code) {
        button.hidden = button.metaData.hidden;
      } else {
        button.hidden = readonly || button.metaData.hidden;
      }
    });

    // 重置子功能原始配置
    $form.getChildFuncConfig().forEach((child) => {
      child.hidden = child.metaData.hidden;
      child.readonly = readonly || child.metaData.readonly;
    });

    // 执行表单的表单式
    return $func.action.doFormFieldExps();
  }
}
