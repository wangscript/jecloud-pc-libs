import Base from '../func/base';
import { pick, toTemplate } from '@jecloud/utils';
import { cleanQuerys } from './grid-query-parser';
export default class DictionaryParam extends Base {
  /**
   * 字典编码
   *
   * @memberof DicParam
   */
  ddCode = '';
  /**
   * 字典名称
   *
   * @memberof DicParam
   */
  ddName = '';
  /**
   * 树形ROOT节点ID
   *
   * @memberof DicParam
   */
  rootId = '';
  /**
   * 异步树
   *
   * @memberof DicParam
   */
  async = false;
  /**
   * 字典对应绑定的字段编码
   *
   * @memberof DicParam
   */
  nodeInfo = '';
  /**
   * 查询条件
   *
   * @memberof DictionaryParam
   */
  querys = [];
  /**
   * 排序条件
   *
   * @memberof DictionaryParam
   */
  orders = [];
  /**
   * 字典多余参数
   *
   * @memberof DicParam
   */
  params = {};
  /**
   * 查询字段
   *
   * @memberof DicParam
   */
  queryField = null;
  /**
   * 树形取值字段
   *
   * @memberof DicParam
   */
  nodeField = 'code';

  /**
   * 快速查询带值
   *
   * @memberof DictionaryParam
   */
  enableValue = false;
  /**
   * 格式化值
   *
   * @memberof DicParam
   */
  formatValue = null;
  /**
   * 自维护字典
   *
   * @memberof DicParam
   */
  zwfFlag = '0';
  /**
   * 查询条件
   *
   * @memberof DicParam
   */
  get j_query() {
    return { custom: cleanQuerys(this.querys), order: this.orders || [] };
  }
  get isRoot() {
    return this.rootId ? '0' : '';
  }
  whereSql = '';
  parentSql = '';
  orderSql = '';

  constructor(options = {}) {
    super();
    Object.assign(this, options);
    // 解析变量，防止查询异常
    this.rootId = toTemplate(this.rootId) || 'ROOT';
  }

  toParams() {
    return pick(this, [
      'ddCode',
      'ddName',
      'rootId',
      'async',
      'params',
      'j_query',
      'nodeInfo',
      'customVariables',
    ]);
  }
}
