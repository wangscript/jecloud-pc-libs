import {
  QueryTypeEnum,
  QueryConnectorEnum,
  QueryOrderEnum,
  FuncDataTypeEnum,
  FuncTreeTypeEnum,
} from '../../enum';
import {
  isArray,
  forEach,
  isString,
  isNotEmpty,
  split,
  encode,
  pick,
  toQuerysTemplate,
  isEmpty,
} from '@jecloud/utils';
import { createQueryItem } from '../../util';
export default class GridQueryParser {
  /**
   * 基础查询参数
   *
   * @memberof GridQueryParser
   */
  basicQuerys = [];
  /**
   * 基础排序参数
   *
   * @memberof GridQueryParser
   */
  basicOrders = [];
  /**
   * 父功能查询
   *
   * @memberof GridQueryParser
   */
  parent = [];
  /**
   * 高级查询
   *
   * @memberof GridQueryParser
   */
  group = [];
  /**
   * 树形查询
   *
   * @memberof GridQueryParser
   */
  tree = [];
  /**
   * 列头查询
   *
   * @memberof GridQueryParser
   */
  column = [];
  /**
   * 列关键字查询
   *
   * @memberof GridQueryParser
   */
  keyword = [];
  /**
   * 列关键字查询文字，用于高亮展示
   *
   * @memberof GridQueryParser
   */
  keywordText = '';
  /**
   * 查询策略
   *
   * @memberof GridQueryParser
   */
  strategy = [];
  /**
   * 数据标记
   *
   * @memberof GridQueryParser
   */
  mark = [];
  /**
   * 自定义查询
   *
   * @memberof GridQueryParser
   */
  custom = [];

  /**
   * 排序条件
   *
   * @memberof GridQueryParser
   */
  order = [];

  /**
   * 功能对象
   *
   * @memberof GridQueryParser
   */
  $func;

  /**
   * 查询配置项，key为查询类型
   *
   * @memberof GridQueryParser
   */
  queryConfigs = {};

  /**
   * 功能数据
   *
   * @readonly
   * @memberof GridQueryParser
   */
  get funcData() {
    return this.$func.getFuncData();
  }
  constructor({ querys = [], orders = [], $func, type = FuncDataTypeEnum.FUNC }) {
    this.$func = $func;
    // 基础查询条件
    this.setQuerys({ querys: querys, type: QueryTypeEnum.BASIC_QUERYS });
    this.setQuerys({ querys: orders, type: QueryTypeEnum.BASIC_ORDERS });
    // 功能查询配置
    this.queryConfigs = {
      // 关键字查询配置
      [QueryTypeEnum.KEYWORD]: this.funcData.getQuerys(QueryTypeEnum.KEYWORD, type),
      // 树形查询配置
      [QueryTypeEnum.TREE]: this.funcData.getQuerys(QueryTypeEnum.TREE, type),
    };
  }

  /**
   * 设置查询条件
   * @param {*} options
   * @param {*} options.querys 查询条件
   * @param {*} options.type 条件类型
   * @param {*} options.cn 条件连接符
   */
  setQuerys({ querys = [], type, cn = QueryConnectorEnum.AND } = {}) {
    //   非符合类型的条件
    if (!Object.values(QueryTypeEnum).includes(type)) return;

    if (isArray(querys)) {
      this[type] = [];
      forEach(querys, (item) => {
        item && this[type].push(createQueryItem(item, cn));
      });
    } else {
      this[type] = querys;
    }
  }

  /**
   * 获取查询条件
   */
  getQuerys() {
    // 查询条件，排除排序条件，查询策略id
    const queryKeys = Object.values(QueryTypeEnum).filter(
      (key) =>
        ![QueryTypeEnum.BASIC_ORDERS, QueryTypeEnum.ORDER, QueryTypeEnum.STRATEGY_ID].includes(key),
    );
    const querys = { custom: [], order: [], _types: [] };
    queryKeys.forEach((key) => {
      const info = this[key];
      if (isNotEmpty(info)) {
        // 数据标记，表单数据
        if ([QueryTypeEnum.MARK, QueryTypeEnum.FORM_DATA].includes(key)) {
          querys[key] = info;
          // 查询策略
        } else if (QueryTypeEnum.STRATEGY === key && isString(info)) {
          querys[QueryTypeEnum.STRATEGY_ID] = info;
        } else {
          const finalQuerys = cleanQuerys(info);
          querys.custom.push(...finalQuerys);
          querys._types.push(key);
        }
      }
    });
    // 排序条件
    querys.order = isNotEmpty(this[QueryTypeEnum.ORDER])
      ? this[QueryTypeEnum.ORDER]
      : this[QueryTypeEnum.BASIC_ORDERS];

    // 格式化变量
    if (this.$func) {
      const model = this.$func.store.activeBean || {};
      const parentModel = this.$func.store.parentStore?.activeBean || {};
      toQuerysTemplate({ querys: querys.custom, model, parentModel });
    }
    return querys;
  }

  /**
   * 重置查询条件
   */
  resetQuerys() {
    Object.values(QueryTypeEnum).forEach((key) => {
      if ([QueryTypeEnum.BASIC_QUERYS, QueryTypeEnum.BASIC_ORDERS].indexOf(key) == -1) {
        this[key] = [];
      }
    });
  }
  /**
   * 关键字查询
   * @param {*} options
   */
  setKeywordQuerys(options = {}) {
    const { querys = {} } = options;
    const keyword = querys;
    const queryType = QueryTypeEnum.KEYWORD;
    // 关键词查询配置
    const keywordCols = this.queryConfigs[queryType] || [];

    // 未配置检索列
    if (isEmpty(keywordCols)) {
      return Promise.resolve({ success: false, message: '未找到可用的查询配置，请检查！' });
    }
    // 设置高亮关键字
    this.keywordText = keyword;

    // 拼接查询条件
    // TODO:columnQueryType
    const keywords = [];
    if (isNotEmpty(keyword)) {
      keywordCols.forEach((col) => {
        keywords.push({ code: col.field, value: keyword, type: col.columnQueryType, cn: 'or' });
      });
    }
    // 设置查询条件
    this.setQuerys({ querys: keywords, type: queryType });
    return Promise.resolve();
  }
  /**
   * 高级查询
   * @param {*} options
   */
  setGroupQuerys(options = {}) {
    const { querys = {} } = options;
    // 查询条件
    let groupQuerys = [];
    Object.keys(querys).forEach((code) => {
      const item = querys[code];
      if (isNotEmpty(item.value)) {
        const queryItem = { code, cn: QueryConnectorEnum.AND, ...pick(item, ['type', 'value']) };
        if ([QueryConnectorEnum.IN, QueryConnectorEnum.NOT_IN].includes(queryItem.type)) {
          // 包含，不包含
          queryItem.value = split(queryItem.value, ',');
        } else if ([QueryConnectorEnum.BETWEEN].includes(queryItem.type)) {
          // 区间
          queryItem.value = [queryItem.value, item.betweenValue];
        } else if (isArray(queryItem.value)) {
          // 数组数据，默认第一个
          queryItem.value = queryItem.value[0];
        }
        groupQuerys.push(queryItem);
      }
    });

    // 设置查询条件
    this.setQuerys({ querys: groupQuerys, type: QueryTypeEnum.GROUP });

    return Promise.resolve();
  }
  /**
   * 查询策略
   * @param {*} options
   */
  setStrategyQuerys(options = {}) {
    const { querys = {} } = options;
    const { id, fn } = querys;
    let queryPromise = null;
    // 自定义方法
    if (isNotEmpty(fn)) {
      queryPromise = Promise.resolve(fn?.());
      // 策略ID
    } else {
      queryPromise = Promise.resolve(id);
    }
    return queryPromise.then((querys) => {
      // 设置查询条件
      this.setQuerys({ querys: querys, type: QueryTypeEnum.STRATEGY });
    });
  }
  /**
   * 父功能查询
   * @param {*} options
   */
  setParentQuerys(options = {}) {
    const { querys = {} } = options;
    this.setQuerys({ querys: querys, type: QueryTypeEnum.PARENT });
    return Promise.resolve();
  }
  /**
   * 排序查询
   * @param {*} options
   */
  setOrderQuerys(options = {}) {
    const { querys = {} } = options;
    // 排序条件
    let orders = [];
    if (querys.type) {
      orders.push(querys);
    }
    this.setQuerys({ querys: orders, type: QueryTypeEnum.ORDER });
    return Promise.resolve();
  }
  /**
   * 树形查询
   * @param {*} options
   */
  setTreeQuerys(options = {}) {
    const { $tree, multiple, singleMode, querys = {} } = options;
    // 过滤非ROOT节点
    const nodes = isArray(querys) ? querys.filter((node) => node?.nodeType !== 'ROOT') : [querys];
    const queryType = QueryTypeEnum.TREE;
    const treeFunc = $tree.type === FuncTreeTypeEnum.FUNC;

    // 查询条件
    let treeQuerys = [];
    if (isNotEmpty(nodes)) {
      // 解析条件
      if (treeFunc) {
        treeQuerys = treeQuery4Func({ funcData: this.funcData, multiple, singleMode, nodes });
      } else {
        const dictionarys = this.queryConfigs[queryType] || [];
        treeQuerys = treeQuery4DD({ dictionarys, multiple, singleMode, nodes });
      }
    }
    // 设置查询条件
    this.setQuerys({ querys: treeQuerys, type: queryType });
    return Promise.resolve();
  }

  /**
   * 树形功能查询
   */
  setTreeFuncQuerys() {
    const querys = [{ code: 'SY_NODETYPE', value: 'ROOT', type: QueryConnectorEnum.NE }];
    // 设置查询条件
    this.setQuerys({ querys, type: QueryTypeEnum.TREE_FUNC });

    return Promise.resolve();
  }

  /**
   * 解析系统j_query查询参数
   * @returns
   */
  toJQuery() {
    return { j_query: encode(this.getQuerys()) };
  }

  /**
   * 条件类型
   *
   * @readonly
   * @memberof GridQueryParser
   */
  get queryType() {
    return QueryTypeEnum;
  }

  /**
   * 连接符类型
   *
   * @readonly
   * @memberof GridQueryParser
   */
  get connectorType() {
    return QueryConnectorEnum;
  }

  /**
   * 排序类型
   *
   * @readonly
   * @memberof GridQueryParser
   */
  get orderType() {
    return QueryOrderEnum;
  }
}

/**
 * 树形查询--字典
 * @param {*} param0
 * @returns
 */
function treeQuery4DD({ dictionarys, multiple, nodes }) {
  const treeQuerys = [];
  dictionarys.forEach((dicItem) => {
    const { nodeInfo, config } = dicItem;
    const queryField = dicItem.queryField || nodeInfo;
    const nodeField = dicItem.nodeField || 'code';
    // 获取跟字典匹配的节点数据
    let dicNodes = nodes.filter(
      (node) => node.id.endsWith('_' + nodeInfo) || node.nodeInfo === nodeInfo,
    );

    // 单选，级联子节点
    if (!multiple) {
      const childNodes = [];
      dicNodes.forEach((node) => {
        node.cascade((n) => {
          if (n !== node) {
            childNodes.push(n);
          }
        });
      });
      dicNodes = dicNodes.concat(childNodes);
    }

    // 获取节点编码，可以根基配置，TODO:格式化数据未处理...
    const codes = dicNodes.map((node) => {
      let value = node[nodeField];
      // id字段格式为：id_字段编码，需要特殊处理
      if (nodeField === 'id') {
        value = split(value, '_')[0] || value;
      }
      return value;
    });

    // 根据配置的
    if (isNotEmpty(codes)) {
      // 字段配置多选处理，使用like
      if (config.multiple) {
        codes.forEach((code) => {
          treeQuerys.push({
            code: queryField,
            value: code,
            type: QueryConnectorEnum.LIKE,
            cn: QueryConnectorEnum.OR,
          });
        });
      } else {
        treeQuerys.push({
          code: queryField,
          value: codes,
          type: QueryConnectorEnum.IN,
        });
      }
    }
  });

  return treeQuerys;
}

/**
 * 树形查询--树形功能
 * @param {*} param0
 * @returns
 */
function treeQuery4Func({ funcData, multiple, singleMode, nodes }) {
  const treeQuerys = [];
  const { funcTreeConfig, pkCode } = funcData.info;
  // 检索模式，精准查找
  if (singleMode) {
    const node = nodes[0];
    treeQuerys.push({
      code: pkCode,
      value: node.id,
      type: QueryConnectorEnum.EQ,
    });
    // 多选，精准查找
  } else if (multiple) {
    treeQuerys.push({
      code: pkCode, // 路径
      value: nodes.map((node) => node.id), //有可能是树形功能当作查询选择时，根节点是ROOT
      type: QueryConnectorEnum.IN,
    });
    // 单选，路径模糊查找
  } else {
    const node = nodes[0];
    treeQuerys.push({
      code: funcTreeConfig.nodePath, // 路径
      value: node.nodePath.replace('/ROOT', ''), //有可能是树形功能当作查询选择时，根节点是ROOT
      type: QueryConnectorEnum.LIKE,
    });
  }
  return treeQuerys;
}

/**
 *  清洗数据
 * @param {*} querys
 */
export function cleanQuerys(querys = [], deep = 0) {
  // 提升到查询第一层，方便后台处理
  const fields = ['ZHID', 'SY_ZHID', 'ROLERANK', 'SY_JESYS', 'EXECUTION_ID_'];
  const firsts = []; // 提升的条件
  const custom = []; // 过滤后的条件

  querys
    .filter((item) => isNotEmpty(item, true))
    .forEach((item) => {
      if (fields.includes(item.code)) {
        firsts.push(item);
      } else {
        if (isArray(item.value)) {
          const info = cleanQuerys(item.value, deep + 1);
          item.value = info.custom;
          firsts.push(...info.firsts);
        }
        // 剔除组合条件 and，or 空数据
        if (
          ![QueryConnectorEnum.AND, QueryConnectorEnum.OR].includes(item.type) ||
          isNotEmpty(item.value)
        ) {
          custom.push(item);
        }
      }
    });
  if (deep === 0) {
    // 最终数据
    const finalQuerys = [];
    finalQuerys.push(...firsts);
    isNotEmpty(custom) &&
      finalQuerys.push(createQueryItem({ type: QueryConnectorEnum.AND, value: custom }));
    return finalQuerys;
  } else {
    // 递归数据
    return { custom, firsts };
  }
}
