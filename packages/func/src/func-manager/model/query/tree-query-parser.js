import { isPlainObject, encode, pick } from '@jecloud/utils';
import DictionaryParam from './dictionary-param';
import { FuncTreeTypeEnum } from '../../enum';
export default class TreeQueryParser {
  /**
   * 类型
   *
   * @memberof TreeQueryParser
   */
  type = FuncTreeTypeEnum.DD;
  /**
   * ROOT节点ID
   *
   * @memberof TreeQueryParser
   */
  rootId = 'ROOT';
  /**
   * 是否返回列表节点
   *
   * @memberof TreeQueryParser
   */
  onlyItem = false;

  /**
   * 多选
   *
   * @memberof TreeQueryParser
   */
  multiple = false;

  /**
   * 字典信息
   *
   * @memberof TreeQueryParser
   */
  dictionarys = [];

  /**
   * 异步请求
   *
   * @memberof TreeQueryParser
   */
  async = false;

  /**
   * 功能编码，树形功能使用
   *
   * @memberof TreeQueryParser
   */
  funcCode;

  /**
   * 查询条件
   *
   * @memberof TreeQueryParser
   */
  j_query = [];

  /**
   * 基础sql
   *
   * @memberof TreeQueryParser
   */
  querys = [];

  constructor(options) {
    Object.assign(
      this,
      pick(options, [
        'type',
        'rootId',
        'onlyItem',
        'multiple',
        'funcCode',
        'async',
        'j_query',
        'querys',
      ]),
    );
    this.addDictionary(options.dictionarys);
  }

  /**
   * 添加字典信息
   * @param {*} items
   * @returns
   */
  addDictionary(items = []) {
    if (this.type !== FuncTreeTypeEnum.DD) return;
    if (isPlainObject(items)) {
      items = [items];
    }
    if (items.filter((item) => item.async).length) {
      this.async = true;
    }
    items.forEach((item) => {
      this.dictionarys.push(new DictionaryParam(item));
    });
    return this;
  }

  /**
   * 生成树形字典查询参数
   * @returns
   */
  toParams(...args) {
    switch (this.type) {
      case FuncTreeTypeEnum.FUNC:
        return this.toFuncTreeParams(...args);
      case FuncTreeTypeEnum.DD:
        return this.toDicTreeParams(...args);
      default:
        return {};
    }
  }
  /**
   * 获取字典树的参数信息
   * @param {Object} ddConfig 单个字典配置，如果没有，取所有字典配置
   * @returns
   */
  toDicTreeParams(ddConfig) {
    let dics = ddConfig
      ? this.dictionarys.filter((item) => item.nodeInfo === ddConfig.nodeInfo)
      : this.dictionarys;
    const data = dics.map((item) => Object.assign(item.toParams(), ddConfig, {}));
    return {
      node: this.rootId,
      onlyItem: this.onlyItem,
      excludes: this.multiple ? '' : 'checked',
      strData: encode(data),
    };
  }

  /**
   * 功能树的查询参数
   * @param {*} param0
   * @returns
   */
  toFuncTreeParams({ rootId } = {}) {
    return {
      node: rootId || this.rootId,
      onlyItem: true,
      excludes: this.multiple ? '' : 'checked',
      funcCode: this.funcCode,
      tableCode: this.tableCode,
      j_query: encode((this.querys || []).concat(this.j_query)),
    };
  }

  setQuerys({ querys = [] }) {
    this.j_query = querys;
  }

  /**
   * 初始化功能树的查询条件
   * @param {*} param0
   * @returns
   */
  initQuerys({ $func }) {
    if (this.type !== FuncTreeTypeEnum.FUNC) return [];
    // 树形功能
    const querys = [];
    // 父功能查询条件
    if ($func?.parentFunc) {
      querys.push(...$func.store.getDefaults().querys);
      this.setQuerys({ querys });
    }
    return querys;
  }
}
