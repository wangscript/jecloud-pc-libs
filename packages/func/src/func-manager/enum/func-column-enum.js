/**
 * 列类型
 * @enum
 */
export const FuncColumnTypeEnum = Object.freeze({
  /**
   * 序号
   */
  SEQ: 'seq',
  /**
   * 多选
   */
  CHECKBOX: 'checkbox',
  /**
   * 单选
   */
  RADIO: 'radio',
  /**
   * 展开
   */
  EXPAND: 'expand',
  /**
   * html
   */
  HTML: 'html',
  /**
   * 操作列--自定义类型
   */
  ACTION: 'action',
  /**
   * 选择列--自定义类型
   */
  SWITCH: 'switch',
});
