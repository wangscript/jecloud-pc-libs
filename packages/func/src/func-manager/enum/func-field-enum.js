import { isApp } from '@jecloud/utils';
/**
 * 字段类型
 * @enum
 */
export const FuncFieldTypeEnum = Object.freeze({
  /**
   * 文本框
   */
  INPUT: { xtype: 'textfield', text: '文本框', component: 'JeInput' },
  /**
   * 数值框
   */
  INPUT_NUMBER: {
    xtype: 'numberfield',
    text: '数值框',
    component: 'JeInputNumber',
  },
  /**
   * 编号
   */
  INPUT_CODE: { xtype: 'textcode', text: '编号', component: 'JeInput' },
  /**
   * 文本域
   */
  TEXTAREA: {
    xtype: 'textarea',
    text: '文本域',
    component: 'JeInputTextarea',
  },
  /**
   * 单选框
   */
  RADIO_GROUP: {
    xtype: 'rgroup',
    text: '单选框',
    get component() {
      return isApp ? 'JeInputSelect' : 'JeRadioGroup';
    },
  },
  /**
   * 复选框
   */
  CHECKBOX_GROUP: {
    xtype: 'cgroup',
    text: '复选框',
    get component() {
      return isApp ? 'JeInputSelect' : 'JeCheckboxGroup';
    },
  },
  /**
   * 下拉框
   */
  SELECT: {
    xtype: 'cbbfield',
    text: '下拉框',
    get component() {
      return isApp ? 'JeInputSelect' : 'JeSelect';
    },
  },
  /**
   * HTML编辑器
   */
  EDITOR_HTML: { xtype: 'ckeditor', text: 'HTML编辑器', component: 'JeEditorHtml' },
  /**
   * 附件
   */
  UPLOAD_INPUT: {
    xtype: 'uxfilefield',
    text: '附件',
    get component() {
      return isApp ? 'JeInputDisplay' : 'JeUpload';
    },
  },
  /**
   * 多附件
   */
  UPLOAD: { xtype: 'uxfilesfield', text: '多附件', component: 'JeUploadFiles' },
  /**
   * 图片选择器
   */
  UPLOAD_IMAGE: { xtype: 'imagepickerfield', text: '图片选择器', component: 'JeUploadImage' },
  /**
   * 日期
   */
  DATE_PICKER: {
    xtype: 'datefield',
    text: '日期',
    get component() {
      return isApp ? 'JeInputDate' : 'JeDatePicker';
    },
  },
  /**
   * 日期区间
   */
  // DATE_RANGE_PICKER: { xtype: 'rangedatefield', text: '日期区间', component: 'JeDateRangePicker' },
  /**
   * 时间
   */
  TIME_PICKER: {
    xtype: 'clocktimefield',
    text: '时间',
    get component() {
      return isApp ? 'JeInputTime' : 'JeTimePicker';
    },
  },
  /**
   * 时间区间
   */
  // TIME_RANGE_PICKER: { xtype: 'rangetimefield', text: '时间区间', component: 'JeTimeRangePicker' },
  /**
   * 查询选择
   */
  INPUT_SELECT_GRID: {
    xtype: 'gridssfield',
    text: '查询选择',
    get component() {
      return isApp ? 'JeInputList' : 'JeInputSelectGrid';
    },
  },
  /**
   * 树形选择
   */
  INPUT_SELECT_TREE: {
    xtype: 'treessfield',
    text: '树形选择',
    get component() {
      return isApp ? 'JeInputTree' : 'JeInputSelectTree';
    },
  },
  /**
   * 人员选择
   */
  INPUT_SELECT_USER: {
    xtype: 'vueuserfield',
    text: '人员选择',
    get component() {
      return isApp ? 'JeInputUser' : 'JeInputSelectUser';
    },
  },
  /**
   * 颜色选择
   */
  INPUT_SELECT_COLOR: { xtype: 'colorfield', text: '颜色选择', component: 'JeColor' },
  /**
   * 图标选择
   */
  INPUT_SELECT_ICON: { xtype: 'iconfield', text: '图标选择', component: 'JeIcon' },
  /**
   * 查询
   */
  SEARCH: { xtype: 'search', text: '查询', component: 'JeSearch' },
  /**
   * 智能查询
   */
  SEARCH_GRID: { xtype: 'searchfield', text: '智能查询', component: 'JeInputSelectGrid' },
  /**
   * 展示字段
   */
  DISPLAY: {
    xtype: 'displayfield',
    text: '展示字段',
    component: 'JeInputDisplay',
  },
  /**
   * 评星
   */
  STAR: { xtype: 'starfield', text: '评星', component: 'JeRate' },
  /**
   * 拼音
   */
  PINYIN: { xtype: 'pinyinfield', text: '拼音', component: 'JePinyin' },
  /**
   * 进度条
   */
  PROGRESS: { xtype: 'barfield', text: '进度条', component: 'JeProgress' },
  /**
   * 分组框
   */
  FIELDSET: {
    xtype: 'fieldset',
    text: '分组框',
    get component() {
      return isApp ? 'JeSection' : 'JeFieldset';
    },
  },
  /**
   * 代码编辑器
   */
  EDITOR_CODE: { xtype: 'codeeditor', text: '代码编辑器', component: 'JeEditorCode' },
  /**
   * 子功能集合
   */
  FUNC_CHILD_FIELD: {
    xtype: 'childfuncfield',
    text: '子功能集合',
    component: 'JeFuncChildrenField',
  },
  /**
   * 子功能
   */
  FUNC_CHILD: { xtype: 'child', text: '子功能', code: '__child', component: 'JeFuncChildren' },
  /**
   * 流程历史
   */
  WORKFLOW_HISTORY: {
    xtype: 'workflowhistory',
    text: '流程历史',
    code: '__workflow_history',
    component: 'WorkflowHistory',
  },
  /**
   * 获得组件类型
   * @param {*} xtype
   * @returns
   */
  getXtype(xtype) {
    return Object.keys(FuncFieldTypeEnum).find((key) => FuncFieldTypeEnum[key]?.xtype === xtype);
  },
  /**
   * 获得对应的组件名
   * @param {*} xtype
   * @returns
   */
  getComponent(xtype) {
    return FuncFieldTypeEnum[FuncFieldTypeEnum.getXtype(xtype)]?.component;
  },
  /**
   * 列表编辑允许使用的类型
   * @param {*} xtype
   * @returns
   */
  includesColumnXtype(xtype) {
    return ![
      FuncFieldTypeEnum.EDITOR_CODE.xtype,
      FuncFieldTypeEnum.EDITOR_HTML.xtype,
      FuncFieldTypeEnum.UPLOAD.xtype,
      FuncFieldTypeEnum.UPLOAD_IMAGE.xtype,
      FuncFieldTypeEnum.FIELDSET.xtype,
      FuncFieldTypeEnum.DISPLAY.xtype,
      FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype,
      FuncFieldTypeEnum.FUNC_CHILD.xtype,
      FuncFieldTypeEnum.WORKFLOW_HISTORY.xtype,
    ].includes(xtype);
  },
});
