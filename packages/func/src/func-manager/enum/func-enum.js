/**
 * 子功能展示方式
 * @enum
 */
export const FuncChildrenLayoutEnum = Object.freeze({
  /**
   * 表单(外部)横向显示
   */
  FORM_OUTER_HORIZONTAL: 'formOuterHorizontal',
  /**
   * 表单(内部)横向显示
   */
  FORM_INNER_HORIZONTAL: 'formInnerHorizontal',
  /**
   * 表单(内部)纵向显示
   */
  FORM_INNER_VERTICAL: 'formInnerVertical',
  /**
   * 表单(内部)分组显示
   */
  FORM_INNER_GROUP: 'formInnerGroup',
  /**
   * 表格(内部)横向显示
   */
  GRID_INNER_HORIZONTAL: 'gridInnerHorizontal',
  /**
   * 表格(外部)并排显示
   */
  GRID_OUTER_HORIZONTAL: 'gridOuterHorizontal',
});

/**
 * 功能元素refkey
 */
export const FuncRefEnum = Object.freeze({
  FUNC_DATA: 'funcData',
  FUNC_EDIT: 'funcEdit',
  FUNC_GRID: 'funcGrid',
  FUNC_FORM: 'funcForm',
  FUNC_TREE: 'funcQueryTree',
  FUNC_SEARCH: 'funcSearch',
  FUNC_QUERY_GROUP: 'funcQueryGroup',
  ...FuncChildrenLayoutEnum,
});
/**
 * 功能页面视图改变动作
 */
export const FuncChangeViewActionEnum = Object.freeze({
  /**
   * 表单返回
   */
  FORM_BACK: 'formBack',
  /**
   * 表单进入
   */
  FORM_INNER: 'formInner',
  /**
   * 导航单击
   */
  CRUMB_CLICK: 'crumbClick',
  /**
   * 功能重置
   */
  FUNC_RESET: 'funcReset',
});
/**
 * 功能类型
 */
export const FuncTypeEnum = Object.freeze({
  /**
   * 树形功能
   */
  TREE: 'TREE',
  /**
   * 普通功能
   */
  FUNC: 'FUNC',
});

/**
 * 功能树形类型
 */
export const FuncTreeTypeEnum = Object.freeze({
  /**
   * 树形功能
   */
  FUNC: 'func',
  /**
   * 树形字典
   */
  DD: 'dd',
  /**
   * 树形自定义接口
   */
  CUSTOM: 'custom',
});

/**
 * 功能数据面板类型
 */
export const FuncDataTypeEnum = Object.freeze({
  /**
   * 功能数据面板
   */
  FUNC: 'func',
  /**
   * 查询选择数据面板
   */
  SELECT: 'select',
});

/**
 * 功能默认事件按钮
 */
export const FuncButtonTypeEnum = Object.freeze({
  /**
   * 表单保存
   */
  FORM_SAVE_BUTTON: 'formSaveBtn',
  /**
   * 表单保存新建
   */
  FORM_NEW_BUTTON: 'formNewBtn',
  /**
   * 表单保存复制
   */
  FORM_COPY_BUTTON: 'formCopyBtn',
  /**
   * 表单返回
   */
  FORM_BACK_BUTTON: 'formBackBtn',
  /**
   * 表单审核
   */
  FORM_SUBMIT_BUTTON: 'formSubmitBtn',
  /**
   * 表单弃审
   */
  FORM_CANCEL_BUTTON: 'formCancelBtn',
  /**
   * 列表添加
   */
  GRID_INSERT_BUTTON: 'gridInsertBtn',
  /**
   * 列表批量添加
   */
  GRID_INSERT_BATCH_BUTTON: 'gridInsertBatchBtn',
  /**
   * 列表保存
   */
  GRID_UPDATE_BUTTON: 'gridUpdateBtn',
  /**
   * 列表删除
   */
  GRID_REMOVE_BUTTON: 'gridRemoveBtn',
  /**
   * 列表编辑
   */
  GRID_EDIT_BUTTON: 'gridEditBtn',
  /**
   * 列表导出
   */
  GRID_EXPORT_BUTTON: 'gridExportXLSBtn',
  /**
   * 列表Action添加
   */
  ACTION_INSERT_BUTTON: 'actionAdd',
  /**
   * 列表Action保存
   */
  ACTION_UPDATE_BUTTON: 'actionUpdate',
  /**
   * 列表Action删除
   */
  ACTION_REMOVE_BUTTON: 'actionRemove',
  /**
   * 列表Action编辑
   */
  ACTION_EDIT_BUTTON: 'actionEdit',
  /**
   * 树形添加
   */
  TREE_INSERT_BUTTON: 'treeInsertBtn',
  /**
   * 树形编辑
   */
  TREE_EDIT_BUTTON: 'treeEditBtn',
  /**
   * 树形删除
   */
  TREE_REMOVE_BUTTON: 'treeRemoveBtn',
  /**
   * 树形转移
   */
  TREE_TRANSFER_BUTTON: 'treeTransferBtn',
});
