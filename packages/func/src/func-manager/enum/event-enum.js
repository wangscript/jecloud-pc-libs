/**
 * 列表支持的事件
 */
export const GridEventEnum = Object.freeze({
  TREE_BEFORE_DROP: 'tree-before-drop', // 功能树拖动前事件
  TREE_DROP: 'tree-drop', // 功能树拖动事件
  TRANSFORM_DEFAULT_VALUES: 'transform-default-values', // 功能默认值处理事件
});
/**
 * 表单支持的事件
 */
export const FormEventEnum = Object.freeze({});
/**
 * 字段支持的事件
 */
export const FieldEventEnum = Object.freeze({});
/**
 * 按钮支持的事件
 */
export const ButtonEventEnum = Object.freeze({});
