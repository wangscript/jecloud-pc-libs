/**
 * 查询类型
 * @enum
 */
export const QueryTypeEnum = Object.freeze({
  /**
   * 基础查询
   */
  BASIC_QUERYS: 'basicQuerys',
  /**
   * 基础排序
   */
  BASIC_ORDERS: 'basicOrders',
  /**
   * 父
   */
  PARENT: 'parent',
  /**
   * 高级
   */
  GROUP: 'group',
  /**
   * 树形
   */
  TREE: 'tree',
  /**
   * 列
   */
  COLUMN: 'column',
  /**
   * 快速
   */
  KEYWORD: 'keyword',
  /**
   * 树形功能（特殊）
   */
  TREE_FUNC: 'treeFunc',
  /**
   * 数据标记
   */
  MARK: 'mark',
  /**
   * 图表
   */
  CHART: 'chart',
  /**
   * 查询策略
   */
  STRATEGY: 'strategy',
  /**
   * 查询策略ID
   */
  STRATEGY_ID: 'strategyId',
  /**
   * 业务数据
   */
  FORM_DATA: 'formData',
  /**
   * 自定义
   */
  CUSTOM: 'custom',
  /**
   * 排序
   */
  ORDER: 'order',
});

/**
 * 排序条件类型
 * @enum
 */
export const QueryOrderEnum = Object.freeze({
  /**
   * 正序
   */
  ASC: 'asc',
  /**
   * 倒序
   */
  DESC: 'desc',
});
/**
 * 连接符类型
 * @enum
 */
export const QueryConnectorEnum = Object.freeze({
  /**
   * 包含
   */
  IN: 'in',
  /**
   * 包含
   */
  IN_SELECT: 'inSelect',
  /**
   * 不包含
   */
  NOT_IN: 'notIn',
  /**
   * 不包含
   */
  NOT_IN_SELECT: 'notInSelect',
  /**
   * 不为空
   */
  NOT_NULL: 'notNull',
  /**
   * 为空
   */
  IS_NULL: 'isNull',
  /**
   * between
   */
  BETWEEN: 'between',
  /**
   * 不等于
   */
  NE: '!=',
  /**
   * 大于
   */
  GT: '>',
  /**
   * 大于等于
   */
  GE: '>=',
  /**
   * 小于
   */
  LT: '<',
  /**
   * 小于等于
   */
  LE: '<=',
  /**
   * 左模糊查询
   */
  LIKE_LEFT: '%like',
  /**
   * 右模糊查询
   */
  LIKE_RIGHT: 'like%',
  /**
   * 模糊查询
   */
  LIKE: 'like',
  /**
   * 等于
   */
  EQ: '=',
  /**
   * and嵌套
   */
  AND: 'and',
  /**
   * or嵌套
   */
  OR: 'or',
});
/**
 * 连接符类型对应text
 */
export const QueryConnectorTextEnum = Object.freeze({
  [QueryConnectorEnum.LIKE]: '模糊',
  [QueryConnectorEnum.EQ]: '精确',
  [QueryConnectorEnum.IN]: '包含',
  [QueryConnectorEnum.NOT_IN]: '不包含',
  [QueryConnectorEnum.NOT_NULL]: '非空',
  [QueryConnectorEnum.BETWEEN]: '区间',
  [QueryConnectorEnum.GT]: '大于',
  [QueryConnectorEnum.GE]: '大于等于',
  [QueryConnectorEnum.LT]: '小于',
  [QueryConnectorEnum.LE]: '小于等于',
});
