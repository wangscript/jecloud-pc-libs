import { ajax, transformAjaxData } from '@jecloud/utils';
import {
  API_COMMON_SAVE,
  API_COMMON_UPDATE,
  API_COMMON_REMOVE,
  API_COMMON_GET_TREE,
  API_COMMON_MOVE,
  API_COMMON_UPDATE_LIST,
  API_COMMON_GET_BEAN,
  API_COMMON_LOAD,
  API_COMMON_CHECK_FIELD_UNIQUE,
  API_COMMON_STATISTICS,
  parseCommonUrl,
  parseCommonTreeUrl,
  API_COMMON_BATCH_MODIFY_LIST_DATA,
} from './urls';

/**
 * 保存
 * @param {*} param0
 * @returns
 */
export function doSaveApi({ params, pd, pkValue, action }) {
  return ajax({
    url: parseCommonUrl(pkValue ? API_COMMON_UPDATE : API_COMMON_SAVE, action),
    headers: { pd },
    params,
  })
    .then(transformAjaxData)
    .then((data) => ({ dynaBean: data }));
}

/**
 * 删除
 * @param {*} param0
 * @returns
 */
export function doRemoveApi({ params, pd, action }) {
  return ajax({
    url: parseCommonUrl(API_COMMON_REMOVE, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 保存
 * @param {*} param0
 * @returns
 */
export function doUpdateListApi({ params, pd, action }) {
  return ajax({
    url: parseCommonUrl(API_COMMON_UPDATE_LIST, action),
    headers: { pd },
    params,
  })
    .then(transformAjaxData)
    .then(({ rows }) => ({ dynaBeans: rows }));
}
/**
 * 表格列的批量修改
 * @param {*} param0
 * @returns
 */
export function doBatchModifyListApi({ params, pd, action }) {
  return ajax({
    url: parseCommonUrl(API_COMMON_BATCH_MODIFY_LIST_DATA, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 加载列表数据
 * @param {*} param0
 * @returns
 */
export function loadGridApi({ params, pd, action }) {
  return ajax({
    url: parseCommonUrl(API_COMMON_LOAD, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}
/**
 * 加载单条数据
 * @param {*} options
 * @returns
 */
export function loadGrid4singleApi(options) {
  return loadGridApi(options).then((data) => data?.rows?.[0]);
}

/**
 * 树形节点转移
 * @param {*} param0
 * @returns
 */
export function doTreeMoveApi({ params, pd, action }) {
  return ajax({
    url: parseCommonUrl(API_COMMON_MOVE, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 保存
 * @param {*} param0
 * @returns
 */
export function doTreeSaveApi({ params, pd, pkValue, action }) {
  return ajax({
    url: parseCommonTreeUrl(pkValue ? API_COMMON_UPDATE : API_COMMON_SAVE, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 删除
 * @param {*} param0
 * @returns
 */
export function doTreeRemoveApi({ params, pd, action }) {
  return ajax({
    url: parseCommonTreeUrl(API_COMMON_REMOVE, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 保存
 * @param {*} param0
 * @returns
 */
export function doTreeUpdateListApi({ params, pd, action }) {
  return ajax({
    url: parseCommonTreeUrl(API_COMMON_UPDATE_LIST, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 树形功能数据
 * @param {*} param0
 * @returns
 */
export function getTreeApi({ params, pd, action }) {
  return ajax({
    url: parseCommonTreeUrl(API_COMMON_GET_TREE, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * bean数据
 * @param {*} param0
 * @returns
 */
export function getBeanApi({ params, pd, action }) {
  return ajax({
    url: parseCommonUrl(API_COMMON_GET_BEAN, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 字段值唯一性验证
 * @param {*} param0
 * @returns
 */
export function checkFieldUniqueApi({ params, pd, action }) {
  return ajax({
    url: parseCommonUrl(API_COMMON_CHECK_FIELD_UNIQUE, action),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}

/**
 * 获得列表统计数据
 * @param {*} param0
 * @returns
 */
export function getStatisticsApi({ params, pd }) {
  return ajax({
    async: false,
    url: parseCommonUrl(API_COMMON_STATISTICS),
    headers: { pd },
    params,
  }).then(transformAjaxData);
}
