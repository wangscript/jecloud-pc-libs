import { useFuncAction } from './func-action';
import { useExpsAction } from './exps-action';
import { useFormAction } from './form-action';
import { useTreeAction } from './tree-action';
import { useGridAction } from './grid-action';
export function useAction($func) {
  const funcs = {
    ...useFormAction(),
    ...useFuncAction(),
    ...useExpsAction(),
    ...useTreeAction(),
    ...useGridAction(),
  };
  const actions = {};
  Object.keys(funcs).forEach((key) => {
    actions[key] = funcs[key].bind(this, $func);
  });
  return actions;
}
