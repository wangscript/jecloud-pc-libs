import { nextTick } from 'vue';
import { FuncChangeViewActionEnum } from '../enum';
/**
 * 功能Action
 * @returns
 */
export function useFuncAction() {
  return {
    doFuncLoad,
    doFuncReset,
    doFuncFieldUpdate,
  };
}

/**
 * 加载功能
 * @param {*} $func
 */
function doFuncLoad($func) {
  const { $modal } = $func;
  const funcData = $func.getFuncData();
  // 弹出功能，更新窗口标题
  if ($modal) {
    $modal.title.value = funcData.info.funcName;
  }
  return funcData;
}
/**
 * 重置功能
 * @param {*} $func
 */
function doFuncReset($func) {
  // 恢复原始页面
  $func.setActiveView($func.store.rawActiveView, FuncChangeViewActionEnum.FUNC_RESET);
  // 刷新子组件
  nextTick(() => {
    Object.values($func.getRefMaps()).forEach((refItem) => {
      refItem.value?.reset?.();
    });
  });
}
/**
 * 子功能集合修改
 * @param {*} param0
 * @returns
 */
function doFuncFieldUpdate($func, { bean }) {
  const funcFields = $func.getChildFuncFields();
  Object.values(funcFields).forEach((field) => {
    field.doUpdateList(bean);
  });
}
