import { encode, isEmpty, createDeferred } from '@jecloud/utils';
import { nextTick, reactive, toRaw } from 'vue';
import { Modal } from '@jecloud/utils';
import { FuncRefEnum, FuncTypeEnum, FuncButtonTypeEnum, FuncChangeViewActionEnum } from '../enum';
import { showSelectWindow, showFuncForm } from '../../func-util';
import { disableRemove4Workflow } from '../workflow';
import { showExportForm } from '../../func-data';
import {
  doRemoveApi,
  doTreeRemoveApi,
  doUpdateListApi,
  doTreeUpdateListApi,
  getBeanApi,
  doBatchModifyListApi,
} from '../api';

export function useGridAction() {
  return {
    doGridRemove,
    doGridEdit,
    doGridInsert,
    doGridInsert4Multi,
    doGridUpdateList,
    doGridExport,
    doGridRemove4View,
    doGridSave4View,
    doGridUpdate4View,
    doGridInsert4Grid,
    doGridBatchModify,
  };
}

/**
 * 列表导出
 * @param {*} $func
 * @param {*} options
 */
function doGridExport($func, options = {}) {
  showExportForm({ $func, options });
}

/**
 * 列表删除
 * @param {*} $func
 */
function doGridRemove($func, options = {}) {
  const { row, removeType } = options;
  const { productCode, tableCode, funcAction, funcCode, funcType, enableWorkflow } =
    $func.getFuncData().info;
  const $grid = $func.getRefMaps(FuncRefEnum.FUNC_GRID).value;
  const { store } = $grid;
  let rows = row ? [row] : $grid.getSelectedRecords();
  // 树形功能点击左侧树删除按钮
  if (removeType == 'tree') {
    rows = [row.bean];
  }
  if (isEmpty(rows)) {
    Modal.alert('请选择要删除的数据！', 'warning');
    return Promise.resolve();
  } else {
    //流程已启动的数据不可删除
    if (disableRemove4Workflow({ rows, enableWorkflow })) {
      Modal.alert('流程已启动不能被删除...', 'warning');
      return Promise.resolve();
    }
    const deferred = createDeferred();
    Modal.confirm(`确定删除选中的【${rows.length}】条数据吗`, () => {
      const ids = [];
      rows.forEach((item) => {
        if (!store.isInsertRecord(item)) {
          ids.push(store.getRecordId(item));
        }
      });
      // 删除操作
      let removePromise;
      if (ids.length) {
        const removeApi = funcType === FuncTypeEnum.TREE ? doTreeRemoveApi : doRemoveApi;
        removePromise = removeApi({
          params: { tableCode, funcCode, ids: ids.join(',') },
          pd: productCode,
          action: funcAction,
        });
      } else {
        removePromise = Promise.resolve();
      }
      removePromise
        .then(() => {
          const recordIds = rows.map((row) => store.getRecordId(row));
          $func.action.doTreeRemove4View({ ids: recordIds });
          setTimeout(() => {
            doGridRemove4View($func, { ids: recordIds });
          }, 100);
          // 刷新统计数据
          store.loadStatistics();
          Modal.notice(`${rows.length} 条记录被删除`, 'success');
          deferred.resolve();
        })
        .catch((err) => {
          Modal.alert(err?.message ?? '请求出错了，请查看控制台！', 'error');
          console.error(err);
          deferred.reject();
        });
    });
    return deferred.promise;
  }
}
/**
 * 列表批量修改
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doGridUpdateList($func, options = {}) {
  const { row, showMessage = true } = options;
  const $grid = $func.getFuncGrid();
  const funcData = $func.getFuncData();
  const { productCode, tableCode, funcCode, funcType, funcAction } = funcData.info;
  const { codeGenFieldInfo } = funcData.fields;
  const { store } = $grid;
  const changes = store.getChanges(row, true);
  if (isEmpty(changes)) {
    showMessage && Modal.message('暂无数据变动，无需修改...');
    return Promise.resolve();
  } else {
    const updates = [];
    const validRows = [];
    changes.forEach((change) => {
      let item = {};
      const record = change._record;
      const idProperty = store.idProperty;
      // 添加
      if (store.isInsertRecord(record)) {
        item = toRaw(record);
        // 修改
      } else {
        Object.keys(change).forEach((key) => {
          item[key] = change[key].newValue;
        });
        item[idProperty] = store.getRecordId(record);
      }
      delete item._record;
      updates.push(item);
      validRows.push(record);
    });
    const funcTree = funcType === FuncTypeEnum.TREE;
    const updateListApi = funcTree ? doTreeUpdateListApi : doUpdateListApi;
    // 提交数据
    return $grid
      .validate(validRows)
      .then(() => {
        return updateListApi({
          params: {
            tableCode,
            funcCode,
            strData: encode(updates),
            codeGenFieldInfo: encode(codeGenFieldInfo), // 编号
          },
          pd: productCode,
          action: funcAction,
        })
          .then(({ dynaBeans, nodes }) => {
            showMessage && Modal.notice('保存成功！', 'success');
            doGridUpdate4View($func, { records: dynaBeans });
            if (funcTree) {
              $func.action.doTreeUpdate4View({ nodes });
            }
            // 刷新统计数据
            store.loadStatistics();
            return { dynaBeans, nodes };
          })
          .catch((err) => {
            Modal.alert(err?.message ?? '请求出错了，请查看控制台！', 'error');
            console.error(err);
          });
      })
      .catch((errorMaps) => {
        console.log('列表校验失败：', errorMaps);
        return Promise.reject(errorMaps);
      });
  }
}
/**
 * 列表批量编辑
 * @param {*} $func
 * @param {*} options
 */
function doGridBatchModify($func, options = {}) {
  const { type, ids = [], j_query, bean } = options;
  if (isEmpty(bean)) {
    return Promise.reject({ message: '请输入要修改的数据！' });
  } else if (type === 'select' && ids.length === 0) {
    return Promise.reject({ message: '请选择要修改的数据！' });
  }
  const funcData = $func.getFuncData();
  const { productCode, tableCode, funcCode, funcAction } = funcData.info;
  const $grid = $func.getFuncGrid();
  return doBatchModifyListApi({
    params: {
      type,
      bean: encode(bean),
      ids: ids.join(','),
      j_query,
      totalCount: $grid?.store.totalCount,
      tableCode,
      funcCode,
    },
    pd: productCode,
    action: funcAction,
  }).then(() => {
    $grid.reload();
  });
}

/**
 * 添加
 * @param {*} $func
 * @param {*} options
 */
function doGridInsert($func, options = {}) {
  const funcData = $func.getFuncData();
  const { parentFunc } = $func;
  // 子功能添加控制
  if (parentFunc) {
    const beanId = parentFunc.store.getBeanId();
    if (!beanId) {
      Modal.alert('请先保存主功能数据！', Modal.status.warning);
      return;
    }
  }

  // 默认值
  const values = $func.getDefaultValues({ buttonCode: FuncButtonTypeEnum.GRID_INSERT_BUTTON });
  // 自定义默认值
  if (options.bean) {
    Object.assign(values, options.bean);
  }
  if (funcData.info.insertType === 'FIRSTADD') {
    //首行录入
    return doGridInsert4Grid($func, { records: [values] });
  } else if (funcData.info.insertType === 'FORMWIN') {
    //弹出表单
    return doGridInsert4Window($func, { bean: values });
  } else {
    //表单录入
    return doGridInsert4Form($func, { bean: values });
  }
}

/**
 * 表单添加
 * @param {*} $func
 * @param {*} param1
 * @returns
 */
function doGridInsert4Form($func, { bean }) {
  // 清除表单校验
  const $form = $func.getFuncForm();
  $form?.clearValidate();
  $func.store.setActiveBean(bean);
  // 跳转页面
  $func.setActiveView(FuncRefEnum.FUNC_EDIT, FuncChangeViewActionEnum.FORM_INNER);
  return nextTick().then(() => ({ bean }));
}
/**
 * 列表添加
 * @param {*} $func
 * @param {*} options
 */
function doGridInsert4Grid($func, options = {}) {
  let { records = [] } = options;
  const $grid = $func.getFuncGrid();
  let insertRecords = [];
  if (records.length) {
    insertRecords = $grid.store.insert(records);
    $grid.setActiveRow(insertRecords[0]);
  }
  return Promise.resolve({ insertRecords });
}

/**
 * 弹窗添加
 * @param {*} $func
 * @param {*} options
 */
function doGridInsert4Window($func, { bean }) {
  const $modal = showFuncForm($func.funcCode, {
    bean,
    isDDFunc: $func.isDDFunc,
    params: { $baseFunc: $func },
  });
  return Promise.resolve({ $modal });
}
/**
 * 多选添加
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doGridInsert4Multi($func, options = {}) {
  if (options.type && options.configInfo) {
    const deferred = createDeferred();
    showSelectWindow({
      title: options.title || '批量添加',
      multiple: true,
      ...options,
      callback(options) {
        const { rows, config } = options;
        // 默认值
        const values = $func.getDefaultValues({
          buttonCode: FuncButtonTypeEnum.GRID_INSERT_BATCH_BUTTON,
        });
        const records = [];
        rows.forEach((row) => {
          const record = {};
          config.sourceFields?.forEach((field, index) => {
            record[config.targetFields[index]] = row[field];
          });
          records.push({ ...values, ...record });
        });
        deferred.resolve({ records, ...options });
      },
    });
    return deferred.promise;
  } else {
    return Promise.reject('请配置批量添加的信息');
  }
}

/**
 * 页面编辑
 * @param {*} $func
 * @param {*} param1
 */
function doGridEdit($func, options = {}) {
  let errorMessage = '';
  // 编辑权限
  if (!$func.hasGridEditPerm()) return Promise.reject({ message: '没有编辑权限' });

  const $grid = $func.getFuncGrid();
  const selectedRecords = $grid.getSelectedRecords();
  const row = options.row || selectedRecords[0];
  if (!row) {
    errorMessage = '请选择要操作的数据！';
    Modal.alert(errorMessage, Modal.status.warning);
    return Promise.reject({ message: errorMessage });
  }

  if (selectedRecords.length > 1) {
    errorMessage = `请选择【1】条要编辑的数据，您已选择【${selectedRecords.length}】条数据！`;
    Modal.alert(errorMessage, Modal.status.warning);
    return Promise.reject({ message: errorMessage });
  }

  let next = Promise.resolve();
  // 读取全量bean数据
  const { productCode, tableCode, funcCode, funcAction, pkCode } = $func.getFuncData().info;
  if (row[pkCode]) {
    next = getBeanApi({
      params: { tableCode, funcCode, pkValue: row[pkCode] },
      action: funcAction,
      pd: productCode,
    });
  }

  $func.store.setActiveRow(row);
  return next.then((bean) => {
    const { link } = options;
    // 列表超链接，弹出表单
    if (link?.openForm) {
      return doGridInsert4Window($func, { bean });
    } else {
      return doGridInsert4Form($func, { bean });
    }
  });
}

/**
 * 添加数据
 * @param {*} param0
 * @returns
 */
function doGridSave4View($func, { record }) {
  const grid = $func.getFuncGrid();
  if (grid) {
    const records = grid.store.insert(record);
    return records[0];
  } else {
    return reactive(record);
  }
}
/**
 * 修改数据
 * @param {*} param0
 * @returns
 */
function doGridUpdate4View($func, { records }) {
  const grid = $func.getFuncGrid();
  //循环更新
  records.forEach((record) => {
    // 格式化处理
    grid?.store.transformInsertRecord(record);
    grid?.store.commitRecord(record);
  });
}
/**
 * 删除数据
 * @param {*} param0
 * @returns
 */
function doGridRemove4View($func, { ids = [] }) {
  const grid = $func.getFuncGrid();
  const tree = $func.getFuncTree();
  if (grid.store.pageSize === -1) {
    grid.store.remove(ids);
  } else {
    // 单前页没有数据刷新数据
    if (grid.store.data.length <= ids.length) {
      // 重新赋值左侧树的j_query
      if (tree) {
        const records = tree.getSelectedRecords();
        grid.setQuerys({ type: 'tree', querys: records, $tree: tree });
      } else {
        grid.store.reload();
      }
    } else {
      grid.store.remove(ids);
      grid.store.totalCount -= ids.length;
    }
  }
  grid.selection = [];
}
