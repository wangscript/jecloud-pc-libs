import { split, isPlainObject, isString, isEmpty, isArray, get } from '@jecloud/utils';

/**
 * 配置信息分隔符
 */
const splitStr = '~';

/**
 * 解析字段配置
 * @param {*} param0
 * @returns
 */
export function parseConfigInfo({ type, configInfo, options = {} }) {
  if (isPlainObject(configInfo) || isEmpty(configInfo)) return configInfo;
  switch (type) {
    case 'pinyin':
      // field~pinyin,field2~PY
      let pinyinFields = [];
      if (configInfo.includes(splitStr)) {
        pinyinFields = configInfo.split(',').map((item) => {
          return {
            name: item.split(splitStr)[0],
            config: item.split(splitStr)[1],
          };
        });
      }
      return { type, targetFields: pinyinFields };
    default:
      // 功能编码|字典编码,表单字段1~表单字段2...,选择组件字段1~选择组件字段2...,S(单选)|M(多选)
      const configInfos = configInfo.split(',');
      const code = configInfos[0];
      const targetFields = split(configInfos[1], splitStr);
      const sourceFields = split(configInfos[2], splitStr);
      const multiple = options.multiple || ['M', 'M_'].includes(configInfos[3]);
      const async = options.async || ['S_', 'M_'].includes(configInfos[3]);
      const fieldMaps = { sourceToTarget: {}, targetToSource: {} };
      targetFields.forEach((field, index) => {
        const sourceField = sourceFields[index];
        fieldMaps.targetToSource[field] = sourceField;
        fieldMaps.sourceToTarget[sourceField] = fieldMaps.sourceToTarget[sourceField] || [];
        fieldMaps.sourceToTarget[sourceField].push(field);
      });
      return {
        type,
        code,
        async,
        targetFields,
        sourceFields,
        multiple,
        fieldMaps,
      };
  }
}

/**
 * 设置配置信息
 * @param {*} param0
 * @returns
 */
export function getConfigInfo({ type, configInfo, rows = [], paths = [] }) {
  const config = isString(configInfo) ? parseConfigInfo({ type, configInfo }) : configInfo;
  const value = {};
  switch (type) {
    default:
      rows = isArray(rows) ? rows : [rows];
      // 循环赋值
      config.sourceFields.forEach((field) => {
        const val = getConfigFieldValue({ rows, paths, field });
        const targetFields = config.fieldMaps.sourceToTarget[field] || [];
        targetFields.forEach((field) => {
          value[field] = val;
        });
      });
      break;
  }
  return value;
}
/**
 * 获得配置项字段的值
 * @param {*} param0
 * @returns
 */
export function getConfigFieldValue({ field, rows, paths }) {
  return rows
    .map((row, index) => {
      if (field.endsWith('_')) {
        return get(paths[index] || {}, field);
      } else {
        return get(row, field) || get(row.bean || {}, field);
      }
    })
    .join(',');
}

/**
 * 查找options数据项对应的text
 * @param {*} param0
 */
export function findOptionsText({ options, value, displayField = 'label', valueField = 'value' }) {
  let dataPromise;
  if (typeof options === 'function') {
    dataPromise = options();
  } else {
    dataPromise = Promise.resolve(options);
  }
  return dataPromise.then((data) => {
    const vals = value ? value.split(',') : [];
    const texts = [];
    vals.forEach((val) => {
      const option = data.find((item) => item[valueField] === val);
      if (option) {
        const text = option[displayField] || val;
        texts.push(text);
      }
    });

    return { data, text: texts.join(',') };
  });
}
