import {
  QueryConnectorEnum,
  QueryOrderEnum,
  FuncFieldTypeEnum,
  QueryConnectorTextEnum,
} from '../enum';
import { isArray, isString, isPlainObject, split, pick } from '@jecloud/utils';
/**
 * 创建查询项
 * @param {*} query
 * @param {*} cn
 * @returns
 */
export function createQueryItem(query, cn = QueryConnectorEnum.AND) {
  const type = query.xtype;
  let value = query.value;
  // in，between关键字
  if (
    [QueryConnectorEnum.IN, QueryConnectorEnum.NOT_IN, QueryConnectorEnum.BETWEEN].includes(type) &&
    isString(value)
  ) {
    value = split(value, ',');
    // 组合查询
  } else if ([QueryConnectorEnum.AND, QueryConnectorEnum.OR].includes(type) && isArray(value)) {
    value = value.map((item) => {
      return createQueryItem(item);
    });
    // 子查询
  } else if (QueryConnectorEnum.IN_SELECT === type && isPlainObject(value)) {
    value.conditions =
      value.conditions?.map?.((item) => {
        return createQueryItem(item);
      }) || [];
  }

  query.value = value;
  query.cn = query.cn || cn;

  // 排序参数
  if (Object.values(QueryOrderEnum).includes(type?.toLocaleLowerCase())) {
    return pick(query, ['code', 'type']);
  }
  return query;
}

/**
 * 获取高级查询字段对应的查询类型
 * @param {string} xtype
 * @returns
 */
export function getGroupQueryTypes4Field(xtype) {
  let queryTypes = [];
  if ([FuncFieldTypeEnum.SELECT.xtype, FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype].includes(xtype)) {
    // 下拉框，单选框，多选框，树形选择
    queryTypes = [
      QueryConnectorEnum.EQ,
      QueryConnectorEnum.IN,
      QueryConnectorEnum.NOT_IN,
      QueryConnectorEnum.NOT_NULL,
    ].map((item) => ({ label: QueryConnectorTextEnum[item], value: item }));
  } else if (
    [
      FuncFieldTypeEnum.INPUT_NUMBER.xtype,
      FuncFieldTypeEnum.DATE_PICKER.xtype,
      FuncFieldTypeEnum.TIME_PICKER.xtype,
    ].includes(xtype)
  ) {
    // 数值框，进度条，日期，时间
    queryTypes = [
      QueryConnectorEnum.BETWEEN,
      QueryConnectorEnum.EQ,
      QueryConnectorEnum.LIKE,
      QueryConnectorEnum.GT,
      QueryConnectorEnum.GE,
      QueryConnectorEnum.LT,
      QueryConnectorEnum.LE,
      QueryConnectorEnum.NOT_IN,
      QueryConnectorEnum.NOT_NULL,
    ].map((item) => ({ label: QueryConnectorTextEnum[item], value: item }));
  } else {
    // 其他字段默认类型
    queryTypes = [
      QueryConnectorEnum.LIKE,
      QueryConnectorEnum.EQ,
      QueryConnectorEnum.IN,
      QueryConnectorEnum.NOT_IN,
      QueryConnectorEnum.NOT_NULL,
    ].map((item) => ({ label: QueryConnectorTextEnum[item], value: item }));
  }

  return queryTypes;
}
