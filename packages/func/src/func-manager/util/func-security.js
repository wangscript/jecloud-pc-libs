import { loadSecurityMicro } from '@jecloud/utils';

/**
 * 初始化功能的密级
 * @param {*} $func
 * @returns
 */
export function initFuncSecurity($func) {
  return loadSecurityMicro({ $func }).then((security) => {
    $func.security = security;
  });
}

/**
 * 初始化密级菜单数据
 * @param {*} param0
 */
export function renderSecurityConfigMenu({ $func, menus }) {
  const { security } = $func;
  return security ? security.renderFuncConfigMenu({ $func, menus }) : menus;
}
/**
 * 字段属性
 * @param {*} param0
 * @returns
 */
export function renderSecurityField({ $func, vnode, field }) {
  $func.security?.renderFuncField({ $func, vnode, field });
}
/**
 * 表单标记
 * @param {*} param0
 * @returns
 */
export function renderSecurityFormMark({ $func }) {
  return $func.security?.renderFormMark({ $func });
}

/**
 * 流程审批人员标记
 * @param {*} param0
 * @returns
 */
export function renderSecurityWorkflowUserMark({ $func, data }) {
  return $func.security?.renderWorkflowUserMark({ $func, data });
}
