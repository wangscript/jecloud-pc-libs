import { watch, nextTick } from 'vue';

/**
 * 监听bean，处理字段表达式
 */
export function useWatchFieldExps({ $func }) {
  // 绑定字段的监听
  const watchFieldExps4Bind = () => {
    const funcData = $func.getFuncData();
    // 解析绑定字段的表达式，并进行监听
    Object.keys(funcData.bindExps).forEach((bindField) => {
      watch(
        () => [bindField, $func.store.activeBean[bindField], $func.store.activeBeanEmitter],
        ([name]) => {
          nextTick(() => {
            $func.action.doFormFieldExps4BindFields({ fieldExps: funcData.bindExps[name] });
          });
        },
      );
    });
  };
  // 所有字段的监听
  const watchFieldExps4All = () => {
    watch(
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      () => {
        nextTick(() => {
          $func.action.doFormFieldExps();
        });
      },
      { deep: true },
    );
  };
  const watchFieldExps = () => {
    watchFieldExps4Bind();
    watchFieldExps4All();
  };

  return { watchFieldExps };
}
