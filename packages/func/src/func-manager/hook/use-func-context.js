import { inject, provide } from 'vue';

export const FuncContextKey = Symbol('jeFuncContextKey');
export const FuncFormContextKey = Symbol('jeFuncFormContextKey');
export const FuncEditContextKey = Symbol('jeFuncEditContextKey');
export const MainFuncContextKey = Symbol('jeMainFuncContextKey');

export const useProvideFunc = (state) => {
  provide(FuncContextKey, state);
};

export const useInjectFunc = () => {
  return inject(FuncContextKey, null);
};
export const useProvideFuncForm = (state) => {
  provide(FuncFormContextKey, state);
};

export const useInjectFuncForm = () => {
  return inject(FuncFormContextKey, null);
};

export const useProvideFuncEdit = (state) => {
  provide(FuncEditContextKey, state);
};

export const useInjectFuncEdit = () => {
  return inject(FuncEditContextKey, null);
};

export const useProvideMainFunc = (state) => {
  provide(MainFuncContextKey, state);
};

export const useInjectMainFunc = () => {
  return inject(MainFuncContextKey, null);
};
