import { FuncButtonTypeEnum } from '../func-manager';

export function doButtonDefaultEvents($func, options) {
  let resultPromise;
  switch (options.type) {
    case 'form':
      resultPromise = doFormButtonEvents($func, options);
      break;
    case 'grid':
      resultPromise = doGridButtonEvents($func, options);
      break;
    case 'action':
      resultPromise = doGridActionButtonEvents($func, options);
      break;
    case 'tree':
      resultPromise = doTreeButtonEvents($func, options);
      break;
  }
  const { button } = options;
  return resultPromise ? resultPromise : Promise.reject(`【${button.text}】按钮事件不存在`);
}

/**
 * 表单按钮事件
 * @param {*} $func
 * @param {*} param1
 */
function doFormButtonEvents($func, { button }) {
  const { doFormBack, doFormSave, doFormAudit } = $func.action;
  switch (button.code) {
    case FuncButtonTypeEnum.FORM_BACK_BUTTON: // 返回
      return doFormBack();
    case FuncButtonTypeEnum.FORM_SAVE_BUTTON: // 保存
      button.loading = true;
      return doFormSave()
        .then((options) => {
          // 错误提示
          if (options?.formError) {
            $func.store.formError = {
              popoverVisible: true,
              errorFields: options.errorFields,
            };
            return Promise.reject(options);
          } else {
            $func.store.formError = {
              popoverVisible: false,
              errorFields: [],
            };
          }
          return options;
        })
        .finally(() => {
          button.loading = false;
        });
    case FuncButtonTypeEnum.FORM_SUBMIT_BUTTON: //审核
      doFormAudit({ button, ackFlag: '1' });
      break;
    case FuncButtonTypeEnum.FORM_CANCEL_BUTTON: //弃审
      doFormAudit({ button, ackFlag: '0' });
      break;
  }
}

/**
 * 列表按钮事件
 * @param {*} $func
 * @param {*} param1
 */
function doGridButtonEvents($func, { button }) {
  const { doGridInsert, doGridRemove, doGridEdit, doGridUpdateList, doGridExport } = $func.action;
  switch (button.code) {
    case FuncButtonTypeEnum.GRID_INSERT_BUTTON: // 添加
      return doGridInsert();
    case FuncButtonTypeEnum.GRID_REMOVE_BUTTON: // 删除
      return doGridRemove();
    case FuncButtonTypeEnum.GRID_UPDATE_BUTTON: // 保存
      button.loading = true;
      return doGridUpdateList()
        .then((options) => {
          // 刷新编号
          const $grid = $func.getFuncGrid();
          $grid.getPlugin().updateSeqConfig({
            currentPage: $grid.store._currentPage,
            pageSize: $grid.store.pageSize,
          });
          button.loading = false;
          return options;
        })
        .catch((errorMaps) => {
          button.loading = false;
          return Promise.reject(errorMaps);
        });
    case FuncButtonTypeEnum.GRID_EDIT_BUTTON: // 编辑
      return doGridEdit();
    case FuncButtonTypeEnum.GRID_EXPORT_BUTTON: // 导出
      return doGridExport();
  }
}
/**
 * 列表Action按钮事件
 * @param {*} $func
 * @param {*} param1
 */
function doGridActionButtonEvents($func, { button, eventOptions }) {
  const { row } = eventOptions;
  const { doGridInsert, doGridRemove, doGridEdit, doGridUpdateList } = $func.action;
  switch (button.code) {
    case FuncButtonTypeEnum.ACTION_INSERT_BUTTON: // 添加
      return doGridInsert();
    case FuncButtonTypeEnum.ACTION_REMOVE_BUTTON: // 删除
      return doGridRemove({ row });
    case FuncButtonTypeEnum.ACTION_UPDATE_BUTTON: // 保存
      return doGridUpdateList({ row });
    case FuncButtonTypeEnum.ACTION_EDIT_BUTTON: // 编辑
      return doGridEdit({ row });
  }
}

/**
 * 树形按钮事件
 * @param {*} $func
 * @param {*} param1
 */
function doTreeButtonEvents($func, { button, eventOptions }) {
  const { doGridInsert, doGridRemove, doGridEdit, doTreeTransfer } = $func.action;
  switch (button.code) {
    case FuncButtonTypeEnum.TREE_INSERT_BUTTON: // 添加
      return doGridInsert();
    case FuncButtonTypeEnum.TREE_REMOVE_BUTTON: // 删除
      return doGridRemove({ removeType: 'tree', ...eventOptions });
    case FuncButtonTypeEnum.TREE_EDIT_BUTTON: // 编辑
      return doGridEdit();
    case FuncButtonTypeEnum.TREE_TRANSFER_BUTTON: // 转移
      return doTreeTransfer(eventOptions);
  }
}
