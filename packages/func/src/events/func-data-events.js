import { getEventTargetNode } from '@jecloud/utils';
import { FuncRefEnum, doEvents4Sync } from '../func-manager';
import { showFuncColumnConfig } from '../func-config/hooks/use-func-config';
/**
 * 功能数据面板事件
 * @param {*} options
 * @returns
 */
export function useFuncDataEvents({ $func }) {
  return {
    onGridCellClick: onGridCellClick.bind(this, $func),
    onGridCellDblclick: onGridCellDblclick.bind(this, $func),
    onGridSort: onGridSort.bind(this, $func),
    onTreeSelectChange: onTreeSelectChange.bind(this, $func),
    onGridHeaderCellClick: onGridHeaderCellClick.bind(this, $func),
    onGridHeaderCellDblclick: onGridHeaderCellDblclick.bind(this, $func),
  };
}
/**
 * 列表双击表头进入列表字段配置
 * @param {*} param0
 * @param {*} options
 */
function onGridHeaderCellDblclick($func, options) {
  const { column } = options;
  showFuncColumnConfig({ $func, column });
}
/**
 * 列表单击表头
 * @param {*} $func
 * @param {*} options
 */
function onGridHeaderCellClick($func, options) {}
/**
 * 列表双击进入表单
 * @param {*} param0
 * @param {*} options
 */
function onGridCellDblclick($func, options) {
  const { row, column } = options;
  const $grid = $func.getFuncGrid();
  // 编辑状态不允许双击进表单
  if (!$grid.isEditByRow(row) && !['checkbox', 'switch'].includes(column.type)) {
    $func?.action.doGridEdit({ row, column });
  }
}
/**
 * 列表单击事件
 * @param {*} param0
 * @param {*} options
 */
function onGridCellClick($func, options) {
  const { column, $event, cell, row } = options;
  if (!getEventTargetNode($event, cell, 'je-column-link-cell').flag) return;

  const funcData = $func.getFuncData();
  const funcColumn = funcData.basic.columns.get(column.field);
  // 超链接事件
  if (funcColumn?.link.event) {
    doEvents4Sync({
      $func,
      eventOptions: options,
      code: funcColumn?.link.event,
    });
  } else if (funcColumn?.link.enable) {
    // 启用超链接进入表单
    $func.action.doGridEdit({ row, link: funcColumn.link });
  }
}
/**
 * 列表排序
 * @param {*} $func
 * @param {*} options
 */
function onGridSort($func, options) {
  const $grid = $func.getFuncGrid();
  $grid.store.data.forEach((item, index) => {
    item.SY_ORDERINDEX = index + 1;
  });
}

/**
 * 左侧树形查询
 * @param {*} param0
 * @param {*} options
 */
export function onTreeSelectChange($func, options) {
  const $tree = $func.getRefMaps(FuncRefEnum.FUNC_TREE);
  const $grid = $func.getRefMaps(FuncRefEnum.FUNC_GRID);
  const { records } = options;
  // 展开所有节点，再进行查询
  if ($tree.value.async && !$tree.value.multiple) {
    // 异步单选，需要展开子节点
    $tree.value
      .getPlugin()
      .setTreeExpandDeep(records, true)
      .then(() => {
        $grid.value.setQuerys({ type: 'tree', querys: records, $tree: $tree.value });
      });
  } else {
    $grid.value.setQuerys({ type: 'tree', querys: records, $tree: $tree.value });
  }
}
