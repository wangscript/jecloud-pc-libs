import { useFunc } from './use-func';
export function useFuncSelect({ props, context }) {
  // 查询选择配置
  const { selectOptions, funcCode } = props;
  const { configInfo } = selectOptions;
  // 功能编码
  const code = funcCode ? funcCode : configInfo?.split(',')[0];
  // 功能对象
  const { $func } = useFunc({ funcCode: code, props, context });
  // 追加功能信息
  Object.assign($func, {
    type: 'select',
  });

  return { $func };
}
