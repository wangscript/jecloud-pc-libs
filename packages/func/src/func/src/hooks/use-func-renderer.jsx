import { h, ref, resolveDynamicComponent, watch } from 'vue';
import { isString } from '@jecloud/utils';
import { Panel } from '@jecloud/ui';
import { FuncRefEnum } from '../../../func-manager';
import { useFuncTree } from './use-func-tree';

export function useFuncRenderer({ props, context, $func }) {
  const { slots } = context;
  const { funcCode, store } = $func;
  const { funcTreeSlot } = useFuncTree({ $func, props, editable: true }); // 功能树
  const children = [
    { code: FuncRefEnum.FUNC_DATA, component: 'JeFuncData', ref: ref() },
    { code: FuncRefEnum.FUNC_EDIT, component: 'JeFuncEdit', ref: ref() },
  ];
  children.forEach((child) => {
    $func.setRefMaps(child.code, child.ref);
  });
  // 设置渲染标记
  watch(
    () => store.activeView,
    () => {
      children.find((item) => item.code === store.activeView).rendered = true;
    },
    { immediate: true },
  );

  // 功能插槽
  const funcSlot = function () {
    return children.map((child) =>
      store.activeView === child.code || child.rendered
        ? h(resolveDynamicComponent(child.component), {
            funcCode,
            key: child.code,
            ref: child.ref,
            style: { display: store.activeView === child.code ? undefined : 'none' },
          })
        : null,
    );
  };
  // 功能标题插槽
  const titleSlot = function () {
    const funcData = $func.getFuncData();
    return props.title ? (
      <Panel.Item class="je-func-title" region="top">
        <div class="je-func-title-content">
          {slots.title?.({ $func }) ?? [
            <i class={funcData.info.icon} />,
            isString(props.title) ? props.title : funcData.info.funcName,
          ]}
        </div>
      </Panel.Item>
    ) : null;
  };

  return { funcSlot, titleSlot, funcTreeSlot };
}
