import { defineComponent, ref } from 'vue';
import { Hooks } from '@jecloud/ui';
import FuncGrid from '../../func-grid';
import { useFuncChildrenField } from '../hooks/use-func-children-field';

export default defineComponent({
  name: 'JeFuncChildrenField',
  inheritAttrs: false,
  props: {
    field: Object,
    funcCode: String,
    fkCode: String,
    width: Number,
    height: Number,
    disabled: Boolean,
    readonly: Boolean,
    actionColumn: Object,
    model: Object,
    formView: Object,
    isFuncField: { type: Boolean, default: true },
    maximize: Boolean,
  },
  emits: ['change', 'private-change'],
  setup(props, context) {
    const { attrs } = context;
    const {
      $grid,
      loading,
      onGridRendered,
      onGridHeaderCellDblclick,
      editable,
      multiple,
      drag,
      errorMessage,
    } = useFuncChildrenField({
      props,
      context,
    });
    const style = { height: '200px', ...Hooks.useStyle4Size({ props }) };
    // 全屏操作
    const fullScreen = ref(false);

    return () => {
      return loading.value ? (
        <div v-loading={loading.value} style={style} />
      ) : props.funcCode ? (
        <FuncGrid
          ref={$grid}
          class={{
            'je-func-children-field': true,
            'is--fullscreen': fullScreen.value,
            'is--disabled': !editable.value,
          }}
          {...style}
          {...attrs}
          fkCode={props.fkCode}
          readonly={!editable.value}
          multiple={editable.value && multiple}
          enableSort={editable.value && drag}
          onHeaderCellDblclick={onGridHeaderCellDblclick}
          onRendered={onGridRendered}
          funcCode={props.funcCode}
          pagerConfig={false}
          pageSize={-1}
          size="mini"
          v-slots={{
            tbar: () => {
              // 标题
              return fullScreen.value ? (
                <div class="je-func-children-field-tbar">
                  <span class="title">{props.field.label}</span>
                  <i
                    class="jeicon jeicon-times close"
                    onClick={() => {
                      fullScreen.value = false;
                    }}
                  />
                </div>
              ) : null;
            },
            bbar: () => {
              // 打开全屏
              return !props.maximize || fullScreen.value ? null : (
                <div class="je-func-children-field-bbar">
                  {errorMessage.value ? (
                    <span class="ant-form-item-explain-error">{errorMessage.value}</span>
                  ) : null}
                  <span
                    class="open"
                    onClick={() => {
                      fullScreen.value = true;
                    }}
                  >
                    打开数据面板
                  </span>
                </div>
              );
            },
          }}
        />
      ) : (
        <div class="no-configinfo">请前往子功能集合进行配置！</div>
      );
    };
  },
});
