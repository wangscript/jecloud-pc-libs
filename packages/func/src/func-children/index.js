import JeFuncChildren from './src/func-children';
import JeFuncChildrenHorizontal from './src/func-children-horizontal';
import JeFuncChildrenVertical from './src/func-children-vertical';
import JeFuncChildrenGroup from './src/func-children-group';
import JeFuncChildrenField from './src/func-children-field';
import JeFuncChildrenFile from './src/func-children-file';
import { withInstall } from '../utils';

JeFuncChildren.installComps = {
  // 注册依赖组件
  Horizontal: { name: JeFuncChildrenHorizontal.name, comp: JeFuncChildrenHorizontal },
  Vertical: { name: JeFuncChildrenVertical.name, comp: JeFuncChildrenVertical },
  Group: { name: JeFuncChildrenGroup.name, comp: JeFuncChildrenGroup },
  FuncField: { name: JeFuncChildrenField.name, comp: JeFuncChildrenField },
  FuncFile: { name: JeFuncChildrenFile.name, comp: JeFuncChildrenFile },
};
export const FuncChildren = withInstall(JeFuncChildren);
export default FuncChildren;
