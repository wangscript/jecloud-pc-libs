/* eslint-disable no-inner-declarations */
import { ref, computed, watch, nextTick } from 'vue';
import { FuncRefEnum, FuncButtonTypeEnum } from '../../func-manager/enum';
import { useFunc } from '../../func/src/hooks/use-func';
import { useInjectFunc } from '../../hooks';
import { isEmpty, isPromise, isNotEmpty, toQuerysTemplate, decode, debounce } from '@jecloud/utils';
import { Form, Hooks } from '@jecloud/ui';
import { useFuncDataEvents } from '../../events/func-data-events';
import { loadFuncInfoApi } from '../../func-manager';
import { Form as AForm } from 'ant-design-vue';

export function useFuncChildrenField({ props, context }) {
  const { expose, emit } = context;
  const $grid = ref();
  const loading = ref(true);
  const editable = computed(() => {
    return !(props.readonly || props.disabled);
  });
  const $parentFunc = useInjectFunc();
  const formItemContext = Form.useInjectFormItemContext();
  const errorMessage = ref('');
  const { fireListener } = Hooks.useListeners({ props });
  const { $func } = useFunc({ funcCode: props.funcCode, props, context });
  const { doGridInsert4Grid } = $func.action;
  // 事件
  $func.entry = 'children-func';
  const { onGridHeaderCellDblclick } = useFuncDataEvents({
    $func,
  });
  $func.resetFunc = () => {
    // 修改中的数据，不做处理，业务自行处理
    if (!$func.doUpdateListIng) {
      $grid.value?.reset();
    }
  };
  const $childField = {
    $func,
    validator(rule = {}) {
      if (rule.required && isEmpty($grid.value.store.data)) {
        errorMessage.value = rule.message;
        return Promise.reject(rule.message);
      } else {
        errorMessage.value = '';
        return $grid.value.validate(true);
      }
    },
    clearValidate() {
      errorMessage.value = '';
    },
    /**
     * 添加数据
     * @param {*} param0
     */
    addRecords({ records = [], useDefaultValues = true }) {
      let data = [];
      // 增加默认值
      if (useDefaultValues) {
        const values = $func.getDefaultValues({
          buttonCode: FuncButtonTypeEnum.GRID_INSERT_BUTTON,
        });
        records.forEach((record) => {
          data.push({ ...values, ...record });
        });
      } else {
        data = records;
      }
      if (data.length) {
        // 添加数据
        return doGridInsert4Grid({ records: data }).then(({ insertRecords }) => {
          // 排序
          const $grid = $func.getFuncGrid();
          $grid.store.data.forEach((item, index) => {
            item.SY_ORDERINDEX = index + 1;
          });
          return insertRecords;
        });
      }
      return Promise.resolve([]);
    },
    /**
     * action事件
     * @param {*} eventName
     * @param {*} param1
     * @returns
     */
    doActionEvent(eventName, { actionType, actionOptions }) {
      const result =
        fireListener(eventName, {
          $fieldFunc: $func,
          $fieldGrid: $func.getFuncGrid(),
          actionType,
          actionOptions: actionOptions || {},
        }) !== false;
      if (isPromise(result)) {
        return result;
      } else {
        return result !== false ? Promise.resolve() : Promise.reject();
      }
    },
  };
  formItemContext?.addSlotItem($childField);
  expose($childField);
  // 重置FormItem，防止列表编辑组建触发本子功能集合的校验函数
  AForm.useInjectFormItemContext();
  const { actionColumn = {} } = props;
  const multiple = actionColumn.removes == '1';
  const drag = actionColumn.drag == '1';
  $func.setRefMaps(FuncRefEnum.FUNC_GRID, $grid);
  // 加载功能数据
  props.funcCode
    ? $func.loadFuncData().then(() => {
        loading.value = false;
      })
    : (loading.value = false);
  // 加载操作列
  const onGridRendered = () => {
    parseActionColumn({
      $func,
      $parentFunc,
      props,
      editable,
      fireListener,
      $childField,
    });
    $grid.value.store.on('before-load', () => {
      // 重置滚动条
      $grid.value?.clearScroll();
    });
  };
  watch(
    () => $grid.value?.store.data,
    // 防止频繁操作
    debounce(() => {
      nextTick(() => {
        const options = {
          $field: $childField,
          $fieldFunc: $func,
          $fieldGrid: $grid.value,
        };
        emit('change', options);
        // 用于功能业务事件处理，用户使用上面的change事件
        emit('private-change', options);
      });
    }, 200),
    {
      deep: true,
    },
  );

  // 批量修改
  $func.doUpdateList = (bean) => {
    doUpdateList({ $func, bean, props, $childField });
  };

  // 追加子功能集合字段
  $parentFunc?.addChildFuncField(props.funcCode, $func);

  //监听是否可编辑
  watch(
    () => editable.value,
    (editable) => {
      if ($grid.value) {
        ShowHideActionColumn({ $grid: $grid.value, editable, props });
      }
    },
  );

  return {
    $func,
    $grid,
    onGridRendered,
    onGridHeaderCellDblclick,
    loading,
    editable,
    multiple,
    errorMessage,
    drag,
  };
}
/**
 * 批量保存和修改
 * @param {*} param0
 */
function doUpdateList({ $func, bean, props, $childField }) {
  const { doGridUpdateList } = $func.action;
  const $grid = $func.getFuncGrid();
  if ($grid && bean) {
    // 增加修改标记
    $func.doUpdateListIng = true;
    const parentFuncData = $func.parentFunc.getFuncData();
    const parentPkCode = parentFuncData.info.pkCode;
    const parentPkValue = $func.parentFunc.store.activeBean?.[parentPkCode];
    const { values = {} } = $func.store.getParentInfo(bean);
    // 外键
    values[props.fkCode] = bean[parentFuncData.info.pkCode];
    $grid.store.data.forEach((item) => {
      // 直接使用子功能带值配置信息
      Object.assign(item, values);
    });

    // 添加操作
    const doInsert = isEmpty(parentPkValue);
    // 操作事件
    const { doActionEvent } = $childField;
    const actionType = 'update';
    return doActionEvent(ActionEvents.BEFORE_ACTION, {
      actionType,
    })
      .then(() => doGridUpdateList({ showMessage: false }))
      .then((options) => {
        // 清除修改标记
        $func.doUpdateListIng = false;
        // 添加操作完成，刷新列表
        if (doInsert) {
          $func.resetFunc();
        }
        // 刷新序号
        $grid.getPlugin().updateSeqConfig({
          currentPage: $grid.store._currentPage,
          pageSize: $grid.store.pageSize,
        });
        // 操作后事件
        doActionEvent(ActionEvents.AFTER_ACTION, {
          actionType,
          actionOptions: options,
        });
      });
  }
  return Promise.resolve();
}
/**
 * 事件名称
 */
const ActionEvents = {
  BEFORE_ACTION: 'before-action',
  AFTER_ACTION: 'after-action',
};

/**
 * 解析操作列，子功能集合使用
 */
function parseActionColumn({ $func, $parentFunc, $childField, props, editable }) {
  // 监听是否可编辑 TODO 逻辑改变,默认都会添加atcion列,封装ShowHideActionColumn方法根据配置进行列的显隐
  //if (!editable.value) return;
  const $grid = $func.getFuncGrid();
  const { actionColumn = {} } = props;
  const {
    buttonAlign = 'right',
    remove = '1',
    removes = '0',
    add = '1',
    adds = '0',
    addsType = 'grid',
    addsConfigInfo,
    addsOtherConfig,
  } = actionColumn;

  // 操作事件
  const { doActionEvent } = $childField;

  const getAddsQuerys = () => {
    const querys = { gridQuerys: [], treeQuerys: [] };
    if (isNotEmpty(addsOtherConfig)) {
      // 如果配置了过滤条件
      if (isNotEmpty(addsOtherConfig.querys)) {
        const configQuerys = decode(addsOtherConfig.querys);
        // 父功能的父功能数据
        const parentModel = $parentFunc?.parentFunc?.store.activeBean || {};
        toQuerysTemplate({
          querys: configQuerys,
          model: props.model,
          parentModel,
        });
        querys.gridQuerys.push(...configQuerys);
        querys.treeQuerys.push(...configQuerys);
      }
      // 如果配置了唯一主键值
      if (isNotEmpty(addsOtherConfig.pkNames)) {
        const tableData = $grid.store.data || [];
        if (tableData.length > 0) {
          const values = [];
          tableData.forEach((item) => {
            values.push(item[addsOtherConfig.pkNames[1]]);
          });
          if (values.length > 0) {
            querys.gridQuerys.push({
              code: addsOtherConfig.pkNames[0],
              type: 'notIn',
              value: values,
            });
          }
        }
      }
    }
    return querys;
  };

  // 点击事件
  const onClick = (options) => {
    const { doGridRemove, doGridInsert4Multi } = $func.action;
    const { actionType } = options;
    switch (actionType) {
      case 'add': // 添加
        // 默认值
        const values = $func.getDefaultValues({
          buttonCode: FuncButtonTypeEnum.GRID_INSERT_BUTTON,
        });
        doActionEvent(ActionEvents.BEFORE_ACTION, { actionType, actionOptions: { values } }).then(
          () => {
            $childField
              .addRecords({ records: [values], useDefaultValues: false })
              .then((insertRecords) => {
                doActionEvent(ActionEvents.AFTER_ACTION, {
                  actionType,
                  actionOptions: { records: insertRecords },
                });
              });
          },
        );
        break;
      case 'adds': // 批量添加
        function commonFunction(title) {
          const selectConfig = {
            type: addsType,
            configInfo: addsConfigInfo,
            ...getAddsQuerys(),
            title,
          };
          doActionEvent(ActionEvents.BEFORE_ACTION, {
            actionType,
            actionOptions: selectConfig,
          }).then(() => {
            doGridInsert4Multi(selectConfig).then(({ records }) => {
              $childField.addRecords({ records, useDefaultValues: false }).then((insertRecords) => {
                doActionEvent(ActionEvents.AFTER_ACTION, {
                  actionType,
                  actionOptions: { records: insertRecords },
                });
              });
            });
          });
        }
        // 为了获取子功能集合功能的名称，如果调取接口成功，则将title传过去，否则为空，使用默认的名称，批量添加
        // 通过子功能配置获取code
        const childrenFuncCode = (addsConfigInfo && addsConfigInfo.split(',')) || [];
        if (childrenFuncCode && childrenFuncCode.length) {
          loadFuncInfoApi({ funcCode: childrenFuncCode[0] })
            .then((res) => {
              commonFunction(res.FUNCINFO_FUNCNAME);
            })
            .catch((err) => {
              commonFunction();
            });
        } else {
          commonFunction();
        }

        break;
      case 'remove': // 删除
        const { row } = options;
        doActionEvent(ActionEvents.BEFORE_ACTION, { actionType, actionOptions: { row } }).then(
          () => {
            doGridRemove({ row }).then(() => {
              doActionEvent(ActionEvents.AFTER_ACTION, { actionType, actionOptions: { row } });
            });
          },
        );
        break;
      case 'removes': // 清空
        doActionEvent(ActionEvents.BEFORE_ACTION, { actionType }).then(() => {
          doGridRemove().then(() => {
            doActionEvent(ActionEvents.AFTER_ACTION, { actionType });
          });
        });
        break;
    }
  };
  // 操作按钮数量，计算列宽使用
  const count = [add == '1', adds == '1' && addsType && addsConfigInfo, removes == '1'].filter(
    (item) => item,
  ).length;
  // 操作列
  const column = {
    width: count > 1 ? count * 20 + 22 : 42,
    resizable: false,
    align: 'center',
    field: 'actionColumn',
    headerAlign: 'center',
    className: 'je-func-children-field-action-cell',
    headerClassName: 'je-func-children-field-action-header',
    slots: {
      header: () => {
        const buttons = [];
        // 添加
        if (add == '1') {
          buttons.push(
            <i
              class="action-item green fas fa-plus-circle"
              onClick={() => onClick({ actionType: 'add' })}
            ></i>,
          );
        }
        // 批量添加
        if (adds == '1' && addsType && addsConfigInfo) {
          buttons.push(
            <i
              class="action-item green fas fa-list-ul"
              onClick={() => onClick({ actionType: 'adds', addsType, addsConfigInfo })}
            ></i>,
          );
        }
        // 清空
        if (removes == '1') {
          buttons.push(
            <i
              class="action-item red fas fa-trash-alt"
              onClick={() => onClick({ actionType: 'removes' })}
            ></i>,
          );
        }
        return buttons;
      },
      default: (options) => {
        // 删除按钮
        return remove == '1' ? (
          <i
            class="action-item red fas fa-times-circle"
            onClick={() => onClick({ actionType: 'remove', ...options })}
          ></i>
        ) : null;
      },
    },
  };
  // 操作列
  // TODO 逻辑改变,默认都会添加atcion列,封装ShowHideActionColumn方法根据配置进行列的显隐
  // if (remove == '1' || removes == '1' || add == '1' || adds == '1') {
  // 按钮位置
  const { collectColumn } = $grid.getTableColumn();
  if (buttonAlign === 'right') {
    column.fixed = 'right';
    collectColumn.push(column);
  } else {
    column.fixed = 'left';
    collectColumn.unshift(column);
  }
  $grid.loadColumn(collectColumn);
  //}
  ShowHideActionColumn({ $grid, editable: editable.value, props });
}

/**
 * 显隐action列
 */
function ShowHideActionColumn({ $grid, editable, props }) {
  if ($grid) {
    // 获得配置项
    const { actionColumn = {} } = props;
    const { remove = '1', removes = '0', add = '1', adds = '0' } = actionColumn;
    // 获得action列
    const actionColumnObj = $grid.getColumnByField('actionColumn');
    // 如果组件可编辑,并且有操作按钮,action列就显示,否则隐藏
    if (editable && (remove == '1' || removes == '1' || add == '1' || adds == '1')) {
      $grid.showColumn(actionColumnObj);
    } else {
      $grid.hideColumn(actionColumnObj);
    }
  }
}
