import { FuncChildrenLayoutEnum } from '../../func-manager/enum';
import { useFuncCommon, useInjectFunc } from '../../hooks';
import { ref, resolveDynamicComponent, h } from 'vue';
import { pick } from '@jecloud/utils';
export function useFuncChildren({ props, context }) {
  const { layout, funcCode, groupName } = props;
  const $child = useFuncCommon({ props, context, funcCode });
  const funcComponent = 'JeFunc';
  const $func = useInjectFunc();
  const funcData = $func.getFuncData();
  // 子功能数据
  const children = $func.getChildFuncConfig();
  // 解析符合布局的子功能数据
  const layoutChildren = ref(
    children.filter((child) => {
      return groupName
        ? child.groupName === groupName // 分组布局，不考虑其他布局
        : (child.layout || funcData.info.childLayout) === layout; // 其他布局
    }) ?? [],
  );

  // 装载表单
  $child.setupForm = () => {
    // 增加表单组件
    const { form } = props;
    if (form) {
      const funcData = $func.getFuncData();
      // 表单标题设置
      const { title } = funcData.info.formOptions;
      form.title = title || form.title;
      $child.setRefMaps(form.code, form.ref);
    }
    return form;
  };
  // 功能默认高度
  const defaultHeight =
    funcData.info.childHeight?.find((item) => item.code === 'default')?.value || 400;
  // 计算高度
  const computeHeight = (height) => {
    if (FuncChildrenLayoutEnum.FORM_OUTER_HORIZONTAL === props.layout) {
      return '100%';
    } else if (FuncChildrenLayoutEnum.FORM_INNER_VERTICAL === props.layout) {
      return Math.max(height, defaultHeight) + 'px';
    } else {
      let maxHeight = 0;
      layoutChildren.value.forEach((item) => {
        maxHeight = Math.max(maxHeight, item.height);
      });
      // 标题高度，表单内部，列表内部使用
      const titleHeight = 38;
      const borderWidth = 2;
      return Math.max(maxHeight, defaultHeight) + titleHeight + borderWidth + 'px';
    }
  };

  // 子项插槽
  const childItemSlot = (child, options) => {
    // 公共属性
    const attrs = {
      key: child.code,
      ref: $child.getRefMaps(child.code),
      'data-code': child.code,
      'data-func': props.funcCode,
      'data-anchor': child.anchor || undefined,
    };
    switch (child.type) {
      case 'file': // 附件
        return h(resolveDynamicComponent('JeFuncChildrenFile'), {
          ...attrs,
          ...pick(options, ['style', 'class']),
        });
      case 'vertical': // 纵向布局分组
        return h(resolveDynamicComponent('JeFuncChildrenVertical'), {
          groupName: child.code,
          groupLayout: true,
          ...attrs,
          ...pick(options, ['style', 'class']),
        });
      default: // 普通功能
        return h(resolveDynamicComponent(child.component ?? funcComponent), {
          funcCode: child.code,
          readonly: child.readonly,
          model: $func.store.activeBean,
          isFuncForm: child.isFuncForm,
          loadFirstData: child.isFuncForm,
          ...child.events,
          ...attrs,
          ...options,
        });
    }
  };

  return {
    children: layoutChildren,
    $child,
    funcComponent,
    $func,
    funcData,
    childItemSlot,
    computeHeight,
  };
}
