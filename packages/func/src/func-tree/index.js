import JeFuncTree from './src/tree';
import { withInstall } from '../utils';
export const FuncTree = withInstall(JeFuncTree);
export default FuncTree;
