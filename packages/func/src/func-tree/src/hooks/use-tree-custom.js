import { useBase } from './use-base';
import { useTreeStore } from './use-tree-store';
import { TreeQueryParser } from '../../../func-manager/model';
import { FuncTreeTypeEnum } from '../../../func-manager/enum';
import { API_COMMON_GET_TREE, parseCommonUrl } from '../../../func-manager';
/**
 * 自定义接口树
 */
export function useTreeCustom({ props, context }) {
  const { url = parseCommonUrl(API_COMMON_GET_TREE), product, params } = props;
  const headers = { pd: product };
  const queryParser = new TreeQueryParser({ type: FuncTreeTypeEnum.CUSTOM });

  const { $tree, treeSlot } = useBase({ props, context });
  const store = useTreeStore({ url, headers, params, queryParser, $tree });
  $tree.setStore(store);
  return { $tree, treeSlot };
}
