import { useBase } from './use-base';
import { useRenderer } from './use-renderer';
import { useTreeStore } from './use-tree-store';
import { TreeQueryParser } from '../../../func-manager/model';
import { FuncTreeTypeEnum } from '../../../func-manager/enum';
import { useInjectFunc } from '../../../hooks';
import { encode } from '@jecloud/utils';
import { API_COMMON_GET_TREE, parseCommonTreeUrl } from '../../../func-manager';
/**
 * 功能树
 */
export function useTreeFunc({ props, context }) {
  const { funcCode, onlyItem } = props;
  const $func = useInjectFunc();
  let url = parseCommonTreeUrl(API_COMMON_GET_TREE);
  const headers = {};
  const queryParser = new TreeQueryParser({
    type: FuncTreeTypeEnum.FUNC,
    funcCode,
    querys: props.querys,
  });
  const funcData = $func?.getFuncData();
  if (funcData) {
    const { productCode, tableCode, funcAction } = funcData.info;
    queryParser.tableCode = tableCode;
    headers.pd = productCode;
    queryParser.initQuerys({ $func });
    url = parseCommonTreeUrl(API_COMMON_GET_TREE, funcAction);
  }

  const { $tree, treeSlot } = useBase({ props, context });
  const store = useTreeStore({ url, headers, queryParser, onlyItem, $func, $tree });
  $tree.setStore(store);
  $tree.mixin({
    reset() {
      const params = queryParser.initQuerys({ $func });
      store.load({ params: { j_query: encode(params) } });
    },
  });
  return { $tree, treeSlot };
}
