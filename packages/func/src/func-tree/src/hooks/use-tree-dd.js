import { useBase } from './use-base';
import { useRenderer } from './use-renderer';
import { useTreeStore } from './use-tree-store';
import { TreeQueryParser } from '../../../func-manager/model';
import { FuncTreeTypeEnum } from '../../../func-manager/enum';
import { useInjectFunc } from '../../../hooks';
import { API_DICTIONARY_LOAD_TREE } from '../../../func-manager';
/**
 * 字典树
 */
export function useTreeDD({ props, context }) {
  const $func = useInjectFunc();
  const url = API_DICTIONARY_LOAD_TREE;
  const { $tree, treeSlot } = useBase({ props, context });
  let { store, onlyItem } = props;
  if (!store) {
    const queryParser = new TreeQueryParser({
      type: FuncTreeTypeEnum.DD,
      dictionarys: props.dictionarys,
      onlyItem,
    });

    store = useTreeStore({ url, queryParser, onlyItem, $func, $tree });
  }
  $tree.setStore(store);
  return { $tree, treeSlot };
}
