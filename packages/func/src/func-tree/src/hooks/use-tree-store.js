import { Data } from '@jecloud/ui';
import { isNotEmpty, toQuerysTemplate, cloneDeep } from '@jecloud/utils';
export function useTreeStore({ url, headers, params, queryParser, onlyItem, $func, $tree }) {
  const store = Data.Store.useTreeStore({
    autoLoad: false,
    proxy: {
      type: 'ajax',
      url,
      headers,
      params: { ...params, ...queryParser.toParams() },
    },
  });

  store.queryParser = queryParser;
  // 异步节点加载
  store.loadNode = function (node) {
    const params = store.queryParser.toParams({
      nodeInfo: node.nodeInfo,
      rootId: node.id.split('_')[0],
    });
    return store.proxy.read({ params }).then((data) => {
      return store.appendChild(node.id, data);
    });
  };
  // 查询前解析查询条件
  store.on('before-load', function ({ options }) {
    options.params = options.params ?? {};
    let parentModel = {};
    if ($func && isNotEmpty($func.parentFunc)) {
      parentModel = $func.parentFunc.store.activeBean || {};
    }
    // 过滤条件参数处理
    if (isNotEmpty(queryParser.dictionarys) && $func) {
      queryParser.dictionarys.forEach((item) => {
        if (isNotEmpty(item.querys)) {
          toQuerysTemplate({ querys: item.querys, useModel: false, parentModel });
        }
        if (isNotEmpty(item.queryParser)) {
          toQuerysTemplate({ querys: item.queryParser?.basicQuerys, useModel: false, parentModel });
        }
        // 字典过滤数据
        if (isNotEmpty(item.dicValueConfig)) {
          item.newDicValueConfig = cloneDeep(item.dicValueConfig);
          toQuerysTemplate({ querys: item.newDicValueConfig, useModel: false, parentModel });
          const customVariables = {};
          item.newDicValueConfig.forEach((item) => {
            if (item) {
              customVariables[item.code] = item.value;
            }
          });
          if (isNotEmpty(customVariables)) {
            item.customVariables = customVariables;
          }
        }
      });
      if (options.params && options.params.strData) {
        delete options.params.strData;
      }
    }

    // 默认参数
    options.params = Object.assign(queryParser.toParams(), options.params);
  });
  // 默认展开第一级
  store.on('load', () => {
    // 1. 快速查询树，默认展开第一层
    // 2. 其他树，第一层有一条数据时展开，多条不展开
    if (!onlyItem || store.data.length === 1) {
      $tree.setTreeExpand(store.data, true);
    }
  });
  return store;
}
