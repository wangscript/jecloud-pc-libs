import { defineComponent } from 'vue';
import { Panel } from '@jecloud/ui';
import { useFuncDataSelect } from './hooks/use-func-data-select';

export default defineComponent({
  name: 'JeFuncDataSelect',
  inheritAttrs: false,
  props: {
    funcCode: String,
    selectOptions: Object,
  },
  setup(props, context) {
    const { attrs } = context;
    // 树形，列表
    const { gridSlot, treeSlot, resultSlot } = useFuncDataSelect({
      props,
      context,
    });
    return () => (
      <Panel {...attrs} class="je-func-data-select">
        {/* 树形查询 */}
        {treeSlot()}
        {/* 列表，高级查询 */}
        {gridSlot()}
        {/* 多选选中结果 */}
        {resultSlot()}
      </Panel>
    );
  },
});
