import { defineComponent } from 'vue';
import { Panel } from '@jecloud/ui';
import { useFuncData } from './hooks/use-func-data';
import { useExtendPanel } from './hooks/use-extend-panel';

export default defineComponent({
  name: 'JeFuncData',
  inheritAttrs: false,
  props: {
    funcCode: String,
    orders: Array,
    querys: Array,
  },
  setup(props, context) {
    const { attrs } = context;
    // 树形，列表
    const { gridSlot, treeSlot } = useFuncData({
      props,
      context,
    });
    // 扩展面板
    const { extendPanelInner, extendPanelOuter } = useExtendPanel();
    return () => (
      <Panel {...attrs} class="je-func-data">
        {/* 扩展面板 外部 */}
        {extendPanelOuter()}
        <Panel class="je-func-data-center">
          {/* 树形查询 */}
          {treeSlot()}
          <Panel>
            {/* 列表，高级查询 */}
            {gridSlot()}
            {/* 扩展面板 内部 */}
            {extendPanelInner()}
          </Panel>
        </Panel>
      </Panel>
    );
  },
});
