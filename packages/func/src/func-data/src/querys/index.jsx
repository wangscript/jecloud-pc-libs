import { useStrategyQuery } from './hooks/use-strategy-query';
import { useKeywordQuery } from './hooks/use-keyword-query';
export function useQuerys(options) {
  return { ...useStrategyQuery(options), ...useKeywordQuery(options) };
}
