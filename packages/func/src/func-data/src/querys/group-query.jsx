import { defineComponent, ref } from 'vue';
import { useGroupQuery } from './hooks/use-group-query';

export default defineComponent({
  name: 'JeFuncGroupQuery',
  inheritAttrs: false,
  props: {},
  emits: [],
  setup(props, context) {
    const { groupQuerySlot } = useGroupQuery({ props, context });
    return () => groupQuerySlot();
  },
});
