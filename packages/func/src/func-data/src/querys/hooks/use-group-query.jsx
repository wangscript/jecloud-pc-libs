import { Panel, Button, Form, Row, Col } from '@jecloud/ui';
import { debounce, isEmpty } from '@jecloud/utils';
import { useInjectFunc } from '../../../../hooks';
import { useProvideFuncTopQuery } from './use-query-context';
import QueryItem from '../group-query-item';
import { reactive, watch, ref } from 'vue';
import { QueryTypeEnum } from '../../../../func-util';
import { FuncGroupQueryView } from '../../../../func-manager';
import { useInjectFuncGrid } from '../../../../func-grid/src/context.js';
/**
 * 高级查询
 * @returns
 */

export function useGroupQuery({ props, context }) {
  const { expose } = context;
  const $func = useInjectFunc();
  const $grid = useInjectFuncGrid();
  const funcData = $func.getFuncData();
  const $plugin = ref();
  const $groupQuery = new FuncGroupQueryView({
    $func,
    props,
    context,
    pluginRef: $plugin,
    panelItem: Panel.useInjectPanelItem(),
  });

  const { info, groupQuerys } = funcData;
  const { groupQueryOptions } = info;
  const { layout, cols, strategy, changeQuery, enableQuery, labelWidth } = groupQueryOptions;
  const resting = ref(false);
  // 查询modal
  const { model, formModel } = $groupQuery;
  // 操作方法
  const { doQuery, doReset, doSaveQuerys, doSort } = useGroupAction({ $grid, model, resting });

  $groupQuery.mixin({
    doQuery,
    doReset,
    doSort,
  });
  groupQuerys.forEach((field) => {
    $groupQuery.createQueryItem(field);
    // 值改变，进行查询
    if (changeQuery) {
      watch(
        () => [model[field.name].type, model[field.name].value, model[field.name].betweenValue],
        () => {
          // 重置不需要查询
          !resting.value && doQuery();
        },
      );
      // 更新formModel
      watch(
        () => [model[field.name].values],
        ([value]) => {
          Object.assign(formModel, value);
        },
        { deep: true },
      );
    }
  });
  // 格栅布局
  const gridLayout = layout === 'gridLayout';
  const parseQuerys = (options) => {
    console.log(options);
  };

  // 查询表单
  const groupQuerySlot = () => {
    return (
      <Form
        ref={$plugin}
        model={model}
        class={{ 'je-func-group-query': true, 'grid-layout': gridLayout }}
      >
        <Row>
          {groupQuerys.map((field) => itemSlot(field))}
          {buttonSlot()}
        </Row>
      </Form>
    );
  };
  // 操作按钮
  const buttonSlot = () =>
    enableQuery ? (
      <Col span={gridLayout ? 24 : undefined} class="query-buttons">
        <Button type="primary" onClick={() => doQuery()}>
          搜索
        </Button>
        <Button onClick={doReset} class="bgcolor-grey">
          清空
        </Button>
        {/* TODO: 禁用查询策略 */}
        {/* {strategy ? (
        <Button type="primary" bgColor="#169454" borderColor="#169454" onClick={doSaveQuerys}>
          存为查询策略
        </Button>
      ) : null} */}
      </Col>
    ) : null;
  // 查询项
  const itemSlot = (field) => {
    return field.isBr ? (
      <Col span={24} />
    ) : (
      <Col
        class="query-col-item"
        span={gridLayout ? field.colSpan * (24 / cols) : undefined}
        style={{ width: gridLayout ? undefined : field.width + 'px' }}
      >
        <QueryItem
          labelCol={{ style: `width:${(gridLayout ? labelWidth : field.labelWidth) || 80}px` }}
          label={field.label}
          field={field}
          v-model:value={model[field.name]}
          onSortClick={(options) => doSort(options)}
        ></QueryItem>
      </Col>
    );
  };

  expose($groupQuery);
  useProvideFuncTopQuery($groupQuery);
  return { groupQuerySlot };
}
/**
 * 操作方法
 * @param {*} param0
 * @returns
 */
function useGroupAction({ $grid, model, resting }) {
  // 查询
  const doQuery = debounce((reset) => {
    // 重置排序条件
    if (reset) {
      $grid.setQuerys({ type: QueryTypeEnum.ORDER, load: false });
    }
    $grid.setQuerys({ type: QueryTypeEnum.GROUP, querys: reset ? undefined : model });
  }, 300);
  // 排序
  const doSort = debounce((order) => {
    // 重置其他排序条件
    Object.keys(model).forEach((code) => {
      if (code !== order.code) {
        model[code].sort = '';
      }
    });
    // 添加列头排序标识
    $grid.sort({
      field: order.code,
      order: order.type ? order.type.toLocaleLowerCase() : null,
    });
    // 查询
    $grid.setQuerys({ type: QueryTypeEnum.ORDER, querys: order });
  }, 100);
  // 重置
  const doReset = () => {
    resting.value = true;
    $grid.clearSort(); // 清空列头排序标识
    Object.values(model).forEach((value) => {
      Object.assign(value, {
        sort: '',
        value: '',
        betweenValue: '',
        type: value.defaultType,
      });
    });
    resting.value = false;
    doQuery(true);
  };

  /**
   * 存为查询策略
   */
  const doSaveQuerys = () => {
    const { group } = $grid.store.queryParser;
    console.log(group);
  };

  return { doReset, doQuery, doSaveQuerys, doSort };
}
