import { inject, provide } from 'vue';

export const FuncTopQueryContextKey = Symbol('jeFuncTopQueryContextKey');

export const useProvideFuncTopQuery = (state) => {
  provide(FuncTopQueryContextKey, state);
};

export const useInjectFuncTopQuery = () => {
  return inject(FuncTopQueryContextKey, null);
};
