import { ref } from 'vue';
import { Button, Dropdown, Menu } from '@jecloud/ui';
import { useInjectFunc } from '../../../../hooks';
/**
 * 查询策略
 */
export function useStrategyQuery({ $grid }) {
  const $func = useInjectFunc();
  const funcData = $func.getFuncData();
  const disabled = funcData.info.disableQuerys.includes('strategy');
  const strategys = ref(funcData.strategys);
  const actions = {
    clear: { text: '全部', id: 'clear' },
    insert: { text: '新建查询策略', id: 'insert', icon: 'fal fa-plus' },
    config: { text: '管理', id: 'config', icon: 'fal fa-cog' },
  };
  // 选中的节点
  const selectItem = ref(funcData.defaultStrategy ?? actions.clear);
  // 查询
  const onClick = ({ item, key }) => {
    if (['insert', 'config'].includes(key)) {
      console.log(item);
    } else {
      selectItem.value = item.data;
      $grid.value.setQuerys({ type: 'strategy', querys: item.data });
    }
  };
  // 查询策略菜单
  const menuSlot = () => {
    return (
      <Menu class="je-func-strategy-dropdown-menu" selectable onClick={onClick}>
        <Menu.Item key={actions.clear.id} data={actions.clear}>
          {actions.clear.text}
        </Menu.Item>
        {strategys.value.map((item, index) => (
          <Menu.Item key={index} data={item}>
            {item.text}
          </Menu.Item>
        ))}
        <Menu.Divider />
        {/* TODO:禁用查询策略管理 */}
        {/* <Menu.Item key={actions.insert.id} icon={actions.insert.icon} data={actions.insert}>
          {actions.insert.text}
        </Menu.Item>
        <Menu.Item key={actions.config.id} icon={actions.config.icon} data={actions.config}>
          {actions.config.text}
        </Menu.Item> */}
      </Menu>
    );
  };
  // 查询策略按钮
  const strategySlot = function () {
    return disabled ? null : (
      <span style={{ display: 'inline-flex', alignItems: 'center' }}>
        查询策略：
        <Dropdown trigger={['click']} v-slots={{ overlay: menuSlot }}>
          <Button
            type="text"
            class={{
              'strategy-button': true,
              'strategy-button-select': selectItem.value.id !== actions.clear.id,
            }}
          >
            {selectItem.value.id != 'clear' ? <span class="select-flag"></span> : null}
            {selectItem.value.text} <i class="fal fa-chevron-down"></i>
          </Button>
        </Dropdown>
      </span>
    );
  };
  return { strategySlot };
}
