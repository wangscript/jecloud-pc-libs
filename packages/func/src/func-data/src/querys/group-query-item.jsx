import { Form, Input, Select } from '@jecloud/ui';
import { defineComponent, ref } from 'vue';
import { useGroupQueryItem } from './hooks/use-group-query-item';

export default defineComponent({
  name: 'JeFuncGroupQueryItem',
  inheritAttrs: false,
  props: {
    field: Object,
    value: {
      type: Object,
      default: () => ({
        sort: '',
        value: '',
        type: '',
        betweenValue: '',
      }),
    },
  },
  emits: ['update:value', 'sort-click'],
  setup(props, context) {
    const $el = ref();
    const { attrs } = context;
    const { fieldSlot, sortSlot, model, queryTypes } = useGroupQueryItem({
      props,
      context,
    });
    return () => (
      <Form.Item ref={$el} class="je-form-func-query-item" colon={false} {...attrs}>
        <Input.Group compact>
          {props.field.showQueryType ? (
            <Select
              class="query-type"
              options={queryTypes}
              v-model:value={model.value.type}
              showArrow={false}
              getPopupContainer={() => document.body}
              dropdownClassName="je-form-func-query-item-querytype"
              allowClear={false}
            ></Select>
          ) : null}
          {fieldSlot()}
          {fieldSlot(true)}
          {sortSlot()}
        </Input.Group>
      </Form.Item>
    );
  },
});
