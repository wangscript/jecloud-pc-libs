import { Panel } from '@jecloud/ui';
export function useExtendPanel() {
  const regions = ['top', 'bottom', 'left', 'right'];
  const colors = [
    'rgba(0, 180, 42, 1)',
    'rgba(20, 201, 201, 1)',
    'rgba(78, 131, 253, 1)',
    'rgba(114, 46, 209, 1)',
  ];
  // 扩展面板 外
  const extendPanelOuter = () => {
    return regions.map((region, index) => {
      return (
        <Panel.Item
          region={region}
          hidden
          size="60"
          style={{ background: colors[index].replace('1)', '.5)') }}
        >
          外部【{region}】
        </Panel.Item>
      );
    });
  };
  // 扩展面板 内
  const extendPanelInner = () => {
    return regions.map((region, index) => {
      return (
        <Panel.Item
          region={region}
          size="60"
          hidden
          style={{ background: colors[index].replace('1)', '.2)') }}
        >
          内部【{region}】
        </Panel.Item>
      );
    });
  };

  return {
    extendPanelInner,
    extendPanelOuter,
  };
}
