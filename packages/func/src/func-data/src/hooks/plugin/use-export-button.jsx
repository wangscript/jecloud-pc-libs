import { reactive } from 'vue';
import { Form, Input, Modal, Radio } from '@jecloud/ui';
import { dateFormat, isNotEmpty, encode } from '@jecloud/utils';
import { loadExportExcelKeyApi, exportGridExcelApi } from '../../../../func-manager';
/**
 * 打开导出表单
 * @param {*} param0
 */
export function showExportForm({ $func }) {
  const verifyData = [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[^\\\\/*?:"\'<>|\x22]+$',
      message: '不能输入非法字符',
    },
  ];
  const { tableCode, funcId, funcCode, funcName, pkCode, productCode } = $func.getFuncData().info;
  const $grid = $func.getFuncGrid();
  const model = reactive({
    title: funcName,
    fileName: funcName + '_' + dateFormat(new Date(), 'YYYY-MM-DD') + '.xls',
    exportType: 'NOWPAGE',
  });
  const rulesRef = reactive({
    title: verifyData,
    fileName: verifyData,
  });
  const { validate, validateInfos } = Form.useForm(model, rulesRef);
  const form = (
    <Form model={model} layout="vertical">
      <Form.Item colon={false} label="标题" name="title" {...validateInfos.title}>
        <Input v-model:value={model.title}></Input>
      </Form.Item>
      <Form.Item colon={false} label="文件名" name="fileName" {...validateInfos.fileName}>
        <Input v-model:value={model.fileName}></Input>
      </Form.Item>
      <Form.Item colon={false} label="数据设定" name="exportType">
        <Radio.Group v-model:value={model.exportType} name="exportType">
          <Radio value="NOWPAGE">当前页</Radio>
          <Radio value="SELECTION">选择数据</Radio>
          <Radio value="ALL">全部</Radio>
        </Radio.Group>
      </Form.Item>
    </Form>
  );

  Modal.dialog({
    title: '导出设定',
    width: 460,
    content: () => form,
    buttonAlign: 'center',
    okButton: {
      closable: false,
      handler({ $modal, button }) {
        button.loading = true;
        validate()
          .then(() => {
            doExportExcel({ $modal, button });
          })
          .catch((e) => {
            button.loading = false;
          });
      },
    },
  });

  /**
   * 导出excel
   * @param {*} param0
   * @returns
   */
  const doExportExcel = ({ $modal, button }) => {
    // 封装参数
    const params = {
      title: model.title,
      fileName: model.fileName,
      tableCode,
      funcCode,
      funcId,
      styleType: 'GRID',
      j_query: [],
    };
    //当前页
    const ids = [];
    if (model.exportType == 'NOWPAGE') {
      const gridData = $grid.store.data;
      gridData.forEach((item) => {
        ids.push(item[pkCode]);
      });
    } else if (model.exportType == 'SELECTION') {
      const checkboxRecords = $grid.getSelectedRecords();
      checkboxRecords.forEach((item) => {
        ids.push(item[pkCode]);
      });
    }
    if (ids.length > 0 || model.exportType == 'ALL') {
      if (model.exportType != 'ALL') {
        params.j_query.push({
          code: pkCode,
          type: 'in',
          value: ids,
          cn: 'and',
        });
      } else {
        params.j_query = $grid.store.queryParser.getQuerys();
      }
      // 调用接口导出excel
      loadExportExcelKeyApi({ params: { data: encode(params) } })
        .then((data) => {
          exportGridExcelApi({ params: { key: data }, pd: productCode })
            .then((obj) => {
              const objectUrl = URL.createObjectURL(obj);
              const a = document.createElement('a');
              a.setAttribute('href', objectUrl);
              a.setAttribute('download', model.fileName);
              a.click();
              button.loading = false;
              $modal.close();
            })
            .catch((e) => {
              button.loading = false;
              Modal.alert(e.message, 'error');
            });
        })
        .catch((e) => {
          button.loading = false;
          Modal.alert(e.message, 'error');
        });
    } else {
      // 没有选中数据
      Modal.alert('没有可导出的数据！', 'warning');
      button.loading = false;
      return;
    }
  };
}
