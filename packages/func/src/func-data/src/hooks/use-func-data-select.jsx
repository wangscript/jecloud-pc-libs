import { nextTick, onMounted, ref } from 'vue';
import { useRenderer } from './use-renderer';
import { useFuncSelectEvents } from '../../../events/func-select-events';
import { Modal, Grid, Data, Panel } from '@jecloud/ui';
import { useBase } from './use-base';
import { FuncDataTypeEnum, FuncColumnTypeEnum } from '../../../func-manager/enum';
import { isNotEmpty, encode, split, isEmpty, cloneDeep } from '@jecloud/utils';
import { loadGridApi } from '../../../func-manager';
export function useFuncDataSelect({ props, context }) {
  const type = FuncDataTypeEnum.SELECT;
  const { $func, $tree, $grid, $groupQuery } = useBase({ props, context, type });
  // 查询选择配置
  const { selectOptions, funcCode } = props;
  const { querys, gridQuerys, treeQuerys, orders, multiple } = selectOptions;
  // 事件
  const { selectConfig, onTreeSelectChange, onGridCellDblclick } = useFuncSelectEvents({
    type: 'grid',
    $func,
    modal: Modal.useInjectModal(),
    selectOptions,
  });
  // 选择结果插槽
  const { resultStore, resultSlot, onSelectionChange, onLoad } = useResultSlot({ $func, $grid });
  // 追加功能信息
  Object.assign($func, {
    select: onGridCellDblclick,
    getResultRecords() {
      return resultStore?.data;
    },
  });
  // 插槽
  const { treeSlot, gridSlot } = useRenderer({
    $func,
    $tree,
    $grid,
    $groupQuery,
    funcCode,
    type,
    gridProps: {
      multiple: multiple ?? selectConfig.multiple,
      querys: gridQuerys || querys,
      orders,
      onCellDblclick: onGridCellDblclick,
      onSelectionChange,
      onLoad,
    },
    treeProps: {
      querys: treeQuerys || querys,
      onSelectionChange: onTreeSelectChange,
    },
  });

  onMounted(() => {
    nextTick(() => {
      // 初始选中值

      initSelectValue({ $func }).then(({ rows = [] }) => {
        resultStore?.loadData(rows);
      });
    });
  });

  return { treeSlot, gridSlot, resultSlot };
}
/**
 * 初始化选中的数据
 * @param {*} param0
 */
function initSelectValue({ $func }) {
  const { selectConfig, selectOptions } = $func;
  const {
    tableCode,
    funcCode,
    productCode = 'meta',
    pkCode,
    funcAction,
  } = $func.getFuncData().info;
  const { model, idProperty = pkCode, addQuerys = '' } = selectOptions;
  const { sourceFields, targetFields } = selectConfig;
  const targetField = targetFields[sourceFields.indexOf(idProperty)];
  // 查找唯一值字段，用于初始数据
  if (idProperty && targetField && model) {
    // 获取初始数据
    const values = split(model[targetField], ',');
    if (isNotEmpty(values)) {
      let querysArr = [{ code: idProperty, type: 'in', value: values }];
      if (addQuerys) {
        querysArr = querysArr.concat(addQuerys);
      }
      return loadGridApi({
        pd: productCode,
        action: funcAction,
        params: {
          tableCode,
          funcCode,
          j_query: encode(querysArr),
        },
      });
    }
  }
  return Promise.resolve({ rows: [] });
}
/**
 * 选择结果
 * @param {*} param0
 * @returns
 */
function useResultSlot({ $func, $grid }) {
  const { selectConfig, selectOptions } = $func;
  // 解析列数据
  const funcData = $func.getFuncData();
  const { pkCode } = funcData.info;
  const { multiple, fieldMaps } = selectConfig;
  const resultStore = Data.Store.useGridStore({ data: [], idProperty: pkCode });
  const columns = funcData.getColumns('select');
  const { name } = selectOptions;
  const targetField = fieldMaps.targetToSource[name];
  const column =
    cloneDeep(
      funcData.basic.columns.get(targetField) ??
        columns.find(
          // 查找非特殊类型 && 非表头
          (item) =>
            !Object.values(FuncColumnTypeEnum).includes(item.type) &&
            isEmpty(item.children) &&
            !item.hidden,
        ),
    ) ?? {};
  // 去除width，宽度自适应
  Object.assign(column, {
    width: undefined,
    resizable: false,
    hidden: false,
    visible: true,
  });
  const gridColumns = [
    { title: 'No.', type: FuncColumnTypeEnum.SEQ, width: 47 },
    column,
    {
      width: 42,
      resizable: false,
      align: 'center',
      headerAlign: 'center',
      className: 'je-func-children-field-action-cell',
      headerClassName: 'je-func-children-field-action-header',
      slots: {
        header: () => <i class="action-item red fas fa-trash-alt" onClick={() => doRemove()} />,
        default: ({ row }) => (
          <i class="action-item red fas fa-times-circle" onClick={() => doRemove(row)}></i>
        ),
      },
    },
  ];
  // 移除选择数据
  const doRemove = (row) => {
    if (row) {
      resultStore.remove(row);
      $grid.value.setCheckboxRow(row, false);
    } else {
      resultStore.loadData([]);
      $grid.value.setAllCheckboxRow(false);
    }
  };
  const $resultPanel = ref();
  return {
    resultStore,
    onLoad({ store }) {
      // 默认选中数据
      const rows = store.data.filter((item) => resultStore.getRecordById(item[pkCode]));
      $grid.value.setSelectRow(rows, true);
    },
    onSelectionChange({ row, checked, newValue }) {
      const store = $grid.value.store;
      if (!multiple) {
        checked = !!newValue;
      }
      // 处理数据,有row，单条选择，没有row，全选操作
      (row ? [row] : store.data).forEach((record) => {
        const id = record[pkCode];
        if (checked) {
          // 添加
          if (!multiple) {
            resultStore.loadData([record]);
          } else if (!resultStore.getRecordById(id)) {
            resultStore.insert(record, -1);
          }
        } else {
          // 删除
          if (!multiple) {
            resultStore.loadData([]);
          } else if (resultStore.getRecordById(id)) {
            resultStore.remove(record);
          }
        }
      });
    },
    resultSlot: () =>
      multiple ? (
        <Panel.Item ref={$resultPanel} region="right" split collapsible size="232">
          <Grid
            size="small"
            class="je-grid-func-select-result"
            columns={gridColumns}
            store={resultStore}
            bodyBorder
            draggable
            v-slots={{
              tbar() {
                return (
                  <div class="title">
                    已选数据<span class="red">（{resultStore.data.length}条）</span>
                    <i
                      class="icon fal fa-chevron-right"
                      onClick={() => {
                        $resultPanel.value.setExpand(false);
                      }}
                    ></i>
                  </div>
                );
              },
            }}
          />
        </Panel.Item>
      ) : null,
  };
}
