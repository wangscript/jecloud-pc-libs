import { Toolbar } from '@jecloud/ui';
import { useQuerys } from '../querys';
import { useButtons } from './use-buttons';
import { isEmpty } from '@jecloud/utils';
export function useToolbar(options) {
  const { $grid, type, $func } = options;
  const { keywordSlot, strategySlot } = useQuerys(options);
  const {
    tbarButtonSlot,
    multiButtonSlot,
    configMenuSlot,
    insertButtonSlot,
    helpButtonSlot,
    tbarBtns,
  } = useButtons(options);
  const funcData = $func.getFuncData();
  const { gridOptions } = funcData.info;
  const tbarSlot = function () {
    // 查询项
    let tbarItems = [strategySlot(), keywordSlot()];
    // 功能列表
    if (type === 'func') {
      // 添加操作按钮，如果是选中状态，显示选中按钮
      if (isEmpty($grid.value?.selection) || tbarBtns.multiButtons.length === 0) {
        // 启用简洁按钮
        if (gridOptions.simpleBar) {
          const array = [...tbarButtonSlot(), <Toolbar.Fill />];
          tbarItems = array.concat(tbarItems);
        } else {
          tbarItems = tbarItems.concat([<Toolbar.Fill />, ...tbarButtonSlot()]);
        }
      } else {
        tbarItems = multiButtonSlot().concat([<Toolbar.Fill />]);
      }
      // 添加功能使用说明按钮
      tbarItems.push(helpButtonSlot());
      // 添加功能配置按钮
      tbarItems.push(configMenuSlot());
      // 添加系统默认新增按钮 (启用简洁按钮没有添加按钮)
      if (!gridOptions.simpleBar) {
        tbarItems.unshift(insertButtonSlot());
      }
    }
    // 隐藏工具条
    return type !== 'func' || !gridOptions.hiddenTbar ? (
      <Toolbar class="je-grid-func-tbar">{tbarItems}</Toolbar>
    ) : null;
  };

  return {
    tbarSlot,
  };
}
