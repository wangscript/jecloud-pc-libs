import { Data, Modal } from '@jecloud/ui';
import { reactive, watch } from 'vue';
import { isEmpty, encode, decode, isNotEmpty, cloneDeep } from '@jecloud/utils';

export function useFuncQueys({ props, context }) {
  const state = reactive({
    tableData: Data.Store.useGridStore({
      data: [],
    }),
    contextText:
      '备注：字段选择范围为本功能内部字段，且可以通过{parent.字段编码}的形式获取主功能表单字段值。也可通过例如{@USER_ID@}的方式得到全局变量',
    SQLFILTER_CN_OPTIONS: [
      { value: 'and', label: '并且' },
      { value: 'or', label: '或者' },
    ],
    inputRender: {
      autofocus: '.ant-input',
    },
    SQLFILTER_TYPE_OPTIONS: [
      { value: '=', label: '精确' },
      { value: 'like', label: '模糊' },
      // { value: 'like%', label: '后模糊' },
      { value: 'in', label: '包含' },
      { value: 'between', label: '区间' },
      { value: '>', label: '大于' },
      { value: '>=', label: '大于等于' },
      { value: '<', label: '小于' },
      { value: '<=', label: '小于等于' },
      { value: '!=', label: '不等于' },
      { value: 'originalLike', label: 'originalLike' },
      { value: 'notIn', label: 'notIn' },
      { value: 'notNull', label: 'notNull' },
      { value: 'isNull', label: 'isNull' },
      { value: 'inSelect', label: 'inSelect' },
      { value: 'notInSelect', label: 'notInSelect' },
    ],
    textareaValue: props.queryParams.value || '',
    querys: props.queryParams.querys || [
      {
        code: 'RESOURCECOLUMN_FUNCINFO_ID',
        value: props.queryParams.funcId,
        type: '=',
        cn: 'and',
      },
      {
        code: 'RESOURCECOLUMN_XTYPE',
        value: 'uxcolumn',
        type: '=',
        cn: 'and',
      },
    ],
    orders: props.queryParams.orders || [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
    configInfo: props.queryParams.configInfo || 'JE_CORE_RESOURCECOLUMN,code,RESOURCECOLUMN_CODE,S',
    loadTableData: props.queryParams.querys,
  });

  // 添加列
  const addColumn = () => {
    var record = {
      cn: '',
      type: '',
      leftvalue: '0',
      code: '',
      value: '',
      rightvalue: '0',
    };
    state.tableData.insert(record, -1);
  };
  //删除列
  const deleteColumn = (row) => {
    state.tableData.remove(row);
    if (state.tableData.data.length <= 0) {
      state.textareaValue = '';
    }
  };
  //列表单元格格式化
  const getSelectLabel = (value, list, valueProp = 'value', labelField = 'label') => {
    const item = list.find((item) => item[valueProp] === value);
    return item ? item[labelField] : null;
  };
  const getPopupContainer = () => {
    return document.body;
  };
  const disposeSql = (type) => {
    //导出Sql
    if (type) {
      const data1 = state.tableData.data;
      const j_query = buildJquerys(data1);
      state.textareaValue = JSON.stringify(j_query);
      state.tableData.loadData(data1);
      state.loadTableData = cloneDeep(JSON.stringify(j_query));
    } else {
      const data = openJquerys(JSON.parse(state.textareaValue));
      state.tableData.loadData(data);
      state.loadTableData = cloneDeep(state.textareaValue);
    }
  };
  const buildJquerys = (datas) => {
    if (isEmpty(datas)) return;
    var buildItems = function (item, index) {
      //有左括号
      if (item.leftvalue == '1') {
        if (item.rightvalue == '1') {
          item = {
            type: item.cn || 'and',
            value: [
              {
                type: item.type,
                code: item.code,
                value:
                  ['inSelect', 'notInSelect'].indexOf(item.type) != -1
                    ? decode(item.value)
                    : item.value,
                cn: item.cn || 'and',
              },
            ], //添加当前项
          };
        } else {
          //没有右括号
          item = {
            type: item.cn || 'and',
            value: [
              {
                type: item.type,
                code: item.code,
                value:
                  ['inSelect', 'notInSelect'].indexOf(item.type) != -1
                    ? decode(item.value)
                    : item.value,
                cn: item.cn || 'and',
              },
            ], //添加当前项
          };
          //在index的下标基础继续循环datas
          for (index++; index < datas.length; index++) {
            //调用自身给item.value中赋值
            var info = buildItems(datas[index], index);
            index = info.index;
            item.value.push({
              type: info.item.type,
              code: info.item.code,
              value:
                ['inSelect', 'notInSelect'].indexOf(info.item.type) != -1
                  ? decode(info.item.value)
                  : info.item.value,
              cn: info.item.cn || 'and',
            });
            //直至找都右括号该结束循环，继续执行最开始的循环
            if (info.item.rightvalue == '1') {
              break;
            }
          }
        }
      }
      //如果没有左右括号，直接赋值
      if (item.leftvalue == '0' && item.rightvalue == '0') {
        item = {
          type: item.type,
          code: item.code,
          value:
            ['inSelect', 'notInSelect'].indexOf(item.type) != -1 ? decode(item.value) : item.value,
          cn: item.cn || 'and',
        };
      }
      return { item: item, index: index };
    };
    var _items = [];
    //数组循环开始
    for (var i = 0; i < datas.length; i++) {
      var item = datas[i];
      var info = buildItems(item, i);
      i = info.index;
      _items.push(info.item);
    }
    return _items;
  };
  const openJquerys = (_items) => {
    if (isEmpty(_items)) return;
    var _datas = [];
    function handle(arr) {
      if (arr.length == 1) {
        arr[0].leftvalue = '1';
        arr[arr.length - 1].rightvalue = '1';
        if (['inSelect', 'notInSelect'].indexOf(arr[0].type) != -1) {
          arr[0].value = encode(arr[0].value);
        }
        _datas.push(arr[0]);
      } else {
        for (var i = 0; i < arr.length; i++) {
          var data = arr[i];
          if (data.type != 'and' && data.type != 'or') {
            if (i != 0 && i != arr.length - 1) {
              data.leftvalue = '0';
              data.rightvalue = '0';
            }
            if (i == 0) {
              data.leftvalue = '1';
              data.rightvalue = '0';
            }
            if (i == arr.length - 1) {
              data.leftvalue = '0';
              data.rightvalue = '1';
            }
            if (['inSelect', 'notInSelect'].indexOf(data.type) != -1) {
              data.value = encode(data.value);
            }
            _datas.push(data);
          } else {
            handle(data.value);
          }
        }
      }
    }
    for (var i = 0; i < _items.length; i++) {
      var data = _items[i];
      if (data.type != 'and' && data.type != 'or') {
        data.leftvalue = '0';
        data.rightvalue = '0';
        if (['inSelect', 'notInSelect'].indexOf(data.type) != -1) {
          data.value = encode(data.value);
        }
        _datas.push(data);
      } else {
        handle(data.value);
      }
    }
    return _datas;
  };
  const codeSwitching = (type) => {
    let value = '';
    if (type) {
      value = JSON.stringify(JSON.parse(state.textareaValue), null, 2);
    } else {
      value = JSON.stringify(JSON.parse(state.textareaValue));
    }
    state.textareaValue = value;
  };

  watch(
    () => props.queryParams.value,
    (val) => {
      if (isNotEmpty(val)) {
        disposeSql(false);
      }
    },
    { immediate: true },
  );

  /**
   *  确定按钮校验
   */
  const okHandler = () => {
    let flag = true;
    if (state.tableData.data.length && isEmpty(state.textareaValue)) {
      Modal.alert('请生成查询配置！', 'warning');
      flag = false;
    } else if (
      state.textareaValue &&
      state.loadTableData != encode(buildJquerys(state.tableData.data))
    ) {
      Modal.alert('请点击生成查询配置按钮，更新查询配置！', 'warning');
      flag = false;
    }
    return flag;
  };

  return {
    state,
    addColumn,
    deleteColumn,
    getSelectLabel,
    getPopupContainer,
    disposeSql,
    codeSwitching,
    okHandler,
  };
}
