import JeFuncQuerys from './src/querys';
import { withInstall } from '../utils';

export const FuncQuerys = withInstall(JeFuncQuerys);
export default FuncQuerys;
