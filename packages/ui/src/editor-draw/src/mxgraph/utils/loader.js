import { loadjs, pick } from '@jecloud/utils';
import { mxgraph } from '..';
import { mxGraphAttrs, initMxGraph } from '.';
import { usePublicPath } from '../../../../hooks/use-public-path';
/**
 * 加载外部静态资源，不使用npm安装
 * @returns
 */
export function loadMxGraph() {
  const publicPath = usePublicPath();
  const basePath = `${publicPath}static/mxgraph`;
  if (mxgraph.inited) {
    return Promise.resolve(mxgraph);
  } else {
    initMxGraph({ basePath });
    return loadjs(
      [
        `${basePath}/mxClient.js`,
        `${basePath}/resourse/libs/sanitizer.min.js`,
        `${basePath}/resourse/libs/pako.min.js`,
      ],
      { returnPromise: true },
    ).then(() => {
      Object.assign(mxgraph, pick(window, [...mxGraphAttrs, 'html_sanitize', 'pako']));
      mxgraph.inited = true;
      return mxgraph;
    });
  }
}
