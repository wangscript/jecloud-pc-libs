import { withInstall } from '../utils';
import JeEditorDraw from './src/editor-draw';
export const EditorDraw = withInstall(JeEditorDraw);

export default EditorDraw;
