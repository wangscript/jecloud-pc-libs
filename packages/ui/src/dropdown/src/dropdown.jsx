import { defineComponent } from 'vue';
import { Dropdown } from 'ant-design-vue';

export default defineComponent({
  name: 'JeDropdown',
  components: { ADropdown: Dropdown },
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    props;
    return () => <a-dropdown {...attrs} v-slots={slots}></a-dropdown>;
  },
});
