import { defineComponent } from 'vue';
import { Select } from 'ant-design-vue';
import { useStyle4Size } from '../../hooks';
import { useAddonSlot, addonProps, addonSlots } from '../../hooks/use-addon';
import { isString, split, pinyin } from '@jecloud/utils';
import { useSelect } from './hooks';

export default defineComponent({
  name: 'JeSelect',
  components: { Select },
  inheritAttrs: false,
  props: {
    editable: Boolean,
    readonly: Boolean,
    disabled: Boolean,
    value: [String, Number, Object],
    multiple: { type: Boolean, default: undefined },
    configInfo: String,
    options: Array,
    showSearch: Boolean,
    height: Number,
    width: Number,
    model: Object,
    parentModel: Object,
    querys: Array,
    dicValueConfig: Array,
    ...addonProps,
  },
  slots: addonSlots,
  emits: ['update:value', 'update:options', 'change', 'before-select'],
  setup(props, context) {
    const { slots, attrs } = context;
    const {
      selectValue,
      computeMultiple,
      computeReadonly,
      onDropdownVisibleChange,
      onChange,
      options,
      filterOption,
      $plugin,
      searchValue,
    } = useSelect({
      props,
      context,
    });

    // 注意，如果发现下拉菜单跟随页面滚动，或者需要在其他弹层中触发 Select，
    // 请尝试使用 getPopupContainer={triggerNode => triggerNode.parentNode} 将下拉弹层渲染节点固定在触发器的父元素中。
    return () => {
      // 个性化配置
      const customProps = {};
      // 多选
      if (computeMultiple.value) {
        Object.assign(customProps, {
          tokenSeparators: [','],
          mode: props.editable ? 'tags' : 'multiple',
        });
        // 字符串转数组
        if (isString(selectValue.value)) {
          selectValue.value = split(selectValue.value, ',');
        }
        // 单选
      } else {
        Object.assign(customProps, {
          showSearch: props.showSearch,
        });
        // 可选可编辑
        if (props.editable && !props.showSearch) {
          Object.assign(customProps, {
            showSearch: false,
            filterOption: false,
            mode: Select.SECRET_COMBOBOX_MODE_DO_NOT_USE,
          });
        }
        // 可选可过滤，可选可编辑，重写filterOption。目的就是没有数据的时候不显示暂无数据
        if (props.editable && props.showSearch) {
          Object.assign(customProps, {
            showSearch: true,
            filterOption: (inputValue, option) => {
              if (!inputValue) return true;

              inputValue = inputValue?.toString().toLocaleLowerCase() ?? inputValue;
              // 模糊查询value，label
              const value = option.value.toString().toLocaleLowerCase();
              const label = option.label?.toString().toLocaleLowerCase() ?? value;
              const labelPinyin = pinyin(label, 'pinyin');

              return (
                value.includes(inputValue) ||
                label.includes(inputValue) ||
                labelPinyin.includes(inputValue)
              );
            },
            mode: Select.SECRET_COMBOBOX_MODE_DO_NOT_USE,
          });
        }
      }

      const element = (
        <Select
          ref={$plugin}
          showArrow
          allowClear
          maxTagCount="responsive"
          getPopupContainer={(triggerNode) => triggerNode.parentNode} // 绑定
          style={useStyle4Size({ props })}
          onDropdownVisibleChange={onDropdownVisibleChange}
          onChange={onChange}
          filterOption={filterOption}
          searchValue={searchValue.value}
          {...attrs}
          {...customProps}
          options={options.value}
          disabled={computeReadonly.value}
          v-model:value={selectValue.value}
          v-slots={slots}
        ></Select>
      );

      return useAddonSlot({ props, context, element });
    };
  },
});
