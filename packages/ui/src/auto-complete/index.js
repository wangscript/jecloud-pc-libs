import { withInstall } from '../utils';
import JeAutoComplete from './src/auto-complete';
export const AutoComplete = withInstall(JeAutoComplete);

export default AutoComplete;
