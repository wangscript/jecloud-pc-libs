import { defineComponent, ref } from 'vue';
import { AutoComplete } from 'ant-design-vue';
import { useModelValue, useExtendMethods } from '../../hooks';

export default defineComponent({
  name: 'JeAutoComplete',
  components: { AutoComplete },
  inheritAttrs: false,
  props: { value: { type: String, default: '' }, allowClear: { type: Boolean, default: true } },
  emits: ['update:value'],
  setup(props, context) {
    const { slots, attrs, expose } = context;
    const value = useModelValue({ props, context });
    const $plugin = ref();
    const methods = useExtendMethods({ plugin: $plugin, keys: ['blur', 'focus'] });
    expose({
      ...methods,
      $plugin,
    });
    return () => (
      <AutoComplete
        ref={$plugin}
        v-model:value={value.value}
        {...attrs}
        v-slots={slots}
      ></AutoComplete>
    );
  },
});
