import { useListeners, useField } from '../../../hooks';
import { pick, get, cloneDeep, toQuerysTemplate } from '@jecloud/utils';
import { useInjectFormItemContext } from '../../../form/src/context';
export function useFuncSelect({ props, context, value, type }) {
  const types = ['tree', 'grid', 'user'];
  if (!types.includes(type)) {
    console.error(`功能选择字段类型不在支持的列表中：${[types.join('，')]}`);
    return;
  }
  const { fireListener } = useListeners({ props, context });
  const { func, getModel, setModel, getName, getLabel, resetModel, getModelOwner } = useField({
    props,
    context,
  });

  if (!func) {
    return;
  }

  const formItemContext = useInjectFormItemContext();

  // 字段选择配置项
  const configInfo = props.selectOptions?.configInfo ?? props.configInfo;
  const config = func.parseConfigInfo({ configInfo });
  // 重置函数
  const onReset = () => {
    // 触发reset事件
    fireListener('reset', { ...getModelOwner() });
    resetModel(config.targetFields);
  };
  // 功能对象
  const $func = func.useInjectFunc();

  // 选择函数
  const onSelect = () => {
    if (props.readonly) return false;
    const model = getModel();
    // 配置项
    const options = cloneDeep(
      Object.assign(
        pick(props, [
          'idProperty',
          'configInfo',
          'querys',
          'orders',
          'product',
          'selectExp',
          'multiple',
          'dicValueConfig',
        ]),
        props.selectOptions ?? {},
      ),
    );
    // 系统选择器
    if (fireListener('before-select', { ...getModelOwner(), options }) !== false) {
      // 解析查询参数
      if (options.querys) {
        toQuerysTemplate({
          querys: options.querys,
          model,
          parentModel: $func?.parentFunc?.store.activeBean,
        });
      }
      func.showSelectWindow({
        value: value.value,
        name: formItemContext?.name,
        model,
        parentModel: props.parentModel || {},
        title: getLabel(),
        type,
        $func, // 增加功能对象
        ...options,
        callback({ rows, paths, config }) {
          const selectResult = fireListener('select', { ...getModelOwner(), rows });
          if (selectResult !== false) {
            const result = func.getConfigInfo({
              type,
              configInfo: config,
              rows,
              paths,
            });
            // 更新model数据
            setModel(result);

            // 设置字段值
            const name = getName();
            const sourceField = config.fieldMaps?.targetToSource[name];
            if (sourceField) {
              value.value = func.getConfigFieldValue({ field: sourceField, rows, paths });
            }
          }
          return selectResult;
        },
      });
    }
  };

  return { onReset, onSelect };
}
