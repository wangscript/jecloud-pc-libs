import { defineComponent, ref } from 'vue';
import AutoComplete from '../../auto-complete';
import Input from '../../input';
import { debounce, isNotEmpty, omit } from '@jecloud/utils';
import { useModelValue } from '../../hooks';

export default defineComponent({
  name: 'JeSearch',
  components: { AutoComplete, Input },
  inheritAttrs: false,
  props: {
    labelField: { type: String, default: 'label' },
    subLabelField: String,
    valueField: { type: String, default: 'value' },
    iconField: { type: String, default: 'icon' },
    value: { type: String },
    enterButton: { type: [String, Boolean], default: false },
  },
  emits: ['search', 'select', 'update:value'],
  slots: ['icon'],
  setup(props, context) {
    const { slots, attrs, emit } = context;
    const searchValue = useModelValue({ props, context, changeEvent: true });
    const options = ref([]);
    // 选中属性节点
    const onSelect = (value, option) => {
      options.value = [option];
      searchValue.value = option.label;
      emit('select', value, option.data);
    };
    // 查询关键字
    const onSearch = debounce((val) => {
      const keyword = val.toString().toLocaleLowerCase();
      const searchP = new Promise((resolve, reject) => {
        if (isNotEmpty(val)) {
          emit('search', keyword, resolve, reject);
        } else {
          emit(
            'search',
            keyword,
            () => {
              resolve([]);
            },
            reject,
          );
        }
      });
      searchP
        .then((items) => {
          options.value = (items ?? []).map((item) => {
            // 绑定数据项
            return {
              label: item[props.labelField],
              value: item[props.labelField],
              data: item,
            };
          });
        })
        .catch(() => {
          options.value = [];
        });
    }, 300);

    // 查询项插槽
    const optionSlot = (option) => {
      const item = option.data?.item || option.data;
      if (slots.option) {
        return slots.option(item);
      }
      const keyword = searchValue.value;
      const splitStr = '`__`';
      let vns = [];
      // 支持两级标题高亮
      [
        { cls: 'label', label: item[props.labelField], icon: item[props.iconField] },
        { cls: 'sublabel', label: item[props.subLabelField] || item?.bean?.[props.subLabelField] },
      ].forEach((labelItem) => {
        if (labelItem.label) {
          const keys = labelItem.label
            .toString?.()
            ?.replaceAll(keyword, splitStr + keyword + splitStr)
            ?.split(splitStr);
          // 图标，只有label有icon
          const icon = labelItem.icon ? <i class={['label-icon', labelItem.icon]}></i> : null;
          vns.push(
            <div class={labelItem.cls}>
              {slots.icon?.(item) ?? icon}
              {keys.map((key) => {
                return key === keyword ? <span class="keyword">{key}</span> : key;
              })}
            </div>,
          );
        }
      });
      return <div class="je-search-option">{vns}</div>;
    };
    return () => (
      <AutoComplete
        class="je-search"
        allow-clear
        {...omit(attrs, ['placeholder'])}
        options={options.value}
        v-model:value={searchValue.value}
        onSearch={onSearch}
        onSelect={onSelect}
        v-slots={{ ...slots, option: optionSlot }}
      >
        {props.enterButton ? (
          <Input.Search
            placeholder={attrs.placeholder}
            onSearch={onSearch}
            allow-clear
            enter-button={props.enterButton}
            v-slots={{
              prefix: () => <i class="fal fa-search" />,
            }}
          ></Input.Search>
        ) : (
          <Input
            placeholder={attrs.placeholder}
            v-slots={{
              prefix: () => <i class="fal fa-search" />,
            }}
          ></Input>
        )}
      </AutoComplete>
    );
  },
});
