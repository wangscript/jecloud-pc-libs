import { defineComponent, ref } from 'vue';

export default defineComponent({
  name: 'JeLoading',
  inheritAttrs: false,
  props: {
    title: String,
  },
  setup(props, { expose, slots }) {
    const title = ref(props.title);
    expose({
      setTitle(t) {
        title.value = t;
      },
    });
    return () => {
      const defaultSlot = slots.default?.() ?? title.value;
      return (
        <div class="je-loading">
          <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
          </div>
          {defaultSlot ? <div class="je-loading-title">{defaultSlot}</div> : null}
        </div>
      );
    };
  },
});
