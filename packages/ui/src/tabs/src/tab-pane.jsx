import { defineComponent } from 'vue';
import { Tabs } from 'ant-design-vue';
export default defineComponent({
  name: 'JeTabPane',
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <Tabs.TabPane {...attrs} v-slots={slots}></Tabs.TabPane>;
  },
});
