import { withInstall } from '../utils';
import JeTabs from './src/tabs';
import JeTabPane from './src/tab-pane';

JeTabs.installComps = {
  // 注册依赖组件
  TabPane: { name: JeTabPane.name, comp: JeTabPane },
};
export const Tabs = withInstall(JeTabs);

export default Tabs;
