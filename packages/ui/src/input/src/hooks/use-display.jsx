import { useListeners, useModelValue } from '../../../hooks';
import { useInjectForm } from '../../../form/src/context';
import { filterEmpty } from '../../../utils/props';
import { isEmpty } from '@jecloud/utils';
import { ref } from 'vue';

export function useDisplay({ props, context }) {
  const { slots } = context;
  const value = useModelValue({ props, context });
  const { fireListener } = useListeners({ props });
  const $form = useInjectForm();
  const padding = ref(props.padding);
  const getParams = () => ({
    $form,
    model: $form?.model,
    value: value.value,
  });
  // inputStyle事件
  const getInputStyle = () => {
    return fireListener('input-style', getParams());
  };
  // renderer事件
  const displaySlot = () => {
    const params = getParams();
    let vnode = filterEmpty(slots.display?.(params) ?? fireListener('renderer', params)) || [];
    return <div class="je-input-display-renderer">{isEmpty(vnode) ? value.value : vnode}</div>;
  };

  return { displaySlot, getInputStyle, padding, value };
}
