import { defineComponent } from 'vue';
import Input from './input';
import { useDisplay } from './hooks/use-display';
import { omit } from '@jecloud/utils';
export default defineComponent({
  name: 'JeInputDisplay',
  inheritAttrs: false,
  props: { value: { type: String, default: '' }, padding: { type: Boolean, default: true } },
  emits: ['update:value', 'renderer', 'input-style'],
  slots: ['display'],
  setup(props, context) {
    const { slots, attrs } = context;
    const { value, displaySlot, getInputStyle } = useDisplay({ props, context });
    return () => {
      return (
        <Input
          class="je-input-display"
          style={getInputStyle()}
          readonly
          allow-clear={false}
          {...attrs}
          v-model:value={value.value}
          v-slots={{ ...omit(slots, ['display']), display: displaySlot }}
        ></Input>
      );
    };
  },
});
