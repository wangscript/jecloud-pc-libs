import ClearableLabeledInput from 'ant-design-vue/es/input/ClearableLabeledInput';
import classNames from 'ant-design-vue/es/_util/classNames';
import { cloneElement } from 'ant-design-vue/es/_util/vnode';
import { ref } from 'vue';
import { getInputClassName, hasPrefixSuffix, hasAddon } from 'ant-design-vue/es/input/util';
import CloseCircleFilled from '@ant-design/icons-vue/CloseCircleFilled';
import { useInjectInput } from '../context';
import { filterEmpty } from '../../../utils/props';
const useOverrideMethods = function (plugin, methods) {
  Object.keys(methods).forEach((key) => {
    plugin['_' + key] = plugin[key];
    plugin[key] = methods[key];
  });
};

const ClearableInputType = ['text', 'input'];

useOverrideMethods(ClearableLabeledInput, {
  setup(props, { slots, attrs }) {
    // TODO:获取input对象，触发reset事件
    const inputContext = useInjectInput();
    const containerRef = ref();
    const onInputMouseUp = (e) => {
      if (containerRef.value?.contains(e.target)) {
        const { triggerFocus } = props;
        triggerFocus?.();
      }
    };
    const renderClearIcon = (prefixCls) => {
      const { allowClear, value, disabled, handleReset } = props;
      if (!allowClear) {
        return null;
      }
      // TODO: 去掉readonly属性判断，主要是editable影响
      const needClear = !disabled && value;
      const className = `${prefixCls}-clear-icon`;
      // TODO:触发reset事件
      const resetFn = (...args) => {
        if (inputContext?.onBeforeReset?.() !== false) {
          handleReset(...args);
          inputContext?.context?.emit('reset');
        }
      };
      return (
        <CloseCircleFilled
          onClick={resetFn}
          // Do not trigger onBlur when clear input
          onMousedown={(e) => e.preventDefault()}
          class={[
            {
              [`${className}-show`]: needClear, //TODO: 增加清除按钮显示样式
            },
            className,
            // 'fal fa-times-circle',
          ]}
          role="button"
        />
      );
    };

    const renderSuffix = (prefixCls) => {
      const { suffix = slots.suffix?.(), allowClear } = props;
      const suffixVNodes = filterEmpty(suffix);
      if (suffixVNodes.length || allowClear) {
        return (
          <span
            class={[
              `${prefixCls}-suffix`,
              {
                [`${prefixCls}-has-suffix`]: suffixVNodes.length,
              },
            ]}
          >
            {renderClearIcon(prefixCls)}
            {suffixVNodes}
          </span>
        );
      }
      return null;
    };

    const renderLabeledIcon = (prefixCls, element) => {
      const {
        focused,
        value,
        prefix = slots.prefix?.(),
        size,
        suffix = slots.suffix?.(),
        display = slots.display?.(), // TODO: 展示插槽
        disabled,
        allowClear,
        direction,
        readonly,
        bordered,
        addonAfter = slots.addonAfter,
        addonBefore = slots.addonBefore,
        inputType,
      } = props;
      const suffixNode = renderSuffix(prefixCls);
      if (!hasPrefixSuffix({ prefix, suffix, allowClear })) {
        return cloneElement(element, {
          value,
        });
      }

      const prefixNode = prefix ? <span class={`${prefixCls}-prefix`}>{prefix}</span> : null;

      const affixWrapperCls = classNames(`${prefixCls}-affix-wrapper`, {
        [`${prefixCls}-affix-wrapper-focused`]: focused,
        [`${prefixCls}-affix-wrapper-disabled`]: disabled,
        [`${prefixCls}-affix-wrapper-sm`]: size === 'small',
        [`${prefixCls}-affix-wrapper-lg`]: size === 'large',
        [`${prefixCls}-affix-wrapper-input-with-clear-btn`]: suffix && allowClear && value,
        [`${prefixCls}-affix-wrapper-rtl`]: direction === 'rtl',
        [`${prefixCls}-affix-wrapper-readonly`]: readonly,
        [`${prefixCls}-affix-wrapper-borderless`]: !bordered,
        // className will go to addon wrapper
        [`${attrs.class}`]: !hasAddon({ addonAfter, addonBefore }) && attrs.class,
        [`${prefixCls}-textarea`]: inputType === 'text',
      });
      return (
        <span
          ref={containerRef}
          class={affixWrapperCls}
          style={attrs.style}
          onMouseup={onInputMouseUp}
        >
          {prefixNode}
          {cloneElement(element, {
            style: { display: display ? 'none' : null },
            value,
            class: getInputClassName(prefixCls, bordered, size, disabled),
          })}
          {display}
          {suffixNode}
        </span>
      );
    };

    const renderInputWithLabel = (prefixCls, labeledElement) => {
      const {
        addonBefore = slots.addonBefore?.(),
        addonAfter = slots.addonAfter?.(),
        size,
        direction,
      } = props;
      const { addonBeforeStyle, addonAfterStyle, addonBeforeClass, addonAfterClass } = attrs;
      // Not wrap when there is not addons
      if (!hasAddon({ addonBefore, addonAfter })) {
        return labeledElement;
      }

      const wrapperClassName = `${prefixCls}-group`;
      const addonClassName = `${wrapperClassName}-addon`;
      const addonBeforeNode = addonBefore ? (
        <span class={[addonClassName, addonBeforeClass]} style={addonBeforeStyle}>
          {addonBefore}
        </span>
      ) : null;
      const addonAfterNode = addonAfter ? (
        <span class={[addonClassName, addonAfterClass]} style={addonAfterStyle}>
          {addonAfter}
        </span>
      ) : null;

      const mergedWrapperClassName = classNames(`${prefixCls}-wrapper`, wrapperClassName, {
        [`${wrapperClassName}-rtl`]: direction === 'rtl',
      });

      const mergedGroupClassName = classNames(
        `${prefixCls}-group-wrapper`,

        {
          [`${prefixCls}-group-wrapper-sm`]: size === 'small',
          [`${prefixCls}-group-wrapper-lg`]: size === 'large',
          [`${prefixCls}-group-wrapper-rtl`]: direction === 'rtl',
        },
        attrs.class,
      );

      // Need another wrapper for changing display:table to display:inline-block
      // and put style prop in wrapper
      return (
        <span class={mergedGroupClassName} style={attrs.style}>
          <span class={[mergedWrapperClassName, 'je-addon-group-wrapper']}>
            {addonBeforeNode}
            {cloneElement(labeledElement, { style: null })}
            {addonAfterNode}
          </span>
        </span>
      );
    };

    const renderTextAreaWithClearIcon = (prefixCls, element) => {
      // TODO: textarea支持input所有插槽
      return renderInputWithLabel(prefixCls, renderLabeledIcon(prefixCls, element));
    };

    return () => {
      const { prefixCls, inputType, element = slots.element?.() } = props;
      if (inputType === ClearableInputType[0]) {
        return renderTextAreaWithClearIcon(prefixCls, element);
      }
      return renderInputWithLabel(prefixCls, renderLabeledIcon(prefixCls, element));
    };
  },
});
