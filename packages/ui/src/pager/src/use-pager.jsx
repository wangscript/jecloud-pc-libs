import { ref, toRaw } from 'vue';
import { omit } from '@jecloud/utils';
import { useModelValue } from '../../hooks';
export const pagerConfigKey = 'pagerConfig';
export const pagerEmits = ['update:pagerConfig', 'update:seqConfig'];
export const pagerProps = { pagerConfig: [Boolean, Object] };

export function usePager({ props, context, config }) {
  const { emit } = context;
  const $plugin = ref();
  // 分页条
  const pagerConfig = config || useModelValue({ props, context, key: pagerConfigKey }).value;
  const pageData = props.store ?? pagerConfig;
  // 刷新方法
  const onLoad = function ({ $pager, type }) {
    const pageInfo = toRaw($pager.pageInfo.value);
    if (props.store) {
      props.store.pagerLoad(type, pageInfo);
    } else if (pagerConfig.onLoad) {
      pagerConfig.onLoad(pageInfo);
    } else {
      emit('load', pageInfo);
    }
  };
  // 分页条插槽
  const pagerSlot = pagerConfig
    ? () => {
        return (
          <Pager
            ref={$plugin}
            background
            onLoad={onLoad}
            size={pagerConfig.size || props?.size}
            v-model:current-page={pageData.currentPage}
            v-model:page-size={pageData.pageSize}
            total={pageData.totalCount}
            v-slots={pagerConfig.slots}
            {...omit(pagerConfig, ['slots', 'pageSize', 'currentPage', 'size', 'onLoad', 'total'])}
          />
        );
      }
    : null;

  return { pagerSlot, $pager: $plugin };
}
