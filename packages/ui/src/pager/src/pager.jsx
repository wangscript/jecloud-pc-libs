import { defineComponent, h, computed, ref, nextTick, toRaw, watch } from 'vue';
import { uniqueId, toNumber, toString, isNumber, isFunction, toTemplate } from '@jecloud/utils';
import { hasEventKey, EVENT_KEYS } from '../../utils/event';
import Select from '../../select';
import Dropdown from '../../dropdown';
import Menu from '../../menu';
import { useInjectGrid } from '../../grid/src/context';
import { useModelValue } from '../../hooks';
const GlobalConfig = {
  pager: {
    size: 'middle',
    pageSize: 30,
    pagerCount: 7,
    align: 'left',
    layouts: ['Sizes', 'Message', 'JumpNumber', 'FullJump', 'Refresh', 'Size'],
    pageSizes: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
    sizes: [
      { text: '宽松', value: 'large', icon: 'jeicon jeicon-spacious' },
      { text: '舒适', value: 'middle', icon: 'jeicon jeicon-comfortable' },
      { text: '紧凑', value: 'small', icon: 'jeicon jeicon-compact-list' },
      { text: '迷你', value: 'mini', icon: 'jeicon jeicon-mini' },
    ],
  },
  i18n(key) {
    const pager = {
      goto: '前往 :',
      pagesize: ' 显示行数 : ',
      total: '共 {total} 条记录',
      message: '{start}-{end}条/共{total}条',
      pageClassifier: '页',
      prevPage: '上一页',
      nextPage: '下一页',
      prevJump: '向上跳页',
      nextJump: '向下跳页',
    };
    return pager[key];
  },
  icon: {
    // pager
    PAGER_JUMP_PREV: 'fal fa-angle-double-left',
    PAGER_JUMP_NEXT: 'fal fa-angle-double-right',
    PAGER_PREV_PAGE: 'fal fa-angle-left',
    PAGER_NEXT_PAGE: 'fal fa-angle-right',
    PAGER_JUMP_MORE: 'fal fa-ellipsis-h',
  },
};
function validSize(size) {
  return !!GlobalConfig.pager.sizes.find((item) => item.value === size);
}
export default defineComponent({
  name: 'JePager',
  components: { Select, Dropdown, Menu },
  props: {
    size: {
      type: String,
      default: () => GlobalConfig.pager.size,
      validator: (value) => validSize(value),
    },
    // 自定义布局
    layouts: {
      type: Array,
      default: () => GlobalConfig.pager.layouts,
    },
    // 当前页
    currentPage: { type: Number, default: 1 },
    // 加载中
    loading: Boolean,
    // 每页大小
    pageSize: { type: Number, default: () => GlobalConfig.pager.pageSize },
    // 总条数
    total: { type: Number, default: 0 },
    // 显示页码按钮的数量
    pagerCount: { type: Number, default: () => GlobalConfig.pager.pagerCount },
    // 每页大小选项列表
    pageSizes: {
      type: Array,
      default: () => GlobalConfig.pager.pageSizes,
    },
    // 列对其方式
    align: { type: String, default: () => GlobalConfig.pager.align },
    // 带边框
    border: { type: Boolean, default: () => GlobalConfig.pager.border },
    // 带背景颜色
    background: { type: Boolean, default: () => GlobalConfig.pager.background },
    // 配套的样式
    perfect: { type: Boolean, default: () => GlobalConfig.pager.perfect },
    // 当只有一页时隐藏
    autoHidden: { type: Boolean, default: () => GlobalConfig.pager.autoHidden },
    transfer: { type: Boolean, default: () => GlobalConfig.pager.transfer },
    className: [String, Function],
    // 自定义图标
    iconPrevPage: String,
    iconJumpPrev: String,
    iconJumpNext: String,
    iconNextPage: String,
    iconJumpMore: String,
  },
  emits: ['update:pageSize', 'update:currentPage', 'update:size', 'page-change', 'load'],
  setup(props, context) {
    const { slots, emit } = context;
    const size = useModelValue({ props, context, key: 'size' });
    const pageSize = useModelValue({ props, context, key: 'pageSize' });
    const currentPage = useModelValue({ props, context, key: 'currentPage' });
    const gridContext = useInjectGrid();

    const xID = uniqueId();

    const refElem = ref();
    // 分页条信息
    const pageInfo = computed(() => {
      const info = {
        pageSize: pageSize.value,
        currentPage: currentPage.value,
        total: props.total,
      };
      // 每页开始，结束条数的索引
      info.start = (currentPage.value - 1) * info.pageSize + 1;
      info.end = info.start + info.pageSize - 1;
      info.pageCount = getPageCount(info.total, info.pageSize);
      info.end = info.end > info.total ? info.total : info.end;
      info.message = toTemplate(props.message || GlobalConfig.i18n('message'), info);
      return info;
    });

    const refMaps = {
      refElem,
    };

    const $xepager = {
      xID,
      props,
      context,
      pageInfo,
      getRefMaps: () => refMaps,
    };

    let pagerMethods = {};
    let pagerPrivateMethods = {};

    // 页数
    const getPageCount = (total, size) => {
      return Math.max(Math.ceil(total / size), 1);
    };

    const computePageCount = computed(() => {
      return getPageCount(props.total, props.pageSize);
    });
    // 刷新编号
    watch(
      () => currentPage.value,
      () => {
        gridContext?.updateSeqConfig(pageInfo.value);
      },
    );

    // 分页事件
    const pageEvent = (type) => {
      const data = { ...toRaw(pageInfo.value), type };
      pagerMethods.dispatchEvent('page-change', data);
      pagerMethods.dispatchEvent('load', data);
    };
    // 跳转页面
    const jumpPageEvent = (page, evnt) => {
      currentPage.value = page;
      if (evnt && page !== props.currentPage) {
        pageEvent('pageChange');
      }
    };
    // 页面条数
    const pageSizeEvent = (value) => {
      const _pageSize = toNumber(value);
      currentPage.value = 1;
      pageSize.value = _pageSize;
      pageEvent('pageSize');
    };

    const triggerJumpEvent = (evnt) => {
      const inputElem = evnt.target;
      const inpValue = toNumber(inputElem.value);
      const pageCount = computePageCount.value;
      const current = inpValue <= 0 ? 1 : inpValue >= pageCount ? pageCount : inpValue;
      inputElem.value = toString(current);
      jumpPageEvent(current, evnt);
    };

    const computeNumList = computed(() => {
      const { pagerCount } = props;
      const pageCount = computePageCount.value;
      const len = pageCount > pagerCount ? pagerCount - 2 : pagerCount;
      const rest = [];
      for (let index = 0; index < len; index++) {
        rest.push(index);
      }
      return rest;
    });

    const computeOffsetNumber = computed(() => {
      return Math.floor((props.pagerCount - 2) / 2);
    });

    const computeSizeList = computed(() => {
      return props.pageSizes.map((item) => {
        if (isNumber(item)) {
          return {
            value: item,
            label: item,
          };
        }
        return { value: '', label: '', ...item };
      });
    });

    const handlePrevPage = (evnt) => {
      const { currentPage } = props;
      const pageCount = computePageCount.value;
      if (currentPage > 1) {
        jumpPageEvent(Math.min(pageCount, Math.max(currentPage - 1, 1)), evnt);
      }
    };

    const handleNextPage = (evnt) => {
      const { currentPage } = props;
      const pageCount = computePageCount.value;
      if (currentPage < pageCount) {
        jumpPageEvent(Math.min(pageCount, currentPage + 1), evnt);
      }
    };

    const handlePrevJump = (evnt) => {
      const numList = computeNumList.value;
      jumpPageEvent(Math.max(props.currentPage - numList.length, 1), evnt);
    };

    const handleNextJump = (evnt) => {
      const pageCount = computePageCount.value;
      const numList = computeNumList.value;
      jumpPageEvent(Math.min(props.currentPage + numList.length, pageCount), evnt);
    };

    const jumpKeydownEvent = (evnt) => {
      if (hasEventKey(evnt, EVENT_KEYS.ENTER)) {
        triggerJumpEvent(evnt);
      } else if (hasEventKey(evnt, EVENT_KEYS.ARROW_UP)) {
        evnt.preventDefault();
        handleNextPage(evnt);
      } else if (hasEventKey(evnt, EVENT_KEYS.ARROW_DOWN)) {
        evnt.preventDefault();
        handlePrevPage(evnt);
      }
    };

    // 上一页
    const renderPrevPage = () => {
      return (
        <button
          class={[
            'je-pager--prev-btn',
            {
              'is--disabled': props.currentPage <= 1,
            },
          ]}
          type="button"
          title={GlobalConfig.i18n('prevPage')}
          onClick={handlePrevPage}
        >
          <i
            class={['je-pager--btn-icon', props.iconPrevPage || GlobalConfig.icon.PAGER_PREV_PAGE]}
          ></i>
        </button>
      );
    };

    // 向上翻页
    const renderPrevJump = (moreIcon) => {
      return (
        <button
          class={[
            'je-pager--jump-prev',
            {
              'is--disabled': props.currentPage <= 1,
            },
          ]}
          type="button"
          title={GlobalConfig.i18n('prevJump')}
          onClick={handlePrevJump}
        >
          {moreIcon ? (
            <i
              class={[
                'je-pager--btn-icon',
                'je-pager--jump-more-icon',
                props.iconJumpMore || GlobalConfig.icon.PAGER_JUMP_MORE,
              ]}
            ></i>
          ) : (
            ''
          )}
          <i
            class={['je-pager--jump-icon', props.iconJumpPrev || GlobalConfig.icon.PAGER_JUMP_PREV]}
          ></i>
        </button>
      );
    };

    // 向下翻页
    const renderNextJump = (moreIcon) => {
      const pageCount = computePageCount.value;
      return (
        <button
          class={[
            'je-pager--jump-next',
            {
              'is--disabled': props.currentPage >= pageCount,
            },
          ]}
          type="button"
          title={GlobalConfig.i18n('nextJump')}
          onClick={handleNextJump}
        >
          {moreIcon ? (
            <i
              class={[
                'je-pager--btn-icon',
                'je-pager--jump-more-icon',
                props.iconJumpMore || GlobalConfig.icon.PAGER_JUMP_MORE,
              ]}
            ></i>
          ) : (
            ''
          )}
          <i
            class={['je-pager--jump-icon', props.iconJumpNext || GlobalConfig.icon.PAGER_JUMP_NEXT]}
          ></i>
        </button>
      );
    };

    // 下一页
    const renderNextPage = () => {
      const pageCount = computePageCount.value;
      return (
        <button
          class={[
            'je-pager--next-btn',
            {
              'is--disabled': props.currentPage >= pageCount,
            },
          ]}
          type="button"
          title={GlobalConfig.i18n('nextPage')}
          onClick={handleNextPage}
        >
          <i
            class={['je-pager--btn-icon', props.iconNextPage || GlobalConfig.icon.PAGER_NEXT_PAGE]}
          ></i>
        </button>
      );
    };

    // 页数
    const renderNumber = (showJump) => {
      const { currentPage, pagerCount } = props;
      const nums = [];
      const pageCount = computePageCount.value;
      const numList = computeNumList.value;
      const offsetNumber = computeOffsetNumber.value;
      const isOv = pageCount > pagerCount;
      const isLt = isOv && currentPage > offsetNumber + 1;
      const isGt = isOv && currentPage < pageCount - offsetNumber;
      let startNumber = 1;
      if (isOv) {
        if (currentPage >= pageCount - offsetNumber) {
          startNumber = Math.max(pageCount - numList.length + 1, 1);
        } else {
          startNumber = Math.max(currentPage - offsetNumber, 1);
        }
      }
      if (showJump) {
        nums.push(renderPrevPage());
      }
      if (showJump && isLt) {
        nums.push(
          <button
            class="je-pager--num-btn"
            type="button"
            onClick={(evnt) => jumpPageEvent(1, evnt)}
          >
            1
          </button>,
          renderPrevJump(true),
        );
      }
      numList.forEach((item, index) => {
        const number = startNumber + index;
        if (number <= pageCount) {
          nums.push(
            <button
              key={number}
              class={[
                'je-pager--num-btn',
                {
                  'is--active': currentPage === number,
                },
              ]}
              type="button"
              onClick={(evnt) => jumpPageEvent(number, evnt)}
            >
              {number}
            </button>,
          );
        }
      });

      if (showJump && isGt) {
        nums.push(
          renderNextJump(true),
          <button
            class="je-pager--num-btn"
            type="button"
            onClick={(evnt) => jumpPageEvent(pageCount, evnt)}
          >
            {pageCount}
          </button>,
        );
      }
      if (showJump) {
        nums.push(renderNextPage());
      }
      return <span class="je-pager--btn-wrapper">{nums}</span>;
    };

    // jumpNumber
    const renderJumpNumber = () => {
      return renderNumber(true);
    };

    const selectRef = ref();
    // sizes
    const renderSizes = () => {
      const sizeList = computeSizeList.value;
      return (
        <span class="je-pager--sizes">
          <span>{GlobalConfig.i18n('pagesize')}</span>
          <Select
            allowClear={false}
            getPopupContainer={() => refElem.value}
            ref={selectRef}
            class="je-pager--sizes-select"
            dropdownClassName="je-pager--sizes-select-dropdown"
            placement="topLeft"
            v-model:value={pageSize.value}
            options={sizeList}
            onChange={pageSizeEvent}
          ></Select>
        </span>
      );
    };

    // Jump
    const renderJump = (isFull) => {
      return h(
        'span',
        {
          class: 'je-pager--jump',
        },
        [
          isFull
            ? h(
                'span',
                {
                  class: 'je-pager--goto-text',
                },
                GlobalConfig.i18n('goto'),
              )
            : null,
          h('input', {
            class: 'je-pager--goto',
            value: props.currentPage,
            type: 'text',
            autocomplete: 'off',
            onKeydown: jumpKeydownEvent,
            onBlur: triggerJumpEvent,
          }),
          isFull
            ? h(
                'span',
                {
                  class: 'je-pager--classifier-text',
                },
                GlobalConfig.i18n('pageClassifier'),
              )
            : null,
        ],
      );
    };

    // FullJump
    const renderFullJump = () => {
      return renderJump(true);
    };

    // PageCount
    const renderPageCount = () => {
      const pageCount = computePageCount.value;
      return h(
        'span',
        {
          class: 'je-pager--count',
        },
        [
          h('span', {
            class: 'je-pager--separator',
          }),
          h('span', pageCount),
        ],
      );
    };

    // total
    const renderTotal = () => {
      return h(
        'span',
        {
          class: 'je-pager--total',
        },
        toTemplate(GlobalConfig.i18n('total'), props),
      );
    };
    // message
    const renderMessage = () => {
      return h(
        'span',
        {
          class: 'je-pager--message',
        },
        pageInfo.value.message,
      );
    };
    // refresh
    const renderRefresh = () => {
      return (
        <span
          class="je-pager--icon-btn je-pager--refresh"
          onClick={() => {
            pageEvent('refresh');
          }}
        >
          <i class="fal fa-sync"></i>
        </span>
      );
    };
    // sizes
    const onChangeSize = function ({ key }) {
      gridContext.size.value = key;
      size.value = key;
    };
    const sizeRef = ref([gridContext?.size?.value]);
    const renderSize = () => {
      return gridContext ? (
        <span class="je-pager--icon-btn je-pager--size">
          <Dropdown
            placement="top"
            trigger={['click']}
            v-slots={{
              overlay: () => {
                return (
                  <Menu onClick={onChangeSize} selectable v-model:selectedKeys={sizeRef.value}>
                    {GlobalConfig.pager.sizes.map((item) => (
                      <Menu.Item key={item.value}>
                        <div style="padding:2px 5px;width:80px;text-align:center;">
                          <i
                            class={item.icon}
                            style="margin-right:10px;font-weight: bold;font-size:14px;"
                          ></i>
                          {item.text}
                        </div>
                      </Menu.Item>
                    ))}
                  </Menu>
                );
              },
            }}
          >
            <i class="jeicon jeicon-width"></i>
          </Dropdown>
        </span>
      ) : null;
    };

    pagerMethods = {
      dispatchEvent(type, params, evnt) {
        emit(type, Object.assign({ $pager: $xepager, $event: evnt }, params));
      },
      prevPage() {
        handlePrevPage();
        return nextTick();
      },
      nextPage() {
        handleNextPage();
        return nextTick();
      },
      prevJump() {
        handlePrevJump();
        return nextTick();
      },
      nextJump() {
        handleNextJump();
        return nextTick();
      },
    };

    pagerPrivateMethods = {
      handlePrevPage,
      handleNextPage,
      handlePrevJump,
      handleNextJump,
    };

    Object.assign($xepager, pagerMethods, pagerPrivateMethods);

    const renderVN = () => {
      const { align, layouts, className } = props;
      const childNodes = [];
      const vSize = size.value;
      const pageCount = computePageCount.value;
      if (slots.left) {
        childNodes.push(
          h(
            'span',
            {
              class: 'je-pager--left-wrapper',
            },
            slots.left(),
          ),
        );
      }
      layouts.forEach((name) => {
        let renderFn;
        switch (name) {
          case 'PrevPage':
            renderFn = renderPrevPage;
            break;
          case 'PrevJump':
            renderFn = renderPrevJump;
            break;
          case 'Number':
            renderFn = renderNumber;
            break;
          case 'JumpNumber':
            renderFn = renderJumpNumber;
            break;
          case 'NextJump':
            renderFn = renderNextJump;
            break;
          case 'NextPage':
            renderFn = renderNextPage;
            break;
          case 'Sizes':
            renderFn = renderSizes;
            break;
          case 'FullJump':
            renderFn = renderFullJump;
            break;
          case 'Jump':
            renderFn = renderJump;
            break;
          case 'PageCount':
            renderFn = renderPageCount;
            break;
          case 'Total':
            renderFn = renderTotal;
            break;
          case 'Message':
            renderFn = renderMessage;
            break;
          case 'Refresh':
            renderFn = renderRefresh;
            break;
          case 'Size':
            renderFn = renderSize;
            break;
        }
        if (renderFn) {
          childNodes.push(renderFn());
        }
      });
      if (slots.right) {
        childNodes.push(
          h(
            'span',
            {
              class: 'je-pager--right-wrapper',
            },
            slots.right(),
          ),
        );
      }
      return h(
        'div',
        {
          ref: refElem,
          class: [
            'je-pager',
            className ? (isFunction(className) ? className({ $pager: $xepager }) : className) : '',
            {
              [`size--${vSize}`]: vSize,
              [`align--${align}`]: align,
              'is--border': props.border,
              'is--background': props.background,
              'is--perfect': props.perfect,
              'is--hidden': props.autoHidden && pageCount === 1,
              'is--loading': props.loading,
            },
          ],
        },
        [
          h(
            'div',
            {
              class: 'je-pager--wrapper',
            },
            childNodes,
          ),
        ],
      );
    };

    $xepager.renderVN = renderVN;

    return $xepager;
  },
  render() {
    return this.renderVN();
  },
});
