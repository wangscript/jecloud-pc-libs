import { defineComponent, ref } from 'vue';
import Input from '../../input';
import Modal from '../../modal';
import IconPanel from './icon-panel';
import { useModelValue, useListeners } from '../../hooks';
export default defineComponent({
  name: 'JeIcon',
  components: { Input, IconPanel, Modal },
  props: { value: { type: String, default: '' } },
  emits: ['update:value', 'select'],
  setup(props, context) {
    const { slots, attrs } = context;
    const { fireListener } = useListeners({ props, context });
    const value = useModelValue({ props, context, changeValid: true });
    const visible = ref(false);
    const panel = ref();

    const showIcon = () => {
      visible.value = true;
      panel.value?.reset();
    };
    const selectIcon = (icon) => {
      const selectResult = fireListener('select', { value: icon, visible });
      if (selectResult !== false) {
        value.value = icon;
        visible.value = false;
      }
    };

    return () => (
      <>
        <Input
          {...attrs}
          allow-clear
          v-model:value={value.value}
          class="je-input-icon"
          v-slots={{
            ...slots,
            prefix: () => <i class={['icon-value', value.value]} style={{ color: value.value }} />,
            suffix: () => <i class="fal fa-icons icon-button" onClick={showIcon} />,
          }}
        ></Input>
        <Modal
          v-model:visible={visible.value}
          show-header={false}
          width="1000"
          body-style="padding:10px 0;"
        >
          <IconPanel
            ref={panel}
            copy={false}
            help={false}
            size="small"
            onSelect={selectIcon}
            v-slots={{
              action: () => (
                <i
                  class="fal fa-times action"
                  style="margin-right:10px;"
                  onClick={() => {
                    visible.value = false;
                  }}
                ></i>
              ),
            }}
          ></IconPanel>
        </Modal>
      </>
    );
  },
});
