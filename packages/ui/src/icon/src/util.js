import jeicon from '../../assets/fonts/jeicon/iconfont.json';
import faicon from '../../assets/fonts/fa/fa.json';
import i18n from '../../assets/fonts/i18n.json';
const jeIcons = [];
jeicon.glyphs.forEach((item) => {
  jeIcons.push(item.font_class);
});

const iconData = {
  jeicon: jeIcons,
  solid: faicon,
};
const icons = [
  { code: 'light', text: '细体图标', cls: 'fal', prefix: 'fa', name: 'solid' },
  { code: 'solid', text: '实心图标', cls: 'fas', prefix: 'fa', name: 'solid' },
  { code: 'jeicon', text: 'JEIcons', cls: 'jeicon', prefix: 'jeicon', name: 'jeicon' },
];

const loadIcons = function ({ keyword, type, page, limit }) {
  const icon = icons.find((item) => {
    return item.code === type;
  });
  let data = iconData[icon?.name];
  if (keyword) {
    data = data.filter((item) => {
      return (
        item.includes(keyword) ||
        i18n[item]?.includes(keyword) ||
        !!item.split('-').find((key) => i18n[key]?.includes(keyword))
      );
    });
  }
  const totalCount = data.length;
  const start = (page - 1) * limit;
  const end = start + limit;
  return { data: data.slice(start, end), totalCount };
};
export { loadIcons, icons };
