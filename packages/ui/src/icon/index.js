import { withInstall } from '../utils';
import JeIcon from './src/icon';
import JeIconPanel from './src/icon-panel';
import JeIconItem from './src/icon-item';
import { icons } from './src/util';

JeIcon.icons = icons; // 默认图标数据

JeIcon.installComps = {
  // 注册依赖组件
  IconPanel: { name: JeIconPanel.name, comp: JeIconPanel },
  IconItem: { name: JeIconItem.name, comp: JeIconItem },
};

export const Icon = withInstall(JeIcon);

export default Icon;
