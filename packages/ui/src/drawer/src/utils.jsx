import { ref } from 'vue';
import Drawer from './drawer';
import { renderVNode } from '../../modal/src/dialog';
/**
 * 函数打开抽屉
 * @param {*} config
 * @returns
 */
export const show = (config) => {
  // v-model:visible
  const visible = ref(config.visible ?? true);
  Object.assign(config, {
    destroyOnClose: true,
    visible: visible.value,
    'onUpdate:visible': (val) => {
      visible.value = val;
    },
  });
  const refEl = ref();
  renderVNode({
    props: config,
    slot: (props) => (visible.value ? <Drawer ref={refEl} {...props} /> : null),
  });
  return refEl.value;
};
