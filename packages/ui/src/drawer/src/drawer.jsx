import { defineComponent, ref } from 'vue';
import { Drawer } from 'ant-design-vue';
import { useModelValue } from '../../hooks';
import { nextZIndex } from '../../modal/src/hooks/use-manager';

export default defineComponent({
  name: 'JeDrawer',
  inheritAttrs: false,
  props: {
    visible: Boolean,
    closeIconSlot: Function,
    extraSlot: Function,
    footerSlot: Function,
    content: Function,
    zIndex: Number,
    width: {
      type: [Number, String],
      default: 375,
    },
  },
  emits: ['update:visible'],
  setup(props, context) {
    const { slots, attrs, expose } = context;
    const visible = useModelValue({ props, context, key: 'visible' });
    const zIndex = ref(props.zIndex ?? nextZIndex());
    const widthValue = ref(props.width);
    expose({
      close() {
        visible.value = false;
      },
      changeWidth(value) {
        widthValue.value = value;
      },
    });
    // 参数插槽
    const propSlots = {
      default() {
        return props.content?.({ visible }) ?? slots.default?.({ visible });
      },
    };
    ['extra', 'footer', 'closeIcon'].forEach((key) => {
      if (props[`${key}Slot`] || slots[key]) {
        propSlots[key] = () => {
          return props[`${key}Slot`]?.({ visible }) ?? slots[key]?.({ visible });
        };
      }
    });
    return () => (
      <Drawer
        {...attrs}
        width={widthValue.value}
        zIndex={zIndex.value}
        v-model:visible={visible.value}
        v-slots={propSlots}
      />
    );
  },
});
