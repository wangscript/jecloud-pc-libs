import { ref } from 'vue';
import { useField } from './field';
import { useFunc } from './use-func';
import {
  isArray,
  isEmpty,
  loadDDItemByCode,
  getDDCache,
  DDTypeEnum,
  loadDDItemByCode4Tree,
  omit,
  isNotEmpty,
  toQuerysTemplate,
  cloneDeep,
  pick,
} from '@jecloud/utils';
import { useModelValue, useListeners } from '.';

export function useConfigInfo({ props, context }) {
  const { fireListener } = useListeners({ props, context });
  const options = useModelValue({ props, context, key: 'options' });
  const multiple = ref(props.multiple);
  const func = useFunc();
  let setValues = () => {};
  let onDropdownVisibleChange = () => {}; // 下拉框展开请求数据，非列表字典起效
  // 配置项
  const config = pick(props, ['configInfo', 'querys', 'dicValueConfig']);
  if (config.configInfo && func) {
    // 功能对象
    const $func = func.useInjectFunc();
    const { setModel, getModelOwner, getModel } = useField({ props, context });
    // 解析配置信息
    const configInfo = func.parseConfigInfo({ configInfo: config.configInfo });
    multiple.value = configInfo.multiple;
    // 设置值
    setValues = (values = []) => {
      if (isNotEmpty(values)) {
        values = isArray(values) ? values : [values];
        // 设置配置项的值
        const data = func.getConfigInfo({ configInfo, rows: values });
        setModel(data);
      }
    };
    // 请求字典项数据
    if (isEmpty(options.value)) {
      // 树形请求
      onDropdownVisibleChange = (open) => {
        if (!open) return;
        // 配置信息
        const cloneConfig = cloneDeep(config);
        // 加载前事件，可以修改配置信息
        if (
          fireListener('before-select', {
            ...getModelOwner(),
            options: cloneConfig,
            data: options,
          }) !== false
        ) {
          // 加载数据
          loadSelectOptions({
            ...cloneConfig,
            model: getModel(),
            parentModel: $func?.parentFunc?.store.activeBean,
            options,
          });
        }
      };
      // 初始化数据
      onDropdownVisibleChange(true);
    }
  }

  return { func, options, multiple, setValues, onDropdownVisibleChange };
}

/**
 * 加载字典数据
 * @param {*} config
 */
function loadSelectOptions(config) {
  const { querys, options } = config;
  config.code = useFunc().parseConfigInfo({ configInfo: config.configInfo }).code;
  const ddCache = getDDCache(config.code);
  if (!ddCache) {
    options.value = [];
    return;
  }
  // 树形或sql查询
  if (ddCache?.type !== DDTypeEnum.LIST || isNotEmpty(querys)) {
    options.value = [];
    loadSelectOptionsByQuery(config).then((data) => {
      options.value = data;
    });
  } else {
    // 普通字典
    loadDDItemByCode(config.code).then((data) => {
      options.value = data.map?.((item) => {
        return Object.assign(item, { value: item.code, label: item.text });
      });
    });
  }
}

/**
 * 加载字典数据,sql
 * @param {*} options
 */
function loadSelectOptionsByQuery(options) {
  const { querys, model, parentModel, dicValueConfig, code } = options;
  toQuerysTemplate({ querys, model, parentModel });
  // 字典过滤数据
  const customVariables = {};
  if (isNotEmpty(dicValueConfig)) {
    toQuerysTemplate({
      querys: dicValueConfig,
      model,
      parentModel,
    });
    dicValueConfig.forEach((item) => {
      customVariables[item.code] = item.value;
    });
  }
  // 解析RootID
  let rootId = 'ROOT';
  querys?.forEach?.((item) => {
    if (item.code === 'SY_PARENT') {
      rootId = item.value;
    }
  });
  return loadDDItemByCode4Tree({
    rootId,
    code,
    querys,
    customVariables,
    excludes: ['children', 'nodeType'],
  }).then((data) => {
    return data.map?.((item) => {
      return Object.assign(omit(item, ['disabled']), {
        value: item.code,
        label: item.text,
      });
    });
  });
}
