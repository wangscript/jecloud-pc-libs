import { defineComponent } from 'vue';
import { Tooltip } from 'ant-design-vue';

export default defineComponent({
  name: 'JeTooltip',
  components: { ATooltip: Tooltip },
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    props;
    return () => <a-tooltip {...attrs} v-slots={slots}></a-tooltip>;
  },
});
