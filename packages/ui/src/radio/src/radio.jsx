import { defineComponent, ref } from 'vue';
import { Radio } from 'ant-design-vue';
import { useExtendMethods } from '../../hooks';

export default defineComponent({
  name: 'JeRadio',
  inheritAttrs: false,
  setup(props, { slots, attrs, expose }) {
    const $plugin = ref();
    const methods = useExtendMethods({ plugin: $plugin, keys: ['blur', 'focus'] });
    expose({
      ...methods,
      $plugin,
    });
    return () => <Radio class="je-radio" {...attrs} v-slots={slots} />;
  },
});
