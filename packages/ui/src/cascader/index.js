/*
 * @Author: hjj
 */
import JeCascader from './src/cascader';
import { withInstall } from '../utils';

export const Cascader = withInstall(JeCascader);

export default Cascader;
