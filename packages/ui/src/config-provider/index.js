import { withInstall } from '../utils';
import JeConfigProvider from './src/config-provider';

const globalConfig = {
  // appConfig Vue全局对象
};
const setup = (config) => {
  Object.assign(globalConfig, config ?? {});
};
/**
 * 获取系统配置
 * @param {String} key 属性值
 * @param {String} defaultVal 默认值
 * @returns 配置信息
 */
const getGlobalConfig = (key, defaultVal) => {
  return globalConfig[key] ?? defaultVal;
};

JeConfigProvider.setup = setup;
JeConfigProvider.getGlobalConfig = getGlobalConfig;
export const ConfigProvider = withInstall(JeConfigProvider, (app) => {
  setup({ appContext: app });
});

export default ConfigProvider;
