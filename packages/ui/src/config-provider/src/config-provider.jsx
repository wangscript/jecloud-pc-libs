import { defineComponent } from 'vue';
export default defineComponent({
  name: 'JeConfigProvider',
  inheritAttrs: false,
  setup(props, { slots }) {
    return () => slots.default?.();
  },
});
