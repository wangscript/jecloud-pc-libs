export * from './components';
import * as components from './components';
import * as File from './utils/file';
export { File };
export { Data } from '@jecloud/utils'; // 兼容代码
let $vue;
export function useAppContext() {
  return $vue;
}
export function setAppContext(app) {
  $vue = app;
}

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
export const install = function (app) {
  setAppContext(app);
  // 遍历注册全局组件
  Object.keys(components).forEach((key) => {
    const component = components[key];
    if (component.install) {
      app.use(component);
    }
  });
  app.config.globalProperties.$JEMessage = components.message;
  app.config.globalProperties.$JENotice = components.notification;
  app.config.globalProperties.$JEModal = components.Modal;
};
// 导出的对象必须具有 install，才能被 Vue.use() 方法安装
export default { install };
