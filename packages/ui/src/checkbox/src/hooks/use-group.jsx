import { ref, h, watch } from 'vue';
import { useConfigInfo, useModelValue } from '../../../hooks';
import { pick, isNotEmpty } from '@jecloud/utils';
import { Row, Col } from 'ant-design-vue';

export function useGroup({ props, context, component, multiple, useRow = true }) {
  const { slots, emit } = context;
  const wrap = ref(props.wrap);
  const { options, setValues } = useConfigInfo({ props, context });
  const modelValue = useModelValue({ props, context, multiple, changeEvent: true });
  const onChange = function (value) {
    let valueOptions = [];
    if (isNotEmpty(options.value)) {
      const vals = multiple ? value : [value.target?.value];
      valueOptions = options.value.filter((item) => vals.includes(item.value));
      setValues(valueOptions);
    }
    emit('change', modelValue.value, valueOptions);
  };

  const itemSlot = (option) => {
    const { label, value, disabled } = option;
    return h(
      component,
      { value, disabled },
      {
        default() {
          return slots.option?.(option) ?? label;
        },
      },
    );
  };

  // 监听布局数据,刷新页面布局
  watch(
    () => props.wrap,
    (newVal) => {
      wrap.value = newVal;
    },
  );

  const optionsSlot = function () {
    const colProps = {};
    if (props.cols > 0) {
      colProps.span = 24 / props.cols;
      wrap.value = true;
    }
    return useRow ? (
      <Row {...pick(props, ['align', 'justify'])} wrap={wrap.value}>
        {options.value?.map((option) => {
          return <Col {...colProps}>{itemSlot(option)}</Col>;
        })}
      </Row>
    ) : (
      options.value?.map((option) => {
        return itemSlot(option);
      })
    );
  };
  return { optionsSlot, onChange, options, modelValue };
}

export const groupProps = {
  model: Object,
  value: String,
  options: Array,
  configInfo: String,
  cols: { type: Number, validator: (value) => [1, 2, 3, 4, 6, 12, 24].includes(value) },
  align: String,
  justify: String,
  wrap: Boolean,
  querys: Array,
  dicValueConfig: Array,
  parentModel: Object,
};

export const groupEmits = ['update:value', 'update:options', 'change'];
export const groupSlots = ['slot'];
