import { defineComponent } from 'vue';
import { Checkbox } from 'ant-design-vue';
import { useStyle4Size } from '../../hooks';
import { useGroup, groupEmits, groupProps, groupSlots } from './hooks/use-group';
import JeCheckbox from './checkbox';
import { filterEmpty } from '../../utils/props';
import { isEmpty } from '@jecloud/utils';
export default defineComponent({
  name: 'JeCheckboxGroup',
  inheritAttrs: false,
  props: groupProps,
  emits: groupEmits,
  slots: groupSlots,
  setup(props, context) {
    const { slots, attrs } = context;
    const { modelValue, optionsSlot, onChange } = useGroup({
      props,
      context,
      component: JeCheckbox,
      multiple: true,
    });
    return () => {
      const vnodes = filterEmpty(slots.default?.());
      return (
        <Checkbox.Group
          class={{
            'je-checkbox-group white-background': true,
            'disabled-background': attrs.disabled,
          }}
          style={useStyle4Size({ props })}
          {...attrs}
          v-model:value={modelValue.value}
          onChange={onChange}
        >
          {isEmpty(vnodes) ? optionsSlot() : vnodes}
        </Checkbox.Group>
      );
    };
  },
});
