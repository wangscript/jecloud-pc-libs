import { defineComponent, ref } from 'vue';
import { Checkbox } from 'ant-design-vue';
import { useExtendMethods } from '../../hooks';
export default defineComponent({
  name: 'JeCheckbox',
  inheritAttrs: false,
  setup(props, { slots, attrs, expose }) {
    const $plugin = ref();
    const methods = useExtendMethods({ plugin: $plugin, keys: ['blur', 'focus'] });
    expose({
      ...methods,
      $plugin,
    });
    return () => <Checkbox class="je-checkbox" ref={$plugin} {...attrs} v-slots={slots} />;
  },
});
