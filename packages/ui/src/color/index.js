import { withInstall } from '../utils';
import JeColor from './src/color';

export const Color = withInstall(JeColor);

export default Color;
