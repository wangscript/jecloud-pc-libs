import { ref } from 'vue';
import { computedDomSize } from '@jecloud/utils';
import { useModelValue } from '../../../hooks';

export function useEditor({ props, context }) {
  const $plugin = ref();
  const value = useModelValue({ props, context, changeEvent: true });
  const style = {};
  props.width && (style.width = `${computedDomSize({ size: props.width, unit: 'px' })}`);
  style.height = `${computedDomSize({ size: props.height || 400, unit: 'px' })}`;
  return { value, style, $plugin };
}
