import { defineComponent } from 'vue';
import { useMonaco } from './hooks/use-monaco';
import Panel from '../../panel';
import JeEditorCodeJsApi from './js-api';

export default defineComponent({
  name: 'JeEditorCode',
  inheritAttrs: true,
  props: {
    value: { type: String, default: '' },
    width: [String, Number],
    height: { type: [String, Number], default: 400 },
    language: { type: String, default: 'javascript' },
    editorOptions: Object,
    readonly: { type: Boolean, default: false },
    hideApi: { type: Boolean, default: false },
  },
  emits: [
    'init',
    'change',
    'save',
    'editor-config',
    'editor-save',
    'update:value',
    'update:language',
  ],
  setup(props, context) {
    const { expose, attrs } = context;
    const { container, getEditor, style, loading, setCursorValue, editorId } = useMonaco({
      props,
      context,
    });
    expose({ getEditor, setCursorValue });
    return () => (
      <Panel
        class="je-editor-code-panel"
        v-loading={loading.value}
        style={{ ...style, ...attrs.style }}
      >
        <Panel.Item>
          <div class="je-editor-code" id={editorId} ref={container}></div>
        </Panel.Item>
        {props.readonly || props.hideApi ? null : (
          <Panel.Item region="right" size="300" collapsed collapsible split>
            <JeEditorCodeJsApi setEditorCursorValue={setCursorValue} />
          </Panel.Item>
        )}
      </Panel>
    );
  },
});
