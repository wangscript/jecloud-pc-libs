import { defineComponent } from 'vue';
import { useJsApi } from './hooks/use-js-api';
import Tree from '../../tree';
import Button from '../../button';
import EditorCode from './editor-code';
import Tooltip from '../../tooltip';

export default defineComponent({
  name: 'JeEditorCodeJsApi',
  inheritAttrs: true,
  props: {
    setEditorCursorValue: Function,
  },
  setup(props, context) {
    const { $apiTree, treeStore, handlerSelect, state, handlerClick } = useJsApi({
      props,
      context,
    });
    return () => (
      <div class="je-editor-code-jsapi">
        <Tree
          store={treeStore}
          ref={$apiTree}
          bodyBorder
          class="je-editor-code-jsapi-tree"
          onCellClick={handlerSelect}
          onSearchSelect={handlerSelect}
          nodeColumn={{ showOverflow: false }}
          v-slots={{
            nodeDefault({ row }) {
              return (
                <Tooltip
                  v-slots={{
                    title() {
                      return row.bean.JEAPI_REMARK || row.text;
                    },
                  }}
                >
                  {row.text}
                </Tooltip>
              );
            },
          }}
        ></Tree>
        <div
          class={{
            'je-editor-code-jsapi-preview': true,
            'is-visible': state.visible,
            'is-maximize': state.maximize,
          }}
        >
          <EditorCode
            hideApi
            readonly
            v-model:value={state.value}
            editorOptions={{ minimap: { enabled: false }, contextmenu: false, lineNumbers: 'off' }}
          />
          <div class="footer">
            <Button
              type="primary"
              v-show={state.showButton}
              onClick={() => handlerClick({ type: 'fn' })}
            >
              应用方法：{state.method}
            </Button>
            <Button
              type="primary"
              v-show={state.showButton}
              onClick={() => handlerClick({ type: 'example' })}
            >
              应用案例
            </Button>
            <Button onClick={() => handlerClick({ close: true })}>关闭</Button>
          </div>
        </div>
      </div>
    );
  },
});
