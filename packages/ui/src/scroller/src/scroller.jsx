import { defineComponent } from 'vue';
import { useOverlayScrollbars } from './use-overlay-scrollbars';
// Scroller组件，具体操作对象为$plugin,操作API参考下面链接
// https://kingsora.github.io/OverlayScrollbars/#!documentation/options

export default defineComponent({
  name: 'JeScroller',
  props: {
    options: {
      type: Object,
      default: () => ({
        scrollbars: {
          autoHide: 'leave',
          autoHideDelay: 100,
        },
      }),
    },
  },
  setup(props, { slots, expose }) {
    const { scroller } = useOverlayScrollbars(props, expose);
    return () => (
      <div class="je-scroller os-host" ref={scroller}>
        <div class="os-resize-observer-host"></div>
        <div class="os-padding">
          <div class="os-viewport">
            <div class="os-content">{slots.default?.()}</div>
          </div>
        </div>
        <div class="os-scrollbar os-scrollbar-horizontal">
          <div class="os-scrollbar-track">
            <div class="os-scrollbar-handle"></div>
          </div>
        </div>
        <div class="os-scrollbar os-scrollbar-vertical">
          <div class="os-scrollbar-track">
            <div class="os-scrollbar-handle"></div>
          </div>
        </div>
        <div class="os-scrollbar-corner"></div>
      </div>
    );
  },
});
