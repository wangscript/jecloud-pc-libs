import { ref, watch } from 'vue';
import { isEmpty, isNotEmpty, getSystemConfig, ajax } from '@jecloud/utils';
import { FileType, calculateValueAndTitle, treeToList } from '../utils';

export function usePreviewFile({ props }) {
  const previewHandleStatus = ref(props.handleStatus); //弹窗右侧操作按钮
  const previewFileList = ref([]); //文件列表
  const previewFileKey = ref(''); //文件key
  const previewFileType = ref(''); //文件预览类型
  const previewFileName = ref(); //预览的文件名称
  const previewFileObj = ref(null); //正在预览的文件
  const previewListDataType = ref(props.treeDataType); //文件列表数据接口是否为树结构
  const previewTreeList = ref([]);
  const selectWidth = ref(0); //文件选择下拉框计算宽度
  const showOpenWindow = ref(props.showOpenNewView); //显示新窗口打开

  //查找最长的文件名称
  const findLongesName = (fileArr) => {
    let longest = 0;
    fileArr.forEach((fileItem) => {
      if (fileItem.label?.length > longest) {
        longest = fileItem.label?.length;
      }
    });
    return longest * 14 > 500 ? 500 : longest * 14;
  };

  //检查文件是否存在
  const checkFileExist = (file) => {
    if (isNotEmpty(file?.value) || isNotEmpty(file?.fileKey)) {
      ajax({
        url: '/je/document/checkFile',
        method: 'POST',
        params: { fileKey: file?.value ?? file?.fileKey },
      }).then((res) => {
        if (res.success) {
          judgeFilePreviewType(file.suffix);
        } else {
          previewFileType.value = FileType.NOFIND;
        }
      });
    }
  };

  //根据文件后缀判断文件预览方式
  const judgeFilePreviewType = (suffix) => {
    //获取文档设置配置
    const jsonSet = getSystemConfig('JE_SYS_DOCUMENT_FORMAT_PREVIEW_SETTING');
    if (isEmpty(jsonSet)) {
      switch (suffix.toLowerCase()) {
        case 'pdf':
          previewFileType.value = FileType.PDF;
          break;
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'bmp':
        case 'ico':
        case 'gif':
          previewFileType.value = FileType.IMAGE;
          break;
        default:
          previewFileType.value = FileType.NOSUPPORT;
          break;
      }
    } else {
      const documentSetList = JSON.parse(jsonSet);
      switch (suffix.toLowerCase()) {
        case 'pdf':
          for (let setItem of documentSetList) {
            if (setItem.type.toLowerCase() == 'pdf' && setItem.format.split(',').includes(suffix)) {
              if (setItem.previewWay == 1) {
                previewFileType.value = FileType.PDF;
              } else {
                previewFileType.value = FileType.NOSUPPORT;
              }
              break;
            }
          }
          break;
        case 'doc':
        case 'docx':
        case 'xls':
        case 'xlsx':
        case 'ppt':
        case 'pptx':
          for (let setItem of documentSetList) {
            if (
              setItem.type.toLowerCase() == 'office' &&
              setItem.format.split(',').includes(suffix)
            ) {
              switch (setItem.previewWay) {
                case 'pdf':
                  previewFileType.value = FileType.PDF;
                  break;
                case 'wps':
                  previewFileType.value = FileType.NOSUPPORT;
                  break;
                case 'no':
                  previewFileType.value = FileType.NOSUPPORT;
                  break;
                default:
                  break;
              }
            }
          }
          break;
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'bmp':
        case 'ico':
        case 'gif':
          //图片格式预览
          for (let setItem of documentSetList) {
            if (setItem.type == '图片' && setItem.format.split(',').includes(suffix)) {
              if (setItem.previewWay == 1) {
                previewFileType.value = FileType.IMAGE;
              } else {
                previewFileType.value = FileType.NOSUPPORT;
              }
              break;
            }
          }
          break;
        case 'mp3':
        case 'wma':
          //音频格式预览
          for (let setItem of documentSetList) {
            if (setItem.type == '音频' && setItem.format.split(',').includes(suffix)) {
              if (setItem.previewWay == 1) {
                previewFileType.value = FileType.AUDIO;
              } else {
                previewFileType.value = FileType.NOSUPPORT;
              }
              break;
            }
          }
          break;
        case 'mp4':
        case 'flv':
        case 'f4v':
        case 'mov':
          //视频格式预览
          for (let setItem of documentSetList) {
            if (setItem.type == '视频' && setItem.format.split(',').includes(suffix)) {
              if (setItem.previewWay == 1) {
                previewFileType.value = FileType.VIDEO;
              } else {
                previewFileType.value = FileType.NOSUPPORT;
              }
              break;
            }
          }
          break;
        default:
          previewFileType.value = FileType.NOSUPPORT;
          break;
      }
    }
  };

  watch(
    () => props.fileList,
    () => {
      if (props.fileList?.length) {
        //判断文件数据结构是否为树结构
        if (previewListDataType.value) {
          // previewFileList.value = calculateValueAndTitle(props.fileList);
          // previewTreeList.value = treeToList(props.fileList);
          // selectWidth.value = findLongesName(previewTreeList.value) + 30;
          if (props.fileList.length > 1) {
            previewFileList.value = calculateValueAndTitle(props.fileList);
            previewTreeList.value = treeToList(props.fileList);
            selectWidth.value = findLongesName(previewTreeList.value) + 40;
          } else {
            previewTreeList.value = treeToList(props.fileList);
            previewFileList.value = previewTreeList.value;
            previewFileList.value?.forEach((fileItem) => {
              fileItem.label = fileItem.text;
              fileItem.value = fileItem.id;
              fileItem.suffix = fileItem.nodeInfoType;
            });
            selectWidth.value = findLongesName(previewTreeList.value) + 40;
          }
        } else {
          //多文件下拉选项
          if (props.fileList?.length > 0) {
            props.fileList.forEach((file) => {
              let fileObj = {
                label: file.relName,
                value: file.fileKey,
                suffix: file.suffix,
              };
              previewFileList.value.push(fileObj);
            });
            selectWidth.value = findLongesName(previewFileList.value);
          }
        }
      }
      //判断是否存在预览的文件
      if (isEmpty(props.fileItem)) {
        //判断列表数据类型是否为树结构
        if (previewListDataType.value) {
          previewFileKey.value = previewTreeList.value[0]?.value;
        } else {
          previewFileKey.value = previewFileList.value[0]?.value;
        }
      } else {
        previewFileKey.value = props.fileItem?.fileKey;
      }
    },
    { immediate: true, deep: true },
  );

  //判断是否存在预览的文件
  if (isEmpty(props.fileItem)) {
    //判断列表数据类型是否为树结构
    if (previewListDataType.value) {
      previewFileKey.value = previewTreeList.value[0]?.value;
    } else {
      previewFileKey.value = previewFileList.value[0]?.value;
    }
  } else {
    previewFileKey.value = props.fileItem?.fileKey;
  }

  return {
    previewFileList,
    previewFileKey,
    previewHandleStatus,
    previewFileType,
    previewFileName,
    previewFileObj,
    previewListDataType,
    previewTreeList,
    selectWidth,
    showOpenWindow,
    judgeFilePreviewType,
    checkFileExist,
  };
}
