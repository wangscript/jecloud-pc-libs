import { defineComponent } from 'vue';
import noFilePreview from './assets/no-file-preview.png';

export default defineComponent({
  name: 'JePreviewNofile',
  inheritAttrs: false,
  setup() {
    return () => (
      <div class="je-preview-file-no-support">
        <div class="no-support-preview-box">
          <img src={noFilePreview} alt="" />
          <div class="no-support-info">没有可预览的文件！</div>
        </div>
      </div>
    );
  },
});
