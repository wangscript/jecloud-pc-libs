import { withInstall } from '../utils';
import JeFile from './src/preview-file';
export const PreviewFile = withInstall(JeFile);

export default PreviewFile;
