import { defineComponent } from 'vue';
import { Progress } from 'ant-design-vue';

export default defineComponent({
  name: 'JeProgress',
  components: { AProgress: Progress },
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    props;
    return () => <a-progress {...attrs} v-slots={slots}></a-progress>;
  },
});
