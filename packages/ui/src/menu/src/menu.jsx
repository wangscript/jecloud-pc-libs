import { defineComponent } from 'vue';
import { Menu } from 'ant-design-vue';

export default defineComponent({
  name: 'JeMenu',
  components: { Menu },
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <Menu {...attrs} v-slots={slots}></Menu>;
  },
});
