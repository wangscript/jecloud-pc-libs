import { getCurrentInstance } from 'vue';
import IconItem from '../../icon/src/icon-item';
export function useMenu(props, { slots }) {
  const instance = getCurrentInstance();
  // 获得key
  const key =
    typeof instance.vnode.key === 'symbol' ? String(instance.vnode.key) : instance.vnode.key;
  // 自定义图标
  const iconSlot = (...args) => {
    return slots.icon?.(...args) ?? props.icon ? (
      <IconItem icon={props.icon} style={{ color: props.iconColor }} />
    ) : null;
  };

  return { iconSlot, key };
}
