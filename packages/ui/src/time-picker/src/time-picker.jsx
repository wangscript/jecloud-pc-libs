import { defineComponent } from 'vue';
import { TimePicker } from 'ant-design-vue';
import { useDatePicker } from '../../date-picker/src/hooks';
import { useAddonSlot, addonProps, addonSlots } from '../../hooks/use-addon';

export default defineComponent({
  name: 'JeTimePicker',
  inheritAttrs: false,
  props: {
    format: { type: String },
    valueFormat: { type: String },
    value: { type: [String, Date] },
    ...addonProps,
  },
  slots: addonSlots,
  emits: ['update:value'],
  setup(props, context) {
    const { slots, attrs } = context;
    const { format, valueFormat, dateValue } = useDatePicker({ props, context, picker: 'time' });
    return () => {
      const element = (
        <TimePicker
          getPopupContainer={(triggerNode) => triggerNode.parentNode} // 绑定
          {...attrs}
          v-model:value={dateValue.value}
          format={format}
          valueFormat={valueFormat}
          v-slots={slots}
        />
      );
      return useAddonSlot({ props, context, element });
    };
  },
});
