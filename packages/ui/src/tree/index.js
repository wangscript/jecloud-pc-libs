import { withInstall } from '../utils';
import JeTree from './src/tree';
import '../grid/src/plugins/vxetable';
import { MethodKeys } from '../grid/src/table/method-keys';

export const Tree = withInstall(JeTree);
Tree.MethodKeys = MethodKeys;
export default Tree;
