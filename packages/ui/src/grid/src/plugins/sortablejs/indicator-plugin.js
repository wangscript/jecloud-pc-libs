import {
  getOffsetBox,
  toggleClass,
  createElement,
  getViewportOffset,
  isFunction,
} from '@jecloud/utils';
/**
 * 拖拽标记插件，支持插入标记
 *
 * @export
 * @param {*} Sortable
 */
export function useIndicatorPlugin(Sortable) {
  let lastSwapEl; // 最后替换元素
  let indicatorEl; // 标记元素
  function IndicatorPlugin() {
    function Indicator(sortable) {
      const { enableInsert, vertical, indicatorRootEl, indicatorScrollEl, sort } = sortable.options;
      Object.assign(this, {
        disabled: sort === false, // 不允许排序，禁用
        indicatorRootEl: indicatorRootEl ?? sortable.el, // 拖拽标记Root容器
        indicatorScrollEl, //拖拽标记滚动条计算容器
        vertical, // 纵向拖动
        enableInsert, // 允许插入标记
        baseClass: 'je-sortable-indicator', // 标记样式
        hoverClass: 'je-sortable-indicator-hover', // 标记样式
        insertTargetClass: 'je-sortable-indicator-insert', // 插入标记样式
        /**
         * 添加标记
         * @param {*} el
         * @param {*} toggles
         * @returns
         */
        toggleTargetClass: (el = lastSwapEl, toggles) => {
          if (!indicatorEl) return;
          indicatorEl.setStyle('display', toggles ? '' : 'none');
          // 添加标记
          toggleClass(el, this.insertTargetClass, toggles?.insert ?? false);
          // toggleClass(el, this.hoverClass, !!toggles);
          // 操作
          if (toggles) {
            if (toggles.insert) {
              indicatorEl.setStyle('display', 'none');
              return;
            }
            // 移动标记
            const { clientWidth, clientHeight } = el;
            const offsetBox = getOffsetBox(el, this.indicatorRootEl);
            const offsetTop = offsetBox.y;
            const offsetLeft = offsetBox.x;
            // 纵向移动
            if (vertical) {
              const scrollTop = -indicatorScrollEl?.scrollTop ?? 0;
              indicatorEl.setStyle({
                width: clientWidth + 'px',
                height: '3px',
                left: offsetLeft + 'px',
                top:
                  scrollTop + (toggles.prev ? offsetTop - 2 : offsetTop + clientHeight - 2) + 'px',
              });
              // 横向移动
            } else {
              const scrollLeft = -indicatorScrollEl?.scrollLeft ?? 0;
              indicatorEl.setStyle({
                width: '3px',
                height: clientHeight + 'px',
                top: offsetTop + 'px',
                left:
                  scrollLeft +
                  (toggles.prev ? offsetLeft - 2 : offsetLeft + clientWidth - 2) +
                  'px',
              });
            }
          }
        },
      });

      // 增加定位标记
      this.indicatorRootEl.style.position = 'relative';
    }

    Indicator.prototype = {
      /**
       * 开始拖动
       * @param {*} param0
       */
      dragStarted({ dragEl }) {
        lastSwapEl = dragEl;
        if (this.disabled) return;
        // 创建拖拽标记
        if (!indicatorEl?.dom) {
          indicatorEl = createElement(document.createElement('div'));
          indicatorEl.addClass(this.baseClass);
          this.indicatorRootEl.appendChild(indicatorEl.dom);
        }
      },
      /**
       * 拖动
       * @param {*} param0
       */
      dragOver({ target, dragEl }) {
        // 拖放自身，不处理
        if (indicatorEl && target === dragEl) {
          this.toggleTargetClass();
          lastSwapEl = target;
        }
      },
      /**
       * 拖动校验
       * @param {*} param0
       * @returns
       */
      dragOverValid(sortableEvent) {
        const {
          completed,
          target,
          activeSortable,
          changed,
          cancel,
          originalEvent,
          sortable,
          dragEl,
        } = sortableEvent;
        if (this.disabled || !activeSortable.options.indicator) return;
        let el = sortable.el;
        this.actions = null;

        if (target && target !== el) {
          // 目标元素位置
          const targetOffset = getViewportOffset(target);
          // 横向拖拽信息
          let dragPoint = originalEvent.clientX; // 鼠标点
          let targetPoint = targetOffset.left; // 目标点
          let rangSize = target.clientWidth; // 拖放区域
          // 纵向拖拽信息
          if (this.vertical) {
            dragPoint = originalEvent.clientY; // 鼠标点
            targetPoint = targetOffset.top; // 目标点
            rangSize = target.clientHeight; // 拖放区域
          }

          // 区域
          let sortRangSize = rangSize * 0.5; // 排序区域
          let insertRangSize = 0; // 插入区域

          // 允许插入操作，可以写函数判断
          if (
            this.enableInsert ||
            (isFunction(this.enableInsert) && this.enableInsert({ target, dragEl }))
          ) {
            sortRangSize = rangSize * 0.3; // 排序区域
            insertRangSize = rangSize * 0.4; // 插入区域
          }
          // 计算拖动范围
          let minSize = targetPoint; // 最小范围
          let maxSize = targetPoint + rangSize; // 最大范围

          // 记录之前替换元素
          let prevSwapEl = lastSwapEl;
          const insertAction =
            dragPoint > minSize + sortRangSize && dragPoint < maxSize - sortRangSize;
          // 移动事件处理
          if (
            activeSortable.options.onMove?.(
              Object.assign(sortableEvent, { action: insertAction ? 'insert' : 'sort' }),
            ) !== false
          ) {
            // 操作
            this.actions = {
              prev: dragPoint > minSize && dragPoint < minSize + sortRangSize, // 排序前
              next: dragPoint > minSize + sortRangSize + insertRangSize && dragPoint < maxSize, // 排序后
              insert: insertAction, // 插入
            };
            // 添加标记
            this.toggleTargetClass(target, this.actions);
            // 更新替换元素
            lastSwapEl = target;
          } else {
            lastSwapEl = null;
          }

          // 清除之前替换元素标记
          if (prevSwapEl && prevSwapEl !== lastSwapEl) {
            toggleClass(prevSwapEl, this.insertTargetClass, false);
          }
        }
        // 停止拖动
        changed();
        completed(true);
        cancel();
      },
      /**
       * 放下
       * @param {*} param0
       * @returns
       */
      drop({ dragEl, putSortable, activeSortable, rootEl }) {
        // 清空悬浮样式
        rootEl?.childNodes?.forEach((node) => {
          toggleClass(node, this.hoverClass, false);
          toggleClass(node, this.insertTargetClass, false);
        });
        const indicator = (putSortable ?? activeSortable)?.indicator;
        if (this.disabled || !indicator || !lastSwapEl) return;
        // 清楚标记
        indicator.toggleTargetClass(lastSwapEl);
        indicatorEl.destroy();
        indicatorEl = null;
        // 操作数据
        const config = {
          dragEl,
          swapEl: lastSwapEl,
          rawSwapEl: lastSwapEl,
          actions: indicator.actions ?? {},
        };
        // 前后元素不处理
        if (
          (config.actions.prev && config.swapEl.previousSibling === config.dragEl) ||
          (config.actions.next && config.swapEl.nextSibling === config.dragEl)
        ) {
          config.swapEl = config.dragEl;
        }
        // 同步数据
        [putSortable, activeSortable].forEach((s) => {
          Object.assign(s?.indicator ?? {}, config);
        });
      },
      nulling() {
        lastSwapEl = null;
        indicatorEl?.destroy();
        indicatorEl = null;
      },
    };

    return Object.assign(Indicator, {
      pluginName: 'indicator',
      eventProperties() {
        return {};
      },
    });
  }
  Sortable.mount(new IndicatorPlugin());
}
