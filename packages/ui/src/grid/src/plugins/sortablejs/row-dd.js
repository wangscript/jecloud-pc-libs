import Sortable from 'sortablejs';
import { nextTick, onUnmounted, toRaw, getCurrentInstance, onMounted } from 'vue';
import { findTree, getEventTargetNode, isObject, createElement, isNotEmpty } from '@jecloud/utils';
import { dragClass } from './index';

export function useRowDD($grid) {
  let sortables = [];
  let initTime;
  const instance = getCurrentInstance();
  // 放下前事件
  const onBeforeDrop = instance?.vnode?.props.onBeforeDrop;
  onMounted(() => {
    nextTick(() => {
      // 加载完成之后在绑定拖动事件
      initTime = setTimeout(() => {
        sortables = rowDDs(Object.assign($grid, { onBeforeDrop }));
      }, 500);
    });
  });

  onUnmounted(() => {
    clearTimeout(initTime);
    sortables?.forEach((sortable) => {
      sortable?.destroy();
    });
  });
}
/**
 * 锁定列
 * @param {*} options
 */
const rowDDs = function (options) {
  const $table = options.$plugin.value;
  const sortables = [];
  ['refTableBody', 'refTableLeftBody', 'refTableRightBody'].forEach((key) => {
    const tbody = $table.getRefMaps()[key];
    if (tbody?.value) {
      sortables.push(rowDD({ ...options, tbody: tbody.value }));
    }
  });
};

/**
 * 行拖动
 *
 * @param {*} $grid
 * @return {*}
 */
const rowDD = ({ $plugin, context, props, tbody, delay = 100, onBeforeDrop }) => {
  const { emit } = context;
  const $table = $plugin.value;
  // 拖动元素
  const dragDom = tbody.$el.querySelector('.vxe-table--body tbody');
  if (!dragDom) return;
  const { refElem, refTableBody } = $table.getRefMaps();
  // 拖拽标记Root容器
  const indicatorRootEl = refElem.value.querySelector('.vxe-table--render-wrapper');
  // 拖拽标记滚动条计算容器
  const indicatorScrollEl = refTableBody.value.$el;
  // 树形
  const tree = !!$table.treeConfig;
  // 树形节点开始拖动标记
  let treeStartDrag = false;
  if (tree) {
    const dragDomEl = createElement(dragDom);
    dragDomEl.on('mousedown', () => {
      treeStartDrag = true;
    });
    dragDomEl.on('mouseup', () => {
      treeStartDrag = false;
    });
  }
  const sortable = Sortable.create(
    dragDom,
    Object.assign(
      {
        $table,
        handle: '.' + dragClass,
        dropRemoveClone: true, // 拖放后删除克隆对象，新增配置
        forceFallback: true, // 不使用H5原生拖动
        dragClass: 'je-sortable-drag', // 拖动元素样式
        delay: tree ? 0 : delay, // 延迟拖动
        vertical: true, // 纵向拖动
        indicator: true, // 拖拽标记
        indicatorRootEl, //拖拽标记Root容器
        indicatorScrollEl, // 拖拽标记滚动条计算容器
        enableInsert: tree, // 启用插入操作
        preventOnFilter: false, // 禁用原生事件，防止点击冲突
        filter(evt, item) {
          // 编辑状态不可拖拽
          const targetInfo = getEventTargetNode(evt, item, 'col--actived');
          return targetInfo.flag;
        },
        onMove: function (sortableEvent) {
          // 拖动校验事件
          const { dragEl, target, action } = sortableEvent;
          const row = $table.getRowNode(dragEl);
          const targetRow = $table.getRowNode(target);
          return (
            onBeforeDrop?.({
              row: toRaw(row?.item),
              target: toRaw(targetRow?.item),
              $table,
              action,
              sortableEvent,
            }) !== false
          );
        },
        // 解决树形拖动前需要将展开节点收起，而且收起方法是Promise，导致nextEl失效报错
        onStartPromise({ item }) {
          if (tree) {
            const node = $table.getRowNode(item);
            return new Promise((resolve, reject) => {
              // 清空延时器
              this.startTimer && clearTimeout(this.startTimer);
              this.startTimer = setTimeout(() => {
                if (treeStartDrag) {
                  $table.setTreeExpand(node.item, false).then(resolve);
                } else {
                  reject();
                }
              }, 200);
            });
          }
          return Promise.resolve();
        },
        onStart: (sortableEvent) => {
          // 重置拖动元素样式
          const { item, from } = sortableEvent;
          const dargItem = from.querySelector('.' + sortable.options.dragClass);
          for (let i = 0; i < item.cells.length; i++) {
            dargItem.cells[i].style.width = item.cells[i].clientWidth + 'px';
          }
        },
        onEnd: function ({ item, clone, from, to }) {
          const indicator = this.indicator;
          if (indicator.disabled) {
            const dragRow = $table.getRowNode(item);
            if (from === to) return;
            // 恢复原始节点
            if (clone && this.options.dropRemoveClone) {
              from.replaceChild(item, clone);
            }
            emit('drop', { row: toRaw(dragRow?.item), $table, action: 'none' });
            return;
          } else if (!indicator.actions) {
            // 无操作
            return;
          }
          if (tree) {
            sortTree({ $plugin, emit, indicator, props });
          } else {
            sortTable({ $plugin, emit, indicator, props });
          }
        },
      },
      Object.assign({}, isObject(props.draggable) ? props.draggable : {}), // 自定义拖动配置
    ),
  );

  // 延迟推动，未成功，清除拖动元素
  if (delay) {
    const old_disableDelayedDrag = sortable._disableDelayedDrag;
    sortable._disableDelayedDrag = function () {
      old_disableDelayedDrag();
      sortable._nulling();
    };
  }
  $table.$el.removeAttribute('draggable');
  return sortable;
};
/**
 * 列表排序
 * @param {*} param0
 * @returns
 */
const sortTable = function ({ indicator, $plugin, emit, props }) {
  const $table = $plugin.value;
  const { store } = props;
  const { dragEl, swapEl, actions } = indicator;
  if (dragEl === swapEl) {
    console.log('没有改变');
    return;
  }
  // 拖动行
  const dragRow = $table.getRowNode(dragEl);
  // 替换行
  const swapRow = $table.getRowNode(swapEl);
  // 行索引
  let oldIndex = dragRow.index;
  let newIndex = swapRow.index;
  // 插入元素后面，增加索引
  if (actions.next) {
    newIndex++;
  }
  const data = $table.data.slice(0);
  // 移动数据
  moveRow({ targetData: data, newIndex, oldIndex });

  const row = toRaw(dragRow.item);
  if (store) {
    store.data = data;
    nextTick(() => {
      emit('drop', { row, $table, action: 'sort' });
    });
  } else {
    $table.loadData(data).then(() => {
      emit('drop', { row, $table, action: 'sort' });
    });
  }
};
/**
 * 树形排序
 * @param {*} param0
 * @returns
 */
const sortTree = function ({ indicator, $plugin, emit, props }) {
  const $table = $plugin.value;
  const { store } = props;
  let { dragEl, swapEl, rawSwapEl, actions } = indicator;
  if (dragEl === swapEl) {
    const swapItem = $table.getRowNode(swapEl);
    const rawSwapItem = $table.getRowNode(rawSwapEl);
    // 不同层级拖动
    if (swapItem.parent != rawSwapItem.parent) {
      swapEl = rawSwapEl;
    } else {
      console.log('没有改变');
      return;
    }
  }

  const { rowField, children, parentField } = $table.treeConfig;
  const options = { children };
  const tableTreeData = $table.getTableData().fullData.slice(0); // 树形数据
  const findTreeNode = (node) => {
    return findTree(tableTreeData, (row) => row[rowField] === node[rowField], options);
  };
  // 拖动行
  const dragRow = $table.getRowNode(dragEl);
  const dragNode = findTreeNode(dragRow.item);
  const oldIndex = dragNode.index;
  // 替换行
  const swapRow = $table.getRowNode(swapEl);
  let swapNode = findTreeNode(swapRow.item);
  let newIndex = swapNode.index;

  // 插入元素后面，增加索引
  if (actions.next) {
    newIndex++;
  }
  // 插入操作，支持插入到子节点
  if (actions.insert) {
    const insertRow = swapNode.item;
    const currRow = dragNode.items.splice(oldIndex, 1)[0]; // 删除原数据
    currRow[parentField] = insertRow[rowField]; // 更新父节点
    insertRow[options.children] = insertRow[options.children] ?? [];
    insertRow[options.children].push(currRow); // 插入新数据，位置最后
  } else {
    // 正常排序
    dragNode.item[parentField] = swapNode.item[parentField]; // 更新父节点
    moveRow({ sourceData: dragNode.items, targetData: swapNode.items, newIndex, oldIndex });
  }
  // 最终数据
  const newNode = findTreeNode(dragNode.item);

  // 操作数据，用于业务逻辑处理
  const nodeInfo = {
    id: newNode.item[rowField],
    toId: swapRow.item[rowField],
    place: actions.insert ? 'inside' : actions.next ? 'below' : 'above',
    parentId: newNode.item[parentField],
    prevId: newNode.items[newNode.index - 1]?.[rowField],
    nextId: newNode.items[newNode.index + 1]?.[rowField],
  };
  // 修改拖动节点的path
  updateNodePath({ newNode, swapRow, nodeInfo });
  // 如果变动了树层级，需要刷新数据
  const row = toRaw(newNode.item);
  if (store) {
    store.data = tableTreeData;
    nextTick(() => {
      emit('drop', { row, $table, action: actions.insert ? 'insert' : 'sort', info: nodeInfo });
    });
  } else {
    $table.loadData(tableTreeData).then(() => {
      emit('drop', { row, $table, action: actions.insert ? 'insert' : 'sort', info: nodeInfo });
    });
  }
};
function updateNodePath({ newNode, swapRow, nodeInfo }) {
  // 获得父节点的信息
  let parentPath = '';
  let parent = '';
  // 如果是排序
  if (isNotEmpty(nodeInfo.prevId)) {
    parentPath = newNode.items[newNode.index - 1]?.parentPath;
    parent = newNode.items[newNode.index - 1]?.parent;
  }
  if (isNotEmpty(nodeInfo.nextId)) {
    parentPath = newNode.items[newNode.index + 1]?.parentPath;
    parent = newNode.items[newNode.index + 1]?.parent;
  }
  // 如果是拖入节点中
  if (isNotEmpty(nodeInfo.toId) && nodeInfo.place == 'inside') {
    parentPath = swapRow.item.nodePath;
    parent = nodeInfo.toId;
  }
  // 修改节点的path属性值
  if (newNode.item.bean && parentPath) {
    newNode.item.parent = parent;
    newNode.item.parentPath = parentPath;
    newNode.item.nodePath = parentPath + '/' + nodeInfo.id;
  }
  // 修改所有子节点的path属性值
  if (newNode.item.children.length > 0) {
    const updateChildrenPath = (nodeChildren) => {
      nodeChildren.children.forEach((node) => {
        node.parent = nodeChildren.id;
        node.parentPath = nodeChildren.nodePath;
        node.nodePath = nodeChildren.nodePath + '/' + node.id;
        if (node.children.length > 0) {
          updateChildrenPath(node);
        }
      });
    };
    updateChildrenPath(newNode.item);
  }
}

function moveRow({ targetData, sourceData = targetData, newIndex, oldIndex }) {
  // 占位符
  const placeholder = '__temp__';
  // 从源数据移除移动数据，插入占位符，保持数组元素索引不变
  const currRow = sourceData.splice(oldIndex, 1, placeholder)[0];
  // 在目标数据的新坐标插入移动数据
  targetData.splice(newIndex, 0, currRow);
  // 删除源数据占位符，还原数组
  sourceData.splice(sourceData.indexOf(placeholder), 1);
}
