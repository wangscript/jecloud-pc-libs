import { defineComponent, ref } from 'vue';
import { columnProps } from 'vxe-table/es/table/src/column';
import { Column } from 'vxe-table';
import { useColumn } from './hooks/use-column';

export default defineComponent({
  name: 'JeColumn',
  components: { Column },
  inheritAttrs: false,
  props: {
    ...columnProps,
    draggable: { type: Boolean, default: true },
    switchConfig: Object,
    actionConfig: Object,
    checkedIcon: String,
    unCheckedIcon: String,
  },
  setup(props, context) {
    const { expose } = context;
    const $plugin = ref({});
    expose({
      $plugin,
    });
    const { props: colProps, slots: colSlots } = useColumn({ context, props });
    return () => <Column ref={$plugin} {...colProps} v-slots={colSlots} />;
  },
});
