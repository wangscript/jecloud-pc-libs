import { defineComponent, ref } from 'vue';
import { Colgroup } from 'vxe-table';

export default defineComponent({
  name: 'JeColgroup',
  inheritAttrs: false,
  props: {
    draggable: { type: Boolean, default: true },
  },
  setup(props, { slots, expose, attrs }) {
    const $plugin = ref({});
    expose({
      $plugin,
    });
    return () => (
      <Colgroup
        ref={$plugin}
        headerClassName={props.draggable ? 'is--draggable' : ''}
        {...attrs}
        v-slots={slots}
      />
    );
  },
});
