/**
 * 开关列
 * @returns
 */
export function useSwitch({ props }) {
  const switchConfig = Object.assign(
    {
      layout: 'checkbox', // checkbox，buttons，switch
      options: [
        { value: '0', icon: props.unCheckedIcon || 'fal fa-square' },
        { value: '1', icon: props.checkedIcon || 'fas fa-check-square', check: true },
      ],
    },
    props.switchConfig ?? {},
  );
  const { options, layout } = switchConfig;
  const itemClick = function (item, config) {
    const { row, column, $table } = config;
    const { computeEditOpts } = $table.getComputeMaps();
    const { activeMethod, enabled } = computeEditOpts.value;
    // 点击事件，非false，可用
    if (activeMethod?.(config) == false || enabled == false) {
      return;
    }
    switch (layout) {
      // 按钮组
      case 'buttons':
        row[column.field] = item.value;
        break;
      // 单选框
      default:
        // 取反
        const valueOption = options.find((option) => option.value !== item.value);
        row[column.field] = valueOption.value;
        break;
    }
  };
  const config = {
    slots: {
      default: (config) => {
        const { row, column } = config;
        const value = row[column.field];
        const defValue = options[0];
        const valueOption = options.find((option) => option.value == value) ?? defValue;

        return (
          <div class={['switch-cell', 'switch-layout-' + layout]}>
            {(layout === 'buttons' ? options : [valueOption]).map((item) => {
              return (
                <label
                  class={{
                    'switch-cell-item': true,
                    'switch-cell-item-checked':
                      layout === 'buttons' ? valueOption.value === item.value : item.check,
                  }}
                  style={item.style}
                  onClick={() => {
                    itemClick(item, config);
                  }}
                >
                  {item.icon ? (
                    <i class={['switch-cell-item-icon', item.icon]} style={item.iconStyle} />
                  ) : null}
                  {item.label ? <span class={['switch-cell-item-label']}>{item.label}</span> : null}
                </label>
              );
            })}
          </div>
        );
      },
    },
    props: { align: 'center', cellRender: {} },
  };

  return config;
}
