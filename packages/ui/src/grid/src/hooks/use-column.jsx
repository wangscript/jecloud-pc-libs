import { reactive, toRaw } from 'vue';
import { defaults, omit, pick } from '@jecloud/utils';
import { useCheckbox } from './use-column-checkbox';
import { useSwitch } from './use-column-switch';
import { useTreeNode } from './use-column-treenode';
import { useAction } from './use-column-action';

export function useColumn(options) {
  const { props, context } = options;
  const { slots } = context;
  const type = props.type;

  let config = useDefault(options);
  // 单选多选
  if (['checkbox', 'radio'].includes(type)) {
    config = useCheckbox(options);
    // 开关列
  } else if (type === 'switch') {
    config = useSwitch(options);
    // 编号
  } else if (type === 'seq') {
    config = useSeq(options);
    // 操作列
  } else if (type === 'action') {
    config = useAction(options);
    // 树形节点
  } else if (props.treeNode) {
    config = useTreeNode(options);
  }
  // 最终属性
  const _props = reactive(
    Object.assign(
      defaults(toRaw(props), config.props),
      pick(config.props, ['resizable', 'draggable']), // 强制覆盖属性
    ),
  );
  // 列头拖动样式
  if (_props.draggable) {
    _props.headerClassName = (_props.headerClassName ?? '') + ' is--draggable';
  }
  const _slots = defaults(config.slots, slots);
  return { props: _props, slots: _slots };
}

/**
 * 默认值
 * @returns
 */
const useDefault = function ({ props }) {
  const config = { slots: {}, props: { cellRender: {} } };
  // 排除冲突属性
  if (props.editRender || props.cellRender) {
    config.props = omit(config.props, ['cellRender']);
  }
  return config;
};

/**
 * 序号类
 * @returns
 */
const useSeq = function () {
  return { props: { draggable: false }, slots: {} };
};
