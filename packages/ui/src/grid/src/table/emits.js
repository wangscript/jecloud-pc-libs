import emits from 'vxe-table/es/table/src/emits';
import { useExtendEvents } from '../../../hooks';
export const tableEmits = [
  ...emits,
  'drop',
  'before-drop',
  'column-drop',
  'selection-change',
  'before-load',
  'load',
];

export function useTableEvents($table) {
  const { emit } = $table.context;
  const events = useExtendEvents({ emit, keys: emits });

  return { ...events };
}
