import { watch, nextTick, ref, getCurrentInstance, computed, onBeforeMount } from 'vue';
import { pick, uniqueId } from '@jecloud/utils';
import { usePlugins } from './plugins';
import { useMethods } from './methods';
import { useTableProps } from './props';
import { useTableEvents } from './emits';
import { useListeners } from '../../../hooks';

export function useTable({ props, context, tree }) {
  const $panel = ref();
  const $plugin = ref();
  const $table = {
    jID: uniqueId(),
    props,
    context,
    instance: getCurrentInstance(),
    $plugin,
    $panel,
    store: props.store,
    selection: ref([]),
  };
  const { fireListener } = useListeners({ props, context });

  const { slots, expose } = context;
  // 加载依赖插件
  usePlugins();

  // 继承方法
  useMethods($table);

  // 暴露方法
  expose($table);

  // 监听尺寸，刷新布局
  watch(
    () => props.size,
    () => {
      nextTick(() => {
        $table.refreshLayout();
      });
    },
  );
  // 绑定数据集
  onBeforeMount(() => {
    nextTick(() => {
      if (props.store?.isStore) {
        // 数据提交后，刷新列表视图
        props.store.on('commit-record', function () {
          const { tableData } = $plugin.value.reactData;
          $plugin.value.reactData.tableData = tableData.slice(0);
        });

        // 绑定使用者的事件
        props.store.on('before-load', function (...args) {
          return fireListener('before-load', ...args);
        });
        props.store.on('load', function (...args) {
          fireListener('load', ...args);
        });
        // 自动加载
        props.store.autoLoad && props.store.load();
      }
    });
  });

  const loading = computed(() => {
    return props.store?.loading ?? props.loading;
  });

  // table事件
  const tableCompEvents = useTableEvents($table);
  // table属性，提取table自有属性，增加默认属性
  const tableCompProps = useTableProps($table, tree);
  // table插槽
  const tableCompSlots = pick(slots, ['default', 'empty']);

  return { $table, $panel, $plugin, tableCompEvents, tableCompProps, tableCompSlots, loading };
}
