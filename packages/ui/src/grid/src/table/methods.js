import { onMounted } from 'vue';
import { isEmpty, forEach, isArray, isNotEmpty } from '@jecloud/utils';
import { useExtendMethods } from '../../../hooks';
import { tableMethodKeys } from './method-keys';
import { useOverrides } from './overrides';
import { initColumnDD, destroyColumnDD } from '../plugins/sortablejs/column-dd';
export function useMethods($grid) {
  const { $panel, $plugin } = $grid;
  const methods = {};
  // 继承table方法
  Object.assign(methods, useExtendMethods({ plugin: $plugin, keys: tableMethodKeys }));
  // 继承panel方法
  Object.assign(methods, useExtendMethods({ plugin: $panel, keys: ['refreshLayout'] }));
  // 扩展方法
  Object.assign(methods, customMethods($grid));

  Object.assign($grid, methods);

  // 重写table方法
  const overrides = useOverrides($grid);
  onMounted(() => {
    Object.keys(overrides).forEach((key) => {
      $plugin.value[`_${key}`] = $plugin.value[key];
      $plugin.value[key] = overrides[key];
    });
    // 保持函数同步
    Object.keys(methods).forEach((key) => {
      if (!$plugin.value[key]) {
        $plugin.value[key] = methods[key];
      }
    });
  });
}

/**
 * 扩展方法
 * @param {*} $grid
 * @returns
 */
function customMethods($grid) {
  const { $plugin, context, selection } = $grid;
  const methods = {
    initColumnDD() {
      initColumnDD($grid);
    },
    destroyColumnDD() {
      destroyColumnDD($grid);
    },
    refreshColumnDD() {
      methods.destroyColumnDD();
      methods.initColumnDD();
    },
    doAction(name, ...args) {
      return $plugin?.value?.[name]?.(...args);
    },
    /**
     *是否多选
     *
     * @return {*}
     */
    hasCheckboxColumn() {
      const $table = $plugin.value;
      let flag = false;
      forEach($table.getColumns(), (column) => {
        if (column.type === 'checkbox') {
          flag = true;
          return false;
        }
      });
      return flag;
    },
    /**
     * 深度展开节点下的所有节点
     * 设置展开树形节点，二个参数设置这一行展开与否
     * 支持单行
     * 支持多行
     * @param {Array/Row} rows 行数据
     * @param {Boolean} expanded 是否展开
     */
    setTreeExpandDeep(rows = [], expanded) {
      const $table = $plugin.value;
      if (!isArray(rows)) {
        rows = [rows];
      }
      const nodes = rows.map((node) => $table.getRowById(node.id));
      return $table.setTreeExpand(nodes, expanded).then(() => {
        const expands = [];
        rows.forEach((row) => {
          expands.push(...(row.children ?? []));
        });
        return expands.length && $table.setTreeExpandDeep(expands, expanded);
      });
    },
    /**
     * 展开节点路径
     *
     * @param {*} row
     * @param {*} expand
     */
    setTreeExpand4Path({ row, expand = true, path, async }) {
      const $table = $plugin.value;
      // 异步展开，只支持路径
      if (async) {
        const paths = path.split('/');
        let id = paths.shift();
        let root = $table.getRowById(id);
        while (!root && paths.length) {
          id = paths.shift();
          root = $table.getRowById(id);
        }
        if (paths.length) {
          if ($table.isTreeExpandByRow(root)) {
            return $table.setTreeExpand4Path({ path: paths.join('/'), async, expand });
          } else {
            // 展开
            return $table.setTreeExpand(root, expand).then(() => {
              return $table.setTreeExpand4Path({ path: paths.join('/'), async, expand });
            });
          }
        } else {
          return Promise.resolve([root]);
        }

        // 同步展开
      } else {
        let expands = [];
        // 根据路径展开
        if (path) {
          const paths = path.split('/');
          while (paths.length) {
            const node = $table.getRowById(paths.shift());
            if (node) {
              expands.push(node);
            }
          }
          // 根据节点展开
        } else {
          const parentField = $table.treeConfig?.parentField ?? 'parent';
          while (row) {
            expands.unshift(row);
            row = $table.getRowById(row[parentField]);
          }
        }
        // 展开
        return $table.setTreeExpand(expands, expand).then(() => {
          return [expands.pop()];
        });
      }
    },
    /**
     * 设置选中数据
     * @param {Object} row 要选中的数据
     * @param {boolean} selected 是否选中
     */
    setSelectedRecords(row, selected = true) {
      const $table = $plugin.value;
      const records = isArray(row) ? row : [row];
      let selectPromise;
      // 选中
      if (methods.hasCheckboxColumn()) {
        selectPromise = $table.setCheckboxRow(row, selected);
      } else {
        selectPromise = selected ? $table.setCurrentRow(records[0]) : $table.clearCurrentRow();
      }
      return selectPromise.then((options) => {
        // 触发事件
        context.emit('selection-change', { $table, row, selected, records });
        return options;
      });
    },
    setSelectRow(...args) {
      return methods.setSelectedRecords(...args);
    },
    /**
     * 触发数据选择改变事件
     */
    triggerSelectionChange(options) {
      const $table = $plugin.value;
      const { treeConfig, checkboxConfig } = $table.props;
      const { records, checked, row, eventType } = options;
      const { multiple } = $grid.props;
      const emitSelectChange = () => {
        // 选中数据
        if (eventType !== 'currentChange' || !multiple) {
          selection.value = options.records;
        }
        // 触发事件
        context.emit('selection-change', options);
      };

      // 严格模式,级联选择子节点
      const cascadeCheckChilds = function () {
        if (isEmpty(checked)) return;
        if (checkboxConfig.checkStrictly && checkboxConfig.cascadeChild) {
          let checkChilds = row.children;
          while (isNotEmpty(checkChilds)) {
            $table.setCheckboxRow(checkChilds, checked);
            let childs = [];
            checkChilds.forEach((node) => {
              childs = childs.concat(node.children);
            });
            checkChilds = childs;
          }
        }
        Object.assign(options, {
          records: $table.getCheckboxRecords(),
          reserves: $table.getCheckboxReserveRecords(),
          indeterminates: $table.getCheckboxIndeterminateRecords(),
        });
      };
      // 严格模式，级联选择父节点
      const cascadeCheckParent = function () {
        if (isEmpty(checked)) return;
        if (checkboxConfig.checkStrictly && checkboxConfig.cascadeParent && checked) {
          let parent = $table.getRowById(row.parent);
          while (parent) {
            $table.setCheckboxRow(parent, checked);
            parent = $table.getRowById(parent.parent);
          }
        }
      };

      // 树形多选
      if (treeConfig && checkboxConfig) {
        cascadeCheckParent();
        if (treeConfig.lazy) {
          const expandPs = records.map((record) => $table.setTreeExpandDeep(record, true));
          Promise.all(expandPs).then(() => {
            cascadeCheckChilds();
            emitSelectChange(options);
          });
        } else if (options._checkBox_) {
          cascadeCheckChilds();
          emitSelectChange(options);
        }
        return;
      }
      emitSelectChange(options);
    },
    /**
     * 获取选中数据
     *
     * @return {Array}
     */
    getSelectedRecords() {
      const $table = $plugin.value;
      // 选中
      if (methods.hasCheckboxColumn()) {
        return $table.getCheckboxRecords();
      } else {
        const row = $table.getCurrentRecord();
        return row ? [row] : [];
      }
    },
    /**
     * 清空选中数据
     */
    clearSelectedRecords() {
      const $table = $plugin.value;
      const hasSelectRows = $table?.getSelectedRecords().length;
      if (hasSelectRows) {
        let promise;
        // 选中
        if (methods.hasCheckboxColumn()) {
          promise = $table.clearCheckboxRow();
        } else {
          promise = $table.clearCurrentRow();
        }
        selection.value = [];
        return promise.then(() => {
          methods.triggerSelectionChange({ records: [], $table });
        });
      }
      return Promise.resolve();
    },
    /**
     * 查找数据
     * @param {Function} filter 过滤函数
     */
    findRecords(filter) {
      const $table = $plugin.value;
      const { fullDataRowIdData } = $table.internalData;
      const rows = [];
      for (const id in fullDataRowIdData) {
        const row = fullDataRowIdData[id].row;
        if (filter?.(row)) {
          rows.push(row);
        }
      }
      return rows;
    },
    /**
     * 判断行是否为激活编辑状态
     * @param {Row} row 行对象
     */
    isEditByRow(row) {
      const $table = $plugin.value;
      const { editStore } = $table.reactData;
      return editStore.actived.row === row;
    },
    /* 
    commitInserts(rows) {
      const $table = $plugin.value;
      rows = isArray(rows) ? rows : [rows];
      const { fullDataRowIdData, fullAllDataRowIdData } = $table.internalData;
      const { editStore } = $table.reactData;
      let inserts = $table.getInsertRecords();
      let insertList = [];
      // 提交新加数据
      if (isNotEmpty(rows)) {
        const ids = rows.map((item) => $table.getRowid(item));
        inserts = rows;
        insertList = inserts.filter((item) => !ids.includes($table.getRowid(item))) ?? [];
      }
      // 更新添加数据的缓存
      const ids = Object.keys(fullDataRowIdData).concat(
        inserts.map((item) => $table.getRowid(item)),
      );
      ids.forEach((rowId) => {
        fullDataRowIdData[rowId] = fullAllDataRowIdData[rowId];
      });
      // 更新添加数据的标识
      editStore.insertList = insertList;
    },
    commit(rows, field) {
      const $table = $plugin.value;
      let inserts = $table.getInsertRecords();
      let updates = $table.getUpdateRecords();
      if (isNotEmpty(rows)) {
        rows = isArray(rows) ? rows : [rows];
        const ids = rows.map((item) => $table.getRowid(item));
        inserts = $table.getInsertRecords().filter((item) => ids.includes($table.getRowid(item)));
        updates = $table.getUpdateRecords().filter((item) => ids.includes($table.getRowid(item)));
      }
      // 提交新加数据
      if (isNotEmpty(inserts)) {
        $table.commitInserts(inserts);
      }
      // 提交修改数据
      if (isNotEmpty(updates)) {
        $table.reloadRow(updates, null, field);
      }
    }, */
  };

  return methods;
}
