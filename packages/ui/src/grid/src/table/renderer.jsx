import { h, resolveComponent } from 'vue';
import {
  each,
  assign,
  set,
  get,
  map,
  isBoolean,
  isArray,
  toString,
  isNotEmpty,
  findTree,
  includes,
} from '@jecloud/utils';
// 组件前缀
const prefix = 'a-';
const parsePrefix = function (name) {
  if (name.startsWith('A')) {
    return 'Je' + name.substring(1);
  }
  return name;
};

function isEmptyValue(cellValue) {
  return cellValue === null || cellValue === undefined || cellValue === '';
}

function getOnName(type) {
  return 'on' + type.substring(0, 1).toLocaleUpperCase() + type.substring(1);
}

function getModelProp(renderOpts) {
  let prop = 'value';
  switch (renderOpts.name) {
    case 'ASwitch':
      prop = 'checked';
      break;
  }
  return prop;
}

function getModelEvent(renderOpts) {
  let type = 'update:value';
  switch (renderOpts.name) {
    case 'ASwitch':
      type = 'update:checked';
      break;
  }
  return type;
}

function getChangeEvent(renderOpts) {
  renderOpts;
  return 'change';
}

function getCellEditFilterProps(renderOpts, params, value, defaultProps) {
  return assign({}, defaultProps, renderOpts.props, { [getModelProp(renderOpts)]: value });
}

function getItemProps(renderOpts, params, value, defaultProps) {
  return assign({}, defaultProps, renderOpts.props, { [getModelProp(renderOpts)]: value });
}

function formatText(cellValue) {
  return '' + (isEmptyValue(cellValue) ? '' : cellValue);
}

function getCellLabelVNs(renderOpts, params, cellLabel) {
  const { placeholder } = renderOpts;
  return [
    h(
      'span',
      {
        class: 'vxe-cell--label',
      },
      placeholder && isEmptyValue(cellLabel)
        ? [
            h(
              'span',
              {
                class: 'vxe-cell--placeholder',
              },
              formatText(placeholder),
            ),
          ]
        : formatText(cellLabel),
    ),
  ];
}

function getOns(renderOpts, params, inputFunc, changeFunc) {
  const { events } = renderOpts;
  const modelEvent = getModelEvent(renderOpts);
  const changeEvent = getChangeEvent(renderOpts);
  const isSameEvent = changeEvent === modelEvent;
  const ons = {};
  each(events, (func, key) => {
    ons[getOnName(key)] = function (...args) {
      func(params, ...args);
    };
  });
  if (inputFunc) {
    ons[getOnName(modelEvent)] = function (targetEvnt) {
      inputFunc(targetEvnt);
      if (events && events[modelEvent]) {
        events[modelEvent](params, targetEvnt);
      }
      if (isSameEvent && changeFunc) {
        changeFunc(targetEvnt);
      }
    };
  }
  if (!isSameEvent && changeFunc) {
    ons[getOnName(changeEvent)] = function (...args) {
      changeFunc(...args);
      if (events && events[changeEvent]) {
        events[changeEvent](params, ...args);
      }
    };
  }
  return ons;
}

function getEditOns(renderOpts, params) {
  const { $table, row, column } = params;
  return getOns(
    renderOpts,
    params,
    (value) => {
      // 处理 model 值双向绑定
      set(row, column.property, value);
    },
    () => {
      // 处理 change 事件相关逻辑
      $table.updateStatus(params);
    },
  );
}

function getFilterOns(renderOpts, params, option, changeFunc) {
  return getOns(
    renderOpts,
    params,
    (value) => {
      // 处理 model 值双向绑定
      option.data = value;
    },
    changeFunc,
  );
}

function getItemOns(renderOpts, params) {
  const { $form, data, property } = params;
  return getOns(
    renderOpts,
    params,
    (value) => {
      // 处理 model 值双向绑定
      set(data, property, value);
    },
    () => {
      // 处理 change 事件相关逻辑
      $form.updateStatus(params);
    },
  );
}

function matchCascaderData(index, list, values, labels) {
  const val = values[index];
  if (list && values.length > index) {
    each(list, (item) => {
      if (item.value === val) {
        labels.push(item.label);
        matchCascaderData(++index, item.children, values, labels);
      }
    });
  }
}

function formatDatePicker(defaultFormat) {
  return function (renderOpts, params) {
    return getCellLabelVNs(
      renderOpts,
      params,
      getDatePickerCellValue(renderOpts, params, defaultFormat),
    );
  };
}

function getSelectCellValue(renderOpts, params) {
  const {
    options = [],
    optionGroups,
    props = {},
    optionProps = {},
    optionGroupProps = {},
  } = renderOpts;
  const { row, column } = params;
  const labelProp = optionProps.label || 'label';
  const valueProp = optionProps.value || 'value';
  const groupOptions = optionGroupProps.options || 'options';
  const cellValue = get(row, column.property);
  if (!isEmptyValue(cellValue)) {
    return map(
      props.mode === 'multiple' ? cellValue : [cellValue],
      optionGroups
        ? (value) => {
            let selectItem;
            for (let index = 0; index < optionGroups.length; index++) {
              selectItem = find(
                optionGroups[index][groupOptions],
                (item) => item[valueProp] === value,
              );
              if (selectItem) {
                break;
              }
            }
            return selectItem ? selectItem[labelProp] : value;
          }
        : (value) => {
            const selectItem = find(options, (item) => item[valueProp] === value);
            return selectItem ? selectItem[labelProp] : value;
          },
    ).join(', ');
  }
  return '';
}

function getCascaderCellValue(renderOpts, params) {
  const { props = {} } = renderOpts;
  const { row, column } = params;
  const cellValue = get(row, column.property);
  const values = cellValue || [];
  const labels = [];
  matchCascaderData(0, props.options, values, labels);
  return (
    props.showAllLevels === false ? labels.slice(labels.length - 1, labels.length) : labels
  ).join(` ${props.separator || '/'} `);
}

function getRangePickerCellValue(renderOpts, params) {
  const { props = {} } = renderOpts;
  const { row, column } = params;
  let cellValue = get(row, column.property);
  if (cellValue) {
    cellValue = map(cellValue, (date) => date.format(props.format || 'YYYY-MM-DD')).join(' ~ ');
  }
  return cellValue;
}

function getTreeSelectCellValue(renderOpts, params) {
  const { props = {} } = renderOpts;
  const { treeData, treeCheckable } = props;
  const { row, column } = params;
  const cellValue = get(row, column.property);
  if (!isEmptyValue(cellValue)) {
    return map(treeCheckable ? cellValue : [cellValue], (value) => {
      const matchObj = findTree(treeData, (item) => item.value === value, {
        children: 'children',
      });
      return matchObj ? matchObj.item.title : value;
    }).join(', ');
  }
  return cellValue;
}

function getDatePickerCellValue(renderOpts, params, defaultFormat) {
  const { props = {} } = renderOpts;
  const { row, column } = params;
  let cellValue = get(row, column.property);
  if (cellValue) {
    cellValue = cellValue.format(props.format || defaultFormat);
  }
  return cellValue;
}

function createEditRender(defaultProps) {
  return function (renderOpts, params) {
    const { row, column } = params;
    const { name, attrs } = renderOpts;
    const cellValue = get(row, column.property);
    return [
      h(resolveComponent(parsePrefix(name)), {
        ...attrs,
        ...getCellEditFilterProps(renderOpts, params, cellValue, defaultProps),
        ...getEditOns(renderOpts, params),
      }),
    ];
  };
}

function defaultButtonEditRender(renderOpts, params) {
  const { attrs } = renderOpts;
  return [
    h(
      resolveComponent(prefix + 'button'),
      {
        ...attrs,
        ...getCellEditFilterProps(renderOpts, params, null),
        ...getOns(renderOpts, params),
      },
      cellText(renderOpts.content),
    ),
  ];
}

function defaultButtonsEditRender(renderOpts, params) {
  const { children } = renderOpts;
  if (children) {
    return children.map((childRenderOpts) => defaultButtonEditRender(childRenderOpts, params)[0]);
  }
  return [];
}

function createFilterRender(defaultProps) {
  return function (renderOpts, params) {
    const { column } = params;
    const { name, attrs } = renderOpts;
    return [
      h(
        'div',
        {
          class: 'vxe-table--filter-antd-wrapper',
        },
        column.filters.map((option, oIndex) => {
          const optionValue = option.data;
          return h(resolveComponent(parsePrefix(name)), {
            key: oIndex,
            ...attrs,
            ...getCellEditFilterProps(renderOpts, params, optionValue, defaultProps),
            ...getFilterOns(renderOpts, params, option, () => {
              // 处理 change 事件相关逻辑
              handleConfirmFilter(params, !!option.data, option);
            }),
          });
        }),
      ),
    ];
  };
}

function handleConfirmFilter(params, checked, option) {
  const { $panel } = params;
  $panel.changeOption(null, checked, option);
}

/**
 * 模糊匹配
 * @param params
 */
function defaultFuzzyFilterMethod(params) {
  const { option, row, column } = params;
  const { data } = option;
  const cellValue = get(row, column.property);
  return toString(cellValue).indexOf(data) > -1;
}

/**
 * 精确匹配
 * @param params
 */
function defaultExactFilterMethod(params) {
  const { option, row, column } = params;
  const { data } = option;
  const cellValue = get(row, column.property);
  /* eslint-disable eqeqeq */
  return cellValue === data;
}

function cellText(cellValue) {
  return [formatText(cellValue)];
}

function renderOptions(options, optionProps) {
  const labelProp = optionProps.label || 'label';
  const valueProp = optionProps.value || 'value';
  return map(options, (item, oIndex) => {
    return h(
      resolveComponent(prefix + 'select-option'),
      {
        key: oIndex,
        value: item[valueProp],
        disabled: item.disabled,
      },
      {
        default: () => cellText(item[labelProp]),
      },
    );
  });
}

function createFormItemRender(defaultProps) {
  return function (renderOpts, params) {
    const { data, property } = params;
    const { name } = renderOpts;
    const { attrs } = renderOpts;
    const itemValue = get(data, property);
    return [
      h(resolveComponent(parsePrefix(name)), {
        ...attrs,
        ...getItemProps(renderOpts, params, itemValue, defaultProps),
        ...getItemOns(renderOpts, params),
      }),
    ];
  };
}

function defaultButtonItemRender(renderOpts, params) {
  const { attrs } = renderOpts;
  const props = getItemProps(renderOpts, params, null);
  return [
    h(
      resolveComponent(prefix + 'button'),
      {
        ...attrs,
        ...props,
        ...getItemOns(renderOpts, params),
      },
      {
        default: () => cellText(renderOpts.content || props.content),
      },
    ),
  ];
}

function defaultButtonsItemRender(renderOpts, params) {
  const { children } = renderOpts;
  if (children) {
    return children.map((childRenderOpts) => defaultButtonItemRender(childRenderOpts, params)[0]);
  }
  return [];
}

function createDatePickerExportMethod(defaultFormat) {
  return function (params) {
    const { row, column, options } = params;
    return options && options.original
      ? get(row, column.property)
      : getDatePickerCellValue(column.editRender || column.cellRender, params, defaultFormat);
  };
}

function createExportMethod(getExportCellValue) {
  return function (params) {
    const { row, column, options } = params;
    return options && options.original
      ? get(row, column.property)
      : getExportCellValue(column.editRender || column.cellRender, params);
  };
}

function createFormItemRadioAndCheckboxRender() {
  return function (renderOpts, params) {
    const { name, options = [], optionProps = {} } = renderOpts;
    const { data, property } = params;
    const { attrs } = renderOpts;
    const labelProp = optionProps.label || 'label';
    const valueProp = optionProps.value || 'value';
    const itemValue = get(data, property);
    return [
      h(
        resolveComponent(`${parsePrefix(name)}Group`),
        {
          ...attrs,
          ...getItemProps(renderOpts, params, itemValue),
          ...getItemOns(renderOpts, params),
        },
        {
          default: () => {
            return options.map((option, oIndex) => {
              return h(
                resolveComponent(parsePrefix(name)),
                {
                  key: oIndex,
                  value: option[valueProp],
                  disabled: option.disabled,
                },
                {
                  default: () => cellText(option[labelProp]),
                },
              );
            });
          },
        },
      ),
    ];
  };
}

/**
 * 检查触发源是否属于目标节点
 */
function getEventTargetNode(evnt, container, className) {
  let targetElem;
  let target = evnt.target;
  while (target && target.nodeType && target !== document) {
    if (
      className &&
      target.className &&
      target.className.split &&
      target.className.split(' ').indexOf(className) > -1
    ) {
      targetElem = target;
    } else if (target === container) {
      return { flag: className ? !!targetElem : true, container, targetElem: targetElem };
    }
    target = target.parentNode;
  }
  return { flag: false };
}

/**
 * 事件兼容性处理
 */
function handleClearEvent(params) {
  const { $event } = params;
  const bodyElem = document.body;
  if (
    // 下拉框
    getEventTargetNode($event, bodyElem, 'ant-select-dropdown').flag ||
    // 级联
    getEventTargetNode($event, bodyElem, 'ant-cascader-menus').flag ||
    // 日期
    getEventTargetNode($event, bodyElem, 'ant-calendar-picker-container').flag ||
    // 时间选择
    getEventTargetNode($event, bodyElem, 'ant-time-picker-panel').flag ||
    // 颜色选择
    getEventTargetNode($event, bodyElem, 'je-color-picker').flag ||
    // 查询选择
    getEventTargetNode($event, bodyElem, 'je-func-select-window').flag ||
    //日期时间选择
    getEventTargetNode($event, bodyElem, 'ant-picker-panel-container').flag
  ) {
    return false;
  }
}

/**
 * 基于 vxe-table 表格的适配插件，用于兼容 ant-design-vue 组件库
 */
export const VXETablePluginAntd = {
  install(vxetablecore) {
    const { interceptor, renderer } = vxetablecore;

    renderer.mixin({
      AAutoComplete: {
        autofocus: 'input.ant-select-selection-search-input',
        renderDefault: createEditRender(),
        renderEdit: createEditRender(),
        renderFilter: createFilterRender(),
        defaultFilterMethod: defaultExactFilterMethod,
        renderItemContent: createFormItemRender(),
      },
      AInput: {
        autofocus: 'input.ant-input',
        renderDefault: createEditRender(),
        renderEdit: createEditRender(),
        renderFilter: createFilterRender(),
        defaultFilterMethod: defaultFuzzyFilterMethod,
        renderItemContent: createFormItemRender(),
      },
      AInputNumber: {
        autofocus: 'input.ant-input-number-input',
        renderDefault: createEditRender(),
        renderEdit: createEditRender(),
        renderFilter: createFilterRender(),
        defaultFilterMethod: defaultFuzzyFilterMethod,
        renderItemContent: createFormItemRender(),
      },
      ASelect: {
        renderEdit(renderOpts, params) {
          const {
            options = [],
            optionGroups,
            optionProps = {},
            optionGroupProps = {},
          } = renderOpts;
          const { row, column } = params;
          const { attrs } = renderOpts;
          const cellValue = get(row, column.property);
          const props = getCellEditFilterProps(renderOpts, params, cellValue);
          const ons = getEditOns(renderOpts, params);
          if (optionGroups) {
            const groupOptions = optionGroupProps.options || 'options';
            const groupLabel = optionGroupProps.label || 'label';
            return [
              h(
                resolveComponent(prefix + 'select'),
                {
                  ...props,
                  ...attrs,
                  ...ons,
                },
                {
                  default: () => {
                    return map(optionGroups, (group, gIndex) => {
                      return h(
                        resolveComponent(prefix + 'select-opt-group'),
                        {
                          key: gIndex,
                        },
                        {
                          label: () => {
                            return h('span', {}, group[groupLabel]);
                          },
                          default: () => renderOptions(group[groupOptions], optionProps),
                        },
                      );
                    });
                  },
                },
              ),
            ];
          }
          return [
            h(
              resolveComponent(prefix + 'select'),
              {
                ...props,
                ...attrs,
                ...ons,
              },
              {
                default: () => renderOptions(options, optionProps),
              },
            ),
          ];
        },
        renderCell(renderOpts, params) {
          return getCellLabelVNs(renderOpts, params, getSelectCellValue(renderOpts, params));
        },
        renderFilter(renderOpts, params) {
          const {
            options = [],
            optionGroups,
            optionProps = {},
            optionGroupProps = {},
          } = renderOpts;
          const groupOptions = optionGroupProps.options || 'options';
          const groupLabel = optionGroupProps.label || 'label';
          const { column } = params;
          const { attrs } = renderOpts;
          return [
            h(
              'div',
              {
                class: 'vxe-table--filter-antd-wrapper',
              },
              optionGroups
                ? column.filters.map((option, oIndex) => {
                    const optionValue = option.data;
                    const props = getCellEditFilterProps(renderOpts, params, optionValue);
                    return h(
                      resolveComponent(prefix + 'select'),
                      {
                        key: oIndex,
                        ...attrs,
                        ...props,
                        ...getFilterOns(renderOpts, params, option, () => {
                          // 处理 change 事件相关逻辑
                          handleConfirmFilter(
                            params,
                            props.mode === 'multiple'
                              ? option.data && option.data.length > 0
                              : isNotEmpty(option.data),
                            option,
                          );
                        }),
                      },
                      {
                        default: () => {
                          return map(optionGroups, (group, gIndex) => {
                            return h(
                              resolveComponent(prefix + 'select-opt-group'),
                              {
                                key: gIndex,
                              },
                              {
                                label: () => {
                                  return h('span', {}, group[groupLabel]);
                                },
                                default: () => renderOptions(group[groupOptions], optionProps),
                              },
                            );
                          });
                        },
                      },
                    );
                  })
                : column.filters.map((option, oIndex) => {
                    const optionValue = option.data;
                    const props = getCellEditFilterProps(renderOpts, params, optionValue);
                    return h(
                      resolveComponent(prefix + 'select'),
                      {
                        key: oIndex,
                        ...attrs,
                        ...props,
                        ...getFilterOns(renderOpts, params, option, () => {
                          // 处理 change 事件相关逻辑
                          handleConfirmFilter(
                            params,
                            props.mode === 'multiple'
                              ? option.data && option.data.length > 0
                              : isNotEmpty(option.data),
                            option,
                          );
                        }),
                      },
                      {
                        default: () => renderOptions(options, optionProps),
                      },
                    );
                  }),
            ),
          ];
        },
        defaultFilterMethod(params) {
          const { option, row, column } = params;
          const { data } = option;
          const { property, filterRender: renderOpts } = column;
          const { props = {} } = renderOpts;
          const cellValue = get(row, property);
          if (props.mode === 'multiple') {
            if (isArray(cellValue)) {
              return includes(cellValue, data);
            }
            return data.indexOf(cellValue) > -1;
          }
          /* eslint-disable eqeqeq */
          return cellValue == data;
        },
        renderItemContent(renderOpts, params) {
          const {
            options = [],
            optionGroups,
            optionProps = {},
            optionGroupProps = {},
          } = renderOpts;
          const { data, property } = params;
          const { attrs } = renderOpts;
          const itemValue = get(data, property);
          const props = getItemProps(renderOpts, params, itemValue);
          const ons = getItemOns(renderOpts, params);
          if (optionGroups) {
            const groupOptions = optionGroupProps.options || 'options';
            const groupLabel = optionGroupProps.label || 'label';
            return [
              h(
                resolveComponent(prefix + 'select'),
                {
                  ...attrs,
                  ...props,
                  ...ons,
                },
                {
                  default: () => {
                    return map(optionGroups, (group, gIndex) => {
                      return h(
                        resolveComponent(prefix + 'select-opt-group'),
                        {
                          key: gIndex,
                        },
                        {
                          label: () => {
                            return h('span', {}, group[groupLabel]);
                          },
                          default: () => renderOptions(group[groupOptions], optionProps),
                        },
                      );
                    });
                  },
                },
              ),
            ];
          }
          return [
            h(
              resolveComponent(prefix + 'select'),
              {
                ...attrs,
                ...props,
                ...ons,
              },
              {
                default: () => renderOptions(options, optionProps),
              },
            ),
          ];
        },
        exportMethod: createExportMethod(getSelectCellValue),
      },
      ACascader: {
        renderEdit: createEditRender(),
        renderCell(renderOpts, params) {
          return getCellLabelVNs(renderOpts, params, getCascaderCellValue(renderOpts, params));
        },
        renderItemContent: createFormItemRender(),
        exportMethod: createExportMethod(getCascaderCellValue),
      },
      ADatePicker: {
        renderEdit: createEditRender(),
        renderCell: formatDatePicker('YYYY-MM-DD'),
        renderItemContent: createFormItemRender(),
        exportMethod: createDatePickerExportMethod('YYYY-MM-DD'),
      },
      AMonthPicker: {
        renderEdit: createEditRender(),
        renderCell: formatDatePicker('YYYY-MM'),
        renderItemContent: createFormItemRender(),
        exportMethod: createDatePickerExportMethod('YYYY-MM'),
      },
      ARangePicker: {
        renderEdit: createEditRender(),
        renderCell(renderOpts, params) {
          return getCellLabelVNs(renderOpts, params, getRangePickerCellValue(renderOpts, params));
        },
        renderItemContent: createFormItemRender(),
        exportMethod: createExportMethod(getRangePickerCellValue),
      },
      AWeekPicker: {
        renderEdit: createEditRender(),
        renderCell: formatDatePicker('YYYY-WW周'),
        renderItemContent: createFormItemRender(),
        exportMethod: createDatePickerExportMethod('YYYY-WW周'),
      },
      ATimePicker: {
        renderEdit: createEditRender(),
        renderCell: formatDatePicker('HH:mm:ss'),
        renderItemContent: createFormItemRender(),
        exportMethod: createDatePickerExportMethod('HH:mm:ss'),
      },
      ATreeSelect: {
        renderEdit: createEditRender(),
        renderCell(renderOpts, params) {
          return getCellLabelVNs(renderOpts, params, getTreeSelectCellValue(renderOpts, params));
        },
        renderItemContent: createFormItemRender(),
        exportMethod: createExportMethod(getTreeSelectCellValue),
      },
      ARate: {
        renderDefault: createEditRender(),
        renderEdit: createEditRender(),
        renderFilter: createFilterRender(),
        defaultFilterMethod: defaultExactFilterMethod,
        renderItemContent: createFormItemRender(),
      },
      ASwitch: {
        renderDefault: createEditRender(),
        renderEdit: createEditRender(),
        renderFilter(renderOpts, params) {
          const { column } = params;
          const { name, attrs } = renderOpts;
          return [
            h(
              'div',
              {
                class: 'vxe-table--filter-antd-wrapper',
              },
              column.filters.map((option, oIndex) => {
                const optionValue = option.data;
                return h(name, {
                  key: oIndex,
                  ...attrs,
                  ...getCellEditFilterProps(renderOpts, params, optionValue),
                  ...getFilterOns(renderOpts, params, option, () => {
                    // 处理 change 事件相关逻辑
                    handleConfirmFilter(params, isBoolean(option.data), option);
                  }),
                });
              }),
            ),
          ];
        },
        defaultFilterMethod: defaultExactFilterMethod,
        renderItemContent: createFormItemRender(),
      },
      ARadio: {
        renderItemContent: createFormItemRadioAndCheckboxRender(),
      },
      ACheckbox: {
        renderItemContent: createFormItemRadioAndCheckboxRender(),
      },
      AButton: {
        renderEdit: defaultButtonEditRender,
        renderDefault: defaultButtonEditRender,
        renderItemContent: defaultButtonItemRender,
      },
      AButtons: {
        renderEdit: defaultButtonsEditRender,
        renderDefault: defaultButtonsEditRender,
        renderItemContent: defaultButtonsItemRender,
      },
    });

    interceptor.add('event.clearFilter', handleClearEvent);
    interceptor.add('event.clearActived', handleClearEvent);
    interceptor.add('event.clearAreas', handleClearEvent);
  },
};

if (typeof window !== 'undefined' && window.VXETable && window.VXETable.use) {
  window.VXETable.use(VXETablePluginAntd);
}

export default VXETablePluginAntd;
