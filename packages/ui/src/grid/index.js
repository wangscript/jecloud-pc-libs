import { withInstall } from '../utils';
import JeGrid from './src/grid';
import JeColumn from './src/column';
import JeColgroup from './src/group';
import { MethodKeys } from './src/table/method-keys';
import './src/plugins/vxetable';
import { renderer } from './src/renderer';
// 注册依赖组件
JeGrid.installComps = {
  Column: { name: JeColumn.name, comp: JeColumn },
  Group: { name: JeColgroup.name, comp: JeColgroup },
};
Object.assign(JeGrid, {
  MethodKeys,
  Renderer: renderer,
});
export const Grid = withInstall(JeGrid);
export default Grid;
