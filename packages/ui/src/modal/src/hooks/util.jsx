import GlobalConfig from './config';
import { isString, isObject, uniqueId } from '@jecloud/utils';

/**
 * 重构ant通知组件的open方法
 *
 * @export
 * @param {*} component
 * @param {*} fas // 使用实心图标
 */
export function transformOpenFn(component, fas) {
  // 重写open方法，支持status类型
  const open = component.open;
  component.open = (options) => {
    let icon = options.icon || GlobalConfig.icon[`MODAL_${options.status}`.toLocaleUpperCase()];
    // 实心图标
    if (fas && icon) {
      icon = icon.replace('fal', 'fas');
    }
    if (icon) {
      options.icon = (
        <i
          class={['ant-message-icon', options.status, icon]}
          style={{ color: options.iconColor }}
        />
      );
    }

    // duration 延时关闭
    options.duration = options.duration ?? 1.5;
    // 唯一key
    options.key = options.key ?? uniqueId('notice-');
    // 关闭方法
    const close = () => {
      component.close(options.key);
    };
    // 增加关闭方法
    if (options.onClick) {
      const oldOnClick = options.onClick;
      options.onClick = () => {
        oldOnClick({ close });
      };
    }

    open(options);
    return { close, key: options.key };
  };
}

/**
 * 格式化参数
 *
 * @export
 * @param {*} content
 * @param {*} args
 * @return {*}
 */
export function transformArgs(options) {
  let args = {};
  const content = options.content;
  if (isString(content)) {
    args = options;
  } else if (isObject(content)) {
    args = content;
  }
  // 兼容 Notification
  args.message = args.message ?? args.title ?? GlobalConfig.i18n('title');
  args.description = args.description ?? args.content;
  return args;
}
