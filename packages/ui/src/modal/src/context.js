import { inject, provide } from 'vue';

export const ModalContextKey = Symbol('jeModalContextKey');

export const useProvideModal = (state) => {
  provide(ModalContextKey, state);
};

export const useInjectModal = () => {
  return inject(ModalContextKey, {});
};
