import { defineComponent, watch, computed } from 'vue';
import { Switch } from 'ant-design-vue';
import { filterEmpty } from '../../utils/props';
import { useModelValue } from '../../hooks';
import { isNotEmpty, isEmpty, pick } from '@jecloud/utils';
import Checkbox from '../../checkbox';

export default defineComponent({
  name: 'JeSwitch',
  inheritAttrs: false,
  props: {
    checkedValue: { type: [String, Number, Boolean], default: '1' },
    unCheckedValue: { type: [String, Number, Boolean], default: '0' },
    checkedChildren: String,
    unCheckedChildren: String,
    checkedIcon: String,
    unCheckedIcon: String,
    emptyValue: { type: Boolean, default: true },
    mode: {
      type: String,
      default: 'switch',
      validator: (value) => ['switch', 'checkbox'].includes(value),
    },
    value: { type: [String, Number, Boolean], default: '0' },
    checked: Boolean,
    label: String,
    labelConfig: Object,
    iconConfig: Object,
    readonly: Boolean,
    disabled: Boolean,
  },
  emits: ['update:value', 'update:checked'],
  setup(props, context) {
    const { slots, attrs } = context;
    const value = useModelValue({ props, context, changeEvent: true });
    const checked = useModelValue({ props, context, key: 'checked' });
    const computeReadonly = computed(() => {
      return props.readonly || props.disabled;
    });
    watch(
      () => value.value,
      () => {
        checked.value = value.value === props.checkedValue;
      },
      { immediate: true },
    );
    watch(
      () => checked.value,
      () => {
        if (checked.value) {
          value.value = props.checkedValue;
          // 禁用为空设置 || value值不为空
        } else if (!props.emptyValue || isNotEmpty(value.value)) {
          value.value = props.unCheckedValue;
        }
      },
    );
    return () => {
      if (props.mode === 'switch') {
        return (
          <Switch
            class="je-switch"
            {...attrs}
            {...pick(props, [
              'checkedChildren',
              'unCheckedChildren',
              'checkedValue',
              'unCheckedValue',
            ])}
            disabled={computeReadonly.value}
            v-model:checked={value.value}
            v-slots={slots}
          />
        );
      } else {
        const label = filterEmpty(
          (slots.label?.() ?? props.label) ||
            (checked.value
              ? slots.checkedChildren?.() ?? props.checkedChildren
              : slots.unCheckedChildren?.() ?? props.unCheckedChildren),
        );
        const icon = props.checkedIcon || props.unCheckedIcon;
        // 原始复选框
        if (isEmpty(icon)) {
          return (
            <Checkbox
              class="je-switch je-switch-checkbox"
              disabled={computeReadonly.value}
              v-model:checked={checked.value}
            >
              {label}
            </Checkbox>
          );
        }
        return (
          <div
            class={[
              'je-switch je-switch-checkbox je-switch-checkbox-custom',
              { 'je-switch-disabled': computeReadonly.value, 'je-switch-checked': checked.value },
            ]}
            onClick={() => {
              if (computeReadonly.value) return;
              checked.value = !checked.value;
            }}
          >
            <i
              class={{
                'je-switch-icon': true,
                [props.checkedIcon]: checked.value,
                [props.unCheckedIcon]: !checked.value,
              }}
              {...props.iconConfig}
            />
            {isNotEmpty(label) ? (
              <span class="je-switch-label" {...props.labelConfig}>
                {label}
              </span>
            ) : null}
          </div>
        );
      }
    };
  },
});
