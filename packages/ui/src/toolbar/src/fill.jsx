import { defineComponent } from 'vue';

export default defineComponent({
  name: 'JeToolbarFill',
  setup(props, { attrs, slots }) {
    return () => (
      <div {...attrs} class="je-toolbar-item-fill">
        {slots.default?.()}
      </div>
    );
  },
});
