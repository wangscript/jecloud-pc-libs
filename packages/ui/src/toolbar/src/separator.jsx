import { defineComponent } from 'vue';
export default defineComponent({
  name: 'JeToolbarSeparator',
  setup(props, { attrs, slots }) {
    return () => (
      <div {...attrs} class="je-toolbar-item-separator">
        {slots.default?.() ?? <span class="separator"></span>}
      </div>
    );
  },
});
