import { defineComponent, ref, computed, watch } from 'vue';
import useFlexGapSupport from 'ant-design-vue/es/_util/hooks/useFlexGapSupport';
import { filterEmpty } from '../../utils/props';

const spaceSize = {
  small: 10,
  middle: 16,
  large: 24,
};
function getNumberSize(space) {
  return spaceSize[space] ? spaceSize[space] : space || 0;
}

export default defineComponent({
  name: 'JeToolbar',
  inheritAttrs: false,
  props: {
    space: {
      // 间距
      type: [String, Number],
      default: 'small',
    },
    direction: { type: String, default: 'horizontal', valid: ['horizontal', 'vertical'] },
    align: { type: String, default: 'center', valid: ['start', 'end', 'center', 'baseline'] },
    justify: { type: String, default: 'start', valid: ['start', 'end', 'center', 'baseline'] },
    wrap: Boolean,
  },
  setup(props, { slots, attrs }) {
    const prefixCls = 'je-toolbar';
    const size = computed(() => props.space ?? 'small');
    const supportFlexGap = useFlexGapSupport();
    const horizontalSize = ref();
    const verticalSize = ref();
    watch(
      size,
      () => {
        [horizontalSize.value, verticalSize.value] = (
          Array.isArray(size.value) ? size.value : [size.value, size.value]
        ).map((item) => getNumberSize(item));
      },
      { immediate: true },
    );

    const style = computed(() => {
      const gapStyle = {};
      if (supportFlexGap.value) {
        gapStyle.columnGap = `${horizontalSize.value}px`;
        gapStyle.rowGap = `${verticalSize.value}px`;
      }
      return [
        {
          ...gapStyle,
          ...(props.wrap && { flexWrap: 'wrap', marginBottom: `${-verticalSize.value}px` }),
        },
        attrs.style,
      ];
    });
    const marginDirection = computed(() => 'marginRight');
    return () => {
      const { wrap, direction = 'horizontal' } = props;

      const items = filterEmpty(slots.default?.());
      const len = items.length;

      if (len === 0) {
        return null;
      }
      const itemClassName = `${prefixCls}-item`;
      const horizontalSizeVal = horizontalSize.value;
      const latestIndex = len - 1;
      return (
        <div
          class={[
            prefixCls,
            `${prefixCls}-${props.direction}`,
            { [`${prefixCls}-align-${props.align}`]: props.align },
            { [`${prefixCls}-justify-${props.justify}`]: props.justify },
            attrs.class,
          ]}
          style={style.value}
        >
          {items.map((child, index) => {
            if (!child) return null;
            if (child.type?.name?.startsWith('JeToolbar')) {
              return child;
            }
            let itemStyle = {};
            if (!supportFlexGap.value) {
              if (direction === 'vertical') {
                if (index < latestIndex) {
                  itemStyle = { marginBottom: `${horizontalSizeVal}px` };
                }
              } else {
                itemStyle = {
                  ...(index < latestIndex && {
                    [marginDirection.value]: `${horizontalSizeVal}px`,
                  }),
                  ...(wrap && { paddingBottom: `${verticalSize.value}px` }),
                };
              }
            }
            return (
              <>
                <div class={itemClassName} style={itemStyle}>
                  {child}
                </div>
              </>
            );
          })}
        </div>
      );
    };
  },
});
