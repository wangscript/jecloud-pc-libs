import { computed, defineComponent, ref } from 'vue';
import { Button } from 'ant-design-vue';
import { useModelValue } from '../../hooks';

export default defineComponent({
  name: 'JeButton',
  components: { Button },
  inheritAttrs: false,
  props: {
    loading: Boolean,
    disabled: Boolean,
    icon: String,
    iconColor: String,
    bgColor: String,
    fontColor: String,
    borderColor: String,
  },
  emits: ['click'],
  setup(props, context) {
    const { slots, attrs, expose, emit } = context;
    const $plugin = ref();
    const disabled = useModelValue({ props, context, key: 'disabled' });
    const loading = useModelValue({ props, context, key: 'loading' });
    // 自定义按钮样式
    const computeStyle = computed(() => {
      const style = {};
      // 背景色
      if (props.bgColor) {
        style.backgroundColor = `${props.bgColor} !important`;
      }
      // 边框色
      if (props.borderColor) {
        style.borderColor = `${props.borderColor} !important`;
      }
      // 字体色
      if (props.fontColor) {
        style.color = `${props.fontColor} !important`;
      }
      return style;
    });
    // 自定义按钮图标颜色，默认跟字体颜色一致
    const computeIconStyle = computed(() => {
      const style = {};
      if (props.iconColor || props.fontColor) {
        style.color = `${props.iconColor ?? props.fontColor} !important`;
      }
      return style;
    });

    const handleClick = (event) => {
      if (loading.value || disabled.value) {
        event.preventDefault();
        return;
      }
      emit('click', event);
    };

    expose({
      $plugin,
      loading,
      disabled,
    });
    const iconSlot = () => {
      if (loading.value)
        return <i class="jeicon jeicon-circle-notch icon-spin" style={computeIconStyle.value} />;
      if (slots.icon) return slots.icon();
      if (props.icon) {
        return <i class={props.icon} style={computeIconStyle.value} />;
      }
    };
    return () => (
      <Button
        disabled={disabled.value}
        {...attrs}
        style={computeStyle.value}
        ref={$plugin}
        class={{ 'je-btn-loading': loading.value }}
        onClick={handleClick}
      >
        {iconSlot()}
        {slots.default?.()}
      </Button>
    );
  },
});
