import { withInstall } from '../utils';
import JeUpload from './src/upload';
import JeUploadImage from './src/upload-image';
import JeUploadFiles from './src/upload-files';
import { showUploadModal } from './src/upload-modal';

JeUpload.installComps = {
  // 注册依赖组件
  UploadFiles: { name: JeUploadFiles.name, comp: JeUploadFiles },
  UploadImage: { name: JeUploadImage.name, comp: JeUploadImage },
};
Object.assign(JeUpload, { showUploadModal });
export const Upload = withInstall(JeUpload);

export default Upload;
