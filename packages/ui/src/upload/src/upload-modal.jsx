import { createDeferred, omit } from '@jecloud/utils';
import Modal from '../../modal';
import { ref, resolveComponent, h } from 'vue';
/**
 * 打开上传窗口
 * @param {Object} options
 * @param {string} [options.type] 类型，默认files，files(多附件)，image(图片选择器)
 * @param {string} [options.title] 标题，默认：上传文件
 * @param {string} [options.width] 宽，默认800
 * @param {string} [options.height] 高，默认350
 * @param {string} [options.modalOptions] 窗口配置
 * @param {string} [options.okButtonText] 确认按钮文字
 * @param {string} [options.cancelButtonText] 取消按钮文字
 * @param {Object} [options.params] 上传参数
 * @param {boolean} [options.multiple]  多选
 * @param {Array} [options.includeSuffix]  允许上传的文件后缀
 * @param {Array} [options.excludeSuffix]  禁用上传的文件后缀
 * @param {number} [options.maxCount]  允许上传的文件最大数量
 * @param {number} [options.maxSize]  允许上传的文件最大值，M
 * @param {Function} [onFileRemove]  文件删除时调用，{value}
 * @returns {Promise} 返回附件数据
 */
export function showUploadModal(options = {}) {
  const {
    title = '文件上传',
    width = 800,
    height = 350,
    type = 'files',
    modalOptions,
    okButtonText = '确定',
    cancelButtonText = '取消',
  } = options;
  const value = ref(options.value);
  const props = omit(options, [
    'title',
    'width',
    'height',
    'type',
    'value',
    'modalOptions',
    'okButtonText',
    'cancelButtonText',
  ]);
  const deferred = createDeferred();
  Modal.window({
    width,
    height,
    title,
    ...modalOptions,
    content: () =>
      h(resolveComponent(type === 'image' ? 'JeUploadImage' : 'JeUploadFiles'), {
        style: { height: '100%' },
        ...props,
        value: value.value,
        'onUpdate:value': function (val) {
          value.value = val;
        },
      }),
    okButton: {
      text: okButtonText,
      handler() {
        deferred.resolve(value.value);
      },
    },
    cancelButton: {
      text: cancelButtonText,
    },
  });
  return deferred.promise;
}
