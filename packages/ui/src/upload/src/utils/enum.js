/**
 * 附件存储的保留字段
 */
export const RetainKeys = ['fileKey', 'relName', 'suffix', 'size', 'security'];
/**
 * 操作按钮
 */
export const FileActions = Object.freeze({
  download: {
    code: 'downLoad',
    text: '下载',
    icon: 'fal fa-download',
    noReadonly: true,
  },
  preview: {
    code: 'preview',
    text: '预览',
    icon: 'jeicon jeicon-eye',
    noReadonly: true,
  },
  upload: {
    code: 'upload',
    text: '新上传',
    icon: 'jeicon jeicon-plus',
  },
  remove: {
    code: 'remove',
    text: '删除',
    icon: 'fal fa-trash-alt',
  },
  property: {
    code: 'property',
    text: '属性',
    icon: 'jeicon jeicon-function',
    noReadonly: true,
  },
  rename: {
    code: 'rName',
    text: '重命名',
    icon: 'fal fa-edit',
  },
  downloadAll: {
    code: 'downloadAll',
    text: '全部下载',
    icon: 'fal fa-download',
    noReadonly: true,
  },
  removeAll: {
    code: 'removeAll',
    text: '清空',
    icon: 'fal fa-trash-alt',
  },
  sortable: {
    code: 'sortable',
    text: '拖动排序',
    icon: 'fal fa-arrows sortable-icon',
    clickEvent: false,
  },
});
