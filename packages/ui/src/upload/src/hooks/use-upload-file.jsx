import { isEmpty } from '@jecloud/utils';
import { ref, computed, watch } from 'vue';
import Tooltip from '../../../tooltip';
import { useUploadCommon } from './use-upload-common';
import { FileActions } from '../utils';
export function useUploadFile({ props, context }) {
  const { slots } = context;
  const { $upload, state, modelValue, computeReadonly, actionClick } = useUploadCommon({
    props,
    context,
  });
  const tempFile = computed(() => state.files[0]); // 文件对象
  const progress = ref(0); // 上传进度
  // 进度
  watch(
    () => state.progress,
    () => {
      progress.value = state.progress[tempFile.value?.relName] || 0;
    },
    { deep: true },
  );
  // 操作按钮
  const actionButtons = [FileActions.preview, '|', FileActions.download];

  // 后缀插槽
  const addonAfterSlot = props.securityActionSlot
    ? () => props.securityActionSlot({ $upload, file: state.files[0] })
    : slots.addonAfter;

  // 展示插槽
  const displaySlot = () => {
    return (
      <>
        <div
          class={['update-input-display ', { 'is--upload': tempFile.value?.fileKey }]}
          onClick={() => {
            if (isEmpty(modelValue.value) && !computeReadonly.value) {
              onSelect();
            }
          }}
        >
          {tempFile.value?.relName ? (
            <Tooltip
              placement="bottomLeft"
              getPopupContainer={(triggerNode) => {
                return document.querySelector('.je-input-select-wrapper') || triggerNode.parentNode;
              }}
              v-slots={{
                title() {
                  return (
                    <div class="je-input-select-upload-tooltip">
                      {actionButtons.map((button) =>
                        button.code ? (
                          <span
                            onClick={() => actionClick({ type: button.code, file: tempFile.value })}
                          >
                            {button.text}
                          </span>
                        ) : (
                          button
                        ),
                      )}
                    </div>
                  );
                },
              }}
            >
              {tempFile.value.name || tempFile.value.relName}
            </Tooltip>
          ) : (
            tempFile.value?.name ||
            tempFile.value?.relName || (
              <span class="update-input-display-placeholder">{props.placeholder}</span>
            )
          )}
        </div>
        {state.uploading ? (
          <div class={{ 'upload-progress': true, 'is--uploaddone': progress.value === 100 }}>
            <div class="upload-progress-bar">
              <div class="upload-progress-bar-item" style={{ width: `${progress.value}%` }}></div>
            </div>
            <div class="upload-progress-text">
              {progress.value === 100 ? '上传完成' : `${progress.value}/100%`}
            </div>
          </div>
        ) : null}
      </>
    );
  };
  // 上传
  const onSelect = () => {
    actionClick({ type: FileActions.upload.code });
  };
  // 重置，如果上传中，取消上传，恢复原始数据
  const onBeforeReset = () => {
    // 删除附件
    if (state.files.length) {
      actionClick({ type: FileActions.remove.code, file: state.files[0] });
      return false;
    }
  };

  return { onSelect, onBeforeReset, displaySlot, addonAfterSlot, modelValue };
}
