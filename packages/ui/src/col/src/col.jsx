import { defineComponent } from 'vue';
import { Col } from 'ant-design-vue';

export default defineComponent({
  name: 'JeCol',
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <Col {...attrs} v-slots={slots} />;
  },
});
