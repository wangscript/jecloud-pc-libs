/**
 * 高亮文字组件
 */
export default {
  name: 'JeHighlightText',
  props: {
    text: String, // 文字
    highlightText: String, // 高亮文字
  },
  setup(props) {
    const splitStr = '`__`';
    return () => {
      const { highlightText, text } = props;
      let vns = [];
      if (text && highlightText) {
        const keys = text
          .toString?.()
          ?.replaceAll(highlightText, splitStr + highlightText + splitStr)
          ?.split(splitStr);
        vns = keys.map((key) => {
          return key === highlightText ? <span class="keyword">{key}</span> : key;
        });
      }
      return <div class="je-highlight-text">{vns}</div>;
    };
  },
};
