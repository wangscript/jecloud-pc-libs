import { defineComponent, nextTick, onMounted, ref, watch } from 'vue';
import { Collapse } from 'ant-design-vue';
import { useModelValue } from '../../hooks';
import { isEmpty } from '@jecloud/utils';

export default defineComponent({
  name: 'JeCollapse',
  inheritAttrs: false,
  props: {
    flexLayout: Boolean, // flex布局
    accordion: Boolean,
    activeKey: [String, Array],
    disableEmptyActive: Boolean, // 禁止全部收起，配合accordion手风琴模式使用
  },
  emits: ['update:activeKey'],
  setup(props, context) {
    const { slots, attrs } = context;
    const activeKey = useModelValue({ props, context, key: 'activeKey' });
    const $el = ref();
    // 手风琴模式，并且不允许activeKey为空
    props.accordion &&
      props.disableEmptyActive &&
      watch(
        () => activeKey.value,
        (nv, ov) => {
          if (isEmpty(nv)) {
            activeKey.value = findNextPanel(ov);
          }
        },
      );
    // 查找下一个可以展开的面板
    const findNextPanel = (key) => {
      let item;
      if (key) {
        item = $el.value.$el.querySelector(`.ant-collapse-item[data-key="${key}"]`);
        item = item.nextElementSibling || item.previousElementSibling || item;
      } else {
        item = $el.value.$el.querySelector('.ant-collapse-item');
      }
      return item?.getAttribute('data-key');
    };
    onMounted(() => {
      nextTick(() => {
        // 手风琴模式，并且不允许activeKey为空
        if (props.accordion && props.disableEmptyActive) {
          // 默认初始化展开第一个
          activeKey.value = findNextPanel();
        }
      });
    });
    return () => (
      <Collapse
        ref={$el}
        class={{ 'je-collapse': true, 'je-collapse-flex': props.flexLayout }}
        v-model:activeKey={activeKey.value}
        accordion={props.accordion}
        {...attrs}
        v-slots={slots}
      />
    );
  },
});
