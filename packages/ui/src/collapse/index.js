import { withInstall } from '../utils';
import JeCollapse from './src/collapse';
import JeCollapsePanel from './src/collapse-panel';
// 注册依赖组件
JeCollapse.installComps = {
  Panel: { name: JeCollapsePanel.name, comp: JeCollapsePanel },
};
export const Collapse = withInstall(JeCollapse);

export default Collapse;
