import { withInstall } from '../utils';
import JeFieldset from './src/fieldset';

export const Fieldset = withInstall(JeFieldset);
export default Fieldset;
