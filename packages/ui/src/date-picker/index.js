import { withInstall } from '../utils';
import { DatePicker as ADatePicker } from 'ant-design-vue';
import JeDatePicker from './src/date-picker';
import { rendererDateValue } from './src/util';

// 注册依赖组件
JeDatePicker.installComps = {
  RangePicker: { name: 'JeRangePicker', comp: ADatePicker.RangePicker },
};
JeDatePicker.rendererDateValue = rendererDateValue;
export const DatePicker = withInstall(JeDatePicker);

export default DatePicker;
