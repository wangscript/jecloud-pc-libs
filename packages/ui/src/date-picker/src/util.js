import { dateUtil } from '@jecloud/utils';
/**
 * 解析日期格式化配置
 * @param {*} param0
 * @returns
 */
export function parseDateFormat({ format, valueFormat, picker }) {
  let tempFormat = '';
  let tempValueFormat = '';
  switch (picker) {
    case 'dateTime':
      tempFormat = format || 'YYYY-MM-DD HH:mm:ss';
      tempValueFormat = valueFormat || 'YYYY-MM-DD HH:mm:ss';
      break;
    case 'year':
      tempFormat = format || 'YYYY';
      tempValueFormat = valueFormat || 'YYYY';
      break;
    case 'quarter':
      tempFormat = format || 'YYYY-Q';
      tempValueFormat = valueFormat || 'YYYY-MM-DD';
      break;
    case 'month':
      tempFormat = format || 'YYYY-MM';
      tempValueFormat = valueFormat || 'YYYY-MM';
      break;
    case 'week':
      tempFormat = format || 'YYYY-wo';
      tempValueFormat = valueFormat || 'YYYY-MM-DD';
      break;
    case 'time':
      tempFormat = format || 'HH:mm:ss';
      tempValueFormat = valueFormat || 'HH:mm:ss';
      break;
    default:
      tempFormat = format || 'YYYY-MM-DD';
      tempValueFormat = valueFormat || 'YYYY-MM-DD';
      break;
  }

  return { format: tempFormat, valueFormat: tempValueFormat };
}
/**
 * 格式化日期数据
 * @param {*} options
 * @returns
 */
export function rendererDateValue(options) {
  const { value } = options;
  const config = parseDateFormat(options);
  if (value && config.format !== config.valueFormat) {
    return dateUtil(value, config.valueFormat).format(config.format);
  } else {
    return value;
  }
}
