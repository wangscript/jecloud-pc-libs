import { useModelValue } from '../../hooks';
import { parseDateFormat } from './util';
export function useDatePicker({ props, context, picker }) {
  const value = useModelValue({ props, context, changeEvent: true });
  const { format, valueFormat } = parseDateFormat({ ...props, picker: picker || props.picker });

  return { format, valueFormat, dateValue: value };
}
