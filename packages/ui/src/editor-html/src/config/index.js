import toolbar from './toolbar';
import plugins from './plugins';
import { useUrls } from './urls';
import { ref } from 'vue';
export function useConfig({ props }) {
  let editor = null;
  const urls = useUrls({ plugins });
  const loading = ref(true);
  const init = {
    width: '100%',
    height: '100%',
    menubar: false, // 隐藏菜单
    statusbar: false, // 隐藏状态栏
    cache_suffix: '?v=5.0.0', // 缓存后缀
    language: 'zh_CN', //语言类型
    plugins,
    ...urls,
    ...toolbar,
    ...props.editorOptions,
  };
  // 主文件
  const tinymceScriptSrc = urls.file_url + init.cache_suffix;
  // 初始化
  const customSetup = init.setup;
  init.setup = function (_editor) {
    editor = _editor;
    customSetup?.(editor);
    setTimeout(() => {
      loading.value = false;
    }, 200);
  };
  // 初始化配置
  window.tinyMCEPreInit = { baseURL: urls.baseURL, suffix: '' };
  return {
    init,
    tinymceScriptSrc,
    loading,
    getEditor() {
      return editor;
    },
  };
}
