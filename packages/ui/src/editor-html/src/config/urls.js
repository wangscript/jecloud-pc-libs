import { usePublicPath } from '../../../hooks/use-public-path';
export function useUrls({ plugins }) {
  const publicPath = usePublicPath();
  const baseDir = 'static/tinymce';
  const urls = {
    plugin_url: `${baseDir}/plugins/{plugin_url}/plugin.min.js`,
    language_url: `${baseDir}/langs/zh_CN.js`, //引入语言包文件
    skin_url: `${baseDir}/skins/ui/oxide`, //皮肤：浅色
    file_url: `${baseDir}/tinymce.min.js`,
  };
  Object.keys(urls).forEach((key) => {
    urls[key] = publicPath + urls[key];
  });

  // 插件url
  urls.external_plugins = plugins.map((plugin) => {
    return urls.plugin_url.replace('{plugin_url}', plugin);
  });

  // 插件根目录，不设置，微应用访问失败
  urls.baseURL = publicPath + baseDir;
  return urls;
}
