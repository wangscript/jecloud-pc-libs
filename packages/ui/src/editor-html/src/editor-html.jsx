import { defineComponent, watch } from 'vue';
import Editor from '@tinymce/tinymce-vue';
import { useConfig } from './config';
import emits from './config/events';
import { isEmpty, omit } from '@jecloud/utils';
import { useEditor } from '../../editor-code/src/hooks/use-editor';
export default defineComponent({
  name: 'JeEditorHtml',
  inheritAttrs: false,
  props: {
    value: { type: String, default: '' },
    width: [String, Number],
    height: { type: [String, Number], default: 400 },
    editorOptions: Object,
  },
  emits: [...emits, 'update:value'],
  setup(props, context) {
    const { slots, attrs, expose, emit } = context;
    const { value, style, $plugin } = useEditor({ props, context });
    const { init, tinymceScriptSrc, getEditor, loading } = useConfig({ props });
    watch(
      () => value.value,
      (newValue) => {
        // 处理null值
        if (isEmpty(newValue, true)) {
          value.value = '';
        } else {
          emit('change', value.value);
        }
      },
    );
    expose({ getEditor, $plugin });
    return () => (
      <div class="je-editor-html" style={{ ...style, ...attrs.style }} v-loading={loading.value}>
        <Editor
          ref={$plugin}
          v-model={value.value}
          {...omit(attrs, ['style', 'onChange'])}
          v-slots={slots}
          init={init}
          tinymceScriptSrc={tinymceScriptSrc}
        />
      </div>
    );
  },
});
