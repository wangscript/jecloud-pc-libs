const babel = require('gulp-babel');
const terser = require('gulp-terser');

module.exports = {
  /**
   * babel转义
   * @returns
   */
  babelParse() {
    return babel({
      plugins: ['@vue/babel-plugin-jsx'],
    });
  },
  terser() {
    return terser({
      mangle: { toplevel: true },
    });
  },
  /**
   * 打包输出目录
   * @returns
   */
  outputDir(libDir) {
    return `${libDir}/es`;
  },
};
