const gulp = require('gulp');
const { buildAssets, buildClean, buildModules, buildStyle, buildSrcStyle } = require('./common');
const utils = require('./utils');
const libs = ['utils', 'ui', 'func', 'plugin'];
// 创建打包任务
libs.forEach((lib) => {
  const libDir = 'packages/' + lib; // 包目录
  const srcDir = libDir + '/src'; // 源码目录
  const outputDir = utils.outputDir(libDir); // 输出目录
  const options = { srcDir, outputDir };
  // lib任务名
  const task = 'build_' + lib;
  // 清除打包目录
  gulp.task(task + '_clean', () => buildClean(options));

  // 打包源码
  gulp.task(task + '_modules', () => buildModules(options));

  // 处理样式
  gulp.task(task + '_style', () => buildStyle(options));

  // 处理资源
  gulp.task(task + '_assets', () => buildAssets(options));

  // 处理src样式
  gulp.task(task + '_src4style', () => buildSrcStyle(options));

  // 合并任务
  let tasks = ['src4style', 'clean', 'assets', 'modules', 'style'];
  if (lib === 'utils') {
    tasks = ['clean', 'modules'];
  }
  gulp.task(
    task,
    gulp.series(...tasks.map((item) => `${task}_${item}`), (done) => {
      console.log(`>>>>> build_${lib} success!`);
      done();
    }),
  );
});

// 构建libs包
gulp.task(
  'build_libs',
  gulp.series(...libs.map((lib) => `build_${lib}`), (done) => {
    done();
  }),
);

module.exports = { libs };
