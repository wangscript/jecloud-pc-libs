const gulp = require('gulp');
const through2 = require('through2'); // 处理文件内容
const { libs } = require('../libs');
// 构建package.json文件
const buildPackage = (folder) => {
  return gulp
    .src([`packages/{${libs.join(',')}}/package.json`])
    .pipe(
      // 生成style/index.js，支持按需加载
      through2.obj(function (chunk, enc, callback) {
        const str = chunk.contents.toString();
        const json = JSON.parse(str);
        json.main = `${folder}/index.js`;
        json.files = [folder, 'LICENSE'];
        chunk.contents = Buffer.from(JSON.stringify(json, null, 2) + '\n');
        this.push(chunk);
        callback();
      }),
    )
    .pipe(gulp.dest('packages'));
};
// 发布文件
gulp.task('publish_package', () => buildPackage('es'));
// 开发文件
gulp.task('debug_package', () => buildPackage('src'));
